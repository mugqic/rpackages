% Generated by roxygen2 (4.1.1): do not edit by hand
% Please edit documentation in R/accessors.r
\name{getFileIndex}
\alias{getFileIndex}
\title{The file index is simply a data.frame which keep tracks of where output files of the R part of the pipelines are located.
 This facilites retrieval of e.g plots, result files, etc.}
\usage{
getFileIndex(project.path = getwd(), type = NULL, as.html = FALSE)
}
\arguments{
\item{project.path}{}

\item{type}{type of result file, e.g. exploratory, If NULL, whole table is returned.}

\item{as.html}{If TRUE, the File colum (path to files) will be turned to an href,
and the whole table wil be transformed to html using hwrite.}
}
\description{
The file index is simply a data.frame which keep tracks of where output files of the R part of the pipelines are located.
 This facilites retrieval of e.g plots, result files, etc.
}

