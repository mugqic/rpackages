\name{initHiSeqProject}
\alias{initHiSeqProject}
\title{This function does two things, 1) create and write out the read sets table and sample
tables, 2) sym link to the fastq.}
\usage{
  initHiSeqProject(nanuq.file,
    fastq.base.dir = "/lb/robot/hiSeqSequencer/hiSeqRuns",
    fastq.links.dir = "raw_reads",
    legacy.fastq.links = FALSE, legacy.design = NULL)
}
\arguments{
  \item{nanuq.file}{Path to the nanuq readset csv file}

  \item{fastq.base.dir}{the base dir where to look for
  fastqs}

  \item{fastq.links.dir}{if fastq.base.dir is specified,
  this is where the links will be written}

  \item{legacy.fastq.links}{[not implemented] if TRUE,
  symbolic links will follow the legacy convention pair1,
  etc.}

  \item{legacy.design}{path to design file}
}
\description{
  This function does two things, 1) create and write out
  the read sets table and sample tables, 2) sym link to the
  fastq.
}

