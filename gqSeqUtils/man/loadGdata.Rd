\name{loadGdata}
\alias{loadGdata}
\title{Perhaps we should store the gene annotations in some tabular format. This file could replace the gene lengths, kgXref, etc. and be used by diff gene expr.}
\usage{
  loadGdata()
}
\description{
  Perhaps we should store the gene annotations in some
  tabular format. This file could replace the gene lengths,
  kgXref, etc. and be used by diff gene expr.
}

