
#' This function leverages many of the plot functions developped in gqUtils to generate EDA plots like clustering, PCA, etc
#'
#' @param assay.data.fofn Path to a tsv file: col1 = sample id, col2 = cufflinks isoforms.tracking files path ,col3 = HTseq output files path
#' @param samples.path Path to a sample annotation file. Must be readable with gqUtils::readTypedTSV(). Column listed by "variables" below will be used to color and shape data points, while the column .label, if present, will be used as a label of samples on the plots.
#' @param variables Comma-separated column names from "samples" above. Will typically be directly from the ini file parameter
#' @param genes.path Path to a tsv listing genes, as installed with our genomes. This file has mandatory columns featureID, SYMBOL and gene.length
#' @param reset.index
#' @export
exploratoryAnalysisRNAseq <-function(
   htseq.counts.path,
   cuffnorm.fpkms.dir,
   genes.path,
   samples.path	  = NULL,
   variables   	  = NULL,
   output.dir  	  = "exploratory",
   reset.index = TRUE )
{
#  myR ; cd /nfs3_ib/bourque-mp2.nfs/tank/nfs/bourque/nobackup/share/projects/kstorey_kbiggar_Cpicta_bellii_PRJBFX-865 ;  R --no-restore --no-save;
# library(gqSeqUtils); htseq.counts.path="DGE/rawCountMatrix.csv"; cuffnorm.fpkms.dir="custom_cufflinks/cuffnorm2";   genes.path ="/nfs3_ib/bourque-mp2.nfs/tank/nfs/bourque/nobackup/share/mugqic_dev/genomes/Chrysemys_picta_bellii/Chrysemys_picta_bellii-3.0.3/annotation/NCBI_Chrysemys_picta_Annotation_Release_101.genes.tsv";  output.dir  = "exploratorytest2" ; samples.path = "samples.tsv"; variables= NULL;

  ## Create output dir
  dir.create(output.dir,showWarnings = FALSE)

  # Cufflinks FPKMs from cuffnorm
  fpkm.eset = read.delim(file.path(cuffnorm.fpkms.dir,"isoforms.fpkm_table"),row.names="tracking_id",header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
  colnames(fpkm.eset)  = gsub("_0$","",colnames(fpkm.eset))
  transcripts = read.delim(file.path(cuffnorm.fpkms.dir,"isoforms.attr_table"),row.names="tracking_id",header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
  fpkm.eset = ExpressionSet( as.matrix(fpkm.eset), featureData=AnnotatedDataFrame( transcripts[rownames(fpkm.eset),] ) )
  fData(fpkm.eset)[["SYMBOL"]] =  fData(fpkm.eset)[["gene_short_name"]]
  fData(fpkm.eset)[["SYMBOL"]][ fData(fpkm.eset)[["gene_short_name"]] == "-"] = featureNames(fpkm.eset)[ fData(fpkm.eset)[["gene_short_name"]] == "-" ]

  # Read HTseq output + annotate the genes
  rc.eset =  read.delim(htseq.counts.path,row.names=1,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)[,-1]
  rc.eset = ExpressionSet( as.matrix(rc.eset) )
  genes   = read.delim(genes.path,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
  if(any( ! c("SYMBOL","gene.length","featureID") %in% colnames(genes) )){
	  stop("SYMBOL, gene.length and featureID should be columns of genes.path")
  }
  rownames(genes)  = genes[["featureID"]]
  if( ! all(featureNames(rc.eset) %in% rownames(genes))  ){
	  stop("Genes in genes.path do not match exactly those the HTseq read counts matrix")
  }
  fData(rc.eset) = genes[featureNames(rc.eset),]

  # Sample annotation
  if( !setequal( sampleNames(rc.eset), sampleNames(fpkm.eset)) ){
	  stop("Samples in HTseq count data not the same as those of cufflinks")
   }else{
	   rc.eset = rc.eset[, sampleNames(fpkm.eset)] # this aligns the two data sets
  }
  if(!is.null(samples.path)){ # if a sample annotation is provided
	  pdata = readTypedTSV(samples.path)
	  if( ! "Sample" %in% colnames(pdata) ){
		  stop("samples.path file has no Sample column identifying the samples")
	  }
	  rownames(pdata) =  pdata[["Sample"]]
	  if( ! ".label" %in% colnames(pdata)){
		   pdata[[".label"]] = rownames(pdata)
	  }
	  if( !setequal(rownames(pdata),sampleNames(fpkm.eset))  ){
		  stop("Sample names in the data files do not match the Sample column in samples.path")
		}
	  pData(fpkm.eset) 	= pData(rc.eset)  =  pdata[sampleNames(fpkm.eset),]
	}else{
	  	pData(fpkm.eset) 	= pData(rc.eset)  =  data.frame("Sample" = sampleNames(fpkm.eset), ".label" = sampleNames(fpkm.eset),row.names=sampleNames(fpkm.eset) )
	}


  ## Deal with the exp variables: limited to the first two!
  if (!is.null(variables)) {
	variables = unlist(strsplit(variables,split=','))
	if( ! all( variables %in% varLabels(rc.eset)) ){stop("Invalid variables specified")}
    df = pData(rc.eset)[, variables, drop = FALSE]
    df = df[, rev(colnames(df)), drop = FALSE]
    df = df[, 1:min(2, ncol(df)), drop = FALSE]
  }else {
    df = NULL
  }

  ## Normalize an create copies of diverse objects (mmmm not scalable to multiple samples... mem usage!)
  y                    = calcNormFactors( DGEList(counts=exprs(rc.eset))               )
  cpm.eset = rpkm.eset = cpm.raw.eset = rc.eset
  exprs(cpm.raw.eset)  = cpm(y,normalized.lib.sizes=FALSE, log=TRUE                    )
  exprs(cpm.eset)      = cpm(y,  log=TRUE                                              )
  exprs(rpkm.eset)     = rpkm(y, log=TRUE, gene.length=fData(rc.eset)[['gene.length']] )

  ## FPKM values...log2 and offset
  logfpkm.eset = fpkm.eset
  exprs(logfpkm.eset) = log2(1+exprs(logfpkm.eset))


  ## Standard exploration
  l = list(

    # Boxplot of raw counts (log2 CPM)
    uA.boxplot(cpm.raw.eset, path = output.dir, fn = "boxplot_gene_raw_log2CPM.pdf", is.log2 = TRUE,
               alt.colnames = pData(cpm.raw.eset)[['.label']], ylab = "log2(CPM)", xlab = "Sample",
               main = "Raw log2(CPM)", desc = "Boxplot visualization of gene log2(CPM), unnormalized counts")

    # Standard plots: Correlation distance clustering of edgeR's log2 CPM, etc.
    ,uA.cordist.hclust(cpm.eset, path = output.dir,fn = "cordist_hclust_log2CPM.pdf",
                       alt.colnames = pData(cpm.eset)[[".label"]],is.log2=TRUE,xlab='Samples',
                       desc = "Hierachical clustering based on the correlation distance, log2(CPM)", main="log2(CPM)",method='ward.D')
    ,uA.pca(cpm.eset, path = output.dir, fn = "pca_log2CPM.pdf", is.log2 = TRUE,
            alt.colnames = pData(cpm.eset)[['.label']], pdata=df,
            main = "log2(CPM)", desc = "PCA (first two components) of the gene log2CPM values")
    ,uA.mds(cpm.eset, path = output.dir, fn = "mds_log2CPM.pdf", is.log2 = TRUE,
            alt.colnames = pData(cpm.eset)[['.label']], pdata=df,
            main = "log2(CPM)", desc = "Multidimensional Scaling (2D) of the gene log2(CPM) values")
    ,uA.pairwise.manhattan.hclust(cpm.eset, path = output.dir, fn = "pw_mean_abs_diff_log2CPM.pdf", is.log2 = TRUE,
                                  alt.colnames = pData(cpm.eset)[['.label']],
                                  main = "log2(CPM)",    lab = "Samples",desc = "Clustering of the mean absolute difference distance for gene log2(CPM) values")

    # Clustering on edgeR's RPKMs (shouldn't make much difference)
    ,uA.cordist.hclust(rpkm.eset, path = output.dir,fn = "cordist_hclust_log2RPKM.pdf",
                       alt.colnames = pData(rpkm.eset)[[".label"]],is.log2=TRUE,xlab='Samples',
                       desc = "Hierachical clustering based on the correlation distance, log2(RPKM)", main="log2(RPKM)",method='ward.D')
    #   ,uA.pca(rpkm.eset, path = output.dir, fn = "pca_log2rpkm.pdf", is.log2 = TRUE,
    #          alt.colnames = pData(rpkm.eset)[['.label']], pdata=df,
    #         main = "log2RPKM", desc = "PCA (first two components) of the gene log2RPKM values")
  )

  # Most variable genes by Standard devitation of log2(CPM)
  fn = file.path(output.dir, "top_sd_heatmap_log2CPM.pdf")
  pheatmap.eset(cpm.eset, file = fn, scale = "row", row.labels = "SYMBOL", col.labels = ".label", cols.ann = variables)
  l = c(l, list(data.frame(Description = "Heat map of most varying genes by log2(CPM) standard deviation",
                           path = fn)))

  # PVCA
  fn = file.path(output.dir, "pca_anova_log2CPM.pdf")
  pca.anova.eset(cpm.eset, fn = fn, 10)
  l = c(l, list(data.frame(Description = "ANOVA R2 of covariates on the first Principal Components, log2(CPM)",
                           path = fn)))

  # Cufflinks FPKMs most variable
  fn = file.path(output.dir, "top_sd_heatmap_cufflinks_logFPKMs.pdf")
  pheatmap.eset(logfpkm.eset, file = fn, scale = "row", row.labels = "SYMBOL", col.labels = ".label", cols.ann = variables)
  l = c(l, list(data.frame(Description = "Heat map of most varying transcripts by log2(FPKM) standard deviation",
                           path = fn)))
  # Cufflinks FPKM exploration
  # l = c(l,list(
  #        # TopSD for now
  #
  #  ))


  # Special case: only two samples, stpud to report empty plots, so remove them and add a scatterplot of gene counts
  if( ncol(rc.eset) == 2)
  {
    cat("The dataset has 2 samples only. Some plots will be excluded and replaced by a scatterplot")
    l = l[c(1,3,5,7,9)] # TODO: sucks to subset this way by index

    #fn = file.path(output.dir, "scatterplot_log2CPM.png") # ah snap, no transparency on abacus
    fn = file.path(output.dir, "scatterplot_log2CPM.pdf")
    dat = cbind(exprs(cpm.eset),fData(cpm.eset));colnames(dat)[1:2] = c("col1",'col2')
    p = ggplot(data=dat,aes(x = col1,   y=col2 )) + geom_point(alpha=0.3) +
      labs(x=paste0(sampleNames(cpm.eset)[1],' (log2CPM)'),y=paste0(sampleNames(cpm.eset)[2],' (log2CPM)'))+
        geom_smooth()+geom_abline(linetype='dashed',alpha=0.8,color='grey')+geom_text(aes(label=SYMBOL),size=0.5)+
        geom_abline(intercept=1,linetype='dashed',alpha=0.8,color='green') + geom_abline(intercept=-1,linetype='dashed',alpha=0.8,color='green')
   # png(file=fn,res=300,units='in',width=4,height=4) # png cause vector graphics suck for many points
    pdf(file=fn,width=4,height=4)
    print(p)
    dev.off()

    l = c(l, list(data.frame(Description = "Scatterplot comparing the log2CPM values of both samples",
                             path = fn)))

  }


  # Reset the index if requested
  if(reset.index){resetFileIndex(output.dir)}

  # Commit all files
  lapply(l,function(res)
  {
    commitToFileIndex(output.dir,path=res$path,type='exploratory',description=res$Description)
  })

  return(melt(l))

}
# exploratoryAnalysisRNAseq("DGE/rawCountMatrix.csv","custom_cufflinks/cuffnorm2","/nfs3_ib/bourque-mp2.nfs/tank/nfs/bourque/nobackup/share/mugqic_dev/genomes/Chrysemys_picta_bellii/Chrysemys_picta_bellii-3.0.3/annotation/NCBI_Chrysemys_picta_Annotation_Release_101.genes.tsv")
























#' This function leverages many of the plot functions developped in gqUtils to generate EDA plots like clustering, PCA, etc
#'
#' @param project.path
#' @param ini.file.path
#' @param rc.eset An ExpressionSet object of the raw read counts, with fData and pData populated appropritely
#' @param fpkm.eset An ExpressionSet object of cufflinks FPKMs, with fData and pData populated appropritely
#' @param sample.descriptors Character vector giving column names of pData(rc.eset) which are to be concatenated to form a column label
#' @param exp.variables Character vector giving column names of pData(rc.eset) which will be used for coloring, shaping, etc
#' @param rc.row.label Which columns of fData(rc.eset) will be used to label gene features
#' @param fpkm.row.label Same as above, for cufflinks fpkms
#' @param output.dir Directory where the files are to be written
#' @param reset.index Boolean, whether or not the file index should be reset (only exploratory type is reset!).
#' @param output.dir Where the result files will be written.
#'
#' @export
exploratoryRNAseq <-function(
  project.path        = getwd()
  ,ini.file.path       = NULL
  ,rc.eset             = getHTSeqReadCountsEset(project.path,ini.file.path)
  ,fpkm.eset           = getCufflinksFpkmEset(project.path,tracking.type='isoforms')
  ,sample.descriptors  = getSampleDescriptors(project.path,ini.file.path) # these two functions already check if exists in sample annot
  ,exp.variables       = getExpVariables(project.path,ini.file.path,stopifnotfound=FALSE)$variables
  ,rc.row.label        = 'SYMBOL' # not parameterized, should not change across data sets
  ,fpkm.row.label      = c('gene_id',"tracking_id") # not parameterized, should not change across data sets
  ,output.dir          = file.path(project.path,'exploratory') # This will be assumed by reporting?
  ,reset.index         = TRUE
)
{
  # library(gqSeqUtils);reset.index=TRUE;rc.eset=getHTSeqReadCountsEset(project.path,ini.file.path);fpkm.eset=getCufflinksFpkmEset(project.path,tracking.type='isoforms') ; sample.descriptors  = getSampleDescriptors(project.path,ini.file.path);exp.variables = getExpVariables(project.path,ini.file.path,stopifnotfound=FALSE)$variables;rc.row.label='SYMBOL';fpkm.row.label='SYMBOL'; output.dir= file.path(project.path,'exploratory') ;fpkm.row.label      = c('gene_id',"tracking_id")

  ## Create dir
  dir.create(output.dir,showWarnings = FALSE)

  ## Make sure fpkm and raw read counts data sets are column-aligned  (not guranteed I think)
  if(!setequal(sampleNames(rc.eset),sampleNames(fpkm.eset))){stop("Gene-level count and fpkm esets do not share the same samples...")}
  fpkm.eset = fpkm.eset[,sampleNames(rc.eset)] # aligned to rc.eset

  ## Deal with the column labels: collapse sample.descriptors to a single variable
  pData(rc.eset)[[".col.labels"]] =  pData(fpkm.eset)[[".col.labels"]] = do.call(paste, pData(rc.eset)[, sample.descriptors , drop = FALSE])

  ## Deal with the exp variables: limited to the first two!
  if (!is.null(exp.variables)) {
    df = pData(rc.eset)[, exp.variables, drop = FALSE]
    df = df[, rev(colnames(df)), drop = FALSE]
    df = df[, 1:min(2, ncol(df)), drop = FALSE]
  }else {
    df = NULL
    warning(".ini Parameters expVariables and sampleDescriptors not specified. Figure will not have any coloring, special labels, etc.")
  }

  ## Normalize an create copies of diverse objects (mmmm not scalable to multiple samples... mem usage!)
  y                    = calcNormFactors( DGEList(counts=exprs(rc.eset))               )
  cpm.eset = rpkm.eset = cpm.raw.eset = rc.eset
  exprs(cpm.raw.eset)  = cpm(y,normalized.lib.sizes=FALSE, log=TRUE                    )
  exprs(cpm.eset)      = cpm(y,  log=TRUE                                              )
  exprs(rpkm.eset)     = rpkm(y, log=TRUE, gene.length=fData(rc.eset)[['gene.length']] )

  ## FPKM values...log2 and offset
  logfpkm.eset = fpkm.eset
  exprs(logfpkm.eset) = log2(1+exprs(logfpkm.eset))


  ## Standard exploration
  l = list(

    # Boxplot of raw counts (log2 CPM)
    uA.boxplot(cpm.raw.eset, path = output.dir, fn = "boxplot_gene_raw_log2CPM.pdf", is.log2 = TRUE,
               alt.colnames = pData(cpm.raw.eset)[['.col.labels']], ylab = "log2(CPM)", xlab = "Sample",
               main = "Raw log2(CPM)", desc = "Boxplot visualization of gene log2(CPM), unnormalized counts")

    # Standard plots: Correlation distance clustering of edgeR's log2 CPM, etc.
    ,uA.cordist.hclust(cpm.eset, path = output.dir,fn = "cordist_hclust_log2CPM.pdf",
                       alt.colnames = pData(cpm.eset)[[".col.labels"]],is.log2=TRUE,xlab='Samples',
                       desc = "Hierachical clustering based on the correlation distance, log2(CPM)", main="log2(CPM)",method='ward')
    ,uA.pca(cpm.eset, path = output.dir, fn = "pca_log2CPM.pdf", is.log2 = TRUE,
            alt.colnames = pData(cpm.eset)[['.col.labels']], pdata=df,
            main = "log2(CPM)", desc = "PCA (first two components) of the gene log2CPM values")
    ,uA.mds(cpm.eset, path = output.dir, fn = "mds_log2CPM.pdf", is.log2 = TRUE,
            alt.colnames = pData(cpm.eset)[['.col.labels']], pdata=df,
            main = "log2(CPM)", desc = "Multidimensional Scaling (2D) of the gene log2(CPM) values")
    ,uA.pairwise.manhattan.hclust(cpm.eset, path = output.dir, fn = "pw_mean_abs_diff_log2CPM.pdf", is.log2 = TRUE,
                                  alt.colnames = pData(cpm.eset)[['.col.labels']],
                                  main = "log2(CPM)",    lab = "Samples",desc = "Clustering of the mean absolute difference distance for gene log2(CPM) values")

    # Clustering on edgeR's RPKMs (shouldn't make much difference)
    ,uA.cordist.hclust(rpkm.eset, path = output.dir,fn = "cordist_hclust_log2RPKM.pdf",
                       alt.colnames = pData(rpkm.eset)[[".col.labels"]],is.log2=TRUE,xlab='Samples',
                       desc = "Hierachical clustering based on the correlation distance, log2(RPKM)", main="log2(RPKM)",method='ward')
    #   ,uA.pca(rpkm.eset, path = output.dir, fn = "pca_log2rpkm.pdf", is.log2 = TRUE,
    #          alt.colnames = pData(rpkm.eset)[['.col.labels']], pdata=df,
    #         main = "log2RPKM", desc = "PCA (first two components) of the gene log2RPKM values")
  )

  # Most variable genes by Standard devitation of log2(CPM)
  fn = file.path(output.dir, "top_sd_heatmap_log2CPM.pdf")
  pheatmap.eset(cpm.eset, file = fn, scale = "row", row.labels = rc.row.label, col.labels = sample.descriptors, cols.ann = exp.variables)
  l = c(l, list(data.frame(Description = "Heat map of most varying genes by log2(CPM) standard deviation",
                           path = fn)))

  # PVCA
  fn = file.path(output.dir, "pca_anova_log2CPM.pdf")
  pca.anova.eset(cpm.eset, fn = fn, 10)
  l = c(l, list(data.frame(Description = "ANOVA R2 of covariates on the first Principal Components, log2(CPM)",
                           path = fn)))

  # Cufflinks FPKMs most variable
  fn = file.path(output.dir, "top_sd_heatmap_cufflinks_logFPKMs.pdf")
  pheatmap.eset(logfpkm.eset, file = fn, scale = "row", row.labels = fpkm.row.label, col.labels = sample.descriptors, cols.ann = exp.variables)
  l = c(l, list(data.frame(Description = "Heat map of most varying transcripts by log2(FPKM) standard deviation",
                           path = fn)))
  # Cufflinks FPKM exploration
  # l = c(l,list(
  #        # TopSD for now
  #
  #  ))


  # Special case: only two samples, stpud to report empty plots, so remove them and add a scatterplot of gene counts
  if( ncol(rc.eset) == 2)
  {
    cat("The dataset has 2 samples only. Some plots will be excluded and replaced by a scatterplot")
    l = l[c(1,3,5,7,9)] # TODO: sucks to subset this way by index

    #fn = file.path(output.dir, "scatterplot_log2CPM.png") # ah snap, no transparency on abacus
    fn = file.path(output.dir, "scatterplot_log2CPM.pdf")
    dat = cbind(exprs(cpm.eset),fData(cpm.eset));colnames(dat)[1:2] = c("col1",'col2')
    p = ggplot(data=dat,aes(x = col1,   y=col2 )) + geom_point(alpha=0.3) +
      labs(x=paste0(sampleNames(cpm.eset)[1],' (log2CPM)'),y=paste0(sampleNames(cpm.eset)[2],' (log2CPM)'))+
        geom_smooth()+geom_abline(linetype='dashed',alpha=0.8,color='grey')+geom_text(aes(label=SYMBOL),size=0.5)+
        geom_abline(intercept=1,linetype='dashed',alpha=0.8,color='green') + geom_abline(intercept=-1,linetype='dashed',alpha=0.8,color='green')
   # png(file=fn,res=300,units='in',width=4,height=4) # png cause vector graphics suck for many points
    pdf(file=fn,width=4,height=4)
    print(p)
    dev.off()

    l = c(l, list(data.frame(Description = "Scatterplot comparing the log2CPM values of both samples",
                             path = fn)))

  }


  # Reset the index if requested
  if(reset.index){resetFileIndex(project.path,'exploratory')}

  # Commit all files
  lapply(l,function(res)
  {
    commitToFileIndex(project.path,path=res$path,type='exploratory',description=res$Description)
  })

  return(melt(l))

}
# TODO:
# - might not be scalable to large multismple datasets because making multiple copies of esets...
# - See e.g. NOIseq, EDAseq, etc. for apparaently interesting expl. analysis plots
#  # Saturation..
# GSVA, composition, etc.
# Grubbs  test feautres?









#' This function implements standard SOP exploratory analysis. Note that additional figures can be genrated outside of the SOP and still be reported
#'
#' @title SOP generic exploratory analysis plots
#' @param e An ExpressionSet object
#' @param path path to where the exploratory figures will be written
#' @param col.labels character vector label listing which column in pData(e) should be used as label
#' @param row.labels character vector label listing which column in fData(e) should be used as label
#' @param cols.ann character vector label listing which column in pData(e) should be used as additional annotations, e.g. color, shape, color bars at the top of heatmap
#' @author Lefebvre F
#' @export
exploratoryAnalysis.expression <- function(e,path='.',col.labels='SampleID',row.labels='SYMBOL',cols.ann=NULL)
{
	pData(e)[['.col.labels']]  = do.call(paste,pData(e)[,col.labels,drop=FALSE]) # more convienient to do this right away
	if(!is.null(cols.ann))
	{
		df = pData(e)[,cols.ann,drop=FALSE]
		df = df[,rev(colnames(df)),drop=FALSE] # because we instructed to put the most important one last
		df = df[,1:min(2,ncol(df)),drop=FALSE]

	}else{df=NULL}
		# df is a data frame of annotations to supply to the uA-style plots.. they require a df of lenght 2 max

	l = list(
		uA.cordist.hclust(e, path, alt.colnames = pData(e)[['.col.labels']]) # hclust
		,uA.mds(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# MDS
		,uA.pca(e,path, alt.colnames = pData(e)[['.col.labels']], pdata = df)# PCA
		)

	# Heatmap of top sd genes. # TODO: might be more elegant to return index like all others but eh.
	fn = file.path(path,'top_sd_heatmap.pdf')
	pheatmap.eset(e,file=fn,scale='row',row.labels=row.labels,col.labels=col.labels,cols.ann=cols.ann)
	l = c(l,list(data.frame('Description'='Heat map of most varying probes (top SD)','path'=fn)))


	# PVCA
	fn = file.path(path,'pca_anova.pdf')
	pca.anova.eset(e,fn=fn,min(ncol(e),10))
	l = c(l,list(data.frame('Description'='ANOVA R2 of covariates on the first Principal Components','path'=fn)))


warning("This function is deprecrated")
	index = do.call(rbind,l)

}

#' This function implements standard SOP exploratory analysis for the RNASEQ de novo pipeline.
#'
#' @title SOP exploratory analysis plots
#' @param read.counts.path, Path to a tsv file containing gene_id, read counts for all samples, header from 2 to ncol must correspond to sample name
#' @param genes.path Path to a tsv listing genes annotations. This file has mandatory columns gene_id and gene.length
#' @param samples.path Path to a sample annotation file. Must be readable with gqUtils::readTypedTSV(). Column listed by "variables" below will be used to color and shape data points, while the column .label, if present, will be used as a label of samples on the plots.
#' @param variables Comma-separated column names from "samples" above. Will typically be directly from the ini file parameter
#' @param output.dir Where the result files will be written.
#' @param reset.index Logical for index file reset
#' @author Johanna Sandoval
#' @export

exploratoryAnalysisRNAseqdenovo <-function(
    read.counts.path,
    genes.path,
    samples.path   = NULL,
    variables      = NULL,
    output.dir     = "exploratory",
    reset.index = TRUE
)
{
    ## Create output dir
    dir.create(output.dir,showWarnings = FALSE)

    #  FPKMs doesn't make a lot of sense in de novo RNASEQ, we'll exclusively use raw counts from RSEM (align and estimate abundance)
    # Read annotations
    annotate.genes= read.delim(genes.path ,row.names=1,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
    # Read raw counts output
    rc.eset =  read.delim(read.counts.path, row.names=1,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
    # Sample annotation
    rc.eset = ExpressionSet( as.matrix(rc.eset), featureData=AnnotatedDataFrame( annotate.genes[ rownames(rc.eset),] ))

    # Annotate the genes
    fData(rc.eset)[["SYMBOL"]] =  rownames(annotate.genes)

    length.column.name="length"
    if(any( ! c(length.column.name) %in% colnames(annotate.genes) )){
        stop(cat("gene_id and length should be columns of genes.path : ", genes.path, "\n"))
        }

    if( ! all(featureNames(rc.eset) %in% rownames(annotate.genes))  ){
        stop("Genes in genes.path do not match exactly those the HTseq read counts matrix")
        }
    fData(rc.eset) = annotate.genes[featureNames(rc.eset),]

    if(!is.null(samples.path)){ # if a sample annotation is provided
        pdata = readTypedTSV(samples.path)
        if( ! "Sample" %in% colnames(pdata) ){
            stop("samples.path file has no Sample column identifying the samples")
        }
        rownames(pdata) =  pdata[["Sample"]]
        if( ! ".label" %in% colnames(pdata)){
            pdata[[".label"]] = rownames(pdata)
        }
        if( !setequal(rownames(pdata),sampleNames(rc.eset))  ){
            stop("Sample names in the data files do not match the Sample column in samples.path")
        }
        pData(rc.eset)  =  pdata[sampleNames(rc.eset),]
    }else{
        pData(rc.eset)  =  data.frame("Sample" = sampleNames(rc.eset), ".label" = sampleNames(rc.eset),row.names=sampleNames(rc.eset) )
    }

    ## Deal with the exp variables: limited to the first two!
    if (!is.null(variables)) {
        variables = unlist(strsplit(variables,split=','))
        if( ! all( variables %in% varLabels(rc.eset)) ){stop("Invalid variables specified")}
        df = pData(rc.eset)[, variables, drop = FALSE]
        df = df[, rev(colnames(df)), drop = FALSE]
        df = df[, 1:min(2, ncol(df)), drop = FALSE]
    }else {
        df = NULL
    }
    exploratoryAnalysisEset (rc.eset, output.dir, reset.index, length.column.name)
}


#' This function implements standard SOP exploratory analysis for the RNASEQ Light pipeline.
#'
#' @title SOP exploratory analysis plots
#' @param read.counts.path, Path to a rawCountMatrix file containing gene_id, read counts for all samples, header from 2 to ncol must correspond to sample name
#' @param genes.path Path to a tsv listing genes annotations. This file has mandatory columns gene_id and gene.length
#' @param samples.path Path to a sample annotation file. Must be readable with gqUtils::readTypedTSV(). Column listed by "variables" below will be used to color and shape data points, while the column .label, if present, will be used as a label of samples on the plots.
#' @param variables Comma-separated column names from "samples" above. Will typically be directly from the ini file parameter
#' @param output.dir Where the result files will be written.
#' @param reset.index Logical for index file reset
#' @author Eloi Mercier
#' @export
exploratoryAnalysisRNAseqLightKallisto <-function(
    read.counts.path,
    genes.path,
    samples.path   = NULL,
    variables      = NULL,
    output.dir     = "exploratory",
    reset.index = TRUE)
{
    ## Create output dir
    dir.create(output.dir,showWarnings = FALSE)

    #  FPKMs doesn't make a lot of sense here, we'll exclusively use raw counts from kallisto
    # Read annotations
    annotate.genes= read.delim(genes.path ,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
    # Read raw counts output
    rc.eset =  read.delim(read.counts.path, row.names=1,header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)

  if(any( ! c("SYMBOL","gene.length","featureID") %in% colnames(annotate.genes) )){
      stop("SYMBOL, gene.length and featureID should be columns of genes.path")
    }
  rownames(annotate.genes)=annotate.genes[,"featureID"]
  # Sample annotation
    rc.eset = ExpressionSet( as.matrix(rc.eset), featureData=AnnotatedDataFrame( annotate.genes[ rownames(rc.eset),] ))

    if( ! all(featureNames(rc.eset) %in% rownames(annotate.genes))  ){
        stop("Genes in genes.path do not match exactly those the HTseq read counts matrix")
        }
    fData(rc.eset) = annotate.genes[featureNames(rc.eset),]

    if(!is.null(samples.path)){ # if a sample annotation is provided
        pdata = readTypedTSV(samples.path)
        if( ! "Sample" %in% colnames(pdata) ){
            stop("samples.path file has no Sample column identifying the samples")
        }
        rownames(pdata) =  pdata[["Sample"]]
        if( ! ".label" %in% colnames(pdata)){
            pdata[[".label"]] = rownames(pdata)
        }
        if( !setequal(rownames(pdata),sampleNames(rc.eset))  ){
            stop("Sample names in the data files do not match the Sample column in samples.path")
        }
        pData(rc.eset)  =  pdata[sampleNames(rc.eset),]
    }else{
        pData(rc.eset)  =  data.frame("Sample" = sampleNames(rc.eset), ".label" = sampleNames(rc.eset),row.names=sampleNames(rc.eset) )
    }

    fData(rc.eset)[,"featureID"]=fData(rc.eset)[,"SYMBOL"]
    exploratoryAnalysisEset (rc.eset, output.dir, reset.index, "gene.length")
}



#' This function implements standard SOP exploratory analysis for the SmallRNA pipeline.
#'
#' @title SOP exploratory analysis plots
#' @param read.counts.path, Path to a
#' @param genes.path Path to a tsv listing genes annotations. This file has mandatory columns gene_id and gene.length
#' @param samples.path Path to a sample annotation file. Must be readable with gqUtils::readTypedTSV(). Column listed by "variables" below will be used to color and shape data points, while the column .label, if present, will be used as a label of samples on the plots.
#' @param variables Comma-separated column names from "samples" above. Will typically be directly from the ini file parameter
#' @param output.dir Where the result files will be written.
#' @param reset.index Logical for index file reset
#' @author Eloi Mercier
#' @export
exploratoryAnalysisSmallRNA <-function(
    read.counts.path,
    all.mature.path,
    all.precursor.path,
    fn.codes.path,
    samples.path   = NULL,
    variables      = NULL,
    output.dir     = "exploratory",
    reset.index = TRUE)
{

  dir.create(output.dir,showWarnings = FALSE)

  # Read raw counts output
  rc.eset =  read.delim(read.counts.path, header=T,quote="",comment.char="",stringsAsFactors=F,check.names=F)
  pdata = read.table(fn.codes.path,header=FALSE,sep='\t',row.names=1); colnames(pdata) = "code"

  annotate.genes=rc.eset[,1:3]; # colnames(annotate.genes)=c("miRNA", "read_count", "precursor")
  colnames(rc.eset) <- gsub("\\(norm\\)", "", colnames(rc.eset))
  rc.eset=rc.eset[, pdata[["code"]]]
  colnames(rc.eset)=rownames(pdata) #shorter names?
  rc.eset = ExpressionSet( as.matrix(rc.eset), featureData=AnnotatedDataFrame(annotate.genes))

  if( ! all(featureNames(rc.eset) %in% rownames(annotate.genes))  ){
      stop("Genes in genes.path do not match exactly those the HTseq read counts matrix")
      }
  fData(rc.eset) = annotate.genes[featureNames(rc.eset),]

  if(!is.null(samples.path)){ # if a sample annotation is provided
      pdata = readTypedTSV(samples.path)
      if( ! "Sample" %in% colnames(pdata) ){
          stop("samples.path file has no Sample column identifying the samples")
      }
      rownames(pdata) =  pdata[["Sample"]]
      if( ! ".label" %in% colnames(pdata)){
          pdata[[".label"]] = rownames(pdata)
      }
      if( !setequal(rownames(pdata),sampleNames(rc.eset))  ){
          stop("Sample names in the data files do not match the Sample column in samples.path")
      }
      pData(rc.eset)  =  pdata[sampleNames(rc.eset),]
  }else{
      pData(rc.eset)  =  data.frame("Sample" = sampleNames(rc.eset), ".label" = sampleNames(rc.eset),row.names=sampleNames(rc.eset) )
  }

  fData(rc.eset)[,"featureID"]=fData(rc.eset)[,"#miRNA"]
  m  = readRNAStringSet(all.mature.path)
  p  = readRNAStringSet(all.precursor.path)
  fData(rc.eset)[,"mature.sequence"] = as.character(m[ fData(rc.eset)[["#miRNA"]] ])
  fData(rc.eset)$mature.length = nchar(fData(rc.eset)$mature.sequence)
  fData(rc.eset)$precursor.sequence = as.character(p[ fData(rc.eset)[["precursor"]] ])
  fData(rc.eset)$precursor.length = nchar(fData(rc.eset)$precursor.sequence)

  fData(rc.eset)[[".total"]] = apply(exprs(rc.eset),MARGIN=1,sum)
  rc.eset = rc.eset[ order(fData(rc.eset)[[".total"]],decreasing=TRUE),]
  rc.eset = rc.eset[!duplicated(fData(rc.eset)[["#miRNA"]]),]
  featureNames(rc.eset) = fData(rc.eset)[["#miRNA"]]
  fData(rc.eset)[["featureID"]] = featureNames(rc.eset)
  fData(rc.eset)[["gene.length"]] = fData(rc.eset)[["mature.length"]]

  exploratoryAnalysisEset (rc.eset, output.dir, reset.index, "mature.length")
}



#' This function encapsulate standard SOP exploratory plots for the RNASEQ de novo pipeline.
#'
#' @title SOP exploratory analysis plots
#' @param e An ExpressionSet object generated from read counts or cufflinks fpkm
#' @param output.dir Where the result files will be written.
#' @param reset.index Logical for index file reset
#' @param length.column.name String for name of the gene length field
#' @param df data.frame with additional variables if a sample annotation is provided
#' @author Johanna Sandoval
#' @export

exploratoryAnalysisEset <-function(
    e,
    output.dir     = "exploratory",
    reset.index = TRUE,
    length.column.name="length",
    eset.type="counts",
    df=NULL
)
{
    if ( eset.type == "fpkm"){
        ## FPKM values...log2 and offset
        logfpkm.eset = e
        exprs(logfpkm.eset) = log2(1+exprs(logfpkm.eset))

        # Initialize list
        l = list()
        #  FPKMs most variable
        fn = file.path(output.dir, "top_sd_heatmap_logFPKMs.pdf")
        pheatmap.eset(logfpkm.eset, file = fn, scale = "row", col.labels = ".label", cols.ann = colnames(df))
        l = c(l, list(data.frame(Description = "Heat map of most varying transcripts by log2(FPKM) standard deviation",
                                 path = fn))
        )
    } else{
        ## Normalize and create copies of diverse objects (mmmm not scalable to multiple samples... mem usage!)
        y                    = calcNormFactors( DGEList(counts=exprs(e))               )
        cpm.eset = rpkm.eset = cpm.raw.eset = e
        exprs(cpm.raw.eset)  = cpm(y,  normalized.lib.sizes=FALSE, log=TRUE)
        exprs(cpm.eset)      = cpm(y,  log=TRUE                                              )
        exprs(rpkm.eset)     = rpkm(y, log=TRUE, gene.length=fData(e)[[length.column.name]] )
        ## Standard exploration
        l = list(

            # Boxplot of raw counts (log2 CPM)
            uA.boxplot(cpm.raw.eset, path = output.dir, fn = "boxplot_gene_raw_log2CPM.pdf", is.log2 = TRUE,
                       alt.colnames = pData(cpm.raw.eset)[['.label']], ylab = "log2(CPM)", xlab = "Sample",
                       main = "Raw log2(CPM)", desc = "Boxplot visualization of gene log2(CPM), unnormalized counts")

            # Standard plots: Correlation distance clustering of edgeR's log2 CPM, etc.
            ,uA.cordist.hclust(cpm.eset, path = output.dir,fn = "cordist_hclust_log2CPM.pdf",
                               alt.colnames = pData(cpm.eset)[[".label"]],is.log2=TRUE,xlab='Samples',
                               desc = "Hierachical clustering based on the correlation distance, log2(CPM)", main="log2(CPM)",method='ward.D')
            ,uA.pca(cpm.eset, path = output.dir, fn = "pca_log2CPM.pdf", is.log2 = TRUE,
                    alt.colnames = pData(cpm.eset)[['.label']], pdata=df,
                    main = "log2(CPM)", desc = "PCA (first two components) of the gene log2CPM values")
            ,uA.mds(cpm.eset, path = output.dir, fn = "mds_log2CPM.pdf", is.log2 = TRUE,
                    alt.colnames = pData(cpm.eset)[['.label']], pdata=df,
                    main = "log2(CPM)", desc = "Multidimensional Scaling (2D) of the gene log2(CPM) values")
            ,uA.pairwise.manhattan.hclust(cpm.eset, path = output.dir, fn = "pw_mean_abs_diff_log2CPM.pdf", is.log2 = TRUE,
                                          alt.colnames = pData(cpm.eset)[['.label']],
                                          main = "log2(CPM)",    lab = "Samples",desc = "Clustering of the mean absolute difference distance for gene log2(CPM) values")
            # Clustering on edgeR's RPKMs (shouldn't make much difference)
            ,uA.cordist.hclust(rpkm.eset, path = output.dir,fn = "cordist_hclust_log2RPKM.pdf",
                               alt.colnames = pData(rpkm.eset)[[".label"]],is.log2=TRUE,xlab='Samples',
                               desc = "Hierachical clustering based on the correlation distance, log2(RPKM)", main="log2(RPKM)",method='ward.D')
        )
        # Most variable genes by Standard devitation of log2(CPM)
        fn = file.path(output.dir, "top_sd_heatmap_log2CPM.pdf")
        pheatmap.eset(cpm.eset, file = fn, scale = "row",row.labels = "featureID", col.labels = ".label", cols.ann = colnames(df))
        l = c(l, list(data.frame(Description = "Heat map of most varying genes by log2(CPM) standard deviation",
                                     path = fn)))
    }
    # Reset the index if requested
    if(reset.index){resetFileIndex(output.dir)}

    # Commit all files
    lapply(l,function(res)
            {
                commitToFileIndex(output.dir,path=res$path,type='exploratory',description=res$Description)
    })
    return(melt(l))

}

exploratoryAnalysisUnitTest<-function(){
    #  myR ; cd /nfs3_ib/bourque-mp2.nfs/tank/nfs/bourque/nobackup/share/projects/kstorey_kbiggar_Cpicta_bellii_PRJBFX-865 ;  R --no-restore --no-save; exploratoryAnalysisUnitTest()
    library(gqSeqUtils); read.counts.path="differential_expression/genes.counts.matrix"; fpkms.dir="differential_expression";   genes.path ="differential_expression/genes.lengths.tsv";  output.dir = "exploratorytest" ;
    #samples.path ='design.csv'; variables= 'coho_vs_hybrid,chinook_vs_hybrid,all_vs_hybrid'; reset.index=TRUE;
    samples.path =NULL; variables=NULL; reset.index=TRUE;
    exploratoryAnalysisRNAseqdenovo(read.counts.path, genes.path,samples.path, variables, output.dir, reset.index)
}

