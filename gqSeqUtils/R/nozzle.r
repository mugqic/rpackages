# Here functions related to reporting with Nozzle


#' This is the main function for report generation. It dispatches according the the pipeline type.
#'
#' Should the output be re-used (adding addition sections, setNextReport(), etc.), the function
#' writePatchedNozzleReport() should be preferned to Nozzle.R::writeReport as it
#' applies patches.
#' @export
mugqicPipelineReport <-function(
		 pipeline
 		,project.path   = getwd() # meh, might want to enforce something here
		,ini.file.path	= NULL # this will trigger a guess using guessCluster()
		,report.path   	= getIniParams('report_path'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE) # would be better to have getReportPath(), a fct that tries to read ini, if NA, then hard default...
		,report.title   = getIniParams('report_title'  	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
		,report.author 	= getIniParams('report_author'  	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
		,report.contact	= getIniParams('report_contact'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)

		,report.css	= getIniParams('report_css'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	)
{
		####### TEMP
#		pipeline = 'RNAseq'
#	 	project.path   = getwd() # meh, might want to enforce something here
#		ini.file.path	= 'bin/rnaSeq.abacus.ini'#NULL # this will trigger a guess using guessCluster()
#		report.path   	= getIniParams('report_path'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
#		report.title    = getIniParams('report_title'  	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
#		report.author 	= getIniParams('report_author'  	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
#		report.contact	= getIniParams('report_contact'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
#		report.css	= getIniParams('report_css'	,'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
		####### TEMP
	
	## Checks on project.path, ini.file.path
	if(!file.exists(project.path))
	{
		stop(paste0(project.path,' does not exist'))
	}
	if(! pipeline %in% getSupportedPipelineTypes() )
	{
		stop(paste0('Unsupported pipeline. Should be one of those:',
			paste(getSupportedPipelineTypes(),collapse=',')))
	}
	if(!is.null(ini.file.path)  )
	{
		if( any( !file.exists(ini.file.path) )){stop('.ini file path does not exist')}
	}

  
	# TODO: perhaps function should be left to NULL, and fall back to hard default within individucal functions??? That would lighten the code here. e.g.
	## Fall back on hard defaults 
	if( is.na(report.path)		){report.path 	= paste(format(Sys.time(),"%Y_%m_%d"),'report',sep='_');print("report_path not found in .ini file. Using hard default")}
	if( is.na(report.title)		){report.title	= paste('MUGQIC',pipeline,'Analysis Standard Report');print("report_title not found in .ini file. Using hard default")}
	if( is.na(report.author)	){report.author	= Sys.info()[['user']];print("report_author not found in .ini file. Using hard default")}
	if( is.na(report.contact)	){report.contact= "bioinformatics.genome@mail.mcgill.ca";print("report_contact not found in .ini file. Using hard default")}
	if( is.na(report.css)		){report.css    = system.file(file.path('css','nozzle.css'),package='Nozzle.R1');print("report_css not found in .ini file. Using hard default")} 
		# TODO: change me to NANUQ collab CSS in system.file(paste(pipeline,'css'),package='gqSeqUtils')
		# TODO: consider putting all the hard defaults somewhere instead of hard coding like this
	## Create report.path
	dir.create(file.path(report.path,'files')	,showWarnings=FALSE,recursive=TRUE)
	dir.create(file.path(report.path,'files','img')	,showWarnings=FALSE,recursive=TRUE)

	## Initialize Nozzle Report
	report <- newCustomReport(report.title)
	report <- setCustomScreenCss( report, report.css )
	report <- setCustomPrintCss( report, report.css )
	report <- setContactInformation( report, email=report.contact, subject="[Question - Bioinformatics Analysis]", message="", label="Contact MUGQIC Bioinformatics Team" )
	report <- setMaintainerName( report, "MUGQIC Bioinformatics" )
	report <- setMaintainerEmail( report, "bioinformatics.genome@mail.mcgill.ca" )
	report <- setMaintainerAffiliation( report, "McGill University and Genome Quebec Innovation Centre" )
	report <- setCopyright( report, owner="McGill University and Genome Quebec Innovation Centre", year=format(Sys.time(),'%Y'), statement="", url=getUrls()[['mugqic']] )
	report <- setGoogleAnalyticsId( report, NA )
	report <- setSoftwareName( report, "gqSeqUtils" )
	report <- setSoftwareVersion( report, packageVersion('gqSeqUtils')  )

	report[['meta']][['creator']][['name']] = report.author # no accessor, weird

	# Logos
	file.copy( list.files(system.file('img',package='gqSeqUtils'),full.names=TRUE)
		,file.path(report.path,'files','img'),recursive=TRUE)# cp logos logo to report
	report =  setLogo(report, file.path('files','img','nanuq.gif') , LOGO.TOP.LEFT)
	report =  setLogo(report, file.path('files','img','gq.svg') , LOGO.BOTTOM.LEFT)
	report =  setLogo(report, file.path('files','img','mcgill.svg') , LOGO.BOTTOM.RIGHT)
	
	###tmptest
	print("report initialization.... DONE")

	## DISPATCH for pipeline specific content (robust to missing steps please)
	report = eval(call(
		 name = paste0('mugqicPipelineReport',pipeline)
		,report,project.path,ini.file.path,report.path))

	###tmptest
	print("report pipeline specificity.... DONE")
	## Additional Analysis Needs
	# - more info section:# For more information, you are invited to consult our wiki.
	# Requests for additional analyses should be directed at our client mangement office. or if hours left..
	# or contact the bioinformatician	
	report = addTo( report,addTo(newSection( "Need Additional Analysis?" ), newParagraph("Contact our client management office at ",asLink('mailto:infoservices@genomequebec.com','infoservices@genomequebec.com'),"." ) ) ) 


	#paragraph1 <- newParagraph( "Nozzle supports a range of different formatting styles: ",
	#			asStrong( "strong" ), "; ", 
	#			asEmph( "emph" ), "; ",
	#			asParameter( "parameter" ), " = ",
	#			asValue( "value" ), "; ",
	#			asFilename( "filename" ), "; ",
	#			asCode( "code" ), "; ", exportId="LIST_FORMAT" );


	
	## Analysis Paramerters and Software Versions
	# table or link? should be a csv in all cases!!!...
	ini.table  =  getIniParams(ini.file.path=ini.file.path, project.path=project.path, as.data.frame=TRUE)
	ini.table.file = file.path('files','ini.csv') 
	write.csv(ini.table,file=file.path(report.path,ini.table.file))
	report <- addTo( report, addTo( newSection( "Analysis Parameters and Software Versions",class=SECTION.CLASS.META )
		,  newTable(ini.table, 'List of  parameters used for the analysis. Look for paramerters moduleVersion.* to find out which version of a particular external tool was used.', file = ini.table.file)
	 ) ) 

	###tmptest
	print("report modules.... DONE")

	## Append meta section About this Report
	report <- addTo( report, addTo( newSection( "Report Meta Information",class=SECTION.CLASS.META, protection = PROTECTION.GROUP)
		, newParameterList( 
			 "Report Creator", paste0( getCreatorName( report ) ,' (username: ',Sys.info()[['user']],')')
			,"Created"	 , getCreatorDate(report)
			,"Renderered with" , getRendererName( report )
			,"Rendered by" , paste( asEmph( getSoftwareName( report ) ) , getSoftwareVersion(report) )
			,"node", Sys.info()[['nodename']] , protection = PROTECTION.GROUP)  
	 ) ) 

	
	###tmptest
	print("report metasection.... DONE")


	## Set next/previous report (NOT USED)
	#report1 <- setNextReport( report1, "nozzle2.html", "Demo 2" );
	#report1 <- setPreviousReport( report1, "nozzle4.html", "Demo 4" );

	## Write out report
	writePatchedNozzleReport( report, filename= file.path(report.path,'index') )
#	return(report)
}






#' Reads Nozzle html and applies a bunch of patches
#' @export
writePatchedNozzleReport <- function(report,filename,...)
{

	writeReport(report,filename,...)
	###tmptest
	print("report writing.... DONE")

	# Patch for clickable logos
	patchHtmlClickableImages(paste0(filename,'.html') 
	,c( file.path('files','img','nanuq.gif')
	  ,file.path('files','img','gq.svg')
	  ,file.path('files','img','mcgill.svg') )
	,c(getUrls()[['nanuq']]
		,getUrls()[['gq']]
		,getUrls()[['mcgill']]) )
	###tmptest
	print("report patching.... DONE")

}

#' This function reads html code and adds hrefs to images.  Will not work is im
#' @export
patchHtmlClickableImages <- function(html.path,img.path, img.link)
{
	lines = readLines(html.path)
	lines = mgsub(
	 paste0( "<img src=\"",img.path,"\"/>"   )
	,paste0("<a href=\"",img.link,"\"><img src=\"",img.path,"\"/></a>")
	,lines)	
	writeLines(lines,html.path)
}
# TODO: move me to gqUtils



