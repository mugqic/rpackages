#' Nozzle report for RNA-Seq De Novo Assembly Pipeline
#'
#' @author Joel Fillon joel.fillon_AT_mcgill.ca
#' @export
mugqicPipelineReportRNAseqDeNovo = function(report, project.path, ini.file.path, report.path) 
{

  citations = getCitations()

  #
  # Introduction
  #
  introduction = newSection("Introduction")
  introduction.text1 = newParagraph(" 
This document contains the description of the current RNA-Seq De Novo Assembly and Differential Analysis Pipeline developed at the McGill University and Genome Quebec Innovation Centre (MUGQIC) as of ", format(Sys.time(), format = "%Y-%m-%d"), ".")
  introduction.text2 = newParagraph("This Pipeline has been developed following the protocol described in the article \"<u>De novo transcript sequence reconstruction from RNA-Seq using the Trinity platform for reference generation and analysis</u>\" published in <i>Nature Protocols</i> on July 2013 ", asReference(citations$trinityProtocol), " and mostly based on the Trinity assembly software suite ", asReference(citations$trinity), asReference(citations$trinityWeb), ".")

  trimming.stats = read.csv(
    file.path(
      project.path,
      "metrics",
      "trimming.stats"
    ),
    header = FALSE,
    sep = "\t",
    comment.char = "",
    check.names = FALSE
  )

  library.min.size = as.character(round(min(trimming.stats[, 3]) / 1000000))
  library.max.size = as.character(round(max(trimming.stats[, 3]) / 1000000))
  library.type = getIniParams(
    'library_type',
    'DEFAULT',
    ini.file.path = ini.file.path,
    project.path = project.path,
    stopifnotfound = FALSE
  )
  introduction.text3 = newParagraph("The RNA sequencing performed at the Innovation Center has generated between ", library.min.size, " and ", library.max.size, " millions ", tolower(library.type), " reads per library using the Illumina Hiseq 2000/2500 sequencer. Base calls are made using the Illumina CASAVA pipeline. Base quality is encoded in phred 33.")

  introduction = addTo(
    introduction,
    introduction.text1,
    introduction.text2,
    introduction.text3
  )


  #
  # Analysis and Results
  #
  results = newSection("Analysis and Results")


  # Trimming
  trimming = newSubSection("Trimming")

  trim.align.file = "alignmentMetrics.tsv"

  trimming.text1 = newParagraph("Read trimming and clipping have been performed using the Trimmomatic software ", asReference(citations$trimmomatic), ". Reads have been trimmed from the 3' end with a minimal phred score of 30. Illumina sequencing adapters have been removed. Read minimal length has been set to 32 bp.")

  samples.unique = sort(unique(as.vector(trimming.stats[, 1])))

  trimming.results.col.sample = NULL
  trimming.results.col.raw = NULL
  trimming.results.col.trim = NULL
  trimming.results.col.pct = NULL

  for (i in 1:length(samples.unique)) {
    trimming.results.col.sample = c(trimming.results.col.sample, samples.unique[i])
    sample.raw.reads.sum = sum(trimming.stats[trimming.stats[, 1] %in% samples.unique[i], 3])
    sample.trimmed.reads.sum = sum(trimming.stats[trimming.stats[, 1] %in% samples.unique[i], 4])
    trimming.results.col.raw = c(trimming.results.col.raw, sample.raw.reads.sum)
    trimming.results.col.trim = c(trimming.results.col.trim, sample.trimmed.reads.sum)
    trimming.results.col.pct = c(trimming.results.col.pct, round(sample.trimmed.reads.sum / sample.raw.reads.sum * 100))
  }

  trimming.results.data.frame = data.frame(
    trimming.results.col.sample,
    format(trimming.results.col.raw, big.mark=","),
    format(trimming.results.col.trim, big.mark=","),
    format(trimming.results.col.pct)
  )

  total.raw.reads.sum = sum(trimming.stats[, 3])
  total.trimmed.reads.sum = sum(trimming.stats[, 4])

  trimming.results.data.frame = rbind(
    trimming.results.data.frame,
    c(
      "TOTAL",
      format(total.raw.reads.sum, big.mark=","),
      format(total.trimmed.reads.sum, big.mark=","),
      format(round(total.trimmed.reads.sum / total.raw.reads.sum * 100))
    )
  )

  # Uppercase "Paired" or "Single"
  cap.library.type = paste0(toupper(substring(library.type, 1, 1)), substring(library.type, 2))

  colnames(trimming.results.data.frame) = c(
    "Sample",
    paste0("Raw ", cap.library.type, " Reads"),
    paste0("Surviving ", cap.library.type, " Reads"),
    paste0("%")
  )

  trimming.results.file = "sampleTrimmingMetrics.tsv"

  write.table(
    trimming.results.data.frame,
    file.path(report.path, trimming.results.file),
    sep = "\t",
    col.names = TRUE,
    row.names = FALSE,
    quote = FALSE
  )

  trimming.table.min = newTable(trimming.results.data.frame[max(1, dim(trimming.results.data.frame)[1] - 6):dim(trimming.results.data.frame)[1], ],
    "Trimming metrics",
    file = trimming.results.file,
    significantDigits = 3
  )

  trimming.results.list = newParameterList(
    asStrong("Raw ", cap.library.type, " Reads"), paste0(" number of ", cap.library.type, " Reads obtained from the sequencer"),
    asStrong("Surviving ", cap.library.type, " Reads"), paste0(" number of ", cap.library.type, " Reads remaining after the trimming step"),
    asStrong("%"), paste0(" percentage of surviving ", cap.library.type, " Reads / raw ", cap.library.type, " Reads"),
    separator = ": "
  )

  trimming = addTo(
    trimming,
    trimming.text1,
    trimming.table.min,
    trimming.results.list
  )

  results = addTo(results, trimming)

  # Normalization
  normalization = newSubSection("Normalization")

  normalization.text1 = newParagraph("Normalization has been performed in order to reduce memory requirement and decrease assembly runtime by reducing the number of reads, using the Trinity normalization utility ", asReference(citations$trinityWeb), " inspired by the Diginorm algorithm ", asReference(citations$diginorm), ": first, a catalog of k-mers from all reads is created; then, each RNA-Seq fragment (single read or pair of reads) is probabilistically selected based on its k-mer coverage value and the targeted maximum coverage value. Studies detailed in the ", asEmph("Nature Protocol"), " article ", asReference(citations$trinityProtocol), " found that normalization results in full-length reconstruction to an extent approaching that based on the entire read set.")

  total.normalized.reads.sum = read.table(
    file.path(
      project.path,
      "insilico_read_normalization/all",
      "normalization.stats.tsv"),
    header = FALSE,
    comment.char = "",
    sep = "\t")[1, 2]

  # Update stats summary with normalization stats
  normalization.results.data.frame = data.frame(
    format(total.normalized.reads.sum, big.mark=","),
    round(total.normalized.reads.sum / total.trimmed.reads.sum * 100)
  )

  colnames(normalization.results.data.frame) = c(
    paste0("Surviving ", cap.library.type, " Reads"),
    paste0("%")
  )

  normalization.table = newTable(normalization.results.data.frame,
    "Normalization metrics",
    significantDigits = 3
  )

  normalization.results.list = newParameterList(
    asStrong("Surviving ", cap.library.type, " Reads"), paste0(" number of ", cap.library.type, " Reads remaining after the normalization step"),
    asStrong("%"), paste0(" percentage of surviving ", cap.library.type, " Reads after normalization / surviving ", cap.library.type, " Reads after trimming"),
    separator = ": "
  )

  normalization = addTo(
    normalization,
    normalization.text1,
    normalization.table,
    normalization.results.list
  )

  results = addTo(results, normalization)

  print("normalization DONE")

  # Trinity De Novo Assembly
  assembly = newSubSection("De Novo Assembly")

  # Copy files to be delivered
  file.copy(file.path(project.path, "trinity_out_dir", "Trinity.fasta.zip"), report.path)
  file.copy(file.path(project.path, "trinity_out_dir", "Trinity.stats.pdf"), report.path)
  file.copy(file.path(project.path, "trinity_out_dir", "Trinity.stats.jpg"), report.path)

  assembly.text1 = newParagraph("The transcriptome has been assembled on normalized reads using the Trinity assembler ", asReference(citations$trinity), asReference(citations$trinityWeb), ". Briefly, the assembly process consists of 3 steps:")
  assembly.text2 = newList(
    newParagraph(asStrong("Inchworm"), " assembles the RNA-Seq data into the unique sequences of transcripts, often generating full-length transcripts for a dominant isoform, but then reports just the unique portions of alternatively spliced transcripts."),
    newParagraph(asStrong("Chrysalis"), " clusters the Inchworm contigs into clusters and constructs complete de Bruijn graphs for each cluster. Each cluster represents the full transcriptonal complexity for a given gene (or sets of genes that share sequences in common). Chrysalis then partitions the full read set among these disjoint graphs."),
    newParagraph(asStrong("Butterfly"), " then processes the individual graphs in parallel, tracing the paths that reads and pairs of reads take within the graph, ultimately reporting full-length transcripts for alternatively spliced isoforms, and teasing apart transcripts that corresponds to paralogous genes.")
  )
  assembly.text3 = newParagraph("Trinity has created a Trinity.fasta file with a list of contigs representing the transcriptome isoforms. Those transcripts are grouped in components mostly representing genes. <br />
Transcript names are prefixed by the component name e.g. transcripts ", asCode("c115_g5_i1"), " and ", asCode("c115_g5_i2"), " are derived from the same isolated de Bruijn graph and therefore share the same component number ", asCode("c115_g5"), ".")

  assembly.text4 = newParagraph(asStrong(asLink(url = "Trinity.fasta.zip", "The full Trinity assembly FASTA file is available here.")))

  trinity.stats.path = file.path(project.path, "/trinity_out_dir/Trinity.stats.csv")
  trinity.stats = read.csv(trinity.stats.path, header = FALSE)[-1, ]

  assembly = addTo(
    assembly,
    assembly.text1,
    assembly.text2,
    assembly.text3,
    assembly.text4
  )


  align.data.frame = data.frame(trinity.stats)
  colnames(align.data.frame) = c("Description", "Value")

  write.table(
    align.data.frame,
    file.path(report.path, trim.align.file),
    sep = "\t",
    col.names = TRUE,
    row.names = FALSE,
    quote = FALSE
  )

  assembly = addTo(
    assembly,
    newTable(
      rbind(
        align.data.frame[1:4, ],
        align.data.frame[align.data.frame$Description == "Median Transcript Length (bp)", ],
        align.data.frame[align.data.frame$Description == "Mean Transcript Length (bp)", ],
        align.data.frame[align.data.frame$Description == "Max. Transcript Length (bp)", ],
        align.data.frame[align.data.frame$Description == "N50 (bp)", ]
      ),
      "Assembly metrics",
      file = trim.align.file,
      significantDigits = 3
    )
  )

  assembly = addTo(assembly, newFigure(
    "Transcript Length Distribution",
    file = file.path("Trinity.stats.jpg"),
    fileHighRes = file.path("Trinity.stats.pdf")
  ))

  results = addTo(results, assembly)

  print("trinity DONE")

  # BLAST Annotation
  blast = newSubSection("BLAST Annotation")

  blast.program = 'blastx'

  blast.db = basename(getIniParams(
      'db',
      'blastx_swissprot',
      ini.file.path = ini.file.path,
      project.path = project.path,
      stopifnotfound = FALSE))

  blast.file = paste0(blast.program, "_Trinity_", blast.db, ".tsv.zip")

  # Copy file to be delivered
  file.copy(file.path(project.path, "blast", blast.file), report.path)

  blast.text1 = newParagraph("Each component longest transcript has been aligned against the ", asStrong(blast.db), " protein database using ", asCode(blast.program), " program from the NCBI BLAST family.")
  blast.text2 = newParagraph("For each longest transcript, the best BLAST hit has been used to annotate its associated component in Differential Expression Results below.")
  blast.text3 = newParagraph(asStrong(asLink(url = blast.file, "The full BLAST results file is available here.")))

  blast = addTo(
    blast,
    blast.text1,
    blast.text2,
    blast.text3
  )

  results = addTo(results, blast)

  print("blast DONE")

if (file.exists(file.path(project.path,"DGE")) | file.exists(file.path(project.path,"differential_expression"))) # unelegant way to deal with missing differential expression (some trinity runs are just for assembly)
{

  # Differential Expression
  expression = newSubSection("Differential Expression")

  expression.design = newSubSubSection("Design")
  expression.design.text = newParagraph("
The analysis design, based on discussion with the client, is presented in the following table.
Each line represents a sample, each column an experimental design, each value (0, 1 or 2) the sample group membership:
  ")
  expression.design.list = newParameterList(
    asStrong("0"), " the sample is not a member of any group.",
    asStrong("1"), " the sample is a member of the control group.",
    asStrong("2"), " the sample is a member of the test case group.",
    separator = ": "
  )

  expression.design.table = read.csv(
    getIniParams(
      'design_file',
      'gq_seq_utils_report',
      ini.file.path = ini.file.path,
      project.path = project.path,
      stopifnotfound = FALSE),
    header = TRUE,
    sep = "\t",
    comment.char = "",
    check.names = FALSE
  )

  expression.design.file = "design.tsv"
  write.table(
    expression.design.table,
    file.path(report.path, expression.design.file),
    sep = "\t",
    col.names = TRUE,
    row.names = FALSE,
    quote = FALSE
  )

  expression.design.table.min = newTable(
    expression.design.table[1:min(6, dim(expression.design.table)[1]), 1:min(8, dim(expression.design.table)[2])],
    "Sample names and experimental designs",
    file = expression.design.file
  )

  expression.design = addTo(
    expression.design,
    expression.design.text,
    expression.design.list,
    expression.design.table.min
  )

  expression.gene = newSubSubSection("Differential Gene Analysis Description")
  expression.gene.text1 = newParagraph("
A primary task in RNA-Seq data analysis is the detection of differentially expressed genes. To
achieve this, RNA-Seq read counts can be obtained for each non-overlapping gene. Read counts
are, to good approximation, linearly related to the abundance of the target transcript ",
asReference(citations$dge),
": if reads were independently sampled from a population with given, fixed fractions of genes, read
counts would follow a multinomial distribution, approximated by the Poisson distribution.
Thus, statistical tests can decide whether, for a given gene, an observed difference in read
counts is significantly greater than natural random variation expectation.
")

  expression.gene.text2 = newParagraph("
Gene abundance estimation for each sample has been performed using RSEM (RNA-Seq by Expectation-Maximization)
", asReference(citations$rsem), asReference(citations$rsemWeb), "
. Differential gene expression analysis is performed using DESeq 
", asReference(citations$deseq), " and edgeR ", asReference(citations$edger), "
 R Bioconductor packages.
")

  expression.gene.table = read.csv(
    file.path(
      project.path,
      "differential_expression",
      "genes.counts.matrix"
    ),
    header = TRUE,
    sep = "\t",
    comment.char = "",
    check.names = FALSE
  )

  expression.gene.file = "geneRawCountMatrix.tsv"
  write.table(
    expression.gene.table,
    file.path(report.path, expression.gene.file),
    sep = "\t",
    col.names = TRUE,
    row.names = FALSE,
    quote = FALSE
  )

  expression.gene.table.min = newTable(
    expression.gene.table[1:min(6, dim(expression.gene.table)[1]), 1:min(8, dim(expression.gene.table)[2])],
    "Matrix of raw read counts per gene per sample",
    file = expression.gene.file
  )

  expression.gene = addTo(
    expression.gene,
    expression.gene.text1,
    expression.gene.text2,
    expression.gene.table.min
  )

  expression.results = newSubSubSection("Results")

  expression.results.text1 = newParagraph("
The following sections provide for each design the results of the differential gene expression.
")

  expression.results.list = newParameterList(
    asStrong("id"), " component id",
    asStrong(paste0(blast.db, ".id")), paste0(blast.db, " BLAST protein database id if available, component id otherwise"),
    asStrong("log_FC"), " log2 Fold Change of gene level expression",
    asStrong("log_CPM"), " log2 Counts Per Million of gene level expression",
    asStrong("deseq.p-value"), " DESeq nominal p-value",
    asStrong("deseq.adj.pvalue"), " DESeq False Discovery Rate (FDR) adjusted p-value",
    asStrong("edger.p-value"), " edgeR nominal p-value",
    asStrong("edger.adj.pvalue"), " edgeR False Discovery Rate (FDR) adjusted p-value",
    asStrong("length"), " weighted average of component transcripts lengths",
    asStrong("effective length"), " weighted average of component transcripts effective lengths (mean number of positions from which a fragment may start within the sequence of transcript)",
    asStrong(paste0("description")), paste0(blast.db, " protein description if available"),
    separator = ": "
  )

  expression.results = addTo(
    expression.results,
    expression.results.text1,
    expression.results.list
  )

  for (i in 2:(dim(expression.design.table)[2])) {
    current.design = colnames(expression.design.table)[i]
    expression.results.item = newSubSubSection(paste(current.design, "Results"))

    expression.results.item.table = read.csv(
      file.path(
        project.path,
        "differential_expression",
        "genes",
        current.design,
        "dge_trinotate_results.csv"
      ),
      header = TRUE,
      sep = "\t",
      comment.char = "",
      check.names = FALSE
    )
  
    expression.results.item.file = paste0(
      current.design,
      "_Genes_DE_results_",
      blast.db,
      ".tsv"
    )

    expression.results.item.dir = file.path(
      "DiffExp",
      current.design
    )

    dir.create(
      file.path(report.path, expression.results.item.dir),
      showWarnings = FALSE,
      recursive = TRUE
    )

    write.table(
      expression.results.item.table,
      file.path(report.path, expression.results.item.dir, expression.results.item.file),
      sep = "\t",
      col.names = TRUE,
      row.names = FALSE,
      quote = FALSE
    )

    expression.results.item.table.min = newTable(
      # The following commented line displays length and description columns
      #expression.results.item.table[1:min(6, dim(expression.results.item.table)[1]), -9:-(dim(expression.results.item.table)[2] - 3)],
      expression.results.item.table[1:min(6, dim(expression.results.item.table)[1]), 1:8],
      "Differential Gene Expression Results",
      file = file.path(expression.results.item.dir, expression.results.item.file)
    )

    expression.results.item = addTo(
      expression.results.item,
      expression.results.item.table.min
    )

    expression.results = addTo(
      expression.results,
      expression.results.item
    )
  }

  expression = addTo(
    expression,
    expression.design,
    expression.gene,
    expression.results
  )

  results = addTo(results, expression)

  print("differential expression DONE")
}

  #
  # References
  #
  references = newSection("References")
  references = addTo(
    references,
    citations$trinityProtocol,
    citations$trinity,
    citations$trinityWeb,
    citations$trimmomatic,
    citations$diginorm,
    citations$rsem,
    citations$rsemWeb,
    citations$dge,
    citations$deseq,
    citations$edger
  )

  # Build report
  report = addTo(
    report,
    introduction,
    results,
    references
  )
  return(report)
}
