

#' blah documentation here
#'
#' # Here is a function related to reporting the bitbucket DNAseq pipeline1.0 with Nozzle
#' @export
mugqicPipelineReportDNAseq <- function(report,project.path,ini.file.path,report.path)
{
	###overview
	intro = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)
	intro.text1=newParagraph("This document contains the description of the current MUGQIC DNA-Seq \
			analysis. The information presented here reflects the current state of the analysis \
			as of ", format(Sys.time(),format="%Y-%m-%d"))
	
	trim.table.tmp=try(read.csv(file.path(project.path,"metrics","trimming.stats"),comment.char = "",header=F,sep="\t", colClasses=c("character","character","numeric","numeric","numeric"),stringsAsFactors=F))
	if (!inherits(trim.table.tmp, "try-error")){
    minLibSize=as.character(floor(min(trim.table.tmp[,3])/1000000))
    maxLibSize=as.character(ceiling(max(trim.table.tmp[,3])/1000000))
  }else{
    minLibSize=50
    maxLibSize=150
  }
  libType=getIniParams('library_type', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  protocol=getIniParams('experiment_type', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  intro.text2=newParagraph("The DNA sequencing performed at the Innovation center generated between ",minLibSize," to \
        ",maxLibSize," million ",tolower(libType)," reads per library using the illumina Hiseq 2000/2500 sequencer. The protocol used for preparing libraries is the " ,asStrong(paste(protocol,"",sep=" ")), "protocol. Base calls are made using the Illumina CASAVA pipeline. Base quality \
        is encoded in phred 33.")

	citeRef=getCitations()
	stepRes = newSection("Analysis and results", exportId="AnaRes", protection = PROTECTION.PUBLIC)
  print("Intro section ... Done")
	##Generate triming section -- could be push in the wrapper
	trimNalign=newSubSubSection("Trimming and Alignment analysis",exportId="trimNalign",protection = PROTECTION.PUBLIC)
	trim    = newSubSubSection("Step 1: Read trimming and clipping of adapters", exportId="trim",protection = PROTECTION.PUBLIC)
	minLength=getIniParams('min_length', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	minQuality=getIniParams('trailing_min_quality', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	trim.text1 = newParagraph("Reads are trimmed from the 3' end to have a phred score of at least ",ifelse(minQuality !="", minQuality,30),
				". Illumina sequencing adapters are removed from the reads, and all reads are required to have a length of at least " ,ifelse(minLength !="", minLength,50), " bp. Trimming and clipping are done\
				with the Trimmomatic software",asReference(citeRef$trimmomatic)," .", exportId="trim.text1",protection = PROTECTION.PUBLIC)
	### create a fake table to only report colnames of the real table in the file
	if (!inherits(trim.table.tmp, "try-error")){	
    sampleUniq=sort(unique(as.vector(trim.table.tmp[,1])))
    trim.table.readset=cbind(trim.table.tmp[,1:4],(trim.table.tmp[,4]/trim.table.tmp[,3])*100)
    colnames(trim.table.readset)=c("Sample","Readset","Raw read #","Surviving read #","Surviving read %")
    trim.col1=NULL
    trim.col2=NULL
    trim.col3=NULL
    for (i in 1:length(sampleUniq)) {
      trim.col1=c(trim.col1,sampleUniq[i])
      trim.col2=c(trim.col2,sum(trim.table.tmp[trim.table.tmp[,1] %in% sampleUniq[i],3]))
      trim.col3=c(trim.col3,sum(trim.table.tmp[trim.table.tmp[,1] %in% sampleUniq[i],4]))
    }
    trim.table=data.frame(trim.col1,trim.col2,trim.col3,(trim.col3/trim.col2)*100)
    colnames(trim.table)=c("Sample","Raw read #","Surviving read #","Surviving read %")
    #write.table(trim.table,file.path(report.path,trim.file),sep="\t",col.names=T,row.names=F,quote=F)
    trimReadset.file="trimReadsetTable.tsv"
    write.table(trim.table.readset,file.path(report.path,trimReadset.file),sep="\t",col.names=T,row.names=F,quote=F)
    trim.stats.table.readset = newTable(trim.table.readset, "By readset trimming statistics" , file = trimReadset.file, exportId="trim.stats.readset",protection = PROTECTION.PUBLIC, significantDigits = 2)
    trimStats.param=newList(newParagraph("Raw reads: the total number of read obtained from the sequencer"),
              newParagraph("Surviving reads: the number of remaining reads after the trimming step"),
              newParagraph("%: Surviving reads / Raw reads"),
              isNumbered = FALSE, exportId = "trim.param",protection = PROTECTION.PUBLIC)
    trim = addTo(trim,trim.text1,trim.stats.table.readset,trimStats.param)
	}else{
    trim.text2 = newParagraph("The trimming step was skipped for this analysis. Unable to find ", file.path(project.path,"metrics","trimming.stats") ,exportId="trim.text2",protection = PROTECTION.PUBLIC)
    trim = addTo(trim,trim.text2)         
	}
  print("trim section ... Done")
	##Generate Alignment section
	align   = newSubSubSection("Step 2: Aligning the reads to the reference sequence", exportId="align",protection = PROTECTION.PUBLIC)

###TODO pick up directly the genome in ini and add specific genome information
	hg1kREf.text="The 1000 genome (b37) reference is currently used for human samples. The 1000 genome reference \
	is similar to the hg19 reference, except that it uses the newer mitochondrial build (NC_012920). Also \
	chromosome nomenclature is slightly different ('1' vs 'chr1', non-chromosomal supercontigs are named differently e.g. GL000207)."
	iniGref=paste(
        getIniParams('scientific_name', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),
        "assembly ",
        getIniParams('assembly', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
    )
	if (iniGref == "b37") {
		genomeRef=hg1kREf.text
	} else {
		genomeRef=paste("The genome used in the analysis is",iniGref)
	}
	align.text1 = newParagraph("The filtered reads are aligned to a reference genome. ", genomeRef, ". The \
				alignment is done by readset of sequencing using the bwa software",asReference(citeRef$bwa)," . It generates \
				a Binary Alignment Map file (.bam). All BAM files from different readsets of a the same sample are merged into \
        a single global BAM file using  the picard software",asReference(citeRef$picard)," .", exportId="align.text1",protection = PROTECTION.PUBLIC)
	align.text2 = newParagraph("Bwa is a fast light-weighted tool that aligns relatively short sequences (queries) to a sequence database \
		(target), such as the human reference genome. It's based on Burrows-Wheeler Transform (BWT). BWA is designed for short queries up \
		to ~200bp with low error rate (< 3%). It does gapped global alignment, supports paired-end reads, and is one of the fastest short \
		read alignment algorithms to date while also visiting suboptimal hits.", exportId="align.text2",protection = PROTECTION.PUBLIC)
		### create a fake table to only report colnames of the real table in the file
	align = addTo(align, align.text1, align.text2)
  print("Align section ... Done")
	### tmp test file need to be uniformly created
	improvement = newSubSection("Step 3: Improvement of the read alignment", exportId="improvement",protection = PROTECTION.PUBLIC)
	improv.text = newParagraph("Unfortunately there is no prefect aligner to date. The alignment step is really sensitive to the aligner \
		parameters as well as technical and biological variations. To help increase the quality and speed of subsequent variant calling, we performed a set \
		of alignment improvement procedures. These procedures consist to realign the surrounding short insertion or deletion, fixing poosible read \
		mate discrepency due to realignment, marking duplicated reads and recalibrating base quality of reads.", exportId="improv.text",protection = PROTECTION.PUBLIC)    
	realign   = newSubSubSection("Step 3 - a: Realigning short insertions and deletions (INDELs)", exportId="realign",protection = PROTECTION.PUBLIC)
	realign.text1 = newParagraph("INDELs in reads (especially near the ends) can trick the mappers into mis-aligning with mismatches. \
		These artifacts resulting from mismatches can harm base quality recalibration and variant detection. Realignment around INDELs helps \
		improve the accuracy of several of the downstream processing", exportId="realign.text1",protection = PROTECTION.PUBLIC)
	realign.text2 = newParagraph("Insertion and deletion realignment is performed on regions where multiple base mismatches \
		are preferred over INDELs by the aligner since it appears to be less costly for the algorithm. Such regions will introduce \
		false positive variant calls which may be filtered out by realigning those regions properly. Mainly realignment will occurs in 3 different region types: ", exportId="realign.text2",protection = PROTECTION.PUBLIC)
	  realignList=newList(newParagraph("Known sites (e.g. dbSNP, 1000 Genomes)"),
		newParagraph("INDELs seen in original alignments (in CIGARs)"),
		newParagraph("Sites where evidence suggests a hidden INDEL"),isNumbered = TRUE, exportId = "realignList",protection = PROTECTION.PUBLIC)
	realign.text3 = newParagraph("Realignment is done using the GATK software",asReference(citeRef$gatk)," .", exportId = "realign.text3",protection = PROTECTION.PUBLIC)
	realign=addTo(realign,realign.text1, realign.text2, realignList, realign.text3)
	fixMate = newSubSubSection("Step 3 - b: Fixing the read mates", exportId="fixMate",protection = PROTECTION.PUBLIC)
	fixMate.text = newParagraph("Once local regions are realigned, the read mate coordinates of the aligned reads need to be \ 
		recalculated since the reads are realigned at positions that differ from their original alignment. Fixing the read \
		mate positions is done with picard software",asReference(citeRef$picard)," .", exportId="fixMate.text",protection = PROTECTION.PUBLIC)
	fixMate =addTo(fixMate,fixMate.text)
	markDup = newSubSubSection("Step 3 - c: Marking duplicates" , exportId="markDup",protection = PROTECTION.PUBLIC)
	markDup.text = newParagraph("Sequencing experiment includes several PCR amplification steps, which could induce coverage bias \
		during the library construction. If this bias is not taken into account it could generate high level of false positives and \
		false negatives during the variant calling steps (SNV, SV and CNV). Thus removing or marking read duplicates is an \
		important step of the sequencing analysis. Aligned reads are duplicates if they have the same 5' alignment positions (for both mates in the \
		case of paired-end reads). All but the best pair (based on alignment score) will be marked as a duplicate in the .bam \
		file. Duplicates reads will be excluded in the subsequent analysis. Marking duplicates is done with picard software",asReference(citeRef$picard)," .", exportId="markDup.text",protection = PROTECTION.PUBLIC)
	markDup= addTo(markDup, markDup.text)
	recalibration = newSubSubSection("Step 3 - d: Base quality recalibration" , exportId="recalibration",protection = PROTECTION.PUBLIC)
	recalibration.text1 = newParagraph("This step allows recalibrating base quality scores of sequencing-by-synthesis reads in an aligned BAM file. After recalibration, the \
		quality scores in the QUAL field in each read in the output BAM are more accurate in that the reported quality score is closer to its actual probability of \
		mismatching the reference genome. Moreover, the recalibration tool attempts to correct for variation in quality with machine cycle and sequence context, and \
		by doing so provides not only more accurate quality scores but also more widely dispersed ones. The recalibration is done not only for the well-known base \
		quality scores but also for base insertion and base deletion quality scores. These are per-base quantities which estimate the probability that the next base \
		in the read was mis-incorporated or mis-deleted (due to slippage, for example).", exportId="recalibration.text1",protection = PROTECTION.PUBLIC)
	recalibration.text2 = newParagraph("The recalibration process is accomplished by analyzing the covariation among several features of a base. For example:", exportId="recalibration.text2",protection = PROTECTION.PUBLIC)
	recalibration.cov=newList(newParagraph("Reported quality score"),
		newParagraph("The position within the read"),
		newParagraph("The preceding and current nucleotide (sequencing chemistry effect) observed by the sequencing machine"),isNumbered = FALSE, exportId = "recalibration.cov",protection = PROTECTION.PUBLIC)
	recalibration.text3 = newParagraph("These covariates are then subsequently applied through a piecewise tabular correction to recalibrate the quality scores of all \
		reads in a BAM file. The recalibration process is done with the GATK software",asReference(citeRef$gatk),".", exportId="recalibration.text3",protection = PROTECTION.PUBLIC)
	recalibration =addTo(recalibration, recalibration.text1, recalibration.text2, recalibration.cov, recalibration.text3)
	improvement = addTo(improvement, improv.text, realign, fixMate, markDup, recalibration)
  print("Improvement section ... Done")
  
  ### Alignment metrics
  
	metricsAlignment = newSubSection("Step 4: By sample sequence and alignment metrics", exportId="metricsSequencing",protection = PROTECTION.PUBLIC)
	metricsAlignment.text1 = newParagraph("General summary statistics are provided for each sample. Sample readsets were merged for clarity.")
	
	metricsAlignment.table=try(read.csv(file.path(project.path,"metrics","SampleMetrics.stats"),header=T,sep="\t",comment.char = "", check.names=F))
	if (!inherits(trim.table.tmp, "try-error") && !inherits(metricsAlignment.table,"try-error")){  
    tNAOrder=match(trim.table[,1],as.vector(metricsAlignment.table$name)) #JT:$notSampleName but $name
    colAS=c(4,6,8)
    if (sum(!is.na(metricsAlignment.table[,9:15,drop=F]))) {
      colAS=c(colAS,9:15)
    }
    colAS=c(colAS,16:22)
    
    sequence.alignment.table=data.frame(
      ( trim.table[,1,drop=F]),
      ( trim.table[,2,drop=F]*2),
      ( trim.table[,3,drop=F]*2), 
      ( trim.table[,3,drop=F] / trim.table[,2,drop=F]*100) ,
      ( metricsAlignment.table[tNAOrder,2,drop=F] ),
      ( ( as.vector(metricsAlignment.table[tNAOrder,2,drop=F])) / (trim.table[,3,drop=F]*2) )*100, 
      ( as.vector( metricsAlignment.table[tNAOrder,2,drop=F]) ) - (as.vector(metricsAlignment.table[tNAOrder,3,drop=F])),
      ( as.vector( metricsAlignment.table[tNAOrder,3,drop=F])),
      ( as.vector( metricsAlignment.table[tNAOrder,3,drop=F]) / (trim.table[,3,drop=F]*2) )*100 ,
      ( metricsAlignment.table[tNAOrder,colAS,drop=F] )
    )

    sequence.alignment.table = as.data.frame(sequence.alignment.table)

    colnames(sequence.alignment.table)=c("Sample", "Raw reads", "Surviving reads", "% ", "Mapped reads", "%", "Not Duplicate", "Duplicate",
                                          "%", "Pair Orientation", "Mean Insert Size", "Standard Deviation", 
      if (sum(!is.na(metricsAlignment.table[,9:15]))) {
        c("WG Mean Coverage", "WG %_bases_above_10", "WG %_bases_above_25", "WG %_bases_above_50", "WG %_bases_above_75", "WG %_bases_above_100", "WG %_bases_above_500")
      }, 
      "CCDS Mean Coverage", "CCDS %_bases_above_10", "CCDS %_bases_above_25", "CCDS %_bases_above_50", "CCDS %_bases_above_75", "CCDS %_bases_above_100", "CCDS %_bases_above_500"
    )

    sequence.alignment.param=NULL
    if (sum(!is.na(metricsAlignment.table[,9:15])) > 0  ) {
      sequence.alignment.param=newList(
        newParagraph("Raw reads: the total number of read obtained from the sequencer"),
        newParagraph("Surviving reads: the number of remaining reads after the trimming step"),
        newParagraph("%: Surviving reads / Raw reads"),
        newParagraph("Mapped reads: the number of read aligned"),
        newParagraph("%: Mapped reads / Surviving reads"),
        newParagraph("Not Duplicate: the number of not duplicated read entries"),
        newParagraph("Duplicate: the number of duplicate read entries providing alternative coordinates"),
        newParagraph("%: Duplicate / Mapped reads"),
        newParagraph("Pair Orientation: the library pared-end read design"),
        newParagraph("Mean Insert Size: the mean distance between the left most base position of the read1 and the right most base position of the read 2"),
        newParagraph("Standard Deviation: the standard deviation of distance between the left most base position of the read1 and the right most base position of the read 2"),
        newParagraph("WG Mean Coverage: total number of aligned read / size of the genome"),
        newParagraph("WG %_bases_above_10: total number of bases with a coverage >= 10x / size of the genome"),
        newParagraph("WG %_bases_above_25: total number of bases with a coverage >= 25x / size of the genome"),
        newParagraph("WG %_bases_above_50: total number of bases with a coverage >= 50x / size of the genome"),
        newParagraph("WG %_bases_above_75: total number of bases with a coverage >= 75x / size of the genome"),
        newParagraph("WG %_bases_above_100: total number of bases with a coverage >= 100x / size of the genome"),
        newParagraph("WG %_bases_above_500: total number of bases with a coverage >= 500x / size of the genome"),
        newParagraph("CCDS Mean Coverage: total number of aligned read in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_10: total number of bases with a coverage >= 10x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_25: total number of bases with a coverage >= 25x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_50: total number of bases with a coverage >= 50x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_75: total number of bases with a coverage >= 75x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_100: total number of bases with a coverage >= 100x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_500: total number of bases with a coverage >= 500x in the CCDS/capture region / size of the CCDS/capture region"),
        isNumbered = FALSE, 
        exportId = "sequence.alignment.param", 
        protection = PROTECTION.PUBLIC
      )	
    }else{
      sequence.alignment.param=newList(
        newParagraph("Raw reads: the total number of read obtained from the sequencer"),
        newParagraph("Surviving reads: the number of remaining reads after the trimming step"),
        newParagraph("%: Surviving reads / Raw reads"),
        newParagraph("Mapped reads: the number of read aligned"),
        newParagraph("%: Mapped reads / Surviving reads"),
        newParagraph("Not Duplicate: the number of not duplicated read entries"),
        newParagraph("Duplicate: the number of duplicate read entries providing alternative coordinates"),
        newParagraph("%: Duplicate / Mapped reads"),
        newParagraph("Pair Orientation: the library pared-end read design"),
        newParagraph("Mean Insert Size: the mean distance between the left most base position of the read1 and the right most base position of the read 2"),
        newParagraph("Standard Deviation: the standard deviation of distance between the left most base position of the read1 and the right most base position of the read 2"),
        newParagraph("CCDS Mean Coverage: total number of aligned read in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_10: total number of bases with a coverage >= 10x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_25: total number of bases with a coverage >= 25x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_50: total number of bases with a coverage >= 50x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_75: total number of bases with a coverage >= 75x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_100: total number of bases with a coverage >= 100x in the CCDS/capture region / size of the CCDS/capture region"),
        newParagraph("CCDS %_bases_above_500: total number of bases with a coverage >= 500x in the CCDS/capture region / size of the CCDS/capture region"),
        isNumbered = FALSE, 
        exportId = "sequence.alignment.param", 
        protection = PROTECTION.PUBLIC
      )
    }

    sequence.alignment.file="sequenceAlignmentTable.tsv"
    write.table(sequence.alignment.table,file.path(report.path,sequence.alignment.file),sep="\t",col.names=T,row.names=F,quote=F)
    sequence.alignment.Sampletable = newTable(sequence.alignment.table[1:min(6,dim(sequence.alignment.table)[1]),1:min(10,dim(sequence.alignment.table)[2])], "By sample sequencing and alignment statistics" , file = sequence.alignment.file, exportId="align.stats",protection = PROTECTION.PUBLIC, significantDigits = 2)
    metricsAlignment=addTo(metricsAlignment,metricsAlignment.text1,sequence.alignment.Sampletable,sequence.alignment.param)
  }else{
    metricsAlignment.text1 = newParagraph("The alignment step or the steps associated to alignment statistics were skipped for this analysis. Unable to find ", file.path(project.path,"metrics","trimming.stats") , " or ", file.path(project.path,"metrics","SampleMetrics.stats"), exportId="trim.text2",protection = PROTECTION.PUBLIC)
    metricsAlignment=addTo(metricsAlignment,metricsAlignment.text1)
  }
  print("metrics alignment section ... Done")
  
  ### Step 5: Single nucleotide variations (SNV)
	snv = newSubSection("Step 5: Single nucleotide variations (SNV)", exportId="snv",protection = PROTECTION.PUBLIC)
	snv.text = newParagraph("SNV calling is a very important process in the DNAseq anlysis. SNV calling process allows identifying the specificty of each \
		sample individually when compared to the reference, but is is also important to know if the individual variants are already reported in other \
		sample form the same study and in public dabatases. To provide the most complete variant information the SNV calling process calls \
		individual variants, but also annotate calls on public variant databases by merging the individual calls into a global cohort variant report.", exportId="snv.test",protection = PROTECTION.PUBLIC)
	snvCall = newSubSubSection("Step 5 - a: SNV calling", exportId="snvCall",protection = PROTECTION.PUBLIC)
	snvCall.text1= newParagraph("Single Nucleotide Variants (SNPs and INDELs) are called using samtools",asReference(citeRef$samtools)," and \
		bcftools",asReference(citeRef$bcftools),". samtools (mpileup) collects summary \
		information in the input BAMs, computes the likelihood of data given each possible genotype and stores the likelihoods in the BCF format. It \
		does not call variants. Bcftools applies the prior and does the actual calling. Every time a mapped read shows a mismatch from the reference \
		genome, it applies Bayesian statistics to figure out whether the mismatch is caused by a real SNP. It incorporates different types of \
		information, such as the number of different reads that share a mismatch from the reference, the sequence quality data, and the expected \
		sequencing error rates, and it essentially figures out whether is more likely that the observed mismatch is due simply to a sequencing error, \
		or because of a true SNV.", exportId="snvCall.text1",protection = PROTECTION.PUBLIC)
	snvCall.text2 = newParagraph("The SNV calls are reprocessed by bcftools (varfilter), which does an additional filtering of the variants and \
		transforms the output into the VCF format. The final vcf files are filtered for long 'N' INDELs which are sometimes introduced and \
		causing excessive memory usage in downstream analysis.", exportId="snvCall.text2",protection = PROTECTION.PUBLIC)
	snvCall = addTo(snvCall,snvCall.text1,snvCall.text2)
	snvAnnot = newSubSubSection("Step 5 - b: SNV annotation", exportId="snvAnnot",protection = PROTECTION.PUBLIC)
	snvAnnot.text1 =newParagraph("An in-house database identifies regions in which reads are confidently mapped to the reference genome. Generally, \
		low mappability corresponds nicely to RepeatMasker regions and decreases substantially with read length increase.  A region is identified \
		as HC = coverage too high, LC = low coverage, MQ = to low mean mapQ (<20) and ND = unmapable region (no data) at the position." , exportId="snvAnnot.text1",protection = PROTECTION.PUBLIC)
	snvAnnot.text2 =newParagraph("The vcf files are also annotated for:", exportId="snvAnnot.text1",protection = PROTECTION.PUBLIC)
	if (iniGref == "b37" | iniGref == "hg19") {
		snvAnno.db=newList(newParagraph("dbSNP using the software SnpSift"),
			newParagraph("variant effects (predicted variant effects on gene, such as amino acid changes) using the SnpEff software."),
			newParagraph("dbNSFP: an integrated database of functional annotations from multiple sources for the comprehensive collection \
				of human non-synonymous SNPs. It compiles prediction scores from four prediction algorithms (SIFT, Polyphen2, LRT and \
				MutationTaster), three conservation scores (PhyloP, GERP++ and SiPhy) and other function annotations."),
			newParagraph("Cosmic (Catalogue of Somatic Mutations in Cancer): Annotates SVNs which are known somatic mutations"),isNumbered = TRUE, exportId = "snvAnno.db",protection = PROTECTION.PUBLIC)
	} else {
		snvAnno.db=newList(newParagraph("dbSNP using the software SnpSift"),
			newParagraph("variant effects (predicted variant effects on gene, such as amino acid changes) using the SnpEff software."),
			isNumbered = TRUE, exportId = "snvAnno.db",protection = PROTECTION.PUBLIC)
	}
	snvAnnot = addTo(snvAnnot, snvAnnot.text1, snvAnnot.text2, snvAnno.db)
	snvMerge = newSubSubSection("Step 5 - c: SNV merge", exportId="snvMerge",protection = PROTECTION.PUBLIC)
	vcfDescriptor=getIniParams('species_vcf_format_descriptor', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	if (!file.exists(file.path(project.path,"variants","allSamples.merged.flt.vcf"))){
    snvCall.text1 = newParagraph("SNV calling step was skipped. Unable to find ",file.path(project.path,"variants","allSamples.merged.flt.vcf") ,exportId="snvCall.text1",protection = PROTECTION.PUBLIC)
    snvCall= addTo(snvCall, snvCall.text1)
	}
	vcfDescriptor.table.tmp=try(read.csv(vcfDescriptor,sep="\t", header=T,comment.char = ""))
	if( !inherits(vcfDescriptor.table.tmp, "try-error")){
    vcfDescriptor.file="vcfFormatDescriptor.tsv"
    write.table(vcfDescriptor.table.tmp,file.path(report.path,vcfDescriptor.file),sep="\t",col.names=T,row.names=F,quote=F)
    vcfDescriptor.table = newTable(vcfDescriptor.table.tmp, "Specie specific global vcf columns description" , file = vcfDescriptor.file, exportId="vcfDescriptor.table",protection = PROTECTION.PUBLIC, significantDigits = 2)
    snvMerge.text = newParagraph("All sample from the cohort are merged together into one global vcf file.",exportId="snvMerge.text",protection = PROTECTION.PUBLIC)
    snvMerge= addTo(snvMerge, snvMerge.text, vcfDescriptor.table)
    snv = addTo(snv, snvCall, snvAnnot, snvMerge)
	}else {
	
	  snvMerge.text = newParagraph("VCF format descriptor for the species was not found",exportId="snvMerge.text",protection = PROTECTION.PUBLIC)
	  snvMerge= addTo(snvMerge, snvMerge.text)
	  snv = addTo(snv, snvCall, snvAnnot, snvMerge)
	}
print("SNV section ... Done")
### SV and CNV section will be inculde in the next version
# 	sv = newSubSection("Step 6: Structural variations (SV)", exportId="sv",protection = PROTECTION.PUBLIC)
# 	sv.text1 = newParagraph("Comparisons of models genomes show that more base pairs are altered as a result of structural variation  ? \
# 		including copy number variation ? than as a result of point mutations. Structural variations ranges from the single base pair \
# 		to large chromosomal events, thus creating more diversity in the genome than a single-base-pair changes." ,exportId="sv.text1",protection = PROTECTION.PUBLIC)
# 	sv.text2 = newParagraph("we can define 3 types of structural variations :" ,exportId="sv.text2",protection = PROTECTION.PUBLIC)
# 	sv.text3 = newParagraph("Indel - Short insertion or deletion events < 50 bp, this type of event is more in between single nucleotid variation \
# 		and structural varaiation. ", asStrong("In this project, their analysis/detection is done during the SNV analysis !"),exportId="sv.text3",protection = PROTECTION.PUBLIC)
# 	sv.text4 = newParagraph("Structural variants (SV) - which are composed of:",exportId="sv.text4",protection = PROTECTION.PUBLIC)
# 	sv.text4.list=newList(newParagraph("Large insertion"),newParagraph("Tranmsposable Element insertion"),newParagraph("Inversion"),isNumbered = FALSE, exportId = "sv.text4.list",protection = PROTECTION.PUBLIC)
# 	sv.text5 = newParagraph("Copy number variations (CNV) - which are composed of:" ,exportId="sv.text5",protection = PROTECTION.PUBLIC)
# 	sv.text5.list=newList(newParagraph("Large deletion"),newParagraph("Interspersed duplication"),newParagraph("Tandem duplication"),isNumbered = FALSE, exportId = "sv.text5.list",protection = PROTECTION.PUBLIC)
# 	sv=addTo(sv,sv.text1,sv.text2,sv.text3,sv.text4,sv.text4.list,sv.text5,sv.text5.list)
# 	svCall = newSubSubSection("Step 6 - a: SV calling", exportId="svCall",protection = PROTECTION.PUBLIC)
# 	svCall.text1=newParagraph("BreakDancer",asReference(citeRef$breakdancer)," and Pindel",asReference(citeRef$pindel)," were used in parallel as \
# 		orthogonal techniques to identify discordant reads from Illumina paired-end sequencing data. Note that breakdancer insertion events \
# 		are automatically removed from the list of structural variant. The small insert-size selection of the library generates numerous \
# 		read-pairs with overlapping mates. This technical artifact does not allow making confident insertion calls based on the insert size distance.",exportId="svCall.text1",protection = PROTECTION.PUBLIC)
# 	svCall.text2=newParagraph("BreakDancer detects cluster of reads that shows an abnormal insert size length or abnormal ends orientations. Short \
# 		insert size or associated to insertion events whereas longer insert size are evidence of deletion or translocation. Inversion will be \
# 		detected by cluster with inverse end orientation whereas the duplication where found by in case of a larger insert-size and an abnormal \
# 		end orientation. The following parameters are used to filter for low quality variants which could introduce false positive calls:",exportId="svCall.text2",protection = PROTECTION.PUBLIC) 
# 	svCall.text2.list=newList(newParagraph("minimum mapping quality 35"),newParagraph("read-pairs within +/- 3 standard deviations of insert size were excluded"),isNumbered = TRUE, exportId = "svCall.text2.list",protection = PROTECTION.PUBLIC) 
# 	svCall.text3=newParagraph("Pindel will look at One End Anchored (OEA) reads. Theses reads correspond to those that have one end correctly mapped \
# 		and the other ones are unmapped. Based on the anchor, the mapped read, and the mean insert size of the library the software will estimate \
# 		the candidate region where the unmapped read is expected to be found. Pindel will split the unmapped end of the OEA read in few pieces \
# 		and perform local realignment of each piece in the candidate region. Each type of structural variant is associated to a particular pattern \
# 		of alignment of its unmapped reads. A structural variant is called when evidence of a structural variant is found at each side of the \
# 		event.Pindel was run according to the following parameters:",exportId="svCall.text3",protection = PROTECTION.PUBLIC) 
# 	svCall.text3.list=newList(newParagraph("minimum match around breakpoint 10bp"),newParagraph("minimum match to reference 50bp"),newParagraph("minimum read mismatch rate 0.1"),isNumbered = TRUE, exportId = "svCall.text3.list",protection = PROTECTION.PUBLIC) 
# 	svCall=addTo(svCall,svCall.text1,svCall.text2,svCall.text2.list,svCall.text3,svCall.text3.list)
# 	cnvCall = newSubSubSection("Step 6 - b: CNV calling", exportId="cnvCall",protection = PROTECTION.PUBLIC)
# 	cnvCall.text1=newParagraph("Copy number variations are detected using the DNACRD approach (manuscript in preparation), which consists of an \
# 		extension of the CGH array algorithm, DNAcopy",asReference(citeRef$dnacopy),", adapted to whole genome sequencing data. The rational of this approach is to cut the \
# 		genome into bins of a given size and to estimate in each the depth of coverage of each bin. Each measure is normalized and is \
# 		compared to the mean coverage value in order to provide a log2 ratio for each bin. The segmentation algorithm of DNAcopy is then \
# 		used to determine consecutive bins that show a significant deviation from the rest of the genome. The strength of the DNACRD approach \
# 		is to provide a very clear signal of CNV whereas most other methods provide noisy signals with high false positive rate. The clarity \
# 		of the signal is obtained by filtering out all bins with more than XX percent of unmappable bases, by apllying a median-based GC \
# 		correction approach and finally by using a specific outlier detection model in order to find the most appropriate thresholds for a \
# 		individual-specific CNV detection.", exportId="cnvCall.text1",protection = PROTECTION.PUBLIC)
# 	cnvCall.text2=newParagraph("We ran DNACRD in a 2 ways approach: firstly, we used a bin size of 30kb with a whole genome coverage \
# 		normalization  in order to obtain the global picture of CNV in the cohort. Secondly, smaller local event in candidate gene \
# 		regions are detected by applying DNACRD with a bin size of 500 bp and applying a chormosme normalization between tumor and \
# 		matched control.", exportId="cnvCall.text2",protection = PROTECTION.PUBLIC)
# 	cnvCall=addTo(cnvCall,cnvCall.text1,cnvCall.text2)
# 	svFilter = newSubSubSection("Step 6 - c: SV and CNV filtering", exportId="svFilter",protection = PROTECTION.PUBLIC)
# 	svFilter.text1 = newParagraph("Structural variant are filtered based on the number of read that supports the call. A call is considered \
# 		as a high quality event if at least 10 reads show an evidence of the event. Copy number variation are filtered based on the \
# 		number of consecutive bins that support the call and based on the deviation of the ratio between the coverage observed in \
# 		a bin and the expected deep of coverage expected under the 2 copies hypothesis. A call is considered as a high quality event \
# 		if at least 5 consecutive bin show a similar evidence of deletions or of duplications.", exportId="svFilter.text1",protection = PROTECTION.PUBLIC)
# 	svFilter.text2 = newParagraph("An in-house database identifies regions in which reads are confidently mapped to the reference genome. \
# 		Generally, Hypo- or hyper-mappable region corresponds to false positive copy number and structural variant calls. We \
# 		filter-out all the variants that were found to overlap for 90% or more of their size with mappability flagged region or with a microsatellite region.", exportId="svFilter.text2",protection = PROTECTION.PUBLIC)
# 	svFilter.text3 = newParagraph("The SV/CNV files are also annotated for:", exportId="svFilter.text3",protection = PROTECTION.PUBLIC)
# 	if (iniGref == "b37" | iniGref == "hg19") {
# 		svFilter.db=newList(newParagraph("overlapped repeat masked regions"),
# 			newParagraph("overlapped genes"),
# 			newParagraph("disrupted genes"),
# 			newParagraph("surrounding genes (? 1kb)"),
# 			newParagraph("Database of Genomic Variants (DGV) corresponding hits"),isNumbered = TRUE, exportId = "svFilter.db",protection = PROTECTION.PUBLIC)
# 	} else {
# 		svFilter.db=newList(newParagraph("overlapped repeat masked regions (if available)"),
# 			newParagraph("overlapped genes (if available)"),
# 			newParagraph("disrupted genes (if available)"),
# 			newParagraph("surrounding genes (? 1kb) (if available)"),isNumbered = TRUE, exportId = "svFilter.db",protection = PROTECTION.PUBLIC)
# 	}
# 	svFilter =addTo(svFilter, svFilter.text1,svFilter.text2,svFilter.text3,svFilter.db)
# 	sv = addTo(sv, svCall, cnvCall, svFilter)
	
	metricsVariant = newSubSection("Step 6: Variants metrics", exportId="metricsVariant",protection = PROTECTION.PUBLIC)
	metricsVariant.text1=newParagraph("The variant metrics and statistics have been generated using the SnpEff software",asReference(citeRef$snpeff)," .")
	metricsVariant=addTo(metricsVariant,metricsVariant.text1)
	summaryMV = newSubSubSection("Step 6 - a: summary SNV metrics ", exportId="summaryMV",protection = PROTECTION.PUBLIC)
	summaryMV.text1 = newParagraph("General summary statistics are provided for the entire set of variant without considering the sample information.")
	smv.table=try(t(read.csv(file.path(project.path,"metrics","allSamples.SNV.SummaryTable.tsv"),header=T,sep="\t",comment.char = "", check.names=F)))
	if(!inherits(smv.table,"try-error")){
    rownames(smv.table)=gsub("_"," ",rownames(smv.table))
    effect.table1=t(read.csv(file.path(project.path,"metrics","allSamples.SNV.EffectsFunctionalClass.tsv"),header=T,sep="\t",comment.char = "", check.names=F))
    rownames(effect.table1)=tolower(gsub("_"," ",rownames(effect.table1)))
    effect.table2=t(read.csv(file.path(project.path,"metrics","allSamples.SNV.EffectsImpact.tsv"),header=T,sep="\t",comment.char = "", check.names=F,skip=1))
    rownames(effect.table2)=paste(tolower(gsub("_"," ",rownames(effect.table2))),"impact")
    smv.table.tmp=round(rbind(smv.table,effect.table1,effect.table2),2)
    smv.table2=cbind(rownames(smv.table.tmp),smv.table.tmp)
    colnames(smv.table2)=c("Metrics","Value")
    smv.param=newList(newParagraph("Number of variants before filter: the total number of variant in the cohort. please not that when mesuring the effect counts each snp coulb be evaluated several times depending on the number of transcripts affected by the SNP."),
              newParagraph("Number of variants filtered out: the number of filtered variant (MAPQ < 15)"),
              newParagraph("%: Number of variants filtered out / Number of variants before filter"),
              newParagraph("Number of not variants: the number of  variant where the reference allele is the same than then aternate allele"),
              newParagraph("%: Number of not variants / Number of variants before filter"),
              newParagraph("Number of variants processed: the number of analysed sfter removing the filter and not variant ones"),
              newParagraph("Number of known variants: number of variant with a non-empty ID field"),
              newParagraph("%: Numbe of known variants / Number of variants processed"),
              newParagraph("Transitions: The number of SNP interchanges of purines (A<->G) or of pyrimidines (C<->T)"),
              newParagraph("Transversions: The number of SNP interchanges of purine for pyrimidine bases (A|G<->C|T)"),
              newParagraph("Ts Tv ratio: Transitions / Transversions"),
              newParagraph("missense: The number of variant in coding region for which the variation generate a codon modification"),
              newParagraph("silent: The number of variant in coding region for which the variation does not generate a codon modification"),
              newParagraph("missense silent ratio: missense / silent"),
              newParagraph("high impact - The number of variant which an effect annoted in one of these categories: SPLICE_SITE_ACCEPTOR, SPLICE_SITE_DONOR, START_LOST, EXON_DELETED, FRAME_SHIFT, STOP_GAINED, STOP_LOST or RARE_AMINO_ACID"),
              newParagraph("low impact - The number of variant which an effect annoted in one of these categories: SYNONYMOUS_START, NON_SYNONYMOUS_START, START_GAINED, SYNONYMOUS_CODING or SYNONYMOUS_STOP"),
              newParagraph("moderate impact - The number of variant which an effect annoted in one of these categories: NON_SYNONYMOUS_CODING, CODON_CHANGE, CODON_INSERTION, CODON_CHANGE_PLUS_CODON_INSERTION, CODON_DELETION, CODON_CHANGE_PLUS_CODON_DELETION, UTR_5_DELETED or UTR_3_DELETED"),
              newParagraph("modifier impact - The number of variant which an effect annoted in one of th other categories")
              ,isNumbered = FALSE, exportId = "smv.param",protection = PROTECTION.PUBLIC)
    smv.file="snvSummaryTable.tsv"
    write.table(smv.table2,file.path(report.path,smv.file),sep="\t",col.names=F,row.names=T,quote=F)
    smvTable = newTable(smv.table2, "All variant summary statistics" , file = smv.file, exportId="smv.stats",protection = PROTECTION.PUBLIC, significantDigits = 2)
    summaryMV=addTo(summaryMV,summaryMV.text1,smvTable,smv.param)
    metricsVariant=addTo(metricsVariant,summaryMV)
    
    cohortMV = newSubSubSection("Step 6 - b: Cohort SNV metrics ", exportId="cohortMV",protection = PROTECTION.PUBLIC)
    cohortMV.text1 = newParagraph("This section gives an overview of the different metrics estimated using the overall set of sample. These metrics descibe either type, effects, localisation or quality of SNV changes")
    cmv.qual.file="snvQuality.jpeg"
    cmv.qual.HDfile="snvQuality.pdf"
    cmv.qual.Tfile="snvQuality.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVQuality.jpeg"),file.path(report.path,cmv.qual.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVQuality.pdf"),file.path(report.path,cmv.qual.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVQuality.tsv"),file.path(report.path,cmv.qual.Tfile)))
    cohortMV.quality.text1=newParagraph("The variant count distribution as a function of the mapping quality. ",asLink(url=cmv.qual.Tfile,"Download data table."))
    cmv.qual=newFigure(file=cmv.qual.file,fileHighRes = cmv.qual.HDfile,exportId="cmv.qual",protection = PROTECTION.PUBLIC)
    ##
    cmv.cov.file="snvCoverage.jpeg"
    cmv.cov.HDfile="snvCoverage.pdf"
    cmv.cov.Tfile="snvCoverage.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVCoverage.jpeg"),file.path(report.path,cmv.cov.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVCoverage.pdf"),file.path(report.path,cmv.cov.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.SNVCoverage.tsv"),file.path(report.path,cmv.cov.Tfile)))
    cohortMV.quality.text2=newParagraph("The variant count distribution as a function of the read coverage. ",asLink(url=cmv.cov.Tfile,"Download data table."))
    cmv.cov=newFigure(file=cmv.cov.file,fileHighRes = cmv.cov.HDfile,exportId="cmv.cov",protection = PROTECTION.PUBLIC)
    ##
    cmv.indel.file="indelLength.jpeg"
    cmv.indel.HDfile="indelLength.pdf"
    cmv.indel.Tfile="indelLength.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.IndelLength.jpeg"),file.path(report.path,cmv.indel.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.IndelLength.pdf"),file.path(report.path,cmv.indel.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.IndelLength.tsv"),file.path(report.path,cmv.indel.Tfile)))
    cohortMV.quality.text3=newParagraph("The INDEL count distribution as a function of their length. ",asLink(url=cmv.indel.Tfile,"Download data table."))
    cmv.indel=newFigure(file=cmv.indel.file,fileHighRes = cmv.indel.HDfile,exportId="cmv.indel",protection = PROTECTION.PUBLIC)
    ##
    cmv.local.file="snvCountRegions.jpeg"
    cmv.local.HDfile="snvCountRegions.pdf"
    cmv.local.Tfile="snvCountRegions.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountRegions.jpeg"),file.path(report.path,cmv.local.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountRegions.pdf"),file.path(report.path,cmv.local.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountRegions.tsv"),file.path(report.path,cmv.local.Tfile)))
    cohortMV.local.text1=newParagraph("The variant spatial distribution over chromomoses using the gene structre as reference. ",asLink(url=cmv.local.Tfile,"Download data table."))
    cmv.local=newFigure(file=cmv.local.file,fileHighRes = cmv.local.HDfile,exportId="cmv.local",protection = PROTECTION.PUBLIC)
    ##
    cmv.chromChange.file="snvChromosomeChange.zip"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.chromosomeChange.zip"),file.path(report.path,cmv.chromChange.file)))
    cohortMV.local.text2=newParagraph(asStrong(asLink(url=cmv.chromChange.file,"Additionally the genomic variant number per Mb representation can found here")))
    ##
    cmv.effect.file="snvCountEffects.jpeg"
    cmv.effect.HDfile="snvCountEffects.pdf"
    cmv.effect.Tfile="snvCountEffects.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountEffects.jpeg"),file.path(report.path,cmv.effect.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountEffects.pdf"),file.path(report.path,cmv.effect.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.CountEffects.tsv"),file.path(report.path,cmv.effect.Tfile)))
    cohortMV.effect.text1=newParagraph("The variant effect distribution. ",asLink(url=cmv.effect.Tfile,"Download data table."))
    cmv.effect=newFigure(file=cmv.effect.file,fileHighRes = cmv.effect.HDfile,exportId="cmv.effect",protection = PROTECTION.PUBLIC)
    ##
    cmv.base.file="snvBaseChange.jpeg"
    cmv.base.HDfile="snvBaseChange.pdf"
    cmv.base.Tfile="snvBaseChange.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.BaseChange.jpeg"),file.path(report.path,cmv.base.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.BaseChange.pdf"),file.path(report.path,cmv.base.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.BaseChange.tsv"),file.path(report.path,cmv.base.Tfile)))
    cohortMV.base.text=newParagraph("The base changes induced by SNV. ",asLink(url=cmv.base.Tfile,"Download data table."))
    cmv.base=newFigure(file=cmv.base.file,fileHighRes = cmv.base.HDfile,exportId="cmv.base",protection = PROTECTION.PUBLIC)
    ##
    cmv.codon.file="snvCodonChange.jpeg"
    cmv.codon.HDfile="snvCodonChange.pdf"
    cmv.codon.Tfile="snvCodonChange.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.codonChange.jpeg"),file.path(report.path,cmv.codon.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.codonChange.pdf"),file.path(report.path,cmv.codon.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.codonChange.tsv"),file.path(report.path,cmv.codon.Tfile)))
    cohortMV.codon.text=newParagraph("The codon changes induced by SNV. ",asLink(url=cmv.codon.Tfile,"Download data table."))
    cmv.codon=newFigure(file=cmv.codon.file,fileHighRes = cmv.codon.HDfile,exportId="cmv.codon",protection = PROTECTION.PUBLIC)
    ##
    cmv.aa.file="snvAminoAcidChange.jpeg"
    cmv.aa.HDfile="snvAminoAcidChange.pdf"
    cmv.aa.Tfile="snvAminoAcidChange.tsv"
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.AminoAcidChange.jpeg"),file.path(report.path,cmv.aa.file)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.AminoAcidChange.pdf"),file.path(report.path,cmv.aa.HDfile)))
    system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.AminoAcidChange.tsv"),file.path(report.path,cmv.aa.Tfile)))
    cohortMV.aa.text=newParagraph("The amino acide changes induced by SNV. ",asLink(url=cmv.aa.Tfile,"Download data table."))
    cmv.aa=newFigure(file=cmv.aa.file,fileHighRes = cmv.aa.HDfile,exportId="cmv.aa",protection = PROTECTION.PUBLIC)
          cohortMV=addTo(cohortMV,cohortMV.text1,cohortMV.quality.text1,cmv.qual,cohortMV.quality.text2,cmv.cov,cohortMV.quality.text3,cmv.indel,cohortMV.local.text1,cmv.local,cohortMV.local.text2,cohortMV.effect.text1,cmv.effect,cohortMV.base.text,cmv.base,cohortMV.codon.text,cmv.codon,cohortMV.aa.text,cmv.aa)
	###
	###
	sampleMV = newSubSubSection("Step 6 - c: Sample SNV metrics ", exportId="cohortMV",protection = PROTECTION.PUBLIC)
	sampleMV.text1 = newParagraph("This section gives an overview of the different metrics estimated using each sample individually. These metrics descibe the density of change in the chromosme and the general type of change found in each sample")
	smv.change.file="snvChangeRate.jpeg"
	smv.change.HDfile="snvChangeRate.pdf"
	smv.change.Tfile="snvChangeRate.tsv"
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.changeRate.jpeg"),file.path(report.path,smv.change.file)))
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.changeRate.pdf"),file.path(report.path,smv.change.HDfile)))
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.changeRate.tsv"),file.path(report.path,smv.change.Tfile)))
	sampleMV.change.text1=newParagraph("The variant change rate (ie. the mean distance in bp between two variant) for each chromosome and each sample. ",asLink(url=smv.change.Tfile,"Download data table."))
	smv.change=newFigure(file=smv.change.file,fileHighRes = smv.change.HDfile,exportId="smv.change",protection = PROTECTION.PUBLIC)
	##
	smv.tstv.file="snvTsTv.jpeg"
	smv.tstv.HDfile="snvTsTv.pdf"
	smv.tstv.Tfile="snvTsTv.tsv"
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.TsTv.jpeg"),file.path(report.path,smv.tstv.file)))
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.TsTv.pdf"),file.path(report.path,smv.tstv.HDfile)))
	system(paste("cp",file.path(project.path,"metrics","allSamples.SNV.TsTv.tsv"),file.path(report.path,smv.tstv.Tfile)))
	sampleMV.tstv.text1=newParagraph("The transitions - transvertions rate for each sample. ",asLink(url=smv.tstv.Tfile,"Download data table."))
	smv.tstv=newFigure(file=smv.tstv.file,fileHighRes = smv.tstv.HDfile,exportId="smv.tstv",protection = PROTECTION.PUBLIC)
	sampleMV=addTo(sampleMV,sampleMV.text1,sampleMV.change.text1,smv.change,sampleMV.tstv.text1,smv.tstv)
	metricsVariant=addTo(metricsVariant,cohortMV,sampleMV)
  }else{
    cohortMV.text1 = newParagraph("SVN metrics steps was skipped, unable to find ", file.path(project.path,"metrics","allSamples.SNV.SummaryTable.tsv"),exportId="cohortMV.text1",protection = PROTECTION.PUBLIC)
    metricsVariant=addTo(metricsVariant,cohortMV.text1)
  }
	
	print("SNV metrics section ... Done")
	##bibliography section
	biblio=newSection( "References" )

	### Build sections and report
	intro = addTo(intro,intro.text1,intro.text2)
	stepRes= addTo(stepRes, trim, align, improvement ,metricsAlignment ,snv, metricsVariant)

	biblio=addTo(biblio,citeRef$trimmomatic,citeRef$bwa,citeRef$picard,citeRef$gatk,citeRef$samtools,citeRef$bcftools,citeRef$snpeff)

	report= addTo(report,intro,stepRes,biblio)

	print("Done with nozzleDNA.r...")

	return(report)
}
