#' onAttach funciton. Runs every time package attached
#'
#' @import methods
 .onAttach <- function(libname, pkgname)
 {
 	options(stringsAsFactors=FALSE)
	Sys.umask('0002')
	theme_set(theme_bw())
 }
# module load mugqic/R
# Rscript -e "library(gqSeqUtils); mugqicPipelineReport(...)"



#' This function inspects a few things to make sure runs smoothly
#'
#'
#'
#' @export
checkSanity <-function(project.path=getwd())
{
	# check parameters in ini file... sample desc, dgea variavles, etc
}


