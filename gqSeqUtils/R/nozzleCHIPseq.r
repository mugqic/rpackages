

#' Generate the CHIPSEQ pipeline report using NOOZLE
#'
#' 
#' @export 
mugqicPipelineReportCHIPseq <- function(report,project.path,ini.file.path,report.path)
{
  citeRef=getCitations()
	###Overview
	Figure.nb=1
	intro = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)
	intro.text1=newParagraph("This document contains the description of the current MUGQIC ChIP-Seq \
			analysis. The information presented here reflects the current state of the analysis \
			as of ", format(Sys.time(),format="%Y-%m-%d"))
  ## Generating the Samples Names and experimental design section 
  design=newSubSubSection("Sample names and experimental designs", exportId="design",protection = PROTECTION.PUBLIC)
  design.text = newParagraph("The following table contains the sample names as well as the experimental designs. \
  For each experimental design (column name), there are three conditions: 0, \
  1 and 2. If a sample is assigned 0, it isn't included in a particular design.\
  Two non-zero conditions are currently supported: 1 for control and 2 for \
  treatment. Next to the design number is the design type, 'N' for narrow peaks \
  (transcription factors) and 'B' for broad peaks (histone marks, etc.).")
  design.table=try(read.csv(getIniParams('design_file', 'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),header=T,sep="\t",comment.char = "",check.names=FALSE))
  if(!inherits(design.table, "try-error")){
    design.file="design.tsv"
    write.table(design.table,file.path(report.path,design.file),sep="\t",col.names=T,row.names=F,quote=F)
    design.group.table = newTable(design.table[1:min(10,dim(design.table)[1]),1:min(8,dim(design.table)[2])], "Sample names and experimental designs" , file = design.file, exportId="design",protection = PROTECTION.PUBLIC)
    design=addTo(design,design.text,design.group.table)
  }else{
    design.text=newParagraph("The design file was not included. Unable to create",file.path(report.path,design.file) , exportId="design",protection = PROTECTION.PUBLIC)
    design=addTo(design,design.text)
  }


  ###Sequencing section
  fileDir=file.path(project.path,"metrics")
  listFile=file.path(fileDir,list.files(fileDir,pattern=paste(".readstats.csv","$",sep=""),recursive=T))
  ##MB:change in the trim.table.tmp creation to avoid error due to operation made on string and to remove warnings
  trim.table.tmp=NULL
  print(paste("NOTICE: processing read stats files: ", paste(listFile, collapse=","),sep=" "))
  for (fi in listFile){
      metrics.table.tmp=read.csv(fi,comment.char = "", stringsAsFactors=F,header=T, sep=',')
      trim.table.tmp=rbind(trim.table.tmp,metrics.table.tmp)
  }
  raw.fragments=try(trim.table.tmp[,"Raw.Fragments"])
  if(inherits(raw.fragments, "try-error")){
    minLibSize=50
    maxLibSize=150
  }else{
    minLibSize=floor(min(as.numeric(trim.table.tmp[,"Raw.Fragments"]))/1000000)
    maxLibSize=ceiling(max(as.numeric(trim.table.tmp[,"Raw.Fragments"]))/1000000)
  }

  intro.text2=newParagraph("The DNA sequencing performed at the Innovation Centre generated between ",minLibSize," to \
	",maxLibSize," millions paired or single end reads per library using the Illumina Hiseq 2000/2500 sequencer. Base calls \
	are completed using the Illumina CASAVA pipeline. Base quality is encoded in phred 33.")


 	stepRes = newSection("Analysis and results", exportId="AnaRes", protection = PROTECTION.PUBLIC)
 	##Generate trimming section
 	ini.minQuality=getIniParams('trailing_min_quality', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
 	ini.minLength=getIniParams('min_length', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
 
 	trimNalign=newSubSubSection("Trimming and Alignment",exportId="trimNalign",protection = PROTECTION.PUBLIC)
 	trim = newSubSection("Step 1: Read trimming and clipping of adapters", exportId="trim",protection = PROTECTION.PUBLIC)
 	trim.text1 = newParagraph("Reads are trimmed from the 3' end to have a phred score of at least ", ifelse(is.na(ini.minQuality), 30,ini.minQuality ),". \
 				Illumina sequencing adapters are removed from the reads, and all reads are\
 				required to have a length of at least ", ifelse(is.na(ini.minLength), 32, ini.minLength)," bp. Trimming and clipping are done\
 				using the Trimmomatic software",asReference(citeRef$trimmomatic)," .", exportId="trim.text1",protection = PROTECTION.PUBLIC)
  trim = addTo(trim,trim.text1)
  if(inherits(raw.fragments, "try-error")){
    trim.text2 = newParagraph("The trimming step was skipped for this analysis.", exportId="trim.text2",protection = PROTECTION.PUBLIC)
    trim = addTo(trim,trim.text2)
  }
  print("trim section ... Done")

  ##Generate Alignment section
  align = newSubSection("Step 2: Aligning the reads to a reference sequence", exportId="align",protection = PROTECTION.PUBLIC)
  ###TODO pick up directly the genome in ini and add specific genome information
  hg1kREf.text="The hg19 reference is currently used for human samples. "
  iniGref=paste(
      getIniParams('scientific_name', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),
      "assembly ",
      getIniParams('assembly', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  )
  if (iniGref == "hg19") {
    genomeRef=hg1kREf.text
  } else {
    genomeRef=paste("The reference genome used in the analysis is ", iniGref, ".")
  }
  align.text1 = newParagraph("The filtered reads are aligned to a reference genome. ", genomeRef, " The \
        alignment is done per readset of sequencing using the BWA software",asReference(citeRef$bwa)," . BWA generates \
        a Binary Alignment Map file (.bam). BAM files from different readsets of the same sample are merged into \
        a single global BAM file using the Picard software",asReference(citeRef$picard)," .", exportId="align.text1",protection = PROTECTION.PUBLIC)
  align.text2 = newParagraph("BWA is a fast light-weighted tool that aligns relatively short sequences (queries) to a sequence database \
    (target), such as the human reference genome. It is based on the Burrows-Wheeler Transform (BWT) and designed for short queries up \
    to ~200 bp with low error rate (< 3%). BWA executes gapped global alignment, supports paired-end reads and is one of the fastest short \
    read alignment algorithms to date while also visiting suboptimal hits.", exportId="align.text2",protection = PROTECTION.PUBLIC)
  
  # Filtering mapped reads
  mapQThreshold=getIniParams('filterReadsMAPQ', 'aln', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  if(is.na(mapQThreshold)) {mapQThreshold =15;}
  filterMapped= newSubSubSection("Base quality filtering mapped reads" , exportId="filterMapped",protection = PROTECTION.PUBLIC)
  filterMapped.text1 = newParagraph("This step allows the filtering of mapped reads based on the alignment quality of the BAM file. \
   The alignment file per sample is filtered using SAMtools",asReference(citeRef$samtools), ". All alignments \
   with MAPQ smaller than ", mapQThreshold, " and samflag 4 (read unmapped) are excluded from the resulting file in BAM format.", exportId="filterMapped.text1",protection = PROTECTION.PUBLIC)
  filterMapped =addTo(filterMapped, filterMapped.text1)

  # mark as duplicates STEP
  markDup = newSubSubSection("Marking duplicates" , exportId="markDup",protection = PROTECTION.PUBLIC)
  markDup.text = newParagraph("Aligned reads are duplicates if they have the same 5' alignment positions (for both mates in the \
    case of paired-end reads). All but the best pair (based on alignment score) will be marked as a duplicate in the BAM \
    file. Duplicate reads will be excluded in the subsequent analysis. Marking duplicates is executed using the Picard software",asReference(citeRef$picard)," .", exportId="markDup.text",protection = PROTECTION.PUBLIC)
  markDup= addTo(markDup, markDup.text)
  
  align = addTo(align, align.text1, align.text2, filterMapped, markDup)
  print("Align section ... Done")
  ### Alignment metrics section
  metricsAlignment = newSubSection("Step 3: Per sample sequence and alignment metrics", exportId="metricsSequencing",protection = PROTECTION.PUBLIC)
  metricsAlignment.text1 = newParagraph("General summary statistics are provided per sample and per readset", exportId="metricsAlignment.text1",protection = PROTECTION.PUBLIC)
  ### Metrics for filtered reads and mark as duplicates results
  fileDir=file.path(project.path,"metrics")
  listFile=file.path(fileDir,list.files(fileDir,pattern=paste(".memstats.csv","$",sep=""),recursive=T))

  if (length(listFile)>0){
    ##MB:change in the aln.table.tmp creation to avoid error due to operation made on string and to remove warnings
    metrics.table=data.frame(NULL)
    header=c()
    aln.table.tmp=NULL
    print(paste("NOTICE: processing post-alignment stats files: ", paste(listFile, collapse=","),sep=" "))
    for (fi in listFile){
      metrics.table.tmp=read.csv(fi,comment.char = "", stringsAsFactors=F,header=T, sep=',')
      aln.table.tmp=rbind(aln.table.tmp,metrics.table.tmp)
    }
    
    ### Report stats by sample and by sample / readset
    trim.table.readset=cbind(trim.table.tmp)
    # Remove the duplicate column, not applicable before the mark as duplicates step.
    trim.table.readset=subset(trim.table.readset, select=-charmatch("Number.of.Duplicate.Reads", dimnames(trim.table.readset)[[2]]))
    sname=unlist(lapply(trim.table.readset[,"SampleName"], function(x) unlist(strsplit(x," ",fixed=T))[1]))
    trim.table=apply(subset(trim.table.readset,select=-1), 2, function(x) {tapply(as.numeric(x),sname, sum)})
    trim.table=cbind(SampleName=rownames(trim.table), apply(subset(trim.table.readset,select=-1), 2, function(x) {tapply(as.numeric(x),sname, sum)}))
    # merge with metrics for filtered /mark as duplicate reads
    aln.table.tmp=subset(aln.table.tmp,select=-charmatch("Number.of.QC.Passed.Reads", dimnames(aln.table.tmp)[[2]]))
    trim.table=merge(trim.table, aln.table.tmp , by=1, suffixes = c("",".filtered.dup"))
    if(!inherits(raw.fragments, "try-error")){
      trim.table=cbind(trim.table,pct.Surviving=(as.numeric(trim.table[,'Fragment.Surviving'])/as.numeric(trim.table[,'Raw.Fragments']))*100,
                              pct.Aligned=round((as.numeric(trim.table[,'Number.of.Aligned.Reads'])/as.numeric(trim.table[,'Number.of.QC.Passed.Reads']))*100,2), 
                              pct.Duplicate=round((as.numeric(trim.table[,'Number.of.Duplicate.Reads'])/as.numeric(trim.table[,'Number.of.Aligned.Reads.filtered.dup']))*100,2))
      trim.table.readset=cbind(trim.table.readset,pct.Surviving=round((as.numeric(trim.table.readset[,'Fragment.Surviving'])/as.numeric(trim.table.readset[,'Raw.Fragments']))*100,2),
                              pct.Aligned=round((as.numeric(trim.table.readset[,'Number.of.Aligned.Reads'])/as.numeric(trim.table.readset[,'Number.of.QC.Passed.Reads']))*100,2)) 
    }else{
      trim.table=cbind(trim.table,pct.Aligned=round((as.numeric(trim.table[,'Number.of.Aligned.Reads'])/as.numeric(trim.table[,'Number.of.QC.Passed.Reads']))*100,2), 
                              pct.Duplicate=round((as.numeric(trim.table[,'Number.of.Duplicate.Reads'])/as.numeric(trim.table[,'Number.of.Aligned.Reads.filtered.dup']))*100,2))
      trim.table.readset=cbind(trim.table.readset,pct.Aligned=round((as.numeric(trim.table.readset[,'Number.of.Aligned.Reads'])/as.numeric(trim.table.readset[,'Number.of.QC.Passed.Reads']))*100,2))
    }
    dimnames(trim.table.readset)[[2]]=gsub("Number.of.","Nb.", dimnames(trim.table.readset)[[2]], fixed=T)
    dimnames(trim.table)[[2]]=gsub("Number.of.","Nb.", dimnames(trim.table)[[2]], fixed=T)
    trim.file="trimAlnSampleTable.tsv"
    write.table(trim.table,file.path(report.path,trim.file),sep="\t",col.names=T,row.names=F,quote=F)
    trim.stats.table = newTable(trim.table, "Per sample trimming and alignment statistics" , file = trim.file, exportId="trim.stats",protection = PROTECTION.PUBLIC, significantDigits = 2)
    trimLane.file="trimAlnLaneTable.tsv"
    write.table(trim.table.readset,file.path(report.path,trimLane.file),sep="\t",col.names=T,row.names=F,quote=F)
    trim.stats.table.readset = newTable(trim.table.readset, "Per readset trimming and alignment statistics" , file = trimLane.file, exportId="trim.stats.readset", protection = PROTECTION.PUBLIC, significantDigits = 2)
    trimStats.param=newList(newParagraph("Raw fragments: the total number of reads obtained from the sequencer"),
              newParagraph("Fragment surviving: the number of remaining reads after the trimming step"),
              newParagraph("% Surviving: Surviving reads / Raw reads"),
              newParagraph("Aligned reads: the number of reads mapped to the reference genome"),
              newParagraph("Aligned reads filtered dup: the number of reads after filtering by mapping quality"),
              newParagraph("Duplicate reads: the number of mapped reads having the same 5' alignment positions (for both mates in the case of paired-end reads) after filtering by mapping quality"),
              newParagraph("% Aligned: mapped reads / Surviving reads"),
              newParagraph("% Duplicate: Duplicate reads / Number of reads after filtering by mapping quality"), isNumbered = FALSE, exportId = "trim.param", protection = PROTECTION.PUBLIC)
    
    metricsAlignment=addTo(metricsAlignment,metricsAlignment.text1,trimStats.param, trim.stats.table, trim.stats.table.readset)
  }else{
    metricsAlignment.text1=newParagraph("The alignment metrics step was skipped, unable to find files with extension memstats.csv in the metrics directory", exportId = "metricsAlignment.text1", protection = PROTECTION.PUBLIC)
    metricsAlignment=addTo(metricsAlignment,metricsAlignment.text1)
  }
  print("metrics alignment section ... Done")
  
  ### CHIPSEQ QC metrics
  chipSeqQC = newSubSection("Steps 4 and 5: Creating QC tag directories and computing ChIP-Seq Quality metrics", exportId="chipSeqQC",protection = PROTECTION.PUBLIC)
  chipSeqQC.text1 = newParagraph("To facilitate the analysis of ChIP-Seq, the HOMER software", asReference(citeRef$homer)," transforms \
  the sequence alignment into a platform independent data structure representing the experiment. All relevant information \
  about the experiment is organized into a 'Tag Directory'. During the creation of tag directories, several quality control\
  routines are run to help provide information and feedback about the quality of the experiment. Several important parameters\
  are estimated that are later used for downstream analysis, such as the estimated length of ChIP-Seq fragments.", exportId="chipSeqQC.text1",protection = PROTECTION.PUBLIC)
  chipSeqQC.text2 = newParagraph("Four quality metrics are obtained using the HOMER software", asReference(citeRef$homer),". \
  Tag count represents the number of reads per unique position. If an experiment is over sequenced, \
  the number of duplicate reads goes up and so does the mean tag count. An average tag count below 1.5 \
  is usually recommended. Tag autocorrelation is the distribution of distance between adjacent reads. \
  Two distributions are shown, one for the same strand and one for the opposite strand. The value at \
  which the opposite strand distribution is maximum usually represents the fragment length. Sequence bias\
  shows the relationship between sequencing reads and the genomic sequence. Small variations at the \
  beginning of the reads can happen; overall the nucleotide ratios should remain constant. GC content \ 
  bias shows the content of GC for each read. If the GC distribution of the reads is shifted compared \
  to the genome distribution, the reads could be over represented in GC rich or GC poor regions. \
  For additional information, please refer to the HOMER website", asReference(citeRef$homerWeb), ".", exportId="chipSeqQC.text2", protection = PROTECTION.PUBLIC)
  chipSeqQC=addTo(chipSeqQC ,chipSeqQC.text1,chipSeqQC.text2)
  fileDir=file.path(project.path,"graphs")
  outDir=file.path(report.path,"graphs")
  print(paste ("CHIPSEQ QC metrics output: ", outDir))
  system(paste("mkdir -p ",outDir, sep=" "))
  listFile=file.path(fileDir,list.files(fileDir,pattern=paste("_QC_Metrics.ps","$",sep=""),recursive=T))
  print(paste("NOTICE: processing QC metrics graphs files: ", paste(listFile, collapse=","),sep=" "))
  for (fi in listFile){
      sampleName=gsub("_QC_Metrics.ps$","", tail(unlist(strsplit(fi,"/")),1))
      fileName.HD = gsub("_QC_Metrics.ps$","_HD_QC_Metrics.png", tail(unlist(strsplit(fi,"/")),1))
      fileName.LD = gsub("_QC_Metrics.ps$","_LD_QC_Metrics.png", tail(unlist(strsplit(fi,"/")),1))
      toFile.png=gsub(".ps$", '.png', fi)
      ps2pdg=try(system(paste("convert -size 1920x1080 -rotate 90 ", fi ,toFile.png, sep=" ")))
      toFile.jpg=gsub(".ps$", '.png', fi)
      ps2jpeg=try(system(paste("convert -size 800x600 -rotate 90 ", fi ,toFile.jpg, sep=" ")))
      if(!inherits(ps2pdg, "try-error") && !inherits(ps2jpeg, "try-error")){
        system(paste("cp",toFile.png,file.path(outDir, fileName.HD ), sep=" "))
        system(paste("cp",toFile.jpg,file.path(outDir, fileName.LD ), sep=" "))
        chipSeqQC.qual=newFigure(file=file.path("graphs", fileName.LD ), "QC metrics for sample ", sampleName ,fileHighRes = file.path("graphs", fileName.HD ), exportId=paste("chipSeqQC.qual",sampleName,sep="."), protection = PROTECTION.PUBLIC)
        chipSeqQC =addTo(chipSeqQC, chipSeqQC.qual)
      }
  }
  if (length(listFile)==0){
    chipSeqQC.qual=newParagraph("The QC metrics step was skipped, unable to find files with extension _QC_Metrics.ps in the graphs directory", exportId = "chipSeqQC.qual", protection = PROTECTION.PUBLIC)
    chipSeqQC =addTo(chipSeqQC, chipSeqQC.qual)
  }
  print("CHIPSEQ QC metrics section ... Done")
  # step 6- wiggle tracks
  wiggle = newSubSection("Step 6: UCSC tracks", exportId="wiggle",protection = PROTECTION.PUBLIC)
  wiggle.text1 = newParagraph("bedGraph Track Format files are generated from the aligned reads using \
  HOMER", asReference(citeRef$homer),". They can be loaded in browsers like IGV or UCSC (for \
  UCSC, some naming conventions and/or coordinate limits may cause some problems).", exportId="wiggle.text1",protection = PROTECTION.PUBLIC)
  wiggle.file="tracks.zip"
  try(system(paste("zip ",file.path(report.path,wiggle.file), file.path("tracks", "*", "*ucsc.bedGraph.gz"), sep=" ")))
  wiggle.text2 = newParagraph(asStrong(asLink(url=wiggle.file,"The coverage tracks are available here")),exportId="wiggle.text2",protection = PROTECTION.PUBLIC)
  wiggle = addTo(wiggle, wiggle.text1, wiggle.text2)
  print("Wiggle section ... Done")
  
  # step 7- peakCall
  peakCall=newSubSection("Step 7: Peak calls", exportId="peakCall",protection = PROTECTION.PUBLIC)
  peakCall.text1 = newParagraph("Peaks are called using the MACS software", asReference(citeRef$MACS),". The table below shows the general\ 
  peak calling strategies for narrow and wide peaks. The mfold parameter used \ 
  in the model building step is estimated from a peak enrichment diagnosis run. \
  The estimated mfold lower bound is 10 and the estimated upper bound can \
  vary between 15 and 100. The default mfold parameter of MACS is [10,30]. ", exportId="peakCall.text1",protection = PROTECTION.PUBLIC)
  peakCall.text2 = newParagraph("The following links will direct you to the MACS peak calls for each design. ", exportId="peakCall.text2",protection = PROTECTION.PUBLIC)
  strategies=data.frame(Type.of.design=c("treatment without control", "treatment with control"), narrow.peaks=c("--mfold=mfold --fix-bimodal --nolambda", "--mfold=mfold --nomodel"), broad.peaks=c("--mfold=mfold --nomodel --nolambda", "--mfold=mfold --nomodel"))
  strategies.file="strategies.table.tsv"
  write.table(strategies,file.path(report.path,strategies.file),sep="\t",col.names=T,row.names=F,quote=F)
  strategies.table= newTable(strategies, "Peak calling strategies using MACS" , file = strategies.file, exportId="strategies.table", protection = PROTECTION.PUBLIC)
  peakCall = addTo(peakCall, peakCall.text1, strategies.table, peakCall.text2)
  fileDir=file.path(project.path,"peak_call")
  reportDir=file.path(report.path,"peak_call")
  system(paste("mkdir -p ",file.path(report.path,"peak_call") ))
  file.transfer=try(system(paste("rsync -avPl --exclude '*.done' ",fileDir, report.path, sep=" ")))
  if(!inherits(file.transfer, "try-error") ){
    listFile=list.files(report.path,pattern=paste("_peaks.xls","$",sep=""),recursive=T)
    for (fi in listFile){
      designName=paste(gsub("_peaks.xls$","", head(tail(unlist(strsplit(fi,"/")),2),1)), collapse=": ")
      peakCall.text2 = newParagraph(asStrong(asLink(url=fi,paste("The peak calls file for design:", designName," is available here"))),exportId=paste("peakCall.file",designName,sep="."), protection = PROTECTION.PUBLIC)
      peakCall = addTo(peakCall, peakCall.text2)
    }  
  }else{
    peakCall.text2 = newParagraph("The peak calls step was skipped. Unable to transfer files from the ", reportDir, " directory." ,exportId="peakCall.text2", protection = PROTECTION.PUBLIC)
    peakCall = addTo(peakCall, peakCall.text2)
  } 
  print("Peak call section ... Done")
 
  # step 8- annotation
  annotation= newSubSection("Step 8: Narrow peaks annotation", exportId="annotation",protection = PROTECTION.PUBLIC)
  annotation.text1 = newParagraph("The peaks called previously are annotated with HOMER", asReference(citeRef$homer)," using RefSeq\
  annotations. Gene ontology and genome ontology analyses are also performed at this stage.", exportId="annotation.text1", protection = PROTECTION.PUBLIC)
  annotation = addTo(annotation, annotation.text1)
  system(paste("mkdir -p ",file.path(report.path,"annotation")))
  fileDir=file.path(project.path,"annotation")
  file.transfer=try(system(paste("rsync -avPl --exclude '*.done' ",fileDir, report.path, sep=" ")))
  if(!inherits(file.transfer, "try-error") ){
    listFile=list.files(report.path,pattern=paste(".annotated.csv","$",sep=""),recursive=T)
    for (fi in listFile){
      annotation.file=fi
      designName=paste(gsub(".annotated.csv$","", head(tail(unlist(strsplit(fi,"/")),2),1)), collapse=": ")
      baseName=gsub(".annotated.csv$","",fi)
      annotation.text2 = newParagraph(asStrong(asLink(url=fi,paste("The gene annotations for design:", designName," are available here"))),exportId=paste("annotation.text",designName,sep="."), protection = PROTECTION.PUBLIC)
      annotation = addTo(annotation, annotation.text2)
      gene.ontology.file=file.path(baseName,"geneOntology.html")
      annotation.text2 = newParagraph(asStrong(asLink(url=gene.ontology.file,paste("The HOMER gene Ontology annotations for design:", designName," are available here"))),exportId=paste("homerGeneOntology.text",designName,sep="."),protection = PROTECTION.PUBLIC)
      annotation = addTo(annotation, annotation.text2)
      genome.ontology.file=file.path(baseName,"GenomeOntology.html")
      annotation.text2 = newParagraph(asStrong(asLink(url=genome.ontology.file,paste("The HOMER genome Ontology annotations for design:", designName," are available here"))), exportId=paste("homerGenomeOntology.text",designName,sep="."),protection = PROTECTION.PUBLIC)
      annotation = addTo(annotation, annotation.text2)
    }  
  }else{
    annotation.text2 = newParagraph("The annotation step for narrow peaks was probably skipped. Unable to transfer files from the ", fileDir, " directory." ,exportId="annotation.text2", protection = PROTECTION.PUBLIC)
    annotation = addTo(annotation, annotation.text2)
  }
  print("Annotation section ... Done")
  
   # step 9- Motif analysis
  motif= newSubSection("Step 9: Narrow peaks motif analysis", exportId="motif",protection = PROTECTION.PUBLIC)
  motif.text = newParagraph("De novo motif analyses are performed using HOMER. The following guidelines \
  are helpful when intepreting de novo motifs results; a candidate motif should typically have: ", exportId="motif.text", protection = PROTECTION.PUBLIC)
  motif.text1 = newList(
  newParagraph(" A low p-value (< 1E-50)"),
  newParagraph(" High sequence complexity"),
  newParagraph(" No simple repeats "),
  newParagraph(" A high % of target sequences and a low % of background sequences"), exportId="motif.text1", protection = PROTECTION.PUBLIC)
  motif = addTo(motif, motif.text, motif.text1)
  fileDir=file.path(report.path,"annotation")
  if(!inherits(file.transfer, "try-error") ){
    listFile=list.files(report.path,pattern=paste("homerResults.html","$",sep=""),recursive=T)
    for (fi in listFile){
      annotation.file=fi
      designName=paste(gsub("homerResults.html$","", head(tail(unlist(strsplit(fi,"/")),3)),1), collapse=": ")
      baseName=gsub("homerResults.html$","",fi)
      de.novo.motifs=file.path(baseName,"homerResults.html")
      motif.text2 = newParagraph(asStrong(asLink(url=de.novo.motifs,paste("The HOMER de novo Motif results for design:", designName," are available here"))), exportId=paste("homerDeNovoMotifs",designName,sep="."), protection = PROTECTION.PUBLIC)
      motif = addTo(motif, motif.text2)
      known.motifs=file.path(baseName,"knownResults.html")
      motif.text2 = newParagraph(asStrong(asLink(url=known.motifs,paste("The HOMER known Motif results for design:", designName," are available here"))), exportId=paste("homerKnownMotifs",designName,sep="."), protection = PROTECTION.PUBLIC)
      motif = addTo(motif, motif.text2)
    }  
    if (length(listFile)==0){
      motif.text2 =newParagraph("The motif analysis for narrow peaks step was skipped, unable to find files homerResults.html in annotation directory", exportId = "motif.text2", protection = PROTECTION.PUBLIC)
      motif = addTo(motif, motif.text2)
    }
  }

  print("Motif section ... Done")

  # step 10- annotationPlots
  # Default values for distal, proximal, 5d, gene desert and other stats
  vector2interval<-function(x,left=T,right=F){ if(length(x)==2) {return(paste(ifelse(left,"(","["), x[1],",",x[2],ifelse(right,")","]"),sep="")); }else{ return(x[1])}}
  prox.distance=c(0,ifelse(is.na(getIniParams('homer_annotate_peaks', 'proximal_distance', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)), 2, getIniParams('homer_annotate_peaks', 'proximal_distance', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)/1000))
  distal.distance=c(prox.distance[2], ifelse(is.na(getIniParams('homer_annotate_peaks', 'distal_distance', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)),10, getIniParams('homer_annotate_peaks', 'distal_distance', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)/1000))
  Sd.distance=c(ifelse(is.na(getIniParams('homer_annotate_peaks', 'distance5d_lower', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)),10, getIniParams('homer_annotate_peaks', 'distance5d_lower', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)/1000), ifelse(is.na(getIniParams('homer_annotate_peaks', 'distance5d_upper', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)),100, getIniParams('homer_annotate_peaks', 'distance5d_upper', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)/1000))
  geneDesert.distance=c(ifelse(is.na(getIniParams('homer_annotate_peaks', 'distance5d_lower', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)),100, getIniParams('homer_annotate_peaks', 'distance5d_lower', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)/1000))
  
  ### Step 10: Annotation and peak location statistics 
  annotationStats=newSubSection("Step 10: Annotation and peak location statistics for narrow peaks", exportId="annotationStats",protection = PROTECTION.PUBLIC)
  # peak stats file
  fileDir=file.path("annotation", "peak_stats.csv")
  if(file.exists(fileDir)){
    annotationPeakStats.df=read.csv(fileDir, header=T)
    annotationPeakStats=newSubSubSection("Peak statistics for narrow peaks", exportId="annotationPeakStats",protection = PROTECTION.PUBLIC)
    annotationPeakStats.text1 = newParagraph("The table below contains different peak statistics such as: number of peaks called by \
    MACS", asReference(citeRef$MACS), ", percentage of peaks near the transcription start sites ([-1000;1000], bp), \
    peak height metrics and average peak width.", exportId="annotationPeakStats.text1",protection = PROTECTION.PUBLIC)
    annotationPeakStats.table = newTable(annotationPeakStats.df[1:min(20,dim(annotationPeakStats.df)[1]),1:min(8,dim(annotationPeakStats.df)[2])], "Peak statistics for narrow peaks" , file = fileDir, exportId=" annotationPeakStats.table",protection = PROTECTION.PUBLIC, significantDigits=2)
    annotationPeakStats=addTo(annotationPeakStats, annotationPeakStats.text1, annotationPeakStats.table)
    # peak annotation plots files
    annotationPlots=newSubSubSection("Peak location statistics", exportId="annotationPlots",protection = PROTECTION.PUBLIC)
    annotationPlots.text = newParagraph("The following figures show different peak location statistics per design. The first \
    figure consists of a pie chart that shows the proportions of the genomic locations of the peaks. The locations are:", exportId="annotationPlots.text1",protection = PROTECTION.PUBLIC)
    annotationPlots.text1 = newList(
      newParagraph("1. Gene (exon or intron) "),
      newParagraph("2. Proximal ",vector2interval(prox.distance)," kb upstream of a transcription start site) "),
      newParagraph("3. Distal ",vector2interval(distal.distance)," kb upstream of a transcription start site) "),
      newParagraph("4. 5d ",vector2interval(Sd.distance)," kb upstream of a transcription start site) "),
      newParagraph("5. Gene desert >= ",vector2interval(geneDesert.distance)," kb upstream or downstream of a transcription start site) "),
      newParagraph("6. Other (anything not included in the above categories) "), exportId="annotationPlots.text1",protection = PROTECTION.PUBLIC)
    annotationPlots.text2 = newParagraph("The second and third figures illustrate the distribution of peaks found within \
      exons and introns. The last figure shows the distribution of peak distance \
      relative to the Transcription Start Sites (TSS).", exportId="annotationPlots.text2",protection = PROTECTION.PUBLIC)
    annotationPlots=addTo(annotationPlots ,annotationPlots.text , annotationPlots.text1, annotationPlots.text2 )
    fileDir=file.path(project.path,"graphs")
    outDir=file.path(report.path,"graphs")
    listFile=file.path(fileDir,list.files(fileDir,pattern=paste("_Misc_Graphs.ps","$",sep=""),recursive=T))
    print(paste("NOTICE: processing annotation metrics graphs files: ", paste(listFile, collapse=","),sep=" "))
    for (fi in listFile){
    ### MB: add regular expression end ($) during gsub otherwise it could also substitue other part of the past and generate errors
      designName=gsub("_Misc_Graphs.ps$","", tail(unlist(strsplit(fi,"/")),1))
      groupName=gsub("_Misc_Graphs.ps$","", head(tail(unlist(strsplit(fi,"/")),2),1))
      toFile.ps2png=gsub(".ps$", ".png", fi)
      toFile.ps2pdf=gsub(".ps$", ".pdf", fi)
      ps2png=try(system(paste("convert -size 2560x1600 -rotate 90 ", fi , toFile.ps2png, sep=" ")))
      ps2pdf=try(system(paste("convert ", fi , toFile.ps2pdf, sep=" ")))
      final.pdf=file.path(outDir,tail(unlist(strsplit(toFile.ps2pdf,"/")),1))
      if(!inherits(ps2png, "try-error") & !inherits(ps2pdf, "try-error")){
        try(system(paste("cp", toFile.ps2pdf, final.pdf , sep=" ")))
        baseName=gsub("_Misc_Graphs.ps$","_Misc_Graphs", tail(unlist(strsplit(fi,"/")),1))
        listPNG=file.path(fileDir,list.files(fileDir,pattern=paste(baseName,"*",".png","$",sep=""),recursive=T))
        print(paste("NOTICE: processing converted annotation metrics graphs files: ", paste(listPNG, collapse=","),sep=" "))
        for(png in listPNG){
          toFile.png=file.path(outDir, tail(unlist(strsplit(png,"/")),1))
          system(paste("cp", png , toFile.png, sep=" "))
          rel.path=file.path("graphs",tail(unlist(strsplit(png,"/")),1))
          rel.path2=file.path("graphs",tail(unlist(strsplit(final.pdf,"/")),1))
          annotationPlotsFigure=newFigure(file=rel.path, "Annotation statistics for design ",designName, fileHighRes = rel.path2, exportId=paste("annotationPlotsFigure", groupName, sep="."), protection = PROTECTION.PUBLIC)
          annotationPlots=addTo(annotationPlots ,annotationPlotsFigure)
        }
      }else{
        annotationPlotsFigure=newParagraph("The annotation plots for narrow peaks step was skipped, unable to transfer *_Misc_Graphs.ps in graphs directory for design", designName, exportId = "annotationPlotsFigure", protection = PROTECTION.PUBLIC)
        annotationPlots=addTo(annotationPlots ,annotationPlotsFigure)
      }      
    }
    if (length(listFile)==0){
      annotationPlots=newParagraph("The annotation plots for narrow peaks step was skipped, unable to find files with extension _Misc_Graphs.ps in graphs directory", exportId = "annotationPlots", protection = PROTECTION.PUBLIC)
    }
    annotationStats=addTo(annotationStats, annotationPeakStats, annotationPlots)
  }else{
      annotationPeakStats=newParagraph("The peak statistics step was skipped, unable to find files ",fileDir ,".", exportId = "annotationPeakStats", protection = PROTECTION.PUBLIC)
      annotationStats=addTo(annotationStats, annotationPeakStats)
  }
  print("Annotation Plots and metrics section ... Done")

  ## Bibliography section
  biblio=newSection( "References" )
  
  ## Build sections and report
  intro = addTo(intro,intro.text1,intro.text2)
  #stepRes= addTo(stepRes, design,trim, align, metricsAlignment , chipSeqQC, wiggle , peakCall, annotation, motif, annotationStats)
   stepRes= addTo(stepRes, design, trim, align, metricsAlignment, chipSeqQC, wiggle , peakCall, annotation, motif, annotationStats)
  biblio=addTo(biblio,citeRef$trimmomatic,citeRef$bwa,citeRef$picard, citeRef$samtools, citeRef$homer,citeRef$homerWeb, citeRef$MACS )
  report= addTo(report,intro,stepRes,biblio)

  return(report)
}
