

#' blah documentation here
#'
#' # Here is a function related to reporting the bitbucket RNAseq pipeline1.0 with Nozzle
#' 
#' @export
mugqicPipelineReportRNAseq <- function(report,project.path,ini.file.path,report.path) 
{
	###overview
	intro = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)
	intro.text1=newParagraph("This document contains the description of the current MUGQIC RNA-Seq Differential \
			analysis. The information presented here reflects the current state of the analysis \
			as of ", format(Sys.time(),format="%Y-%m-%d"))
	trim.table.tmp=try(read.csv(file.path(project.path,"metrics","trimming.stats"),header=F,sep="\t",comment.char = "" ,check.names=FALSE))
	if (!inherits(trim.table.tmp, "try-error")){
    minLibSize=as.character(floor((min(trim.table.tmp[,3])*2)/1000000))
    maxLibSize=as.character(ceiling((max(trim.table.tmp[,3])*2)/1000000))
  }else{
    minLibSize=50
    maxLibSize=150
  }
	strand=getIniParams('strand_info', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	strand=strsplit(strand,"-",fixed=T)[[1]][2]
	libType=getIniParams('library_type', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	protocol=getIniParams('protocol', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	intro.text2=newParagraph("The RNA sequencing performed at the Innovation center generated between ",minLibSize," to \
			",maxLibSize," million ",tolower(libType)," reads per library using the illumina Hiseq 2000/2500 sequencer. The protocol used for preparing libraries is the " ,asStrong(paste(strand,protocol,"",sep=" ")), "protocole. Base calls are made using the Illumina CASAVA pipeline. Base quality \
			is encoded in phred 33.")
	print("Intro section ... Done")
	citeRef=getCitations()
	stepRes = newSection("Analysis and results", exportId="AnaRes", protection = PROTECTION.PUBLIC)
	##Generate triming section -- could be push in the wrapper
  minLength=getIniParams('min_length', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  minQuality=getIniParams('trailing_min_quality', 'trimmomatic', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	trimNalign=newSubSubSection("Trimming and Alignment analysis",exportId="trimNalign",protection = PROTECTION.PUBLIC)
	trim    = newSubSubSection("Step 1: Read trimming and clipping of adapters", exportId="trim",protection = PROTECTION.PUBLIC)
	trim.text1 = newParagraph("Reads are trimmed from the 3' end to have a phred score of at least ",ifelse(minQuality !="", minQuality,30),
				". Illumina sequencing adapters are removed from the reads, and all reads are\
				required to have a length of at least " , ifelse(minLength !="", minLength,32), ". Trimming and clipping are done\ 
				with the Trimmomatic software",asReference(citeRef$trimmomatic)," .", exportId="trim.text1",protection = PROTECTION.PUBLIC)
	### create a fake table to only report colnames of the real table in the file
	if (!inherits(trim.table.tmp, "try-error")){  
    sampleUniq=sort(unique(as.vector(trim.table.tmp[,1])))
    trim.col1=NULL
    trim.col2=NULL
    trim.col3=NULL
    for (i in 1:length(sampleUniq)) {
      trim.col1=c(trim.col1,sampleUniq[i])
      trim.col2=c(trim.col2,sum(trim.table.tmp[trim.table.tmp[,1] %in% sampleUniq[i],3]))
      trim.col3=c(trim.col3,sum(trim.table.tmp[trim.table.tmp[,1] %in% sampleUniq[i],4]))
    }
    trim.table=data.frame(trim.col1,trim.col2,trim.col3)
    colnames(trim.table)=c("Sample","Raw read #","Surviving read #")
    trim = addTo(trim,trim.text1)
  }else{
    trim.text2 = newParagraph("The trimming step was skipped for this analysis. Unable to find ", file.path(project.path,"metrics","trimming.stats") ,exportId="trim.text2",protection = PROTECTION.PUBLIC)
    trim = addTo(trim,trim.text2)         
    trimNalign.stats.table=newParagraph("The trimming step was skipped for this analysis. Unable to find ", file.path(project.path,"metrics","trimming.stats") ,exportId="trimNalign.stats.table",protection = PROTECTION.PUBLIC)
    trimNalignStats.param=newParagraph(" ",exportId="trimNalignStats.param",protection = PROTECTION.PUBLIC)
    trimNalign.additional=newParagraph(" ",exportId="trimNalign.additional",protection = PROTECTION.PUBLIC)

  }
	print("Trimming section ... Done")
	##Generate Alignment section
	align   = newSubSubSection("Step 2: Aligning the reads to the reference sequence", exportId="align",protection = PROTECTION.PUBLIC)

###TODO pick up directly the genome in ini and add specific genome information
	hg1kREf.text="The 1000 genome (b37) reference is currently used for human samples. The 1000 genome reference \
	is similar to the hg19 reference, except that it uses the newer mitochondrial build (NC_012920). Also \
	chromosome nomenclature is slightly different ('1' vs 'chr1', non-chromosomal supercontigs are named differently e.g. GL000207)"
	iniGref=paste(
        getIniParams('scientific_name', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),
        "assembly ",
        getIniParams('assembly', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
    )
	if (iniGref == "b37") {
		genomeRef=hg1kREf.text
	} else {
		genomeRef=paste("The genome used in the analysis is",iniGref)
	}
	align.text = newParagraph("The filtered reads are aligned to a reference genome. ", genomeRef, ". The \
				alignment is done with the STAR-rna software in a 2 pass-mode",asReference(citeRef$STARaligner)," . It generates \
				a Binary Alignment Map file (.bam). ", exportId="align.text",protection = PROTECTION.PUBLIC)
		### create a fake table to only report colnames of the real table in the file
# 	align.table=read.table(file.path(project.path,"metrics","alignment.stats"))
# 	colnames(align.table)=c("Sample","align read #")
# 	align.file="alignTable.tsv"
	#write.table(align.table,file.path(report.path,align.file),sep="\t",col.names=T,row.names=F,quote=F)
	#align.stats.table = newTable(colnames(align.table), "By sample alignment statistics" , file = align.file, exportId="align.stats",protection = PROTECTION.PUBLIC)
	align = addTo(align,align.text)
	print("Alignment section ... Done")
	## Generate trimming and aligment metrics stats
	trimNalignStats=newSubSubSection("Metrics for the Trimming and Alignment steps", exportId="trimNalignStats",protection = PROTECTION.PUBLIC)	
	rnaqcMetricsTable=try(read.csv2(file.path(project.path,"metrics","rnaseqRep","metrics.tsv"),header=T,sep="\t",check.names=FALSE))
	if (!inherits(rnaqcMetricsTable, "try-error") && !inherits(trim.table.tmp, "try-error") ){  
    tNAOrder=match(as.vector(rnaqcMetricsTable$Sample),trim.table[,1])
    posCol=match(c("Mapped","Alternative Aligments","rRNA","Mean Per Base Cov.","Exonic Rate","Genes Detected"),colnames(rnaqcMetricsTable))
    trimNalignTable=data.frame(trim.table[tNAOrder,1],
          trim.table[tNAOrder,2]*2,
          trim.table[tNAOrder,3]*2,
          (trim.table[tNAOrder,3]/trim.table[tNAOrder,2])*100,
          as.numeric(as.vector(rnaqcMetricsTable[,posCol[1]])),
          (as.numeric(as.vector(rnaqcMetricsTable[,posCol[1]]))/(trim.table[tNAOrder,3]*2))*100,
          as.numeric(as.vector(rnaqcMetricsTable[,posCol[2]])),
          (as.numeric(as.vector(rnaqcMetricsTable[,posCol[2]]))/as.numeric(as.vector(rnaqcMetricsTable[,posCol[1]])))*100,
          as.numeric(as.vector(rnaqcMetricsTable[,posCol[3]])),
          (as.numeric(as.vector(rnaqcMetricsTable[,posCol[3]]))/(trim.table[tNAOrder,3]*2))*100,
          round(as.numeric(as.vector(rnaqcMetricsTable[,posCol[4]]))),
          as.numeric(as.vector(rnaqcMetricsTable[,posCol[5]])),
          as.numeric(as.vector(rnaqcMetricsTable[,posCol[6]])))
    colnames(trimNalignTable)=c("Sample","Raw reads","Surviving reads","%","Aligned read","%","Alternative aligments","%","rRNA reads","%","Coverage","Exonic Rate","Genes")
    trimNalign.file="trimAlignmentTable.tsv"
    write.table(trimNalignTable,file.path(report.path,trimNalign.file),sep="\t",col.names=T,row.names=F,quote=F)
    trimNalign.stats.table = newTable(trimNalignTable, "Trimming and alignment statistics provided for each sample" , file = trimNalign.file, exportId="trimNalign.stats",protection = PROTECTION.PUBLIC, significantDigits = 2)
    trimNalignStats.param=newList(newParagraph("Raw reads: the total number of read obtained from the sequencer"),
              newParagraph("Surviving reads: the number of remaining reads after the trimming step"),
              newParagraph("%: Surviving reads / Raw reads"),
              newParagraph("Mapped reads: the number of read aligned to the reference"),
              newParagraph("%: Mapped reads / Surviving reads"),
              newParagraph("Alternative Aligments: the number of duplicate read entries providing alternative coordinates"),
              newParagraph("%: Alternative Aligments / Mapped reads"),
              newParagraph("rRNA reads: the number of reads aligning to rRNA regions as defined in the transcript model definition"),
              newParagraph("%: rRNA reads / Surviving reads"),
              newParagraph("Coverage: mean coverage = the number of bp aligning to reference / total number of bp in the reference"),
              newParagraph("Exonic Rate:  the fraction mapping reads within exons"),
              newParagraph("Genes:  the number of Genes with at least 5 reads"),isNumbered = FALSE, exportId = "trimNalignStats.param",protection = PROTECTION.PUBLIC)
  } 
  
	### tmp test file need to be uniformly created
	reportRNAseqQC.file="reportRNAseqQC.zip"
	if (!file.exists(file.path(report.path,reportRNAseqQC.file))) {
		  #file.symlink(file.path(project.path,"metrics","rnaseqRep.zip"),file.path(report.path,reportRNAseqQC.file))		
      try(file.symlink(file.path("..","metrics","rnaseqRep.zip"),file.path(report.path,reportRNAseqQC.file)))
	}
	if (file.exists(file.path(report.path,reportRNAseqQC.file))){
    trimNalign.additional=newParagraph(asStrong(asLink(url=reportRNAseqQC.file,"Additional metrics can be found in the original RNAseqQC report available here")))
  }else{
    trimNalign.additional=newParagraph("The RNAseqQC report step was skipped for this analysis. Unable to find ", file.path(report.path,reportRNAseqQC.file),"." ,exportId="trimNalign.additional",protection = PROTECTION.PUBLIC)
  }
	if (inherits(rnaqcMetricsTable, "try-error")){
      trimNalignStats.text = newParagraph("The RNAseqQC step was skipped for this analysis. Unable to find ", file.path(project.path,"metrics","rnaseqRep","metrics.tsv"),"." ,exportId="trimNalignStats.text",protection = PROTECTION.PUBLIC)
      trimNalignStats=addTo(trimNalignStats,trimNalignStats.text, trimNalign.additional)
  }else{
    trimNalignStats=addTo(trimNalignStats,trimNalign.stats.table,trimNalignStats.param,trimNalign.additional)
  }
  trimNalign=addTo(trimNalign,trim,align,trimNalignStats)
	print("TrimNAlign metrics section ... Done")
	##Generate wiggle section
	wiggle  = newSubSection("Step 3: Generating wiggle graphs/tracks", exportId="wiggle",protection = PROTECTION.PUBLIC)
	wiggle.text1 = newParagraph("Wiggle tracks format files are generated from the aligned reads using BedGraphToBigWig.\
				This file is a representation of read alignments that can be easily downloaded in browsers \
				like IGV or UCSC (for UCSC some name conventions and/or coordinate limits may cause some \
				problems, in this case please contact us for file re-formatting). ", exportId="wiggle.text",protection = PROTECTION.PUBLIC)
	wiggle.file="tracks.zip"
	if (!file.exists(file.path(report.path,wiggle.file))) {
		#file.symlink(file.path(project.path,"tracks.zip"),file.path(report.path,wiggle.file))
		try(file.symlink(file.path("..","tracks.zip"),file.path(report.path,wiggle.file)))
	}
	if (file.exists(file.path(report.path,wiggle.file))){
    wiggle.text2 = newParagraph(asStrong(asLink(url=wiggle.file,"The wiggle tracks are available here")), exportId="wiggle.text2", protection = PROTECTION.PUBLIC)
  }else{
    wiggle.text2 = newParagraph("The wiggle tracks generation step was skipped for this analysis. Unable to find ", file.path(report.path,wiggle.file), exportId="wiggle.text2", protection = PROTECTION.PUBLIC)
  }
	wiggle = addTo(wiggle, wiggle.text1, wiggle.text2)
	print("Wiggle section ... Done")
	##Generate cufflink section
	fpkm    = newSubSection("Step 4: FPKM analyis", exportId="fpkm",protection = PROTECTION.PUBLIC)
	fpkm.cuff    = newSubSubSection("Generating FPKM values", exportId="fpkm.cuff",protection = PROTECTION.PUBLIC)
	fpkm.text = newParagraph("The Cufflinks program",asReference(citeRef$cufflinks)," is used to assemble aligned RNA-Seq reads into transcripts and to \
				estimate their abundance (FPKM).  In RNA-Seq experiments, cDNA fragments are sequenced and mapped back\
				 to genes and ideally, individual transcripts. Properly normalized, the RNA-Seq fragment counts can be\ 
				 used as a measure of the relative abundance of transcripts, and Cufflinks measures transcript abundances \
				in ", asStrong("Fragments Per Kilobase of exon per Million fragments mapped (FPKM)"),", which is analagous to single-read\
				 \"RPKM\". In paired-end RNA-Seq experiments, fragments are sequenced from both ends, providing two reads\
				 for each fragment. To estimate isoform-level abundances, one must assign fragments to individual transcripts, \
				which may be difficult because a read may align to multiple isoforms of the same gene. Cufflinks uses a \
				statistical model of paired-end sequencing experiments to derive a likelihood for the abundances of a set \
				of transcripts given a set of fragments. Once transcripts are assembled and their corresponding \
				FPKM estimated, these transcripts are annotated with the known reference set of transcripts obtained from \
				the Ensembl database.", exportId="fpkm.text",protection = PROTECTION.PUBLIC)
	fpkm.cuff=addTo(fpkm.cuff,fpkm.text)
	fpkm = addTo(fpkm, fpkm.cuff)
	metrics = newSubSection("Step 5: Metrics and exploratory analysis", exportId="metrics",protection = PROTECTION.PUBLIC)
	metrics.text=newParagraph("We use several metrics and exploratory analysis in order to control the data quality and to verify the biological reliability of the data.", exportId="metrics.text",protection = PROTECTION.PUBLIC)
	fpkm.metrics=newSubSubSection("FPKM metrics", exportId="fpkm.metrics",protection = PROTECTION.PUBLIC)
	fpkm.metrics.text= newParagraph("The pairwise sample correlation analysis allows controlling the general transcipts expression consistency between samples. \
					It can also serve to control for sample mix-up or for error in name asssigment. \
					Thus samples belonging to the same design group/condition are expected to show \
					higher level of correlation.")
					
	fpkm.table=try(read.csv(file.path(project.path,"metrics","rnaseqRep","corrMatrixSpearman.txt"),header=T,sep="\t",comment.char = "",check.names=FALSE))
	if (!inherits(fpkm.table, "try-error")){
    colnames(fpkm.table)[1]="Vs."
    fpkm.table.file="corrMatrixSpearman.txt"
    write.table(fpkm.table,file.path(report.path,fpkm.table.file),sep="\t",col.names=T,row.names=F,quote=F)
    fpkm.metrics.table = newTable(fpkm.table, "Pairwise Pearson's correlation value by sample" , file = fpkm.table.file, exportId="fpkm.metrics.table",protection = PROTECTION.PUBLIC, significantDigits = 2)
  }else{
    fpkm.metrics.table=newParagraph("The cufflinks fpkm generation step was skipped for this analysis. Unable to find ", file.path(project.path,"metrics","rnaseqRep","corrMatrixSpearman.txt"), ".", exportId="fpkm.metrics.table", protection = PROTECTION.PUBLIC)
  }
	saturation.file="saturation.zip"
	if (!file.exists(file.path(report.path,saturation.file))) {
		#file.symlink(file.path(project.path,"tracks.zip"),file.path(report.path,wiggle.file))
		try(file.symlink(file.path("..","metrics","saturation.zip"),file.path(report.path,saturation.file))) ### temporay for test
	}
	fpkm.metrics.text2= newParagraph("Saturation plots show if there is enough sequencing depth to saturate gene expression \
					at various range of expression. In RNA-seq experiments, saturation would be reached when an increment in the number of reads does not \
					result in additional true expressed transcripts being detected. The precision of any sample statistics (FPKM or RPKM) 
					is affected by sample size (sequencing depth). The saturation analysis will resample a series of subsets from \
					total RNA reads and then calculate RPKM values using each subset. By doing this, we are able to check if the \
					current sequencing depth was saturated or not (or if the RPKM values were stable or not). For each sample we \
					estimate the Percent Relative Error (PRE). The PRE measures how the RPKM estimated from a subset of reads deviates from real \
					expression levels) and the median RPKM of the set of transcripts. Saturation plots are generated independently for four different \
					set of transcripts: high intermediate, moderate and low expressed transcripts (corresponding to quartiles Q1 to Q4 of median RPKM).") 
	if (file.exists(file.path(report.path,saturation.file))){				
    fpkm.metrics.saturation=newParagraph(asStrong(asLink(url=saturation.file,"The Saturation graphs are available here")), exportId="fpkm.metrics.saturation",protection = PROTECTION.PUBLIC)
  }else{
    fpkm.metrics.saturation=newParagraph("The Saturation graphs are not available. Unable to find", file.path("..","metrics","saturation.zip"), ".", exportId="fpkm.metrics.saturation",protection = PROTECTION.PUBLIC)
  }
	fpkm.metrics=addTo(fpkm.metrics,fpkm.metrics.text,fpkm.metrics.table,fpkm.metrics.text2)
	print("fpkm section ... Done")
	### exploratory
	exploratory=newSubSubSection("Exploratory analysis", exportId="exploratory",protection = PROTECTION.PUBLIC)
	exploratory.files=getFileIndex(file.path(project.path, "exploratory"))
	exploratory.used.files=vector()
  exploratory.intro=c("First","Secondly","Finally")
	exploratory.text1=newParagraph("The exploratory analysis is based on the use of gene read counts per million (CPM) and transcripts RPKM values. \
	  The main goal of this type of approach is to detect the possible presence of outlier samples and to explore the homogeneity of biological \
		replicates. Thus if no outlier are detected and replicates are fairly homogenous, the results of following gene and transcript analysis will be reinforced." , exportId="exploratory.text1",protection = PROTECTION.PUBLIC)
  exploratory=addTo(exploratory,exploratory.text1)
  pcaG=grep("pca_log2CPM",exploratory.files$File)
  if (length(pcaG) > 0) {
    exploratory.used.files=c(exploratory.used.files, pcaG)      
    exploratory.text2=newParagraph(exploratory.intro[length(exploratory.used.files)],", we analyzed how different samples are connected to each other using principal component analysis (PCA) on \
		gene expression data (log2CPM) without prior statistical filtering. The plot shows the projection of the samples onto the two-dimensional \
		space spanned by the first and second principal component. These are the orthogonal directions in which the data exhibits the largest and \
		second-largest variability. This two component are usually suficient to differenceciate groups of samples describing the principal conditions \
		of the analysis design.", exportId="exploratory.text2",protection = PROTECTION.PUBLIC)
    exploratory.pca.file="pca_log2CPM.png"
    exploratory.pca.HDfile="pca_log2CPM.pdf"
    try(system(paste("cp",exploratory.files$File[exploratory.used.files[length(exploratory.used.files)]],file.path(report.path,exploratory.pca.HDfile))))
    try(system(paste("convert",file.path(report.path,exploratory.pca.HDfile),file.path(report.path,exploratory.pca.file))))
    exploratory.pca=newFigure(file=exploratory.pca.file,fileHighRes = exploratory.pca.HDfile,exportId="exploratory.pca",protection = PROTECTION.PUBLIC, exploratory.files$Description[exploratory.used.files[length(exploratory.used.files)]])
    exploratory=addTo(exploratory,exploratory.text2,exploratory.pca)
  }else{
    exploratory.text2=newParagraph("Exploratory analysis was skipped. Unable to find the corresponding pca_log2CPM file",exportId="exploratory.text2",protection = PROTECTION.PUBLIC)
    exploratory=addTo(exploratory,exploratory.text2)
  
  }
  hclG=grep("cordist_hclust_log2RPKM",exploratory.files$File)
  if (length(hclG) > 0) {
    exploratory.used.files=c(exploratory.used.files, hclG)
    exploratory.text3=newParagraph(exploratory.intro[length(exploratory.used.files)],", we used RPKM values of the entire set of transcripts to estimate the correlation distance between each sample. \
            A hierarchical clustering of these distances (Ward approach) is realized to visualize the transcript expression divergences between samples.", exportId="exploratory.text4",protection = PROTECTION.PUBLIC)
    exploratory.hclust.file="cordist_hclust_log2RPKM.png"
    exploratory.hclust.HDfile="cordist_hclust_log2RPKM.pdf"
    try(system(paste("cp",exploratory.files$File[exploratory.used.files[length(exploratory.used.files)]],file.path(report.path,exploratory.hclust.HDfile))))
    try(system(paste("convert",file.path(report.path,exploratory.hclust.HDfile),file.path(report.path,exploratory.hclust.file))))
    exploratory.hclust=newFigure(file=exploratory.hclust.file,fileHighRes = exploratory.hclust.HDfile,exportId="exploratory.hclust",protection = PROTECTION.PUBLIC, exploratory.files$Description[exploratory.used.files[length(exploratory.used.files)]])
    exploratory=addTo(exploratory,exploratory.text3,exploratory.hclust)
  }else{
    exploratory.text3=newParagraph("Exploratory analysis was partially or totally skipped. Unable to find the corresponding cordist_hclust_log2RPKM file",exportId="exploratory.text3",protection = PROTECTION.PUBLIC)
    exploratory=addTo(exploratory,exploratory.text3)
  
  }
  heatG=grep("top_sd_heatmap_log2CPM",exploratory.files$File)
  if (length(heatG) > 0) {
    exploratory.used.files=c(exploratory.used.files, heatG)
    exploratory.text4=newParagraph(exploratory.intro[length(exploratory.used.files)],", genes with the most variable expression data (log2CPM standard deviation) are used to vizualize how the most /
            variable gene are expressed among samples. The heatmap plot indicates if it exists specific pattern of variation among these genes that could /
            be used to discriminates between group of samples.", exportId="exploratory.text4",protection = PROTECTION.PUBLIC)
    exploratory.heatmap.file="top_sd_heatmap_log2CPM.png"
    exploratory.heatmap.HDfile="top_sd_heatmap_log2CPM.pdf"    
    try(system(paste("cp",exploratory.files$File[exploratory.used.files[length(exploratory.used.files)]],file.path(report.path,exploratory.heatmap.HDfile))))
    try(system(paste("convert",file.path(report.path,exploratory.heatmap.HDfile),file.path(report.path,exploratory.heatmap.file))))
    exploratory.heatmap=newFigure(file=exploratory.heatmap.file,fileHighRes = exploratory.heatmap.HDfile, exportId="exploratory.heatmap",protection = PROTECTION.PUBLIC, exploratory.files$Description[exploratory.used.files[length(exploratory.used.files)]])
    exploratory=addTo(exploratory,exploratory.text4,exploratory.heatmap)
  }else{
    exploratory.text4=newParagraph("Exploratory analysis was partially or totally skipped. Unable to find the corresponding top_sd_heatmap_log2CPM file",exportId="exploratory.text4",protection = PROTECTION.PUBLIC)
    exploratory=addTo(exploratory,exploratory.text4)  
  }
	toZip=exploratory.files$File[exploratory.used.files*-1]
	toZipDescription=paste(basename(toZip),exploratory.files$Description[exploratory.used.files*-1],sep=": ")
	write(x=toZipDescription,file=file.path(project.path,"exploratory","FileDescription.txt"))
	toZip=c(toZip,file.path(project.path,"exploratory","FileDescription.txt"))
	exploratory.additional.zip="exploratory.zip"
	try(zip(file.path(report.path,exploratory.additional.zip),toZip,flags="-j"))
	metrics.additional=newSubSubSection("Additional Metrics files available", exportId="metrics.additional",protection = PROTECTION.PUBLIC)
	if(length(exploratory.used.files)>0 && file.exists(file.path(report.path,reportRNAseqQC.file))){
    metrics.additional.link1=newParagraph(asStrong(asLink(url=exploratory.additional.zip,"Additional exploratory analysis can be found here")))
    metrics.additional.link2=newParagraph(asStrong(asLink(url=reportRNAseqQC.file,"Additional metrics can be found in the original RNAseqQC report available here")))
    metrics.additional=addTo(metrics.additional,fpkm.metrics.saturation,metrics.additional.link1,metrics.additional.link2)
    
  }else{
    metrics.additional.link2=newParagraph("Additional metrics are not available.", exportId="metrics.additional.link2",protection = PROTECTION.PUBLIC)
    metrics.additional=addTo(metrics.additional.link2)
  }
	metrics = addTo(metrics, metrics.text, fpkm.metrics, exploratory, metrics.additional)
	print("Exploratory and metrics section ... Done")
	
	##Generate differential expression section
	diffExp= newSubSection("Step 6: Differential expression analysis - Methods", exportId="diffExp",protection = PROTECTION.PUBLIC)
	design=newSubSubSection("Step 6 - a: Differential analysis design", exportId="design",protection = PROTECTION.PUBLIC)
	design.text = newParagraph("The experimental design resulted from a discussion with the client. The designs used \
				in differential analysis are presented in the following table, which contains the sample \
				names as well as the sample group membership per design. For each experimental design (column \
				name), three conditions/groups are possible: 0, 1 and 2. If a sample is assigned 0, it's not \
				included in a particular analysis. If a sample is assigned 1, the sample is considered as a member \
				of the control group. If a sample is assigned 2, the sample is considered as a member of the \
				test/case group.")
	design.table=try(read.csv(getIniParams('design_file', 'gq_seq_utils_report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),header=T,sep="\t",comment.char = "",check.names=FALSE))
	if (!inherits(design.table, "try-error")){
    design.file="design.tsv"
    write.table(design.table,file.path(report.path,design.file),sep="\t",col.names=T,row.names=F,quote=F)
    design.group.table = newTable(design.table[1:min(6,dim(design.table)[1]),1:min(8,dim(design.table)[2])], "Sample names and experimental designs" , file = design.file, exportId="design",protection = PROTECTION.PUBLIC)
  }else{
    design.group.table = newParagraph("The experimental design file defined in the configuration file is not available. The design file is needed to generate DGE", exportId="design.group.table", protection = PROTECTION.PUBLIC)    
  }
  design=addTo(design,design.text,design.group.table)
  diffExp=addTo(diffExp,design)
  dge=newSubSubSection("Step 6 - b: Differential gene analysis description")
  dge.cite=newJournalCitation( authors = "A. Mortazavi, B.A. Williams, K. McCue, L. Schaeffer, B. Wold", title="Mapping and quantifying mammalian transcriptomes by RNA-Seq", publication="Nat Methods", issue = "5",  pages = "621-628", year = "2008",number="")
	dge.text1= newParagraph("A primary task in the analysis of RNA-seq data is the detection of differentially expressed genes. For \
				this purpose, count data from RNA-Seq should be obtained for each non-overlapping gene. Read counts \
				are found to be (to good approximation) linearly related to the abundance of the target transcript",
				asReference(dge.cite) ,
				". If reads were independently sampled from a population with given, fixed fractions of genes, the read \
				counts would follow a multinomial distribution, which can be approximated by the Poisson distribution. \
				Thus, we can use statistical testing to decide whether, for a given gene, an observed difference in read \
				counts is significant, that is, whether it is greater than what would be expected just due to natural random \
				variation. The differencial gene expression analysis is done using DESeq",asReference(citeRef$deseq)," and edgeR",asReference(citeRef$edger)," R Bioconductor package")
	dge.text3= newParagraph("Read counts are obtained using HTSeq and are represented as a table which reports, for each sample (columns), \
				the number of reads mapped to a given gene (rows).")
	dgeTable=try(read.csv(file.path(project.path,"DGE","rawCountMatrix.csv"),header=T,sep="\t",comment.char = "",check.names=FALSE))
	if (!inherits(dgeTable, "try-error")){
    dgeTable.file="rawCountMatrix.tsv"
    write.table(dgeTable,file.path(report.path,dgeTable.file),sep="\t",col.names=T,row.names=F,quote=F)
    dge.count.table= newTable(dgeTable[1:min(6,dim(dgeTable)[1]),1:min(8,dim(dgeTable)[2])], "Matrix of raw read counts per gene per sample" , file = dgeTable.file, exportId="dge.table",protection = PROTECTION.PUBLIC)    
  }else{
    dge.count.table=newParagraph("The DGE step was skipped. Unable to find ", file.path(project.path,"DGE","rawCountMatrix.csv"),exportId="dge.count.table",protection = PROTECTION.PUBLIC)
  }
  dge=addTo(dge,dge.text1,dge.text3,dge.count.table)
  diffExp=addTo(diffExp,dge)
	print("DGE description section ... Done")
	cuffdiff=newSubSubSection("Step 6 - c: Differential transcripts analysis description" , exportId="cuffdiff",protection = PROTECTION.PUBLIC) 
	cuffdiff.cite = newJournalCitation( authors = "C. Trapnell, D.G. Hendrickson, M. Sauvageau, \
				L. Goff, J.L. Rinn & L. Pachter", title="Differential analysis of gene regulation at transcript resolution \
				with RNA-seq", publication="Nature Biotechnology", issue = "31",  pages = "46-53", year = "2013",number="")
	cuffdiff.text = newParagraph("FPKM values calculated by Cufflinks are used as input. The transcript quantification engine of \
				Cufflinks, Cuffdiff",asReference(citeRef$cufflinks)," , is used to calculate transcript expression levels in more than one condition and \
				test them for signficant differences. To identify a transcript as being differentially expressed, Cuffdiff \
				tests the observed log-fold-change in its expression against the null hypothesis of no change (i.e. \
				the true log-fold-change is zero). Because measurement errors, technical variability, and cross-replicate \
				biological variability might result in an observed log-fold-change that is nonzero, Cuffdiff assesses \
				significance using a model of variability in the log-fold-change under the null hypothesis. This model \
				is described in details in Trapnell, Hendrickson " , asEmph("et al.") , asReference(cuffdiff.cite))
	cuffdiff = addTo(cuffdiff, cuffdiff.text)
        diffExp=addTo(diffExp,cuffdiff)
	goseq=newSubSubSection("Step 6 - d: Gene Ontology (GO) analysis of the differential expression results" , exportId="goseq",protection = PROTECTION.PUBLIC) 
	goseq.text=newParagraph("One simple, but extremely widely used, systems biology technique for highlighting biological processes \
				is to look at gene category the show over-representation in the diferential analysis results. In order to perform this analysis, genes are grouped into \
				categories defined by the common biological properties and then tested to find categories that are over represented \
				amongst the differentially expressed genes. Gene Ontology (GO) terms are the most commonly used categories. \
				RNA-seq data suffers from a bias in detecting differential expression for long genes. this means \
				that when using a standard analysis, any category containing a preponderance of long genes will be more \
				likely to show up as being over-represented than a category containing genes of average lengths. Thus the GO analysis \
				is performed using of the goseq R Bioconductor package",asReference(citeRef$goseq)," which provides methods for performing Gene Ontology \
				analysis of RNA-seq data, taking this length bias into account")
	goseq=addTo(goseq,goseq.text)
	#cuffdiff.table.know = 
	diffExp=addTo(diffExp,goseq)
	print("DGE method section ... Done")
	diffExpRes=newSubSection("Step 7: Differential expression analysis - Results", exportId="design",protection = PROTECTION.PUBLIC)
	diffExpRes.text1=newParagraph("The following sections provide the results of the differential gene expresssion and \
				differential transcript expression per design.")
	diffExpRes.text2=newParagraph("The results of the differential gene expression analysis are generated using DESeq and edgeR. Raw read counts \
				generated by HTSeq are used as input. The first column is the gene id, the second column is the gene \
				symbol, the third column is the log2 fold change of the level of expression of each gene, the fourth \
				column is the nominal p-value of the test and the fifth column is the FDR adjusted p-value of the test.")
	diffExpRes.text3=newParagraph("The results of the differential transcript expression analysis are generated using cuffdiff. \
				FPKM values calculated by cufflinks are used as input. The first column is the transcript id, the second \
				column is the gene symbol, the third column is the log2 fold change of the level of expression of each \
				transcript, the fourth column is the nominal p-value of the test and the fifth column is the FDR adjusted \
				p-value of the test.")
	diffExpRes.text4=newParagraph("The results of the differential gene and transcript expression analyses are processed in a gene ontology \
				analysis using goseq. There are two gene ontology analyses per design. The first one is based on the \
				differentially expressed genes from the DESeq/edgeR results. Every gene having an FDR adjusted p-value \
				under 0.05 is considered as differentially expressed. The second analysis is based on the differentially \
				expressed transcripts from the cuffdiff results. The threshold for differentially expressed selection is the same as \
				in the first analysis. The first column is the ID of the category enriched, the second column is the FDR \
				adjusted p-value of category enrichment, the third column is the GO id and the fourth column is a brief \
				description of the GO term. ", asStrong("If there are no results for a particular design, it means the FDR adjusted \
				GO enrichment was not significant (p-value > 0.05)."))
	diffExpRes=addTo(diffExpRes,diffExpRes.text1,diffExpRes.text2,diffExpRes.text3)
    if (!inherits(design.table, "try-error")){
        for ( i in 2:(dim(design.table)[2])) {
        #i=2
    # 		print(paste(colnames(design.table)[2],"results"))
        tmpDesignResSect=newSubSubSection(paste(colnames(design.table)[i],"results"),exportId=paste(colnames(design.table)[i],"results",sep="."),protection = PROTECTION.PUBLIC)
    # 		print(file.path(project.path,"DGE",colnames(design.table)[2],"dge_results.csv"))
        tmpDesignResTab=try(read.csv(file.path(project.path,"DGE",colnames(design.table)[i],"dge_results.csv"),header=T,sep="\t",comment.char = "",check.names=FALSE))
    # 		print(head(tmpDesignResTab))
        if (!inherits(tmpDesignResTab, "try-error")){
            dir.create(file.path(report.path,"DiffExp",colnames(design.table)[i]), showWarnings = FALSE, recursive = TRUE)
            tmpDesignRes.file=file.path("DiffExp",colnames(design.table)[i],paste(colnames(design.table)[i],"Genes_DE_results.tsv",sep="_"))
            write.table(tmpDesignResTab,file.path(report.path,tmpDesignRes.file),sep="\t",quote=F,col.names=T,row.names=F)
            tmpDesignRes.table=newTable(tmpDesignResTab[1:min(6,dim(tmpDesignResTab)[1]),1:min(8,dim(tmpDesignResTab)[2])],"Differential gene expression results" , file = tmpDesignRes.file,exportId=paste(colnames(design.table)[i],"table",sep="."),protection = PROTECTION.PUBLIC)
        }else{
            tmpDesignRes.table=newParagraph("Differential gene expression step was skipped for this design, unable to find results file: " ,file.path(project.path,"DGE",colnames(design.table)[i],"dge_results.csv") ,protection = PROTECTION.PUBLIC)
        }
        tmpGOResTab=try(read.csv(file.path(project.path,"DGE",colnames(design.table)[i],"gene_ontology_results.csv"),header=T,sep="\t",comment.char = "",check.names=FALSE))
        if (!inherits(tmpGOResTab, "try-error")){
            if ( dim(tmpGOResTab)[1] >= 1 ) {
            tmpGORes.file=file.path("DiffExp",colnames(design.table)[i],paste(colnames(design.table)[i],"Genes_GO_results.tsv",sep="_"))
            write.table(tmpGOResTab,file.path(report.path,tmpGORes.file),sep="\t",quote=F,col.names=T,row.names=F)
            tmpGORes=newTable(tmpGOResTab[1:min(6,dim(tmpGOResTab)[1]),1:min(8,dim(tmpGOResTab)[2])],"GO results of the differentially expressed genes" , file = tmpGORes.file, exportId=paste(colnames(design.table)[i],"GOgene",sep="."),protection = PROTECTION.PUBLIC)
            } else {
            tmpGORes=newParagraph(asStrong("No FDR adjusted GO enrichment was significant (p-value < 0.05) based on the differentially expressed gene results for this design."), exportId=paste(colnames(design.table)[i],"GOgene",sep="."),protection = PROTECTION.PUBLIC)
            }
        }else{
            tmpGORes=newParagraph("GO enrichment for diferentially expressed genes step was skipped for this design. Unable to find file ", file.path(project.path,"DGE",colnames(design.table)[i],"gene_ontology_results.csv"), exportId=paste(colnames(design.table)[i],"GOgene",sep="."),protection = PROTECTION.PUBLIC)
        }
        ##tmpDesignResSect=addTo(tmpDesignResSect,tmpDesignRes.table,tmpGORes)
        tmpDesignResTab.file2=file.path("DiffExp",colnames(design.table)[i],"Transcripts_DE_results.csv")
        tmpDesignResTab2=try(formatCuffdiffOutput(file.path(project.path,"cuffdiff",colnames(design.table)[i]),file.path(report.path,tmpDesignResTab.file2)))
        if (!inherits(tmpDesignResTab2, "try-error")){
            tmpDesignResTab.file2=file.path("DiffExp",colnames(design.table)[i],paste(colnames(design.table)[i],"Transcripts_DE_results.csv",sep="_"))
            tmpDesignRes.table2=newTable(tmpDesignResTab2[1:min(6,dim(tmpDesignResTab2)[1]),1:min(8,dim(tmpDesignResTab2)[2])],"Differential transcripts expression results" , file = tmpDesignResTab.file2,protection = PROTECTION.PUBLIC)
        }else{
            tmpDesignRes.table2=newParagraph("Differential transcript expression step was skipped for this design. Unable to find results files ", exportId=paste(colnames(design.table)[i],"DGEtranscript",sep="."),protection = PROTECTION.PUBLIC)
        }
#         tmpGOResTab2=try(read.csv(file.path(project.path,"cuffdiff/known",colnames(design.table)[i],"gene_ontology_results.csv"),header=T,sep="\t",comment.char = "",check.names=FALSE))
#         if (!inherits(tmpGOResTab2, "try-error")){
#             if ( dim(tmpGOResTab2)[1] >= 1 ) {
#                 tmpGORes.file2=file.path("DiffExp",colnames(design.table)[i],paste(colnames(design.table)[i],"Transcripts_GO_results.tsv",sep="_"))
#                 write.table(tmpGOResTab2,file.path(report.path,tmpGORes.file2),sep="\t",quote=F,col.names=T,row.names=F)
#                 tmpGORes2=newTable(tmpGOResTab2[1:min(6,dim(tmpGOResTab2)[1]),1:min(8,dim(tmpGOResTab2)[2])],"GO results of the differentially expressed transcripts" , file = tmpGORes.file2,exportId=paste(colnames(design.table)[i],"GOtranscript",sep="."),protection = PROTECTION.PUBLIC)
#             } else {
#             tmpGORes2=newParagraph(asStrong("No FDR adjusted GO enrichment was significant (p-value < 0.05) based on the differentially expressed transcript results for this design."))
#             }
#         }else{
#             tmpDesignRes.table2=newParagraph("GO enrichment of differentially expressed transcripts step was skipped for this design. Unable to find file ", file.path(project.path,"cuffdiff/known",colnames(design.table)[i],"gene_ontology_results.csv"), exportId=paste(colnames(design.table)[i],"GOtranscript",sep="."),protection = PROTECTION.PUBLIC)
#         }
        tmpDesignResSect=addTo(tmpDesignResSect,tmpDesignRes.table2,tmpDesignRes.table,tmpGORes)
        diffExpRes=addTo(diffExpRes,tmpDesignResSect)
        }   
    }
    cufflinksGeneraleOutput=newSubSubSection("Transcriptome Assembly with Cufflinks", exportId="Cuffgeneral")
    cuffFiles=c(file.path("cufflinks",list.files("cufflinks", recursive=T)),file.path("cuffdiff",list.files("cuffdiff",recursive=T)),file.path("cuffnorm",list.files("cuffnorm",recursive=T)))
    cuffArchive.file="cuffAnalysis.zip"
    try(zip(file.path(report.path,cuffArchive.file),cuffFiles))
    if (file.exists(file.path(report.path,cuffArchive.file))) {
        cuffGeneral.intro=newParagraph("In order to provide the most complete analysis results the entire set of cufflinks files are provided in the following archive:")
        cuff.additional=newParagraph(asStrong(asLink(url=cuffArchive.file,"Complete Cuff suite analysis files are available here")))
        cuffGeneral.text=newParagraph("A reference-based transcript assembly was performed, which allows \
                            the detection of known and novel transcripts isoforms. Transcript assembly is accomplished using Cufflinks",asReference(citeRef$cufflinks),".")
        cuffGeneral.text2 = newParagraph("Cufflinks constructs a parsimonious set of transcripts that \"explains\" the reads observed in an RNA-Seq experiment. \
                            The assembly algorithm explicitly handles paired-end reads by treating the alignment for a given pair as a single object in the \
                            covering relation. Cufflinks tries to find th emost parsimonious set of transcripts by performing a minimum-cost maximum matching. \
                            Reference annotation based assembly seeks to build upon available information about the transcriptome of an organism to find \
                            novel genes and isoforms. When a reference GTF is provided, the reference transcripts are tiled with faux-reads that will aid \
                            in the assembly of novel isoforms. These faux-reads are combined with the sequencing reads and are input into the regular \
                            Cufflinks assembler. The assembled transfrags are then compared to the reference transcripts to determine if they are \
                            different enough to be considered novel. Individual results of these assemblies are available in the cufflinks/ folder in the archive.")
        cuffGeneral.text3 =  newParagraph("Cuffmerge was then used to merge all assemblies gtf to a single one (cufflinks/AllSamples/merged.gtf). ")
        cuffGeneral.text4 =  newParagraph("The resulting merged assembly gtf file was used as a reference to estimate the abundance of each transcript and to subsequently perform a differential analysis for each design as provided in the design file using cuffdiff. The entire set of result files for the cuffdiff analysis is provided in the cuffdiff folder of the archive.")
        cuffGeneral.text5 =  newParagraph("As cuffdiff generates normalized data using only the sample implicated in the design comparison, we additionally run the cuffnorm tool to generate a normalized data set that includes all the samples. The resulting tables of normalized expression valuesare provided in the cuffnorm folder of the archive.")   
        cufflinksGeneraleOutput=addTo(cufflinksGeneraleOutput,cuffGeneral.intro,cuff.additional,cuffGeneral.text,cuffGeneral.text2,cuffGeneral.text3,cuffGeneral.text4,cuffGeneral.text5)
    } else {
        cufflinksGeneraleOutput=addTo(cufflinksGeneraleOutput,newParagraph("Cuff suite analysis was skiped."))
    }
    diffExpRes=addTo(diffExpRes,cufflinksGeneraleOutput)

    print("cuff suite subsection ... Done")
    ##bibliography section
    biblio=newSection( "References" )


    ### Build sections and report
    intro = addTo(intro,intro.text1,intro.text2)
    stepRes= addTo(stepRes,trimNalign,wiggle,fpkm,metrics,diffExp,diffExpRes)

    biblio=addTo(biblio,citeRef$trimmomatic,citeRef$STARaligner,citeRef$cufflinks,citeRef$deseq,citeRef$edger,citeRef$goseq,cuffdiff.cite,dge.cite)

    report= addTo(report,intro,stepRes,biblio)
    return(report)
}
