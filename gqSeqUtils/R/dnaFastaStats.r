#' A simple function that leverages Biostrings to generate a bunch of stats from dna FASTA files.
#' 
#' @param filename The input fasta file (not vectorized for now)
#' @param type generic, genome or trinity. Not a lot of differences for now
#' @param output.prefix path+filename prefix to append to output files
#' @param write.stats Boolean, should a csv and pdf of stats be written out
#'
#' @export
dnaFastaStats = function(filename, type = c("generic", "genome", "trinity"), output.prefix = 'stats', write.stats = TRUE) {
  type = match.arg(type)
  # module load mugqic_dev/R ;cd /mnt/parallel_scratch_mp2_wipe_on_august_2014/bourque/bourque_group/projects/jfillon_pipelines/rnaseq_denovo/fclark_lobster_permethrin_denovo_PRJBFX-416/rnaseq_denovo/dev/DeNovo/results/trinity_out_dir; R ;
  # library(gqSeqUtils); type = 'trinity' ; filename = 'Trinity.fasta' ; output.prefix = "test" ; write.stats = TRUE

  # Read sequence
  s = readDNAStringSet(filename)
	s.width  = as.numeric(width(s)) # do this upfront to avoid integer overflow
  
  # Length summary statistics
  s.summary = unlist(as.list(summary(s.width,digits=Inf)))
  names(s.summary) = paste0(names(s.summary), " Sequence Length (bp)")
  
  # Length NXX statistics
  s.width.ordered = rev(sort(s.width))
  cum.fraction = cumsum(s.width.ordered) / sum(s.width.ordered)
  s.N = sapply(as.character(seq(10, 90, by = 10)), function(p) {
    s.width.ordered[which(cum.fraction >= as.numeric(p) / 100)[1]]
  }, simplify = TRUE)
  names(s.N) = paste0('N', names(s.N), " (bp)")
  
  # alphabet Frequencies
  s.af = alphabetFrequency(s)
  s.af.total.perc = apply(s.af, MARGIN = 2, sum) / sum(s.width) * 100
  names(s.af.total.perc) =  paste0("Total ", names(s.af.total.perc), " %")
  
  # N-content distribution
  s.n.summary  = unlist(as.list(summary(s.af[, "N"] / s.width * 100,digits=Inf)))
  names(s.n.summary) = paste0(names(s.n.summary), " Sequence N %")
  
  # GC-content
  # s.gc = letterFrequency(s, letters = c("GC")) / s.width * 100
  
  # Generic statistics
  stats = c(
    "Nb. Sequences"           = length(s),
    "Total Sequences Length (bp)"  = sum(s.width),
    #"N50"                     = N50(s.width),
    s.summary,
    "Nb. Duplicate Sequences" = sum(duplicated(s)),
    s.N,
    s.n.summary,
    s.af.total.perc
  )

  # Axis labels for histogram plot
  xlabel = "Sequence Length (bp, log10 scale)"
  ylabel = "Sequence Count"

  if (type == 'trinity') {
    names(stats) = gsub("Sequence", "Transcript", names(stats))
    xlabel = gsub("Sequence", "Transcript", xlabel)
    ylabel = gsub("Sequence", "Transcript", ylabel)
    # Insert "Nb. Components" as second column
    stats = c(stats[1], "Nb. Components" = length(unique(str_extract(names(s), "(^.*comp[0-9]+_c[0-9]+)|(^.*c[0-9]+_g[0-9]+)"))), stats[2:length(stats)])
  }
  if (type == 'genome') {
    names(stats) = gsub("Sequence", "Contig", names(stats))
    xlabel = gsub("Sequence", "Contig", xlabel)
    ylabel = gsub("Sequence", "Contig", ylabel)
  }
  
  # ## Sliding window frequency content
  # plot ( letterFrequencyInSlidingView(largest.contig , view.width = 1000, c("GC"))[, 1] / 1000, ylab = "GC content")
  # ## dinucl. freqs
  # head(dinucleotideFrequency(assembly))

  if (write.stats) {
    # Create CSV stats file
    write.csv(data.frame(stats), file = paste0(output.prefix, ".csv"))

    # Create histogram plot of sequence length distribution with N50
    sequence.lengths = ggplot(data.frame(length = s.width), aes(x = length))
    sequence.lengths = sequence.lengths +
                       geom_histogram(colour = "darkgrey", fill = "white", binwidth = 0.02) +
                       scale_x_log10(        
                       breaks = c(200, 500, 1000, 2000, 5000, 10000, 20000, 50000)) +
                       geom_vline(
                         data = data.frame(Legend = paste0("N50 = ", stats["N50 (bp)"], " bp"), vals = stats["N50 (bp)"]),
                         aes(xintercept = vals, shape = Legend),
                         colour = "black",
                         show_guide = TRUE) +
                       xlab(xlabel) +
                       ylab(ylabel) +
                       ggtitle("Contig Length Distribution") +
                       theme(plot.title = element_text(face = "bold"), legend.title = element_blank())

    # Create PDF file of stats + histogram plot
    pdf(file = paste0(output.prefix, ".pdf"), height = 5, width = 7)
    textplot(data.frame("Statistic" = format(stats,
                                             digits = 2,
                                             big.mark = ",",
                                             scientific = FALSE,
                                             drop0trailing = TRUE)))
    suppressWarnings(print(sequence.lengths))
    dev.off()

    # Create JPEG image of histogram plot
    jpeg(file = paste0(output.prefix, ".jpg"), height = 500, width = 700)
    sequence.lengths = sequence.lengths +
                         theme(plot.title = element_text(size = 16),
                               axis.title = element_text(size = 16),
                               axis.text = element_text(size = 14),
                               legend.text = element_text(size = 16))
    print(sequence.lengths)
    dev.off()
  }

  return(stats)
}

