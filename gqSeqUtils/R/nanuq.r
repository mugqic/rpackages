# Here functions related to parsgin data from Nanuq
# TODO: move us to the diff. packages or drop that idea and move everything to essentially a few packges?
# TODO: some data/ folder with readsets.txt??



#' This function is necessary until the day they start using unique identifiers reported by nanuq (file prefix?)
#'
#' @export
locateIllmnSeqFastq <- function(run,lane,idx,base.dir,sequencer,prefix=NA)
{

	#  

	# run=c('0666','0331');lane=c('5','1');idx=c('Index_20','Index-2');base.dir=fastq.base.dir;sequencer=names(fastq.base.dir);prefix=NA
	#require(plyr)
	df = data.frame(run,lane,idx,'desc'=paste(run,lane,idx),'base.dir'=unname(base.dir),sequencer,stringsAsFactors=FALSE)
	df = adply(df, 1, within,{

		run.dir = list.files(base.dir,full.names=TRUE)

		if(sequencer=='hiseq')
		{
			#print(run.dir)
			run.dir = run.dir[grepl(paste0('_',run,'HS'),run.dir) ]  # PARSING RULE HERE
		}
		if(sequencer=='miseq')
		{
			#print(run.dir)
			run.dir = run.dir[grepl(paste0('_',run,'_'),run.dir) ]  # PARSING RULE HERE
		}
		
		run.dir = run.dir[!grepl('^old', basename(run.dir))] # excludes old_ runs
		if(length(run.dir)!=1){stop(paste('Could not resolve run folder unambigously for run',run))}
		run.dir = file.path(run.dir,'sequences')
		if(!file.exists(run.dir)){stop(paste('subdir sequences does not seem to exist for run',run))}
		paths = list.files(run.dir)



		if(idx==''){idx.pattern=''}else{idx.pattern=paste0(idx,'\\.')} # because sometimes unindexed
		paths = paths[
			# format: "HI.0666.008.Index_8.gDNA-CA488-COV-NGS-1_R2.fastq.gz"
			grepl(paste0('^(HI|MI)\\.','0*',run,'\\.','0*',lane,'\\.',idx.pattern),paths) |
			# format: 8_Index_7.TashT_R2.fastq.gz
			grepl( paste0('^',lane,'_',gsub('(_|-)','.',idx.pattern) ),paths)	
		]  # PARSING RULE HERE
		paths = file.path(run.dir,paths)
		rm(idx.pattern)
		paths = paths[grepl('fastq.gz$',paths)] # md5 sometimes
		n.paths = length(paths)
		if(! n.paths %in% c(1,2)){warning(paste('Found',n.paths,'fastqs for',desc))}
		paths = list(paths)	

	

	})
	warning('There is (yet) no unambigous way to link from a read set in the LIMS nanuq to the corresponding fastq files on disk (e.g. using a unique identifier for read sets). An awkward combination of run, region, index and stock sample name is used to name files. Regular expression matching is therefore necessary to track down fastq files, but this is not 100% safe and should be double checked manually.')
	return(df)
}# TODO: guessing based on regex is really not ideal and prone to errors. They need to setup unique ids for read sets



#locateHiSeqFastq(run=c('M00833_0039'),lane='1',idx='Index_10',base.dir='/lb/robot/miSeqSequencer/miSeqRuns',sequencer='miseq',prefix=NA)



#' This function initialises a project.
#' 
#' @param nanuq.file Path to the nanuq readset csv file
#' @param nanuq.sequencer Will tell the function what kind of sheet it is (HiSeq, MiSeq).
#' @param nanuq.fastq.links.dir if fastq.base.dir is specified, this is where the links will be written
#' @param legacy.fastq.links [not implemented] if TRUE, symbolic links will follow convention to nest within run/sample etc. and rename to pair1, pair2. It seems much preferenable to one day have info about sample in a table instead of encoded within nested dir names and file names
#' @param legacy.design path to design file
#' @param overwrite.sheets Whether or not samples.csv and readsets.csv should be overwriten if they already exist
#' @param project.path 
#' 
#' @export
initIllmSeqProject <- function(
		 nanuq.file=NULL
		,link.fastqs = FALSE
		,sequencer=NULL
		,fastqs.links.dir = 'raw_reads'
		,fastqs.legacy.linking=TRUE
		,legacy.design=NULL
		,grid.fastqs.base.dir = c('hiseq'="/lb/robot/hiSeqSequencer/hiSeqRuns",'miseq'="/lb/robot/miSeqSequencer/miSeqRuns")
    ,overwrite.sheets = TRUE
    ,project.path=getwd())
{

	# nanuq.file="nanuq.csv";fastqs.links.dir = 'raw_reads';fastqs.legacy.linking=TRUE;legacy.design=NULL;grid.fastqs.base.dir = c('hiseq'="/lb/robot/hiSeqSequencer/hiSeqRuns",'miseq'="/lb/robot/miSeqSequencer/miSeqRuns");sequencer='miseq'
	#require(stringr)
	#require(plyr)
	#options(stringsAsFactors=FALSE)

	if(!is.null(nanuq.file))
	{
	
		# Read the nanuq sample sheet
		nsheet = read.csv(file=nanuq.file,stringsAsFactors=FALSE,comment.char=''
				,colClasses='character', check.names=FALSE)
		colnames(nsheet) = paste0('nanuq.',colnames(nsheet))


		# Read set sheet with Controlled name columns
		rs = data.frame(
		 "ReadSetID"	= nsheet[['nanuq.Read Set Id']]
		,"ID"		= paste(
						nsheet[['nanuq.Run']],
						nsheet[['nanuq.Region']],
						nsheet[['nanuq.Multiplex Key',exact=FALSE]]
						,sep='_') # TEMP...
		,"SampleID"	= nsheet[['nanuq.Name']]
		,"LibraryID"	= nsheet[['nanuq.Library Barcode']]
		,"RunID"	= nsheet[['nanuq.Run']]
		,"Lane"		= nsheet[['nanuq.Region']]
		,"Index"	= nsheet[['nanuq.Multiplex Key',exact=FALSE]]
		,"Adapater1"    = nsheet[,grepl('Adaptor Read 1',colnames(nsheet))] 
		,"Adapater2"    = nsheet[,grepl('Adaptor Read 2',colnames(nsheet))] 
		,nsheet , check.names=FALSE,stringsAsFactors = FALSE)
		rownames(rs) 	= rs[['ReadSetID']]


		if(link.fastqs )
		{
			# Determine the fastq base dir (HARD CODED TO ABACUS)
			fastq.base.dir = grid.fastqs.base.dir
			if(! sequencer %in% names(fastq.base.dir )){stop(paste0('sequencer must be ',paste(names(fastq.base.dir),collapse=',')))}
			fastq.base.dir = fastq.base.dir[sequencer]
		
			# Locate Fastq files (blargh)
			fastqs = locateIllmnSeqFastq(rs[['RunID']],rs[['Lane']],rs[['Index']],base.dir=fastq.base.dir,sequencer=names(fastq.base.dir))
			rownames(fastqs) =  paste(fastqs[['run']],fastqs[['lane']],fastqs[['idx']],sep='_') 
			colnames(fastqs) = paste0('fastqs.',colnames(fastqs))
			rs = cbind(rs,fastqs[rs[['ID']],])
		
			# Rename columns to relfect the fact that we are working on sym links
			colnames(rs)[colnames(rs)=='fastqs.run.dir'] = 'fastqs.grid.run.dir'
			colnames(rs)[colnames(rs)=='fastqs.paths'] = 'fastqs.grid.paths'

			# Symlink
			rs[['fastqs.links.dir']] = fastqs.links.dir
			rs[['fastqs.legacy.linking']] = fastqs.legacy.linking
			rs = adply(rs, 1, within,{
				if(fastqs.legacy.linking) # esti de gossage inutile de nesting
				{
					fastqs.paths = basename(fastqs.grid.paths[[1]])
					fastqs.paths = gsub('.*R1.fastq.gz'
						,paste(nanuq.Name,get('nanuq.Quality Offset'),'pair1.fastq.gz',sep='.')
					,fastqs.paths)
					fastqs.paths = gsub('.*R2.fastq.gz'
						,paste(nanuq.Name,get('nanuq.Quality Offset'),'pair2.fastq.gz',sep='.')
					,fastqs.paths)
					fastqs.paths = file.path(
						 fastqs.links.dir
						,nanuq.Name
						,paste0('run',nanuq.Run,'_',nanuq.Region)
						,fastqs.paths)
				}else{
					fastqs.paths = 	file.path(fastqs.links.dir,basename(fastqs.grid.paths[[1]]))
				}			
				suppressWarnings(file.remove(fastqs.paths))
				dir.create(unique(dirname(fastqs.paths)),
						showWarnings=FALSE,recursive = TRUE)
				file.symlink(fastqs.grid.paths[[1]] , fastqs.paths)
				fastqs.paths = list(fastqs.paths)
			})
		}

	}else{ # Create a blank dummy sample sheet
		rs = data.frame(
		 "ReadSetID"	= c('rs1','rs2','rs3')
		,"ID"		= ''
		,"SampleID"	= c('s1','s2','s2')
		,"LibraryID"	= ''
		,"RunID"	= ''
		,"Lane"		= ''
		,"Index"	= ''
		,"Adapater1"    = '' 
		,"Adapater2"    = '')
		rownames(rs) 	= rs[['ReadSetID']]
	}


	## Initialize read set and sample sheets

	# Let's first initialize or read the legacy design
	if(!is.null(legacy.design))
	{
		if(!file.exists(legacy.design)){stop('Legacy design file not found')}
		design = read.delim(legacy.design,colClasses='character'
				,check.names=FALSE,comment.char='',quote='')
	}else{
		design = data.frame('sample'=unique(rs[['SampleID']]) 
				,'Design1'=0) # a blank example
	}
	rownames(design) = design[,1]
	design = design[,-1,drop=FALSE]
	colnames(design)  = paste('.groupComp',colnames(design),sep='_')
	design = design[unique(rs[['SampleID']]),,drop=FALSE]

	# sample table with controlled column name
	s = data.frame(
		 'SampleID'= unique(rs[['SampleID']])
		,'AssemblyGroup'='','DonorID'='','Var1'='','Var2'='','Var3'='','VarN'=''
		,'outlierFlag'='','batch'='','Comment'='ok'
		,design,'Note'=''	
	, check.names=FALSE,stringsAsFactors = FALSE)
	rownames(s) = s[['SampleID']]

	# Write to masked formats
  if( ! file.exists( file.path(project.path,"samples.csv")    ) | overwrite.sheets )
  {
  	writeMaskedTable(s,fn=file.path(project.path,"samples.csv"))
  	for(i in which(sapply(rs,is.list)) )# collapse list columns with ":" as a separator
  	{
  		rs[,i] =   sapply(rs[,i],function(x) paste(unlist(x,recursive=TRUE), collapse=':')  )
  	}
  	writeMaskedTable(rs,fn=file.path(project.path,"readsets.csv"))
  }
}





