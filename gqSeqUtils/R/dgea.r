
#' This function figures out whether a design has degrees of freedom left for overdispersion estimations
#' just design matrix for the moment
#'
#' @export
isDesignReplicated <- function(obj)
{
	nrow(obj) >  ncol(obj)	
}
# TODO: move to gqUtils + cleanup gqUtils of all those old seq functions


#' Retrive samples associated to contrass from contrast matrix and design matrix
#'
#' @export
retrieveInvolvedSamples <- function(design,cont.matrix)
{
	x = ( ( 1*(design!=0) ) %*% (1*(cont.matrix!=0)) ) != 0
	sapply(colnames(x),function(cont){  rownames(x)[x[,cont]]    },simplify=FALSE)
		
}
# TODO: move to gqUtils	



#' This central edgeR  function 
#'
#' This function accepts an eset, a design and a contrast matrix. It then fits an edgeR model and returns 
#' a list object of results. Those results are formatted in different convenient ways.
#' 
#' @title Fit edgeR dispersion model to count data
#' @param eset An ExpressionSet object of rw read counts
#' @param design design matrix. rownames(design) should equal sampleNames(eset)
#' @param coefs cont.matrix. Contrasts formulas. Overrides cont.matrix
#' @param variables Optional, but if given, function will create the 'replication' table
#' @param ... arguments to pass down to estimateDisp()
#' @return A list with: edgeR fit, top tables, eset, design, cont.matrix, etc.
#' @keywords edgeR
#' @author Lefebvre F
#' @seealso ...
#' @note Assumes replicates are uncorrelated. Corr. replicates should be averaged beforehand.
#' @references ...
#' 
#' @export
dgeaEdgerFit <- function(counts, design, coefs, ngenes.adjust.method="BH", ngenes.p.values=c(0.05,0.10,0.25), prior.df=NULL  )
{

	# library(gqUtils);library(gqSeqUtils);library(NBPSeq);  data(arab); counts = new('ExpressionSet',exprs=arab);f=factor(gsub('.$','',sampleNames(counts))); design = model.matrix(~-1+f); rownames(design)=sampleNames(counts);coefs = c('fhrcc-fmock','fmock-fhrcc') ;count.adjust.method="BH"; count.p.values=c(0.05,0.10,0.25)

	### Re-format the count object to ExpressionSet
	if(is(counts,'matrix') | is(counts,'data.frame'))
	{
		counts = ExpressionSet(assayData=as.matrix(counts))
	}
	if(!is(counts,'ExpressionSet') )
	{	
		stop('counts must be either an ExpressionSet, matrix or data.frame')
	}

	# To ensure uniform behavior of top table, make sure fData and pdata is populated
	if(length(fvarLabels(counts))==0)
	{
		fData(counts)[['featureID']] = featureNames(counts)
	}
	if(length(varLabels(counts))==0)
	{
		pData(counts)[['SampleID']]  = sampleNames(counts)
	}

	# Checks on design
	if(!identical(sampleNames(counts),rownames(design))){
		stop('rownames(design) do not match colnames(counts)')
	}

	# Build and check the contrast matrix from formulas if cont.matrix not specified
	cont.matrix = makeContrasts(contrasts = coefs, levels = design)
	if(!identical(colnames(design),rownames(cont.matrix)))
	{
		stop('colnames(design) do not match rownames(cont.matrix). Check coefs argument.')
	}
	
	# EdgeR required container
	y 	= DGEList(counts=exprs(counts),genes = fData(counts)) 

	# TMM normalize
	y 	= calcNormFactors(y) 

	# From the design matrix, detect whether the linear model has df>0
	replicated = isDesignReplicated(design)

	# Estimate dispersions
	if(replicated){
		 y 	= estimateGLMCommonDisp(y, design)
		 y 	= estimateGLMTrendedDisp(y, design)
		 if( is.null(prior.df) )
		 {
			 y 	= estimateGLMTagwiseDisp(y, design)
		}else{
			 y 	= estimateGLMTagwiseDisp(y, design, prior.df = prior.df)		
		}
		#y = estimateDisp(y,design, ... ) 
		fit	= glmFit(y, design)
	}else{
		fit	= glmFit(y, design, dispersion=0) # Poisson...	
	}

	# We will use the fit objects as a container. Store non-saved objects
	fit$counts.eset 		= counts
	fit$ngenes.adjust.method	= ngenes.adjust.method
	fit$ngenes.p.values 		= ngenes.p.values
	fit$y				= y
	fit$cont.matrix 		= cont.matrix
	fit$replicated 			= replicated
	fit$coefs 			= coefs


	# Likelihood ratio tests
	fit$lrts = sapply(colnames(cont.matrix),function(coef)
			{
				lrt 	= glmLRT(fit, contrast = cont.matrix[,coef,drop=TRUE]   )
			},simplify=FALSE) 
	
	# EdgeR top tables
	fit$TopTags 	= lapply(fit$lrts,function(lrt)
	{
		top 			= topTags(lrt, n=Inf, adjust.method="BH", sort.by="p.value") # we enforce BH fdr here
		top$table[['FC']] 	= toFold(top$table[['logFC']])
		top$table[['Rank']] 	= 1:nrow(top$table) # having the rank is always useful
		top
	})
	
	# Involved arrays in each contrasts
	fit$involved.samples = retrieveInvolvedSamples(design,cont.matrix) 

	
	# We want to unnest pvalues, FDR, etc. Annoying that glmLRT does not accept matrix - yet
	for( el in c("logFC", "logCPM", "LR", "PValue", "FDR", "FC","Rank") )
	{
		fit[[el]] = do.call( cbind ,  sapply(fit$TopTags, function(top)
				{
					top = top$table[rownames(fit),el,drop=FALSE]
				},simplify=FALSE)  )
		colnames(fit[[el]]) = names(fit$TopTags)
	}
	fit[['avg_logCPM']] = fit[['logCPM']] # to solve name conflict with per-sample logCPM

	# per sample logCPM
	fit[['logCPM']] = cpm(y,normalized.lib.sizes=TRUE, log=TRUE)
	fit[['logCPM.eset']] = new('ExpressionSet'  # Sample as previously but stored as an ExpressionSet
		, exprs		= fit[['logCPM']]
		, phenoData	= new('AnnotatedDataFrame', pData(fit$counts.eset) )
		, featureData	= new('AnnotatedDataFrame',fData(fit$counts.eset)  ) )


	# Number of DE genes
	fit$ngenes = sapply(fit$lrts, function(lrt)
		{
			co = sapply(ngenes.p.values,function(p.value)
			{
		        	dt = decideTestsDGE(lrt, adjust.method=ngenes.adjust.method, p.value=p.value) 
				list('Up'= sum(dt==1),'Down'= sum(dt==-1),'Both'=sum(dt!=0))
			},simplify=TRUE)
			colnames(co) = ngenes.p.values; t(co)
		},simplify=FALSE)
		

	return(fit)
}
# TODO:
# the object becomes quite large... redundancy. You've been lazy. Maybe define a class with accessors
# Find a way to allow for testing of multiple effects in the model:
# All contrasts (F-test like)
#	fit$lrt.all = glmLRT(fit,contrast=cont.matrix)
#	fit$TopTags.all = topTags(fit$lrt.all, n=Inf, adjust.method="BH", sort.by="p.value") 




#' A function to generate the design, coefs from pdata,variables,factor1,factor2,bs1,bs2 OR all pw comps?
#' @param ... arguments to pass down to estimateDisp()
#' @export
dgeaEdgerAnalysis <- function(eset, variables, analysis.type='manual.manual' 
			,factor1=NULL,base.level.1=NULL,factor2=NULL,base.level.2=NULL
			,design=NULL, coefs=NULL, ngenes.adjust.method="BH", ngenes.p.values=c(0.05,0.10,0.25), prior.df=NULL  )
{
	

	# eset=e; variables=v; analysis.type='ind.pw'; factor1='Treat'; base.level.1='m'; factor2='Tissue'; base.level.2='spleen'; design=NULL; coefs=NULL; ngenes.adjust.method="BH"; count.p.values=c(0.05,0.10,0.25)

#eset=e; variables=v[1]; analysis.type='ind.pw'; factor1='Treat'; base.level.1='m'; factor2=NULL; base.level.2=NULL; design=NULL; coefs=NULL; ngenes.adjust.method="BH"; ngenes.p.values=c(0.05,0.10,0.25)


	# Check analysis.type
	avail.types = c('manual.manual','ind.manual','ind.pw','ind.1wpw','ind.1wbl','ind.2wbl') #,'ind.2wpw'
	if(! analysis.type %in% avail.types) {stop(paste0('Known types are: ',paste(avail.types,collapse=', ')))}

	# Check variables
	if(any(! variables %in% varLabels(eset)    )){stop('variables must be in varLabels(eset)')}

	if(analysis.type=='manual.manual')
	{
		if( is.null(design)|is.null(coefs) )
		{
			stop('analysis.type manual.manual requires design matrix and coefs specified')
		}	
	}else{ # else, Class parameterization
		
		Class 	= factor(apply(pData(eset)[, variables, drop = FALSE],  MARGIN = 1,
				 function(ro) paste(ro, collapse = "_")))
	      	design  = model.matrix(~-1 + Class )
		colnames(design) = gsub("^Class", "", colnames(design))
		rownames(design) = sampleNames(eset)

		if(analysis.type=='ind.manual') # This is the case where we want to use the Class parameterization but want specific contrasts
		{
			if(is.null(coefs)){stop('ind.manual needs coefs specifed')}
		}
		if(analysis.type=='ind.pw')
		{
			
			coefs = pairwise.comparisons(colnames(design))
		}
		if(analysis.type=='ind.1wpw')
		{
			if(is.null(factor1)){stop('factor1 must be defined for analysis.type ind.1wpw')}
			coefs = sapply(unique(pData(eset)[[factor1]]),
				function(bl) dgANOVAContrasts(eset,variables,factor1,bl,unlist=TRUE) )
			coefs = coefs[coefs %in% pairwise.comparisons(colnames(design)) ]
		}
		if(analysis.type %in% c('ind.1wbl'))
		{
			if(is.null(factor1) | is.null(base.level.1))
				{stop('factor1 and base.level.1 must be defined for analysis.type ind.1wbl')}
			coefs = dgANOVAContrasts(eset, variables
				,f1 = factor1, f1.baseline = base.level.1, sep = "_", unlist=TRUE) 
		}
		if(analysis.type %in% c('ind.2wbl'))
		{
			if(is.null(factor1) | is.null(base.level.1)|is.null(factor2) | is.null(base.level.2))
			{stop('factor1, base.level.1,factor2, base.level.2 must all be defined for analysis.type ind.2wbl')}
			coefs = dgANOVAContrasts(eset, variables
				,f1 = factor1, f1.baseline = base.level.1
				,f2 = factor2, f2.baseline = base.level.2, sep = "_", unlist=TRUE) 
		}
	}
	fit = dgeaEdgerFit(counts=eset, design=design, coefs=coefs, ngenes.adjust.method=ngenes.adjust.method, 
			ngenes.p.values=ngenes.p.values,  prior.df = prior.df )

	# Store analysis parameters
	fit$analysis.params = list(
			 'variables'	= variables
			,'analysis.type'= analysis.type
			,'factor1'	= factor1
			,'base.level.1'	= base.level.1
			,'factor2'	= factor2
			,'base.level.2'	= base.level.2
	)

	# Create a table counting the replication
	Replication = pData(eset)[,variables,drop=FALSE]
	Replication[, sapply(Replication,is.numeric) ]  = 'Numeric' # handle numeric variables
	Replication = as.data.frame(melt(table(Replication))) # this counts
	Replication = Replication[order(Replication$value, decreasing = TRUE),  ] # sort
	colnames(Replication)[ncol(Replication)] = 'Nb. Samples'
	if(ncol(Replication)==2){colnames(Replication)[1]=variables}
	fit$Replication = Replication




	return(fit)

}










#' This function simply accepts a fit object as returned by dgeaEdgerAnalysis() and generates
#' top genes heatmaps.
#'
#'
#' @export
dgeaEdgerWriteTopHeatMaps <- function(
		  obj
		, output.dir 		= 'heatmaps'
		, number 		= 50
		, fdr			= 1
		, p.value		= 0.01
		, lfc			= log2(1)
		, row.labels		= NULL
		, remove.dup.row.labels = FALSE
		, col.labels		= NULL
		, cols.ann		= obj$analysis.params$variables
		, ... )
{ 
# obj=fit; output.dir = 'heatmaps'; number= 50;fdr= 1;p.value= 0.01;lfc=0;row.labels=NULL;remove.dup.row.labels = FALSE; col.labels	= NULL;cols.ann	= obj$analysis.params$variables;

	# Create dir
	dir.create(output.dir , showWarnings=FALSE, recursive=TRUE )
	
	# Initialize an eset from the logCPM
	e = obj[["logCPM.eset"]]

	# Iterate over coefs
	for( coef in obj[['coefs']] )
	{
		i  = which( obj[['coefs']]  %in% coef )
		fn = file.path(output.dir, paste0(i,'_top_heatmap.pdf') )# the output file
		print(coef)
		print(i)

		# Select samples
		involved.samples = obj$involved.samples[[coef]]

		# Select features
		top = obj$TopTags[[coef]]$table
		top = top[top$FDR<=fdr & top$PValue<=p.value & abs(top$logFC)>=lfc ,]
		top = top[1:min(number,nrow(top))  ,]
		
		if(nrow(top)<2)
		{
			warning("Cannot plot heatmap. Need at least 2 rows and two columns ")
            		pdf(file = fn , height = 2, width = 2)
           		plot.new()
           		dev.off()
		}else{

			# if only two cols, don't scale
			if(length(involved.samples)>2){scale='row'}else{scale='none'}
			
		 	pheatmap.eset(e=e, file = fn, which.rows = rownames(top), 
	    			 row.labels = row.labels, remove.dup.row.labels = remove.dup.row.labels, 
	   			 which.cols = involved.samples, col.labels = col.labels,
				 cols.ann = cols.ann,  scale = scale) 
		}
	}		
}

		




#' This function write the analysis output file
#'
#
#' @export
dgeaEdgerWriteAnalysisOutput <- function(obj, output.dir=getwd(),fn='analysis_output.csv', ret=FALSE, features.subset = rownames(obj) )
{ 
	# obj=fit
	# Contrast specific columns
	dat = do.call( cbind ,lapply(c('PValue','FDR',"logFC",'FC','Rank','counts'),function(coltype)
	{
		mat = obj[[coltype]]
		colnames(mat) = paste(coltype,colnames(mat),sep='.')
		return(mat)
	}) )

	# Append Annotation
	dat = cbind(dat,obj$genes)

	# Write 
	write.csv(dat[features.subset,],file=file.path(output.dir,fn),row.names=FALSE)

	# return
	if (ret){ return(dat) }
}







#' This function write out the top tables for each coefficient
#'
#' @param front.cols Columns  in obj$genes that will be put first in the table. If null, nothing is moved around
#' @param eg.link Will format the columns specifed as hyperlink to entrez gene, and append them to front.cols.
#' a better practice is to provide species specific links with the feat annotations upfront.
#' @export 
dgeaEdgerWriteTopTables <- function(obj, output.dir='top_tables', front.cols=NULL, p.value=0.05, eg.link=NULL)
{
	# obj = fit ; output.dir='top_tables' ;  front.cols=NULL;  p.value=0.05; eg.link='featureID'

	# Iterate over coefs
	for( coef in obj[['coefs']] )
	{
		i  = which( obj[['coefs']]  %in% coef )
	
		# Select samples
		involved.samples = obj$involved.samples[[coef]]

		# Select features
		top = obj$TopTags[[coef]]$table

		# Append counts
		raw = obj$counts[rownames(top),involved.samples ];
		colnames(raw) = paste('counts',colnames(raw),sep='.')
		top =  cbind(top,raw) 

		# eg.link
		if(!is.null(eg.link))
		{
			for(col in eg.link)
			{
				top[[col]] = paste0("<a href=\"http://www.ncbi.nlm.nih.gov/gene/?term="
					,top[[col]],"\">",top[[col]],"</a>")  
			}
			front.cols = unique(c(front.cols,eg.link)) # put them up front
		}

		# Put columns in front
		if(any(! front.cols %in% colnames(top))){stop('front.cols not an existing feature annotation')}
		top = cbind(top[,front.cols,drop=FALSE],top[, ! colnames(top) %in% front.cols     ])

		# Put "Rank" column as very first
		top = cbind(top[,'Rank',drop=FALSE],top[, ! colnames(top) %in% "Rank"    ])
		
		# Abridge the table
		top = top[top$PValue<=p.value,]
		
		###################################################
		csvTop 	= CSVFile(shortName = paste0(i,'_top_tables'), reportDirectory = output.dir
				,title=paste0('',coef) )
		htmlTop 	= HTMLReport(shortName = paste0(i,'_top_tables'),reportDirectory = output.dir
				,title=paste0('',coef))
		hwrite(paste0('The following table ranks features by their p-value for coefficient '
			,hwrite(coef, style='font-style:italic'),
			'. To limit file sizes, is abridged to features with P<'
			,p.value,' are listed. This table can be downloaded in the '
			,hwrite('csv format',link=paste0(i,'_top_tables.csv'),style='text-decoration:underline')
			,'. The full ranking is available in the '
			,hwrite('analysis output csv file'
				,link='analysis_output.csv',style='text-decoration:underline')
			,'.'), p=page(htmlTop),br=TRUE)
		hwrite('<br>A description of the edgeR results columns is available here',link='edger_details.html',  p=page(htmlTop))
		publish(top, list(csvTop, htmlTop))
		finish(htmlTop)
		# Column legend, i.e. whwat is FC,LR and so on.	
    return(NULL)
	}
}
# TODO: might be nice to order raw counts by obj$variables... this way all samples from same group place next to each other. Also annoying to have no control over nb. sig digits, nb. of rows by default, etc. Colors for sig genes would be nice.




