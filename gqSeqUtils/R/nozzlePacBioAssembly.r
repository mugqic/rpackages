#' Nozzle report for PacBio assembly reports.
#'
#' Created 2013-10-16
#' @author Julien Tremblay julien.tremblay_AT_mail.mcgill.ca 
#' @export
mugqicPipelineReportPacBioAssembly <- function(report, project.path, ini.file.path, report.path) 
{
	
	# Overview
	sIntroduction = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)

	sIntroduction.text1=newParagraph(
		"This document contains the current MUGQIC PacBio sequence assembly analysis results. The information presented here reflects the current state of the analysis as of ", format(Sys.time(),format="%Y-%m-%d")
	)
	
	# Reads stats table.
	stReadCounts             = read.table(file.path(project.path,"/report/summaryTableReads.tsv"), sep="\t", header=F, skip=1, check.names=FALSE)
	stReadCounts[,2]         = prettyNum(stReadCounts[,2], big.mark=",",scientific=F)
	colnames(stReadCounts)   = c("Description", "Value")
	tReadCounts              = newTable(table=stReadCounts)

  # Reads stats table format 2
  stReadCounts2            = read.table(file.path(project.path,"/report/summaryTableReads2.tsv"), sep="\t", header=T, skip=0, check.names=FALSE, comment.char="!")
	stReadCounts2            = prettyNum(stReadCounts2, big.mark=",",scientific=F)
	tReadCounts2             = newTable(table=stReadCounts2)

  # Assembly table
	stReadAssembly           = read.table(file.path(project.path,"/report/summaryTableAssembly.tsv"), sep="\t", header=F, skip=1, check.names=FALSE)
	stReadAssembly[,2]       = prettyNum(stReadAssembly[,2], big.mark=",",scientific=F)
	colnames(stReadAssembly) = c("Description", "Value")
	tReadAssembly            = newTable(table=stReadAssembly)

	#maxLength           = tCounts[3,2]
	#minLength           = tCounts[4,2]
	totalReads           = tReadCounts$table[2,2]
	averageSubReadLength = tReadCounts$table[5,2]
	smrtCells            = tReadCounts$table[1,2]
	#technology          = tCounts[6,2]
	#libPrep             = tCounts[7,2]

	sIntroduction.text2 = newParagraph(
		"The PacBio sequencing performed at the Innovation Centre generated a total \
		of ",totalReads," raw subreads of average length ",averageSubReadLength," bp using ",smrtCells," SMRT Cells in a PacBio RSII  sequencer."
		#The protocol used for preparing libraries is the " ,libPrep,". \ 
		#Base calls are made using the PacBio SmrtAnalysis (smrtpipe) pipeline"
	)
	sIntroduction = addTo(
		sIntroduction, 
		sIntroduction.text1, 
		sIntroduction.text2
	)

	print("Intro section ... Done")
	
	# Analysis and results
	
	# Copy files
	flist <- list.files( paste0(project.path,"/report"), "^.+[.]jpeg|^.+[.]pdf$", full.names = TRUE)
	file.copy(flist, report.path);
	flist <- list.files( paste0(project.path,"/mummer"), "^.+[.]png$", full.names = TRUE)
	file.copy(flist, report.path);
	flist <- list.files( paste0(project.path,"/blast"), "^.+[.]tsv$", full.names = TRUE)
	file.copy(flist, report.path);

	# Build report
	sResults           = newSection("Analysis and results", exportId="Results", protection = PROTECTION.PUBLIC)

	ssCounts           = newSection("Read count and statistics");
	#ssCounts          = addTo(ssCounts, tReadCounts); 
	ssCounts           = addTo(ssCounts, tReadCounts2); ### 
	fReadsDistrib      = newFigure( file="pacBioGraph_histoReadLength.jpeg", fileHighRes="pacBioGraph_histoReadLength.pdf", protection=PROTECTION.PUBLIC )
	fReadsDistrib.text = newParagraph(
		"Figure 1 shows subreads count in function of subreads length. Data represented in this plot contains raw (unfiltered) subreads from all ", smrtCells, " SMRTCells used for this sample."
	);
	fReadsScore        = newFigure( file="pacBioGraph_readLengthScore.jpeg", fileHighRes="pacBioGraph_readLengthScore.pdf", protection=PROTECTION.PUBLIC )
	fReadsScore.text   = newParagraph(
		"Figure 2 shows reads quality scores (from 0 to 1) of all subreads. Data represented in this plot contains raw (unfiltered) subreads from all ", smrtCells, " SMRTCells used for this sample."
	);
	ssCounts           = addTo(ssCounts, fReadsDistrib)
	ssCounts           = addTo(ssCounts, fReadsDistrib.text)
	ssCounts           = addTo(ssCounts, fReadsScore)
	ssCounts           = addTo(ssCounts, fReadsScore.text)
	
	ssCounts2          = newSection("Assembly statistics");
	ssCounts2          = addTo(ssCounts2, tReadAssembly);

	# BLAST
	ssBlast            = newSection("Contigs BLAST hits and coverage");
	ssBlast.text1      = newParagraph(
		"The Blast searches were done using the dc-megablast option against the nt database. Please consult the blast documentation for more
		information ( http://www.ncbi.nlm.nih.gov/blast/discontiguous.shtml ). A csv version of this data is available (see below)."
	);
	
	tBlast           = read.table(paste0(project.path,"/blast/blastCov.tsv"), sep="\t", header=T, check.names=FALSE, quote="", stringsAsFactors=FALSE, comment.char="")
	#colnames(tBlast) = c( "query id", "subject id", "Coverage(X)", "% identity", "alignment length", "mismatches", "gap opens", "q. start", "q. end", "s. start", "s. end", "evalue", "bit score")
  #colnames(tBlast) = c( "query id", "subject id", "Coverage(X)", "% identity", "alignment length", "mismatches", "gap opens", "q. start", "q. end", "s. start", "s. end", "evalue", "bit score", "Reference name", "Kingdom", "Scientific name", "Common name")
	stBlast          = tBlast[1:20,]
	#stBlast          = tBlast
	tstBlast         = newTable(table=stBlast)
	ssBlast          = addTo(ssBlast, ssBlast.text1, tstBlast)
	
	# MUMMER
	ssMummer       = newSection("Mummer (nucmer) Dot plots");
	ssMummer.text1 = newParagraph(
		"In this alignment dotplot shows the sequences similarity of this assembly to the above reference. Please consult the
		nucmer and mummerplot documentation for more information."
	);
	
	ssMummer.text2 = newParagraph(
		"In this alignment dotplot the same sequence is layed out on each axis and a point is plotted at every position where \
		the sequences show similarity. Please consult the nucmer and mummerplot documentation for more information (
		nucmer, mummerplot). Points and lines (red or blue) away from the diagonal indicate similarities within or across
		contigs."
	);
		
	# Grab files
	fileList       = list.files(paste0(project.path,"/report"))
	fReferencePos  = grep("nucmer.delta.png", fileList, fixed=T)
	fSelfPos       = grep("nucmer.self.delta.png", fileList, fixed=T)
	#fReferencePath = paste0(project.path, fileList[fReferencePos])
	#fSelfPath      = paste0(project.path, fileList[fSelfPos])
	fReferencePath = fileList[fReferencePos]
	fSelfPath      = fileList[fSelfPos]

	print(paste0("fRef: ", fReferencePath ))
	print(paste0("fSelf: ", fSelfPath ))
	
	fReference     = newFigure(file=fReferencePath, protection=PROTECTION.PUBLIC)
	fSelf          = newFigure(file=fSelfPath, protection=PROTECTION.PUBLIC)
	ssMummer       = addTo(ssMummer, ssMummer.text1);
	ssMummer       = addTo(ssMummer, fReference)
	ssMummer       = addTo(ssMummer, ssMummer.text2);
	ssMummer       = addTo(ssMummer, fSelf)

	# INCLUDED FILES
	ssFiles          = newSection("Files included");
	ssFiles.text1    = newParagraph("Here is a list of key files generated through our analysis pipeline and their description.");

  numberOfPolishingRounds = getIniParams('polishing_rounds', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE)
  print( paste0( "number of polishing rounds: ", numberOfPolishingRounds) )

	finalContigs.file=paste0("polishing", numberOfPolishingRounds, "/data/consensus.fasta.gz")
	if (!file.exists(file.path(report.path,"consensus.fasta.gz"))) {
		file.symlink(file.path("..", finalContigs.file), file.path(report.path, "consensus.fasta.gz") )
	}
	ssFiles.text2 = newParagraph(asStrong(asLink(url=paste0("", "consensus.fasta.gz"),"The fasta file containing final contigs is available here.")),protection = PROTECTION.PUBLIC)
	ssFiles            = addTo(ssFiles, ssFiles.text1, ssFiles.text2)
	
  finalBlast.file=paste0("blast/blastCov.tsv")
	if (!file.exists(file.path(report.path,"blastCov.tsv"))) {
		file.symlink(file.path("..", finalBlast.file), file.path(report.path, "blastCov.tsv") )
	}
	ssFiles.text3    = newParagraph(asStrong(asLink(url=paste0("", "blastCov.tsv"),"The BLAST file containing contigs comparison against nt database is available here.")),protection = PROTECTION.PUBLIC)
	ssFiles          = addTo(ssFiles, ssFiles.text3)

	sResults         = addTo(sResults, ssCounts)
	sResults         = addTo(sResults, ssCounts2)
	sResults         = addTo(sResults, ssBlast)	
	sResults         = addTo(sResults, ssMummer)
	sResults         = addTo(sResults, ssFiles)	

	sMethod          = newSection("Assembly method");
	sMethod.text1    = newParagraph(
		"Contigs assembly was done using what is refer as the HGAP workflow
		Briefly, raw subreads where generated from raw .bas.h5 PacBio data files. A subread length cutoff value was extracted from subreads and used into the preassembly (BLASR) \
        step which consists of aligning reads short subreads on long subreads. Since errors in PacBio reads is random, the alignment of multiple short reads on longer reads allows \
        to correct sequencing error on long reads. These long corrected reads are then used as seeds into assembly (Celera assembly) which gives contigs. These contigs are then 'polished' by aligning \
        raw reads on contigs (BLASR) that are then processed through a variant calling algorithm (Quiver) that generates high quality consensus sequences using local realignments and \
		PacBio quality scores."
	);
	sMethod        = addTo(sMethod, sMethod.text1)

	# References
	citeRef=getCitations()
	sReferences = newSection("References", exportId="References", protection = PROTECTION.PUBLIC)
	sReferences = addTo(sReferences, citeRef$HGAP, citeRef$BLASR, citeRef$Celera, citeRef$PacBio)

	# Parameters
	#sParameters = newSection("Parameters", exportId="Parameters", protection = PROTECTION.PUBLIC)
	
	### Build sections and report
	
	#sResults = addTo(
	#	ssCounts,
	#	ssCounts2,
	#	ssMummer,
	#	ssBlast
	#)

	#biblio = newSection( "References" )
	#biblio = addTo(biblio,citeRef$PacBio,citeRef$Celera,citeRef$BLASR,citeRef$HGAP)

	report= addTo(
		report,
		sIntroduction,
		sMethod,
		sResults,
		sReferences
	)

	return(report)
}
