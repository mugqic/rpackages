
#' This is a function to read the individual DGEA reporting files. Assumes files are output of edger.r and deseq.r in mugqic/tools
#' @param A function to apply to each table to filter it, e.g. by p-value or FDR or what not
#' @export
readMugqicLegacyDGEAs <- function(
	 dge.dir    = file.path("DGE","isoforms_nr")
	,pattern    = "dge_results.csv"
	,filter.fct = function(df){ rep(TRUE,times=nrow(df))   } )
	#,filter.fct = function(df){   df[["deseq.p-value"]]  <= 1  |  df[["edger.p.value"]] <= 1  } )
{
	# dge.dir=file.path("DGE","isoforms_nr");pattern="dge_results.csv"; filter.fct = function(df){   df[["deseq.p-value"]]  <=0.05 |  df[["edger.p.value"]] <=0.05  };
	fns     = list.files(dge.dir,pattern=pattern,recursive=TRUE,full.names=TRUE)
	coefs    = gsub("^design_","", basename(dirname(fns)) ) # these are the names of the comparisons
	dgeas   = sapply(fns,function(fn)
	{
		dgea = read.table(file=fn,stringsAsFactors=FALSE,check.names=FALSE,comment.char='',quote='',sep='\t',header=TRUE)
		rownames(dgea) = dgea[["id"]]
		dgea[["id"]] = NULL
		if(!is.null(filter.fct))
		{
			keep = filter.fct(dgea)
			dgea = dgea[keep,]
		}
		return(dgea)
	},simplify=FALSE) 
	names(dgeas) = coefs
	return(dgeas)
}


#' This function improves on the tab-delimited trinotate Report. this functions makes the following asumptions (put in documentation of args instead)
#' dgea.files: a named characetr vector to dgea files
#' @param additional.append.table A data frame to join, by it's rownames with "transcript_id". a left outer join will be performed will be performed.
#' @export
TrinotatePlus = function(
		trinotate.report.file	         = file.path("trinotate","trinotate_annotation_report.tsv"),
		assembly.fasta			         = file.path("trinity_out_dir","Trinity.fasta"), 
		min.transcript.length	         = 500, 
		report.unannotated               = TRUE,
		additional.table                 = readMugqicLegacyDGEAs(),
		output.prefix			         = file.path("trinotate") )
{
	#suppressMessages(library(gqSeqUtils));trinotate.report.file = file.path("trinotate","trinotate_annotation_report.tsv");assembly.fasta = file.path("trinity_out_dir","Trinity.fasta"); min.transcript.length = 500; output.prefix	= file.path("trinotate"); ncores=2; report.unannotated = TRUE; additional.table = readMugqicLegacyDGEAs() ; 


	##	
	##### Assebmly Sequences : Read
	message("Reading sequences...")
	assembly        = readDNAStringSet(assembly.fasta)
	names(assembly) = gsub(" len=.*","",names(assembly) )
    assembly = data.frame("sequence.width"=width(assembly),"sequence"=as.character(assembly),row.names=names(assembly),stringsAsFactors=FALSE)	
    all.transcripts = rownames(assembly)
	#rm(assembly); gc()
	
	##	
	#####  Trinotate report : Read (might seem complicated, but trinotate.xls is ill-formatted)
	message("Reading the Trinotate report table...")
	trinotate = readLines(trinotate.report.file) 
	trinotate  = strsplit(trinotate ,split='\t')
	trinotate  = lapply(trinotate ,function(line){
	 	line[  1: length(trinotate[[1]]) ]
	})
	trinotate  = do.call(rbind, trinotate )
	trinotate  = data.frame(trinotate, stringsAsFactors=FALSE, check.names=FALSE)
	colnames(trinotate) = trinotate[1,]
	trinotate  = trinotate[-1,]
	# There are duplicate transcripts, define the row.names
	rownames(trinotate) = paste(trinotate[["transcript_id"]],trinotate[["prot_id"]])
	if( !setequal(trinotate[["transcript_id"]]  , all.transcripts  ) ){stop("Names in trinotate file and fasta file do not all match")}
	#	trinotate = trinotate[trinotate$transcript_id %in% c('c97887_g1_i1','c98758_g1_i3','c98880_g1_i2'),]	

	##	
	##### Filter assembly for long enough sequences
	message("Filtering for minimim transcript length...")
	keep = assembly[["sequence.width"]] >=  min.transcript.length
	assembly = assembly[  keep,]
	message(sprintf("%s transcripts left out of %s",sum(keep),length(keep)))
	
	##	
	##### Trinotate: remove empty rows
	if(!report.unannotated)
	{
		message("Removing unannotated transcripts...")
		keep = apply(trinotate,MARGIN=1,function(ro)  !identical(unique(ro[3:length(ro)]) ,'.')    )
		trinotate  = trinotate[keep,]
		message(sprintf("%s transcripts left out of %s",sum(keep),length(keep)))
	}
	
	##	
	##### Trinotate: Various formatting tasks
	message("Formating the Trinotate report... this may take a long time")	
	format.trinotate.multientries <- function(lines, l1.max.entries=10, format.fct)
	{
		# lines = trinotate[["Top_BLASTX_hit"]];   l1.max.entries=10; format.fct <- function(x){sprintf("%s %s (%s)",x[1],gsub("(^RecName: Full=|;$)","",x[2]),gsub(".*; ","",x[3]))}
		lines	= strsplit( lines , split="\\`")
		lines	= lapply(lines,strsplit,split="\\^")
		lines   = lapply(lines,function(x)x[1:min(l1.max.entries,length(x))])
		lines   = sapply(lines,function(l1){ 
					if(identical(l1[[1]],".")){return('.')}
					l1 = sapply(l1,function(l2){
							l2  = format.fct( l2 )
				    })
					l1 = unique(l1) # long transcript will have e.g multiple pfam domain hits
					l1 = paste( l1, collapse=' /// ')
				 })  
	} 
	
	# BLASTX
	trinotate[["Top_BLASTX_hit.accession"]]   = format.trinotate.multientries(trinotate[["Top_BLASTX_hit"]], 1, function(x){x[2]})
	trinotate[["Top_BLASTX_hit.description"]] = format.trinotate.multientries(trinotate[["Top_BLASTX_hit"]], 1, function(x){ gsub("(^RecName: Full=|;$)","",x[6]) })
	trinotate[["Top_BLASTX_hit.evalue"]]  	= format.trinotate.multientries(trinotate[["Top_BLASTX_hit"]], 1, function(x){ gsub("^E:","",x[5]) })
	trinotate[["Top_BLASTX_hit.species"]] 	= format.trinotate.multientries(trinotate[["Top_BLASTX_hit"]], 1, function(x){gsub(".*; ","",x[7])})
	trinotate[["Top_BLASTX_hit.url"]] 		=format.trinotate.multientries(trinotate[["Top_BLASTX_hit"]], 1, function(x){sprintf("http://www.uniprot.org/uniprot/%s",x[2])})
	trinotate[["Top_BLASTX_hit"]] = NULL
	
	trinotate[["Top_BLASTP_hit.accession"]]   = format.trinotate.multientries(trinotate[["Top_BLASTP_hit"]], 1, function(x){x[2]})
	trinotate[["Top_BLASTP_hit.description"]] = format.trinotate.multientries(trinotate[["Top_BLASTP_hit"]], 1, function(x){ gsub("(^RecName: Full=|;$)","",x[6]) })
	trinotate[["Top_BLASTP_hit.evalue"]]  	= format.trinotate.multientries(trinotate[["Top_BLASTP_hit"]], 1, function(x){ gsub("^E:","",x[5]) })
	trinotate[["Top_BLASTP_hit.species"]] 	= format.trinotate.multientries(trinotate[["Top_BLASTP_hit"]], 1, function(x){gsub(".*; ","",x[7])})
	trinotate[["Top_BLASTP_hit.url"]] 		=format.trinotate.multientries(trinotate[["Top_BLASTP_hit"]], 1, function(x){sprintf("http://www.uniprot.org/uniprot/%s",x[2])})
	trinotate[["Top_BLASTP_hit"]] = NULL
	
    # Trinotate: prot_id is useless
	trinotate[["prot_id"]] = NULL
	
	# Pfam Column
    trinotate[["Pfam.accession"]]   = format.trinotate.multientries( trinotate[["Pfam"]], Inf, function(x){x[1]})
	trinotate[["Pfam.description"]] = format.trinotate.multientries( trinotate[["Pfam"]], Inf, function(x){substr(x[3],1,100)})
	trinotate[["Pfam.url"]] = format.trinotate.multientries( trinotate[["Pfam"]], Inf, function(x){sprintf("http://pfam.xfam.org/family/%s",x[1])})
	trinotate[["Pfam"]] = NULL	
		
	# eggNOG column
    trinotate[["eggnog.accession"]]  =  format.trinotate.multientries(trinotate[["eggnog"]], Inf, function(x){x[1]})
	trinotate[["eggnog.description"]]  =  format.trinotate.multientries(trinotate[["eggnog"]], Inf, function(x){substr(x[2],1,100)})
	trinotate[["eggnog.url"]]  =  format.trinotate.multientries(trinotate[["eggnog"]], Inf, function(x){sprintf("http://eggnog.embl.de/version_4.0.beta/cgi/search.py?search_term_0=%s&search_species_0=-1",x[1] )})
	trinotate[["eggnog"]] = NULL
	
	# GO column
	trinotate[["gene_ontology.accession"]]  =  format.trinotate.multientries(trinotate[["gene_ontology"]], Inf, function(x){x[1]})
	trinotate[["gene_ontology.description"]]  =  format.trinotate.multientries(trinotate[["gene_ontology"]], Inf, function(x){x[3]})
	trinotate[["gene_ontology.url"]]  =  format.trinotate.multientries(trinotate[["gene_ontology"]], Inf, function(x){sprintf("http://amigo.geneontology.org/amigo/search/ontology?q=%s",x[1])})
	trinotate[["gene_ontology"]] = NULL

	# sort(sapply(trinotate,function(x)max(nchar(x))))

	##	
	#####  JOIN!
	message("Joining trinotate report, sequences and additional table")
	fdata = merge(trinotate,assembly,all.x=TRUE,all.y=FALSE,by.x="transcript_id",by.y=0)
    if(!is.null(additional.table))
	{
		dat = sapply(additional.table,function(df)
		{
			if(any( ! rownames(df) %in% all.transcripts )){stop("Names in additional table and assembly names do not match")}
			 df = merge(df,fdata,all.x=TRUE,all.y=TRUE ,by.x=0,by.y="transcript_id")  
			colnames(df)[1] = 'transcript_id'
			df
		},simplify=FALSE)
		if(is.null(names(dat))){names(dat)=c(1:length(dat))}
	}else{
		dat = list("trinotate"=fdata)
	}
	#save.image()

	##	
	#####  Write Out HTML, csv
	message("Writing to csv...")
	for(n in names(dat))
	{
		message(sprintf("Writing %s..",n))
		write.csv(dat[[n]],file= paste0(output.prefix,"_",n,".csv") ,row.names=F)
	}
	
	return(fdata)	
}










# #' This function improves on the tab-delimited trinotate Report. this functions makes the following asumptions (put in documentation of args instead)
# #' dgea.files: a named characetr vector to dgea files
# #' @param additional.append.table A data frame to join, by it's rownames with "transcript_id". a left outer join will be performed will be performed.
# #' @export
# TrinotatePlus.old = function(
# 		trinotate.report.file	         = file.path("trinotate","trinotate_annotation_report.tsv"),
# 		assembly.fasta			         = file.path("trinity_out_dir","Trinity.fasta"),
# 		min.transcript.length	         = 500,
# 		report.unannotated               = FALSE,
# 		additional.table                 = readMugqicLegacyDGEAs(),
# 		output.prefix			         = file.path("trinotate"),
# 	    description.text                 = "The following table lists contigs (>500 bp) found differentially expressed at a \
# significance level of P<0.01. The contig annotations are produced by the Trinotate pipeline and a description of the columns is \
# available at trinotate.sourceforge.net. Note that contigs with no annotation do not appear the table. Depending on your system\'s performance, loading the entire table may take about a minute. \
# You can use the different filter and search functions on the different columns to isolate relevant information. For instance, \
# transcripts could be filtered for a more stringeant FDR (deseq.adj.pvalue), a larger contig length (sequence.width) or filter for a keyword of \
# interest, e.g. enter \"Stress\" in the search field below.",
# #		additional.table.left.outer.join = FALSE,
# #        stop.if.check.fails              = TRUE,
# 		ncores=1
# 		)
# {
# 	#suppressMessages(library(gqSeqUtils));trinotate.report.file = file.path("trinotate","trinotate_annotation_report.tsv");assembly.fasta = file.path("trinity_out_dir","Trinity.fasta"); min.transcript.length = 500; output.prefix	= file.path("trinotate"); ncores=2; report.unannotated = FALSE; additional.table = readMugqicLegacyDGEAs() ;
#
# 	warning("This function is deprecated")
#
# 	##
# 	##### Assebmly Sequences : Read
# 	message("Reading sequences...")
# 	assembly        = readDNAStringSet(assembly.fasta)
# 	names(assembly) = gsub(" len=.*","",names(assembly) )
#     assembly = data.frame("sequence.width"=width(assembly),"sequence"=as.character(assembly),row.names=names(assembly),stringsAsFactors=FALSE)
#     all.transcripts = rownames(assembly)
# 	#rm(assembly); gc()
#
# 	##
# 	#####  Trinotate report : Read (might seem complicated, but trinotate.xls is ill-formatted)
# 	message("Reading the Trinotate report table...")
# 	trinotate = readLines(trinotate.report.file)
# 	trinotate  = strsplit(trinotate ,split='\t')
# 	trinotate  = lapply(trinotate ,function(line){
# 	 	line[  1: length(trinotate[[1]]) ]
# 	})
# 	trinotate  = do.call(rbind, trinotate )
# 	trinotate  = data.frame(trinotate, stringsAsFactors=FALSE, check.names=FALSE)
# 	colnames(trinotate) = trinotate[1,]
# 	trinotate  = trinotate[-1,]
# 	# There are duplicate transcripts, define the row.names
# 	rownames(trinotate) = paste(trinotate[["transcript_id"]],trinotate[["prot_id"]])
# 	if( !setequal(trinotate[["transcript_id"]]  , all.transcripts  ) ){stop("Names in trinotate file and fasta file do not all match")}
# 	#	trinotate = trinotate[trinotate$transcript_id %in% c('c97887_g1_i1','c98758_g1_i3','c98880_g1_i2'),]
#
# 	##
# 	##### Filter assembly for long enough sequences
# 	message("Filtering for minimim transcript length...")
# 	keep = assembly[["sequence.width"]] >=  min.transcript.length
# 	assembly = assembly[  keep,]
# 	message(sprintf("%s transcripts left out of %s",sum(keep),length(keep)))
#
# 	##
# 	##### Trinotate: remove empty rows
# 	if(!report.unannotated)
# 	{
# 		message("Removing unannotated transcripts...")
# 		keep = apply(trinotate,MARGIN=1,function(ro)  !identical(unique(ro[3:length(ro)]) ,'.')    )
# 		trinotate  = trinotate[keep,]
# 		message(sprintf("%s transcripts left out of %s",sum(keep),length(keep)))
# 	}
#
# 	##
# 	##### Trinotate: Various formatting tasks
# 	message("Formating the Trinotate report... this may take a long time")
# 	format.trinotate.multientries <- function(lines, l1.max.entries=10, format.fct)
# 	{
# 		# lines = trinotate[["Top_BLASTX_hit"]];   l1.max.entries=10; format.fct <- function(x){sprintf("%s %s (%s)",x[1],gsub("(^RecName: Full=|;$)","",x[2]),gsub(".*; ","",x[3]))}
# 		lines	= strsplit( lines , split="\\`")
# 		lines	= lapply(lines,strsplit,split="\\^")
# 		lines   = lapply(lines,function(x)x[1:min(l1.max.entries,length(x))])
# 		lines   = sapply(lines,function(l1){
# 					if(identical(l1[[1]],".")){return('.')}
# 					l1 = sapply(l1,function(l2){
# 							l2  = format.fct( l2 )
# 				    })
# 					l1 = unique(l1) # long transcript will have e.g multiple pfam domain hits
# 					l1 = paste( l1, collapse='<br>')
# 				 })
# 	}
# 	trinotate[["Top_BLASTX_hit"]] = format.trinotate.multientries(
# 		lines = trinotate[["Top_BLASTX_hit"]], l1.max.entries=1,
# 		format.fct = function(x){sprintf("<a href=\"http://www.uniprot.org/uniprot/%s\">%s</a> %s (%s)",x[2],x[2],gsub("(^RecName: Full=|;$)","",x[6]),gsub(".*; ","",x[7]))}
# 	)
# 	trinotate[["Top_BLASTP_hit"]] = format.trinotate.multientries(
# 		lines = trinotate[["Top_BLASTP_hit"]], l1.max.entries=1,
# 		format.fct = function(x){sprintf("<a href=\"http://www.uniprot.org/uniprot/%s\">%s</a> %s (%s)",x[2],x[2],gsub("(^RecName: Full=|;$)","",x[6]),gsub(".*; ","",x[7]))}
# 	)
#
#     # Trinotate: prot_id is useless
# 	trinotate[["prot_id"]] = NULL
#
# 	# Pfam Column
#     trinotate[["Pfam"]]  =  format.trinotate.multientries(
# 		lines = trinotate[["Pfam"]], l1.max.entries=Inf,
# 		format.fct = function(x){sprintf("<a href=\"http://pfam.xfam.org/family/%s\">%s</a> %s",x[1],x[1],substr(x[3],1,100))}
# 	)
#
# 	# eggNOG column
#     trinotate[["eggnog"]]  =  format.trinotate.multientries(
# 		lines = trinotate[["eggnog"]], l1.max.entries=Inf,
# 		format.fct = function(x){sprintf("<a href=\"http://eggnog.embl.de/version_4.0.beta/cgi/search.py?search_term_0=%s&search_species_0=-1\">%s</a> %s",x[1],x[1], substr(x[2],1,100) )}
# 	)
#
# 	# GO column
# 	trinotate[["gene_ontology"]]  =  format.trinotate.multientries(
# 		lines = trinotate[["gene_ontology"]], l1.max.entries=Inf,
# 		format.fct = function(x){sprintf("<a href=\"http://amigo.geneontology.org/amigo/search/ontology?q=%s\">%s</a> %s",x[1],x[1],x[3])}
# 	)
#
#
# 	##
# 	#####  JOIN!
# 	message("Joining trinotate report, sequences and additional table")
# 	fdata = merge(trinotate,assembly,all.x=FALSE,all.y=FALSE,by.x="transcript_id",by.y=0)
#     if(!is.null(additional.table))
# 	{
# 		dat = sapply(additional.table,function(df)
# 		{
# 			if(any( ! rownames(df) %in% all.transcripts )){stop("Names in additional table and assembly names do not match")}
# 			#  df = merge(df,fdata,all.x=FALSE,all.y=FALSE,by.x=0,by.y="transcript_id")
# 			 df = merge(df,fdata,all.x=FALSE,all.y=TRUE ,by.x=0,by.y="transcript_id")  # TEMP wants all!
# 			colnames(df)[1] = 'transcript_id'
# 			df
# 		},simplify=FALSE)
# 		if(is.null(names(dat))){names(dat)=c(1:length(dat))}
# 	}else{
# 		dat = list("trinotate"=fdata)
# 	}
#
#
# 	##
# 	#####  Write Out HTML, csv
# 	message("Writing HTML reports...")
# 	for(n in names(dat))
# 	{
# 		message(sprintf("Writing %s..",n))
# 		htmlRep <- HTMLReport(shortName = paste(basename(output.prefix),n,sep='_'),title=sprintf("Differentially expressed Transcripts for %s",n),reportDirectory = dirname(output.prefix) )
# 		publish( description.text, htmlRep)
# 		publish( dat[[n]], htmlRep)
# 		finish(htmlRep)
# 		write.csv(dat[[n]],file= paste0(output.prefix,"_",n,".csv") ,row.names=F) # TEMP?
# 	}
#
# 	return(fdata)
#
# }






