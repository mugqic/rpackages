#' Nozzle report for PacBio assembly reports.
#'
#' Created 2013-10-16
#' @author Julien Tremblay julien.tremblay_AT_mail.mcgill.ca 
#' @export
mugqicPipelineReportRRNATagger <- function(report, project.path, ini.file.path, report.path) 
{
	
	# Overview
	sIntroduction = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)

	sIntroduction.text1=newParagraph(
		"This document contains the current MUGQIC rRNA amplicon sequences analysis results. The information presented here reflects the current state of the analysis as of ", format(Sys.time(),format="%Y-%m-%d")
	)
	
	# Read stats table.
	text                     = readLines(file.path(project.path, "countReport.tsv"),encoding='UTF-8', n = -1L)

	stReadCounts             = text[1:8]
	stReadCounts             = data.frame( do.call( rbind, strsplit( stReadCounts, '\t' ) ) ) 
	stReadCounts[,2]         = prettyNum(stReadCounts[,2], big.mark=",",scientific=F)
	stReadCounts             = as.matrix(stReadCounts)

	stClusters               = text[(length(text) - 1) : length(text) ]
	stClusters               = data.frame( do.call( rbind, strsplit( stClusters, '\t' ) ) )
	stClusters               = as.matrix(stClusters)

	allCounts                = rbind(stReadCounts, stClusters)
	colnames(allCounts   )   = c("Description", "Value")
	tReadCounts              = newTable(table=allCounts)

	rawBarcodes              = text[13 : length(text)-2 ]
	rawBarcodes              = data.frame( do.call( rbind, strsplit( rawBarcodes, '\t' ) ) ) 
	rawBarcodes              = as.matrix(rawBarcodes)
	colnames(rawBarcodes)    = c("name", "sequence", "count", "perc")
	tRawBarcodes             = newTable(table=rawBarcodes)

	totalReads               = stReadCounts[1,2]
	sequencer                = getIniParams('sequencer', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	readConfig               = getIniParams('readConfig', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE) 

	sIntroduction.text2 = newParagraph(
		"The rRNA amplicon sequencing performed at the Innovation Centre generated a total \
		of ",totalReads," raw reads using a ",sequencer," sequencer. Table 1 describes reads \
		counts throughout various steps of the pipeline."
		#The protocol used for preparing libraries is the " ,libPrep,". \ 
		#Base calls are made using the PacBio SmrtAnalysis (smrtpipe) pipeline"
	)
	sIntroduction = addTo(
		sIntroduction, 
		sIntroduction.text1, 
		sIntroduction.text2
	)

	print("Intro section ... Done")
	
	# Analysis and results
	
	# Copy files
	#flist <- list.files( paste0(project.path,"/report"), "^.+[.]jpeg|^.+[.]pdf$", full.names = TRUE)
	#file.copy(flist, report.path);
	#flist <- list.files( paste0(project.path,"/mummer"), "^.+[.]png$", full.names = TRUE)
	#file.copy(flist, report.path);
	#flist <- list.files( paste0(project.path,"/blast"), "^.+[.]csv$", full.names = TRUE)
	#file.copy(flist, report.path);

	# Build report
	
	# RESULTS (MASTER)
	sResults       = newSection("Analysis and results", exportId="Results", protection = PROTECTION.PUBLIC)

	# RESULTS-QC
	ssQC           = newSection("QC")
	ssQC.text1     = newParagraph(
		"Table 1 shows reads count throughout pipeline processing."
	);
	ssQC           = addTo(ssQC, ssQC.text1); 
	ssQC           = addTo(ssQC, tReadCounts); 

	ssQC.text2     = newParagraph(
		"Table 2 shows reads count for each sample (i.e. barcodes). These are the raw counts meaning that no quality filtering has been performed at this point.
		This table is useful to see if some samples failed to amplify or sequenced."
	);
	ssQC           = addTo(ssQC, ssQC.text2); 
	ssQC           = addTo(ssQC, tRawBarcodes); 
	
	ssQC.text3     = newParagraph(
		"The link below point to a pdf file having Q score boxplots for each samples. In these plots, the x axis refers to the position of the sequenced base and the y
		axis refers to the Q score distribution. The Q scores vary between 0 (worst) and 40 (best)."
	);
	unassembledQscores.file="qual_stats_unassembled.pdf"
	if (!file.exists(file.path(report.path,"qual_stats_unassembled.pdf"))) {
		file.symlink(file.path("..", "qual_stats_unassembled.pdf"), file.path(report.path, "qual_stats_unassembled.pdf") )
	}
	ssQC.text4     = newParagraph(asStrong(asLink(url=unassembledQscores.file,"The PDF file containing Qscore distribution is available here.")), protection = PROTECTION.PUBLIC)
	ssQC           = addTo(ssQC, ssQC.text3); 
	ssQC           = addTo(ssQC, ssQC.text4); 

	sResults       = addTo(sResults, ssQC)

	# RESULTS-TAXONOMY
	ssTax          = newSection("Taxonomy")
	ssQC.text3     = newParagraph(
		"QC passed reads where clustered (see methods) and classified using the RDP classifier. These classified clusters where then used to generate a OTU (Operational Taxonomic Unit) table which is a matrix showing the number of clusters of each lineages for each samples. This table was rarefied to 1000 reads per sample and used to generate figures."
	);

	libraryType = getIniParams('libraryType', 'DEFAULT', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE) 	

	#ex = pmatch("ex", libraryType)
	#nc1 = pmatch("nc1", libraryType)
	#nc2 = pmatch("nc2", libraryType)
	fTaxRel = NULL
	fTaxAbs = NULL
	textTax = ""

	if(libraryType == "ex"){
		if (!file.exists(file.path(report.path,"Assembled"))) {
			file.symlink(file.path("..", "assembled"), file.path(report.path, "Assembled") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.jpeg"))) {
			file.symlink(file.path("..", "assembled/tax_summary/taxonomy_phylum_L2.jpeg"), file.path(report.path, "taxonomy_phylum_L2.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.pdf"))) {
			file.symlink(file.path("..", "assembled/tax_summary/taxonomy_phylum_L2.pdf"), file.path(report.path, "taxonomy_phylum_L2.pdf") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.jpeg"))) {
			file.symlink(file.path("..", "assembled/tax_summary/taxonomy_phylum_L2_relative.jpeg"), file.path(report.path, "taxonomy_phylum_L2_relative.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.pdf"))) {
			file.symlink(file.path("..", "assembled/tax_summary/taxonomy_phylum_L2_relative.pdf"), file.path(report.path, "taxonomy_phylum_L2_relative.pdf") )
		}

		fTaxRel = newFigure( file="taxonomy_phylum_L2_relative.jpeg", fileHighRes="taxonomy_phylum_L2_relative.pdf", protection=PROTECTION.PUBLIC )
		fTaxAbs = newFigure( file="taxonomy_phylum_L2.jpeg", fileHighRes="taxonomy_phylum_L2.pdf", protection=PROTECTION.PUBLIC )
		textTax = newList(
			newParagraph("Tables, OTU tables, figures are located in the assembled/ folder from the report root directory/"),
			newParagraph("Clustering intermediate files are located in assembled/obs/."),
			newParagraph("OTU tables (in .tsv and .biom format) are located in /assembled/otu_tables/"),
			newParagraph("RDP raw table is located in /assembled/rdp/"),
			newParagraph("Taxonomic summaries are located in /assembled/tax_summary/")
		);
		ssTax          = addTo(ssTax, textTax)
	
		ssTax.text2    = newParagraph(
			"Figure 1 shows relative taxonomic distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text2)
		if (!file.exists(file.path(report.path,"Duk"))) {
			file.symlink(file.path("../../", "duk"), file.path(report.path, "Duk") )
		}
		if (!file.exists(file.path(report.path,"Fastqs"))) {
			file.symlink(file.path("../", "fastqs"), file.path(report.path, "Fastqs") )
		}
		ssTax          = addTo(ssTax, fTaxRel); 
		ssTax.text3    = newParagraph(
			"Figure 2 shows absolute distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text3)
		ssTax          = addTo(ssTax, fTaxAbs); 
		sResults       = addTo(sResults, ssTax)
		
	}else if(libraryType == "nc1"){
		if (!file.exists(file.path(report.path, "Reads_1"))) {
			file.symlink(file.path("..", "reads_1"), file.path(report.path, "Reads_1") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.jpeg"))) {
			file.symlink(file.path("..", "reads_1/tax_summary/taxonomy_phylum_L2.jpeg"), file.path(report.path, "taxonomy_phylum_L2.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.pdf"))) {
			file.symlink(file.path("..", "reads_1/tax_summary/taxonomy_phylum_L2.pdf"), file.path(report.path, "taxonomy_phylum_L2.pdf") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.jpeg"))) {
			file.symlink(file.path("..", "reads_1/tax_summary/taxonomy_phylum_L2_relative.jpeg"), file.path(report.path, "taxonomy_phylum_L2_relative.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.pdf"))) {
			file.symlink(file.path("..", "reads_1/tax_summary/taxonomy_phylum_L2_relative.pdf"), file.path(report.path, "taxonomy_phylum_L2_relative.pdf") )
		}
		fTaxRel = newFigure( file="taxonomy_phylum_L2_relative.jpeg", fileHighRes="taxonomy_phylum_L2_relative.pdf", protection=PROTECTION.PUBLIC )
		fTaxAbs = newFigure( file="taxonomy_phylum_L2.jpeg", fileHighRes="taxonomy_phylum_L2.pdf", protection=PROTECTION.PUBLIC )
		textTax = newList(
			newParagraph("Tables, OTU tables, figures are located in the Reads_1/ folder from the report root directory/"),
			newParagraph("Clustering intermediate files are located in Reads_1/obs/."),
			newParagraph("OTU tables (in .tsv and .biom format) are located in Reads_1/otu_tables/"),
			newParagraph("RDP raw table is located in Reads_1/rdp/"),
			newParagraph("Taxonomic summaries are located in Reads_1/tax_summary/")
		);
		ssTax          = addTo(ssTax, textTax)
	
		ssTax.text2    = newParagraph(
			"Figure 1 shows relative taxonomic distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text2)
		if (!file.exists(file.path(report.path,"Duk"))) {
			file.symlink(file.path("../../", "duk"), file.path(report.path, "Duk") )
		}
		if (!file.exists(file.path(report.path,"Fastqs"))) {
			file.symlink(file.path("../", "fastqs"), file.path(report.path, "Fastqs") )
		}
		ssTax          = addTo(ssTax, fTaxRel); 
		ssTax.text3    = newParagraph(
			"Figure 2 shows absolute distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text3)
		ssTax          = addTo(ssTax, fTaxAbs); 
		sResults       = addTo(sResults, ssTax)

	}else if(libraryType == "nc1"){
		if (!file.exists(file.path(report.path,"Reads_2"))) {
			file.symlink(file.path("..", "reads_2"), file.path(report.path, "Reads_2") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.jpeg"))) {
			file.symlink(file.path("..", "reads_2/tax_summary/taxonomy_phylum_L2.jpeg"), file.path(report.path, "taxonomy_phylum_L2.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.pdf"))) {
			file.symlink(file.path("..", "reads_2/tax_summary/taxonomy_phylum_L2.pdf"), file.path(report.path, "taxonomy_phylum_L2.pdf") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.jpeg"))) {
			file.symlink(file.path("..", "reads_2/tax_summary/taxonomy_phylum_L2_relative.jpeg"), file.path(report.path, "taxonomy_phylum_L2_relative.jpeg") )
		}
		if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_relative.pdf"))) {
			file.symlink(file.path("..", "reads_2/tax_summary/taxonomy_phylum_L2_relative.pdf"), file.path(report.path, "taxonomy_phylum_L2_relative.pdf") )
		}
		fTaxRel = newFigure( file="taxonomy_phylum_L2_relative.jpeg", fileHighRes="taxonomy_phylum_L2_relative.pdf", protection=PROTECTION.PUBLIC )
		fTaxAbs = newFigure( file="taxonomy_phylum_L2.jpeg", fileHighRes="taxonomy_phylum_L2.pdf", protection=PROTECTION.PUBLIC )
		textTax = newList(
			newParagraph("Tables, OTU tables, figures are located in the Reads_2/ folder from the report root directory/"),
			newParagraph("Clustering intermediate files are located in Reads_2/obs/."),
			newParagraph("OTU tables (in .tsv and .biom format) are located in Reads_2/otu_tables/"),
			newParagraph("RDP raw table is located in Reads_2/rdp/"),
			newParagraph("Taxonomic summaries are located in Reads_2/tax_summary/")
		);
		ssTax          = addTo(ssTax, textTax)
	
		ssTax.text2    = newParagraph(
			"Figure 1 shows relative taxonomic distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text2)
		if (!file.exists(file.path(report.path, "Duk"))) {
			file.symlink(file.path("../../", "duk"), file.path(report.path, "Duk") )
		}
		if (!file.exists(file.path(report.path,"Fastqs"))) {
			file.symlink(file.path("../", "fastqs"), file.path(report.path, "Fastqs") )
		}
		ssTax          = addTo(ssTax, fTaxRel); 
		ssTax.text3    = newParagraph(
			"Figure 2 shows absolute distribution of your samples"
		)
		ssTax          = addTo(ssTax, ssTax.text3)
		ssTax          = addTo(ssTax, fTaxAbs); 
		sResults       = addTo(sResults, ssTax)
	
	}else{
		stop("Wrong libraryType...")
	}
		

	# METHODS
  Ns             = getIniParams('N', 'itags_QC', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE) 	
  lqThreshold    = getIniParams('lq_threshold', 'itags_QC', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE)
  qscore2        = getIniParams('qscore2', 'itags_QC', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE)
  qscore1        = getIniParams('qscore1', 'itags_QC', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=TRUE)

	sMethod        = newSection("Methods");
	sMethod.text1  = newList(
		newParagraph(asStrong("1-")," Reads are first scanned for contaminants (e.g. Illumina, 454 or PacBio adapter sequences) and PhiX reads. Usually a small proportion of reads are contaminants and accordingly, 0-25% is PhiX reads."),
		newParagraph(asStrong("2-"), "From step 1, paired-end reads may be disrupted. This means that one of the read pairs might be lost due to the screening in step #1. All of these unpaired reads are discarded. This is usually a fairly small proportion of all reads. This step is not performed if single end, PacBio or 454 reads are processed"),
		newParagraph(asStrong("3-"), "All reads are then trimmed to a fix length (default: 165 bp, but that may change depending on quality on sequencing run and amplicon length). This is because in the next step, read pairs will be assembled to reconstitute the original 16S amplicons. Although we have good results with the software we use for that task (FLASH), we observed better results if low quality ends of reads are trimmed before being paired-end assembled. If reads are not to be paired-end assembled (i.e. single end reads, PacBio or 454 reads), step 4 is not performed. It is nevertheless a good practice to trim reads to a fixed length before downstream analyses."),
		newParagraph(asStrong("4 if paired-end reads)-"), "Reads are assembled (overlapping paired assembly) with the FLASH software."),
		newParagraph(asStrong("5-"), " Primer sequences may or may not be removed from the assembled/single end reads. This step is optional."),
		newParagraph(asStrong("6-"), " The trimmed assembled/single-end reads from step #5 are filtered for quality. All reads having an average quality score lower than ", qscore1 ," or more than ", Ns, " Ns and ", lqThreshold, " nucleotides below quality ", qscore2 ," are discarded. These reads will be referred to as filtered reads from now on."),
		newParagraph(asStrong("7-"), " Filtered reads are then clustered with our in-house clustering algorithm. Briefly, reads are clustered at 100% identity and then clustered/denoised at 99% identity (dnaclust). Clusters having abundance lower than 3 are discarded. Remaining clusters are then scanned for chimeras with UCHIME denovo and UCHIME reference and clustered at 97% (dnaclust) to form the final clusters/OTUs."),
		newParagraph(asStrong("8-"), " OTUs are then analyzed for taxonomic distribution using a combination of in-house programs and scripts from the Qiime (qiime.org) software suite. Briefly, OTUs are classified with the RDP classifier using an in-house training set containing the complete Greengenes database supplemented with eukaryotic sequences from the Silva databases and a customized set of mitochondria and chloroplasts 16S sequences. ITS2 database consist of the UNITE ITS database (ITS1-ITS2) region. The RDP classifier gives a score (0.00 to 1.00) to each taxonomic depth of each OTUs. Each taxonomic depth having a score >= 0.5 is kept to reconstruct the final lineage. For PacBio datatype, clusters/OTUs are also blasted against nt."),
		newParagraph(asStrong("9 (if 16S)-"), " Using taxonomic lineages obtained from step 9, a raw OTU table is generated. From that raw OTU table, an OTU table containing both bacterial and archeal organisms is generated. From this latter OTU table, one more OTU table is generated: one rarefied to 1000 reads per samples. Additional OTU tables in which OTUs having abundance less than 1 (singlets) in all samples are discarded."),
		newParagraph(asStrong("10-"), " A summary of reads count throughout the different steps of the pipeline is then generated.")
	);
	sMethod          = addTo(sMethod, sMethod.text1)

	# References
	citeRef=getCitations()
	sReferences = newSection("References", exportId="References", protection = PROTECTION.PUBLIC)
	sReferences = addTo(sReferences, citeRef$DNACLUST, citeRef$QIIME, citeRef$FLASH, citeRef$UCHIME)

	report= addTo(
		report,
		sIntroduction,
		sMethod,
		sResults,
		sReferences
	)

	return(report)
}
