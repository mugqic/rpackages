
#########################
# File indexing related accessors
#########################

#' The file index is simply a data.frame which keep tracks of where output files of the R part of the pipelines are located.
#'  This facilites retrieval of e.g plots, result files, etc.
#'
#' @param project.path
#' @param type type of result file, e.g. exploratory, If NULL, whole table is returned.
#' @param as.html If TRUE, the File colum (path to files) will be turned to an href, 
#' and the whole table wil be transformed to html using hwrite.
#'
#'
#' @export
getFileIndex <- function(project.path=getwd(),type=NULL,as.html=FALSE)
{
  require(hwriter)
  idx.fn = file.path(project.path,'index.RData')
  if( !file.exists(idx.fn) ) # Initialize if does not yet exist
  {
    index = data.frame("Id"=character(),"type"=character(),"Description"=character(),"File"=character())
  }else{
    index = readRDS(idx.fn)
  }
  if(!is.null(type)) # subset to requested type
  {
    index = index[index$type %in% type,]
  }
  if(as.html)
  {
    index[['File']] = sapply(index[['File']],function(p){
      hwrite(basename(p),link=p) # could change this in the future, like just the extension, or what not
    })
    index = hwrite(index)
  }
  return(index)
}


#' This functions takes care of removing registered files in the index, either all of them or just of a given type
#'
#' @param project.path
#' @param type type of result file, e.g. exploratory, If NULL, whole table is deleted.
#'
#'
#' @export
resetFileIndex <- function(project.path=getwd(), type=NULL)
{
  idx.fn = file.path(project.path,'index.RData')
  if(is.null(type))
  {
    suppressWarnings( file.remove(idx.fn) )
  }else{
    index = getFileIndex(project.path=project.path,type=type)
    index = index[! index$type %in% type,]
    saveRDS(index,file=idx.fn)
  }
}

#' This funciton adds a new file to the index object. If does not exist yet, creates it.
#' 
#' @param project.path
#' @param path path to the file to commit
#' @param type type of result file. E.g. "exploratory" for exploratory analysis, "qc" for quality control, "custom" for custom results
#' @param description A textual description of the resul in the file. This will typically appear as is in the html report (html tags are ok here!).
#'
#'
#' @export
commitToFileIndex <- function(project.path=getwd(),path,type=c('exploratory','qc','addendum'),description='No description available')
{
  type = match.arg(type)   
  #project.path=getwd();path='temp.pdf';type='exploratory';description='no description';pdf("temp.pdf");plot(1);dev.off()
  if(!file.exists(path)){stop("File specified by arg path does not seem to exist")}
  index = getFileIndex(project.path=project.path)
  index = rbind(index,data.frame('Id'=as.character(1+nrow(index)),'type'=type,'Description'=description,'File'=path
                                 ,check.names=FALSE))
  saveRDS(index, file = file.path(project.path, "index.RData"))
}





#########################
## Data-related Accessors 
#########################


#' This is a function to get cufflinks fpkm valus as an annotatd ExpressionSet Object.
#'
#' @param project.path project path
#' @param sub.path sub path where files are located
#' @param filename.pattern regular expression to find data file
#' @param fvarlabels Which columns of the file list feature attributes
#' @param annotate.samples Whether or not the pData should be populated with getSamples()
#' 
#' @export
getCufflinksFpkmEset <-function(
		fns 			= NULL
 		,project.path	= getwd()
		,sub.path	  	= file.path('fpkm','known')
		,tracking.type	= c("isoforms","genes")
		,fvarlabels=c("tracking_id","class_code","nearest_ref_id","gene_id"
		,"gene_short_name","tss_id","locus","length","SYMBOL")
    	,annotate.samples = TRUE
		)
{
	# project.path=getwd();sub.path = file.path('fpkm','known');tracking.type="isoforms";fvarlabels=c("tracking_id","class_code","nearest_ref_id","gene_id","gene_short_name","tss_id","locus","length","SYMBOL");	annotate.samples=TRUE
	require(Biobase)
	tracking.type 	 = match.arg(tracking.type)
	filename.pattern = paste0(tracking.type,".fpkm_tracking$")

	# Locate the files 
	if(is.null(fns))
	{
		fns = list.files(file.path(project.path,sub.path),pattern=filename.pattern,recursive=TRUE,full.names=TRUE)
	}
	
	# Read the files isoforms.fpkm_tracking
	dat = sapply(fns, function(fn)
	{
		x = read.delim(fn,quote='')
		rownames(x) = x[['tracking_id']]
		x[['SYMBOL']] = x[['gene_short_name']] 
		x[order(rownames(x)),]# return sorted, just to make sure
	},simplify=FALSE)
	names(dat) = basename(dirname(names(dat))) # this is expected to match sample names

	# Check consistency between files
	check =  sapply(dat,function(co)   identical(rownames(co),rownames(dat[[1]])  )    ) 
	if(any(!check)){stop(paste(c('Inconsistent feature names for fpkm for the following files:',fns),collapse=' '))}

	# Extract the fdata, but before check same across all samples
	fdata  = lapply(dat,function(df){df[,colnames(df) %in% fvarlabels]})
	if( length(unique(fdata)) != 1 ){
		stop('Something seems wrong with cufflinks isoforms.fpkm_tracking \
			files: feature info not identical..')
	}
	fdata = fdata[[1]] # no need to check!

	# Extract the fpkms
	eset = sapply(dat,function(df)df[['FPKM']])
	eset = new('ExpressionSet',exprs = eset, featureData=AnnotatedDataFrame(fdata))
	
	# Get the sample annot
  if(annotate.samples)
  {
    pData(eset) = getSamples(project.path=project.path, samples = sampleNames(eset),warn.not.equal=TRUE,full=TRUE )
  }
	
	return(eset)
}
# TODO: cummeRbund better option? Cuffdiff/links output is much richer..
# TODO: will just try to read all rownames(pdata).. checks like for raw reads


# Get an ExpressionSet object from HTseq-count output files
#' This Function loads the read counts calculated the Perl pipeline using HTSeq-count.
#'
#' @param project.path
#' @param ini.file.path Path to ini file. This is used to look up the gene annotation path.
#' @param annotate If TRUE, will annotate object with getSamples() and getGenes()
#' @param sub.path Where to look in project.path for the sample-wise tab-delim read counts file
#' @param filename.pattern Pattern in the file names:   SampleIDpattern. This pattern will be removed to yield column names (sample names)
#' @param feature.exclusion.list character vector of feature names to remove from the final matrix. See default values 
#' @param ... further arguments to getSamples() other than path
#' @export
getHTSeqReadCountsEset <- function(
		 fns 					= NULL
		,project.path           = getwd()
		,ini.file.path          = NULL
        ,annotate               = TRUE
		,sub.path               = "raw_counts"
		,filename.pattern       = "\\.readcounts\\.csv$"
		,feature.exclusion.list = c("no_feature", "ambiguous", "too_low_aQual", "not_aligned", "alignment_not_unique",
		                            "__no_feature","__ambiguous","__too_low_aQual","__not_aligned","__alignment_not_unique")
     )
		
{
  #  project.path=getwd();sub.path="raw_counts";filename.pattern="\\.readcounts\\.csv$";feature.exclusion.list= c("no_feature", "ambiguous", "too_low_aQual", "not_aligned", "alignment_not_unique"); annotate=TRUE
	require(Biobase)

	# Read HT-seq's csv file
	if(is.null(fns)) # if file names not explicit, search recursively
	{
		fns = list.files(file.path(project.path,sub.path),pattern=filename.pattern, full.names=TRUE)
	}
	counts = sapply(fns,function(fn){
			dat = read.delim(fn,header=FALSE,row.names=1)
			colnames(dat) = gsub(filename.pattern,'',basename(fn))
			dat	
	},simplify=FALSE)
	
	# Check consistency and cbind to a matrix if ok
	check =  sapply(counts,function(co)   identical(rownames(co),rownames(counts[[1]])  )    ) 
	if(any(!check)){stop(paste(c('Inconsistent feature names for raw read counts for the following files:',fns),collapse=' '))}
	counts = do.call(cbind,counts)

	# turn into an ExpressionSet
	counts = new("ExpressionSet",exprs=as.matrix(counts))

	# Apply exlusion list but attach as an atribute
	attr(counts,"excluded.features") = counts[ featureNames(counts) %in% feature.exclusion.list ,]
	counts = counts[! featureNames(counts) %in% feature.exclusion.list ,]
	
  # Annotate using getSamples() and getGenes()
  if(annotate)
  {
    # Sample annotation
    pData(counts)  = getSamples(project.path=project.path
                                ,samples = sampleNames(counts),warn.not.equal=TRUE,full=TRUE )
    
    # Feature Annotation 
    fData(counts) = getGenes(project.path=project.path, ini.file.path = ini.file.path
                            ,features = featureNames(counts) ,warn.not.equal = TRUE
                            ,stop.if.not.found=TRUE, param.name='geneAnnotation'
                            ,param.type = 'DEFAULT',attempt.parse.gtf = 'referenceGtf') 
  }

	return(counts)
}





#' This is an R parser for the rsem output. It is not particularly smart for not, Sample ids are assumed to 
#' be the imediate sub-dir of the file. Also, it was only tested in the context of Trinity companion script. 
#' It could be that RSEM usually reports addition anotations from the gtf or whatever, and this funciton will need
#' to be improved.
#'
#' @param project.path
#' @param sub.path The path relative to project path that should be recursively searched for RSEM .results files
#' @param type genes or isoforms?
#' @param value.col which column should be used as value, i.e. exprs()
#' @param annotate.samples Should getSamples() be called to fill phenoData
#' @param round.expected.counts RSEM returns fractional counts. Should they be rounded or not
#'
#' @export
getRsemEset <- function(   project.path = getwd()
                           ,sub.path = "rsem"
                           ,type     = c("genes","isoforms")
                           ,value.col= c("expected_count","TPM","FPKM","IsoPct","effective_length")
                           ,annotate.samples = TRUE
                           ,round.expected.counts=TRUE
)
{
  # project.path=getwd();sub.path="rsem";type="genes";annotate.samples = TRUE;value.col="expected_count";round.expected.counts=TRUE
  type      = match.arg(type) 
  value.col = match.arg(value.col)
  
  # File format assumptions TODO: will need revision in the context of regular RNA-seq in terms of gene annotation
  expected.cols =  switch(type,
                          genes    = c("gene_id"      ,"transcript_id(s)","length","effective_length" ,"expected_count","TPM","FPKM"),
                          isoforms = c("transcript_id","gene_id"         ,"length","effective_length" ,"expected_count","TPM","FPKM","IsoPct")
  )
  
  # What columns will be fvarLabels
  fvarlabels = setdiff(expected.cols, c("expected_count","TPM","FPKM","IsoPct","effective_length","length"))# why length???
  
  # Recursively look for files
  filename.pattern = paste0(type, ".results$")
  fns = list.files(file.path(project.path, sub.path), pattern = filename.pattern, 
                   recursive = TRUE, full.names = TRUE)
  
  
  # Read each individual file
  dat = sapply(fns, function(fn) {
    cat("Reading ",basename(fn),"\n")
    x = read.delim(fn, quote = "",check.names=FALSE,comment.char='')
    # make sure format as expected
    if(any( ! expected.cols %in%  colnames(x) ) ){stop("Not all expected columns found in RSEM output")}
    rownames(x) = x[[1]] # first column should be the id
    x[order(rownames(x)), ] # re-ordering to compare multiple output files
  }, simplify = FALSE)
  
  # Sample ids are assumed to be the imediate sub-dir of the file
  names(dat) = basename(dirname(names(dat)))
  
  # Make sure same features
  check = sapply(dat, function(co) identical(rownames(co), rownames(dat[[1]])))
  if (any(!check)) {stop(paste(c("Inconsistent feature names between some RSEM output files:", fns), collapse = " "))}
  
  # Define the featureData
  fdata = lapply(dat, function(df) {
    df[, colnames(df) %in% fvarlabels]
  })
  if (length(unique(fdata)) != 1) {
    stop("Something seems wrong with RSEM output files: feature annotation between files not identical..")
  }
  fdata = fdata[[1]]
  fdata[['SYMBOL']] = rownames(fdata) # this is just a stansdard column useful to have elsewhere
  
  # Store in an ExpressionSet object  
  eset =sapply(dat, function(df){df[[value.col]]}  )
  eset = new("ExpressionSet", exprs = eset, featureData = AnnotatedDataFrame(fdata))
  if (annotate.samples) {
    pData(eset) = getSamples(project.path = project.path, 
                             samples = sampleNames(eset), warn.not.equal = TRUE, 
                             full = TRUE)
  }
  
  # Round expected counts (RSEM returns fractional counts)
  if(round.expected.counts & value.col=='expected_count')
  {
    exprs(eset) = round(exprs(eset))
  }
  
  return(eset)
}
# TODO: I don't like the fact that RSEM does not report the same "length" for all samples.. (eqvl of cufflinks cuffcompare)
# if FPKM are not based on same gene model, then not comparable?!




#' This is a simple accessor to the readset sheet, typically readsets.csv.
#' @param project.path directory of the sample sheet. Typically project.path
#' @param fn sample sheet filename. Defaults to readsets.csv
#' @param ... further args to pass down to readMaskedTable()
#' @export
getReadSets <- function( project.path=getwd(),fn='readsets.csv', ...)
{
	# project.path=getwd() ; fn='readsets.csv' ;full=TRUE

	# Checks
	if(!file.exists(file.path(project.path,fn)))
	{
		stop('read set sheet csv not found. This file needs to be created.\
			 See function initHiSeqProject() to create one on the fly \
			from a nanuq read set sheet')
	}
		
	# Read
	pdata = readMaskedTable(fn=file.path(path,fn),...)
	# pdata = readMaskedTable(fn=fn)

	# Inspect mandatory columns, set SampleID as table key
	#mandatory.columns = c("SampleID","outlierFlag", "batch","Comment")
	#if (any(!mandatory.columns %in% colnames(pdata))) {
	#	stop(paste("Malformed pdata. Columns ", paste(mandatory.columns, 
	#	    collapse = ", "), " should be present.", sep = ""))
	#}
	if (anyDuplicated(pdata[["ReadSetID"]]) | any(pdata[["SampleID"]] == ""))
	{
		stop(paste("ReadSetID in the readset annotation has to be a unique identifier and SampleID cannot be emtpy", 
		    sep = ""))
	}
	rownames(pdata) = pdata[["ReadSetID"]]
	 
	
	# Special handling of links and paths to fastqs
	#pdata[['fastqs.links']] = strsplit(pdata[['fastqs.links']],split=':')
	pdata[['fastqs.paths']] = strsplit(pdata[['fastqs.paths']],split=':')
	names(pdata[['fastqs.paths']]) = names(pdata[['fastqs.paths']]) = rownames(pdata)
  
	# Return full table or just the SampleID
	return(pdata)  	
}



#' Readsets/samples joined table
#' @export
getPdata <- function(project.path=getwd(),readsets.fn='readsets.csv',...)
{
	s = getSamples(project.path=project.path,...)	
	rs = getReadSets(project.path=project.path,fn=readsets.fn,...)

	if(any(! rs$SampleID %in% s$SampleID)){stop('All SampleIDs in read set sheet should br defined in sample sheet')}

	rs = cbind(s[rs$SampleID,],rs)
	rownames(rs) = rs$ReadSetID
	rs

}



#' This function reads extended annotations for genes (gene models not trasncript). This is typically used for DGEA. 
#' This function tries to be smart about fetching gene annotations, trying first to fetch the table specified in the .ini file
#' (typically parameter geneAnnotation). If the latter param is absent, tries parsing a gtf file (typicailly referenceGtf). 
#' 
#' @param ini.file.path
#' @param project.path
#' @param features If NULL, the entire table is returned. If non-null, will return return the annotation subsetted and ordered by features
#' @param warn.not.equal if TRUE, will issue a warning if not all members of the annotation are requested by the features argument. The function will always throw an error if features are not all defnined in the annotation.  
#' @param stop.if.not.found If TRUE and all attempts at returning an annotation have failed, function will throw an Error instead 
#' of returning NA.
#' @param param.name Name of the parameter which points to the gene annotation tab-delimited file
#' @param param.type Type of the parameter which points to the gene annotation tab-delimited file
#' @param attempt.parse.gtf Parameter name which points to gtf file to attempt parsing of. 
#' 
#' @export
getGenes <- function( 
  project.path        = getwd()
  ,ini.file.path       = NULL
  ,features            = NULL
  ,warn.not.equal      = TRUE
  ,stop.if.not.found   = TRUE
  ,param.name          = 'geneAnnotation' 
  ,param.type          = 'DEFAULT'
  ,attempt.parse.gtf   = 'referenceGtf'
)
{
  # library(gqSeqUtils);project.path = getwd();ini.file.path = NULL;features = NULL;warn.not.equal = TRUE;stop.if.not.found  = TRUE;param.name = 'geneAnnotation' ;param.type = 'default';attempt.parse.gtf = 'referenceGtf'
  
  ## Read the gene annotation from the file specified by ini param geneAnnotation, also try to retrieve referenceGtf
  fn = getIniParams(param.name,param.type, ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  gtf = getIniParams(attempt.parse.gtf,param.type, ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
  
  # Deal with behavior logic
  if( !is.na(fn) ) # If geneAnnotation available, read it
  {
    ann = read.delim(fn,colClasses='character',check.names=FALSE,comment.char='',quote='', stringsAsFactors=FALSE) 
  }else if( !is.na(gtf) ){ # else, will attemp to parse a gtf
    cat(paste0("\nCould not find parameter \"",param.name,"\" in .ini file pointing to a gene annotation file, trying to parse gtf file \"",gtf,"\", this will take a while. Generally preferable to define a proper gene annotation table\n") )
    ann =  extractGeneAnnotationFromGtf(gtf)
    warning("No gene annotation found. Gtf parsed instead.")
  }else if(! stop.if.not.found){
    return(NA)
  }else{
    stop("Could not locate a gene annotation table nor a gtf.")
  }
  
  # Inspect the data frame for standard columns: featureID
  if(!"featureID" %in% colnames(ann)){
    stop("Cannot find column featureID in the annotation specifed by parameter geneAnnotation. This column specifies the unique gene identifier and is mandatory")
  }
  if(anyDuplicated(ann[['featureID']])){stop("featureID must be a unique identifier")}
  rownames(ann) = ann[['featureID']]
  
  #  Inspect the data frame for standard columns: gene SYMBOL
  if( ! "SYMBOL" %in% colnames(ann) )
  {
    warning("Column SYMBOL not found in the loaded gene annotation specified by parameter geneAnnotation. Have set one, identical to column featureID")
    ann[["SYMBOL"]] = ann[["featureID"]]
  }
  
  # Ensure SYMBOL has a value everywhere
  ms = is.na(ann[['SYMBOL']]) | grepl('^ *$',ann[['SYMBOL']]) # NAs or empty symbols..
  ann[['SYMBOL']][ ms ]  = ann[['featureID']][ ms ] # are replace with featureID
  
  #  Inspect the data frame for standard columns: gene.length
  if( ! "gene.length" %in% colnames(ann)  )
  {
    stop("Column gene.length is absent from gene annotation. gqSeqUtils::calculateGeneLengthsFromGtf() could be used to append it.")
  }
  
  # Make sure gene.length is numeric
  ann[["gene.length"]] = as.numeric( ann[["gene.length"]] ) # will naturally throw warning is weird coercions
  
  ## Deal with requested features
  if(!is.null(features))
  {
    if(any( ! features %in% rownames(ann)   )){stop("Annotation missing for one or more features")}
    if(any( ! rownames(ann)  %in%  features ) & warn.not.equal ){warning("features and annotations not exactly equal.")}
    ann = ann[features,]  # subsets and re-orders as requested
  } 
  
  return(ann)    
}




#' This is a simple accessor to the sample sheet, typically samples.csv. The sample sheet is used to keep track of information like
#' labels, experimental variables, flags, etc. For now, the location of the sample sheet is enforced to project.path/samples.csv
#' , it could eventually be changed to something specified in the .ini file...
#' 
#' @param project.path directory of the sample sheet. Typically project.path
#' @param samples A character vector of samples to return. Table will be returned in the same order
#' @param warn.not.equal If TRUE, will issue a warning if not all samples in the sample sheet are request.
#' @param full Should the full samples annotation table be returned or just the SampleIDs
#' @param ... further args to pass down to readMaskedTable()
#' @export
getSamples <- function( project.path=getwd(),samples=NULL,warn.not.equal=TRUE,full=TRUE,...)
{
  # project.path=getwd() ; samples=NULL;warn.not.equal=TRUE;full=TRUE
  fn = file.path(project.path,'samples.csv')
  
  # Checks
  if(!file.exists(fn))
  {
    stop('sample sheet \"samples.csv\" not found. This file needs to be created.\
         See function initHiSeqProject() to create one on the fly \
         from a nanuq read set sheet')
  }
  
  # Read
  pdata = readMaskedTable(fn=fn,...)
  
  # Inspect mandatory columns, set SampleID as table key
  mandatory.columns = c("SampleID","AssemblyGroup","outlierFlag", "batch","Comment")
  if (any(!mandatory.columns %in% colnames(pdata))) {
    stop(paste("Malformed pdata. Columns ", paste(mandatory.columns, 
                                                  collapse = ", "), " should be present.", sep = ""))
  }
  if (anyDuplicated(pdata[["SampleID"]]) | any(pdata[["SampleID"]] == ""))
  {
    stop(paste("SampleID in the sample annotation has to be a unique identifier", 
               sep = ""))
  }
  rownames(pdata) = pdata[["SampleID"]]
  
  # Subset and reorder on requested samples
  if(!is.null(samples))
  {
    if(any( ! samples %in% rownames(pdata)   )){stop("Annotation missing for one or more samples")}
    if(any( ! rownames(pdata)  %in%  samples ) & warn.not.equal ){warning("samples and annotations not exactly equal.")}
    pdata = pdata[samples,]  # subsets and re-orders as requested
  } 
  
  # Return full table or just the SampleID
  if(full){return(pdata)}else{return(rownames(pdata))}     
  }







################################
## Parameters related accessors
################################

#' This function calls getIniParams() looking for the parameter sampleDescriptors. If absent from ini file, then returns SampleID.
#'
#' @param project.path
#' @param ini.file.path
#' @param stop.if.inconsistent Whether or not to read the sample sheet and make sure the descriptoes are columns of the latter.
#'
#' @export
getSampleDescriptors <-function(project.path=getwd(),ini.file.path=NULL, stop.if.inconsistent=TRUE)
{
  descriptors = getIniParams(param='sampleDescriptors', param.type='downstreamAnalyses', ini.file.path=ini.file.path, project.path=project.path 
                             , stopifnotfound=FALSE, as.data.frame=FALSE, split=',')
  if(identical(NA,descriptors))
  {
    warning("Could not find paramter sampleDescriptors in .ini file. Fell back on SampleID ")
    descriptors='SampleID'
  }
  
  # Checks
  if(stop.if.inconsistent)
  {
    s = getSamples(project.path)
    if(any(! descriptors %in% colnames(s)   ))
    {
      stop(paste0("Sample descriptors ",paste(descriptors,collapse=', '), ' are not column names of the sample sheet. See colnames( getSamples() )'))
    }
  }
  return(descriptors)
}




#' This function calls getIniParams() looking for the parameters expVariables and expVariablesBaselineLevels.
#' These parameters specify which columns in the sample sheet define the exp. variables of interest and the "natural baseline levels".
#' If expVariables is absent from the .ini file, the function throws an error. If the baseline levels are absent, then some will be defined with a warning.
#' Finally, this fucntion will also inspect the sample-sheet
#'
#' @export 
getExpVariables <- function(project.path=getwd(),ini.file.path=NULL, stop.if.inconsistent=TRUE,stopifnotfound=TRUE)
{
  # First read the exp. variables
  variables = getIniParams(param='expVariables', param.type='downstreamAnalyses', ini.file.path=ini.file.path, project.path=project.path , stopifnotfound=FALSE, as.data.frame=FALSE, split=',') 
  bl.levels = getIniParams(param='expVariablesBaselineLevels', param.type='downstreamAnalyses', ini.file.path=ini.file.path, project.path=project.path 
                           , stopifnotfound=FALSE, as.data.frame=FALSE, split=',')
  
  if( identical(NA,variables) )
  {
   if(stopifnotfound)
   {
     stop(paste0("Parameter \"expVariables\" is absent from the .ini file"))
   }else{return(NULL)} 
  }
  
  
  if(stop.if.inconsistent)
  {
    
    if(identical(NA,variables))
    {
      stop(paste0("Parameter \"expVariables\" is absent from the .ini file"))
    }
    
    s = getSamples(project.path)
    
    # Make sure the variables actually exist
    if(any(! variables %in% colnames(s)))
    {
      stop(paste0('\"',paste(variables,collapse=', '),'\" is or are not all column names of the sample annotation. See getSamples()'))
    }
    
    # Make sure the var names and levels are valid, i.e. no NA, "", start with a number, etc.
    for(var in variables)
    {
      if(!synthaxValid(var)){stop(paste0("\"",var,"\" is not a synthaxially valid variable name. See make.names()"))}
      x = s[[var]]
      if(is.numeric(x))
      {
        if(any(is.na(x))){stop(paste0("\"",var,"\" is a numerical covariate with illegal values. Check this."))}
      }
      if(is.character(x))
      {
        if(!synthaxValid(x)){stop(paste0("\"",var,"\" does not have synthaxically valid level names. Check this, see make.names()."))}
      }
    }
    
    # Now inspect the baseline levels
    # there must the length(variables)
    # eacch must match (and therefore be valid!!!)
    if(identical(NA, bl.levels))
    {
      warning("Parameter \"expVariablesBaselineLevels\" not found in .ini file. Will use alphabetical order to define default values!")
      bl.levels  = sapply(variables,function(v) sort(s[[v]])[1] )
    }else{
      if(length(bl.levels)!=length(variables))
      {
        stop("The length of expVariables does not match the length of expVariablesBaselineLevels")
      }
      for(i in 1:length(variables))
      {
        if(! bl.levels[i] %in% s[[ variables[i] ]]  &  is(s[[ variables[i] ]],"character")    )
        {
          stop(paste0("\"",bl.levels[i],"\" is not a level of variable \"",variables[i],"\""))
        }
      }
    } 
  }
  return(list("variables"=variables,"bl.levels"=bl.levels)) 
}





#' This function is used to fetch the content of the perl pipeline ini files. Quick and dirty
#'
#' @param param The name of the parameter
#' @param param.type The parameter type, e.g. align
#' @param ini.file.path Path to the .ini parameter file. Can also be a character vector of multiple files to concatenate
#' @param project.path Project directory
#' @param stopifnotfound Will throw error if TRUE, will return NA if not found.
#' @param as.data.frame If param=NULL, then all params are returned. Setting as.data.frame to TRUE returns as a human readable nice table instead of R nested lists object.
#' @param split When returning single values, it is split with strsplit(split)[[1]] before returning
#' @param comment.char lines beginning with this or spaces and this will be ignored. Use comment.char to read in commented parameters
#'
#' @export
getIniParams <- function(param=NULL, param.type='DEFAULT', ini.file.path=NULL, project.path=getwd() , stopifnotfound=TRUE, as.data.frame=FALSE, split=NULL,comment.char='#')
{
	# param=NULL; param.type='DEFAULT'; ini.file.path=c("1.ini","2.ini"); project.path=getwd(); stopifnotfound=TRUE; as.data.frame=FALSE; split=NULL;comment.char='#'
	
	# Temporary fix to account for upper-case [DEFAULT] section
	if(param.type=="default"){param.type="DEFAULT"}
	
	
	# If file not explicited, guess it
	if(is.null(ini.file.path))
	{
		cluster = guessCluster()
		ini.file.path  = list.files(file.path(project.path,'bin'),full.names=TRUE,pattern='\\.ini$')
		ini.file.path  = ini.file.path[grepl(cluster,ini.file.path)]
		if(length(ini.file.path)!=1)
		{
        stop('Unable to guess .ini file path')
		}else{
		    warning(paste0("Path to .ini file was guessed to bin/*.",cluster,".ini using the cluster name. Make sure this is ok!!"))
		}
	}

	if( any( !file.exists(ini.file.path) ) | !file.exists( project.path) )
	{
		stop('.ini file and/or project.path do not exist')
	}
	
	# Read lines
	lines = unlist( sapply(ini.file.path,readLines,warn=FALSE,simplify=FALSE), use.names = FALSE	)
	#lines = readLines(ini.file.path,warn=FALSE) # read
	#lines = lines[lines!='' & !grepl('^#',lines)]	# exclude commented and empty
	
	# Fix [default again]
	lines = sub("\\[default\\]","\\[DEFAULT\\]",lines)
	
	# remove head spaces
	lines  = sub("^ *","",lines)
	
	# excude comment.char
	if(comment.char != '')
	{
		lines = lines[!grepl(paste0("^",comment.char),lines)]
	}
	
	# exclude empty lines
	lines = lines[ lines != '' ] 
	
	# Parse to list
	headers = lines[grepl('^\\[.*\\]$',lines)] # extract headers
	headers.pos = which(lines %in% headers) # position of headers
	starts = headers.pos
	ends   = c(c(headers.pos-1)[-1],length(lines))
    ini = mapply(function(s,e){lines[s:e]},starts,ends)
    names(ini) = sapply(ini,function(x) {   gsub('(\\[|\\])','',x[1])    })
	ini = sapply(ini,function(x){x[-1]})
	ini = ini[sapply(ini,length)>0]
	ini = melt(ini)
	ini$value = as.character(ini$value)
	ini$param = sub(" *=.*","",ini$value)
	ini$param.value = sub(".*?= *","",ini$value,perl=TRUE) # this support space before or after "="
	ini = ini[rev(rownames(ini)),]
	ini = ini[  !duplicated( ini[,c("L1","param")] )  ,]# will overload the last ini content 
	ini = ini[rev(rownames(ini)),]
    ini = by(ini,INDICES=ini$L1,function(df){ split(df$param.value,df$param) })# Turn back to list, yeah, not very neat I know
	ini = lapply(ini,function(x)x)
	
	
	# Check param type
	if( ! param.type %in% names(ini) )
	{
		if( "DEFAULT" %in% names(ini) ){
			param.type="DEFAULT"
		}else{
			if(stopifnotfound)
		 	{
		 			stop(paste('param type',param.type,'not found in .ini file ot it is empty, and no DEFAULT section to fall back on.'))
		 	}else{
				return(NA)
			}
		}
	}

	# Particular param
	if(is.null(param)) # then return whole list
	{
		if( as.data.frame )
		{
			ini = lapply(ini,function(x)
			{
				if(length(x)==0){ x = c("dummyParameter"="")   } # bugfix. melt() would crash and not report empty sections
				x	
			})
			ini = melt(lapply(ini,as.list))
			colnames(ini) = c('value','name','type')
			ini = ini[,c('type','name','value')]
		}
		return(ini)
	}
  
  # Deal with the logic of looking at DEFAULT if  not in param.type
	if(! param %in% names(ini[[param.type]]) ) #If not found in specified param type, look in DEFAULT
	{
		if(param %in% names(ini[['DEFAULT']]) )
		{
		    ret =  ini[['DEFAULT']][[param]]   
		}else{

			if(stopifnotfound)
			{
				stop(paste0('param name \"',param,'\" not found in .ini file.'))
			}else{
        		return(NA)
			}
			
		}
	}else{
	  ret = ini[[param.type]][[param]]  
	}
  
  # Split the return value
  if(!is.null(split))
  {
    ret = strsplit(  ret  ,split=split)[[1]] 
  }
 
  if(length(ret)==0){ret=''} 
  return(ret)


}	

























