#' Get supported pipeline types
#' @export
getSupportedPipelineTypes <- function()
{
  c('RNAseq','DNAseq', 'PacBioAssembly', 'CHIPseq', 'RNAseqDeNovo', 'RRNATagger', 'RRNATaggerDiversity')
}
# TODO: ugh. Woudl be prettier to use global variables



#' List of URLs used by this package
#'
#' @export
getUrls <- function()
{
  list(
    'nanuq' = 'https://genomequebec.mcgill.ca/nanuq',
    'ccapply' = 'https://computecanada.ca/index.php/en/apply-for-an-account',
    'gq' = 'http://www.genomequebec.com',
    'mcgill' = 'http://www.mcgill.ca',
    'mugqic' = 'http://gqinnovationcenter.com'
  )
}

#getLinks <- function()
#{
#  list(
#    'tophat' =
#    ''
#  )
#}


#' Get all common/shared text section
#' @export
getTextChunk <- function(project.path,ini.file.path,citeRef,type="DNA-seq",minLibSize=50,maxLibSize=150)
{
  list(
    'intro1' = newParagraph(
      "This document contains the description of the current MUGQIC ",
      type,
      " analysis. The information presented here reflects the current state of the analysis as of ",
      format(Sys.time(),format="%Y-%m-%d")),

    'intro2' = newParagraph(
      "The DNA sequencing performed at the Innovation center generated between ",
      minLibSize,
      " to ",
      maxLibSize,
      " million ",
      tolower(getIniParams('libraryType', 'default', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)),
      " reads per library using the illumina Hiseq 2000/2500 sequencer. The protocol used for preparing libraries is the " ,
      asStrong(paste(getIniParams('experimentType', 'default', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),"",sep=" ")), 
      "protocol. Base calls are made using the Illumina CASAVA pipeline. Base quality is encoded in phred 33."),

    'trimming1' = newParagraph(
      "Reads are trimmed from the 3' end to have a phred score of at least ",
      ifelse(getIniParams('minQuality', 'trim', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE) !="",getIniParams('minQuality', 'trim', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE) ,30),
      ". Illumina sequencing adapters are removed from the reads, and all reads are   required to have a length of at least" ,
      ifelse(getIniParams('minLength', 'trim', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE) !="", getIniParams('minLength', 'trim', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE),32), 
      " b.p. Trimming and clipping are done with the Trimmomatic software",
      asReference(citeRef$trimmomatic),
      " .", 
      exportId="trim.text1",
      protection = PROTECTION.PUBLIC)
)
}

##Get all known citation as nozzle object
##export
getCitations <- function()
{
  list(
    'tophat' = newJournalCitation(
      authors = "C. Trapnell, L. Pachter, and S.L. Salzberg",
      title = "TopHat: discovering splice junctions with RNA-Seq",
      publication = "Bioinformatics",
      issue = "25",
      number = "9",
      pages = "1105-1111",
      year = "2009",
      url = "http://bioinformatics.oxfordjournals.org/content/25/9/1105"),

    'gatk' = newJournalCitation(
      authors = "M.A. DePristo, E. Banks, R. Poplin, K.V. Garimella, J.R. Maguire, C. Hartl, A.A. Philippakis, G. Del Angel, M.A. Rivas, M. Hanna, et al.",
      title = "A framework for variation discovery and genotyping using next-generation DNA sequencing data",
      publication = "Nature Genetics",
      issue = "43",
      number = "5",
      pages = "491-498",
      year = "2011",
      url = "http://www.nature.com/ng/journal/v43/n5/full/ng.806.html"),

    'cufflinks' = newJournalCitation(
      authors = "A. Roberts, H. Pimentel, C. Trapnell, and L. Pachter",
      title = "Identification of novel transcripts in annotated genomes using RNA-Seq",
      publication = "Bioinformatics",
      issue = "27",
      number = "17",
      pages = "2325-2329",
      year = "2011",
      url = "http://bioinformatics.oxfordjournals.org/content/27/17/2325"),

    'findpeaks' = newJournalCitation(
      authors = "A.P. Fejes, G. Robertson, M. Bilenky, R. Varhol, M. Bainbridge, and S.J.M. Jones",
      title = "FindPeaks 3.1: a tool for identifying areas of enrichment from massively parallel short-read sequencing technology",
      publication = "Bioinformatics",
      issue = "24",
      number = "15",
      pages = "1729-1730",
      year = "2008",
      url = "http://bioinformatics.oxfordjournals.org/content/24/15/1729"),

    'dge' = newJournalCitation(
      authors = "A. Mortazavi, B.A. Williams, K. McCue, L. Schaeffers and B. Wold",
      title = "Mapping and quantifying mammalian transcriptomes by RNA-Seq",
      publication = "Nature Methods",
      issue = "5",
      number = "",
      pages = "621-628",
      year = "2008",
      url = "http://www.nature.com/nmeth/journal/v5/n7/full/nmeth.1226.html"),

    'deseq' = newJournalCitation(
      authors = "S. Anders and W. Huber",
      title = "Differential expression analysis for sequence count data",
      publication = "Genome Biology",
      issue = "11",
      number = "10",
      pages = "R106",
      year = "2010",
      url = "http://genomebiology.com/2010/11/10/R106"),

    'edger' = newJournalCitation(
      authors = "M.D. Robinson, D.J. McCarthy, and G.K. Smyth",
      title = "edgeR: a Bioconductor package for differential expression analysis of digital gene expression data",
      publication = "Bioinformatics",
      issue = "26",
      number = "1",
      pages = "139-140",
      year = "2010",
      url = "http://bioinformatics.oxfordjournals.org/content/26/1/139"),

    'goseq' = newJournalCitation(
      authors = "M.D. Young, M.J. Wakefield, G.K. Smyth, and A. Oshlack",
      title = "Gene ontology analysis for RNA-seq: accounting for selection bias",
      publication = "Genome Biology",
      issue = "11",
      number = "R14",
      pages = "",
      year = "2010",
      url = "http://genomebiology.com/2010/11/2/R14"),

    'tuxedo' = newJournalCitation(
      authors = "Trapnell C., Roberts A., Goff L., Pertea G., Kim D., Kelley D.R., Pimentel H., Salzberg S.L., Rinn J.L., Pachter L.",
      title = "Differential gene and transcript expression analysis of RNA-seq experiments with TopHat and Cufflinks",
      publication = "Nature Protocols",
      issue = "7",
      number = "3",
      pages = "562-78",
      year = "2012",
      url = "http://www.nature.com/nprot/journal/v7/n3/full/nprot.2012.016.html"),

    'trimmomatic' = newWebCitation(
      authors = 'Usadel Lab',
      title = 'Trimmomatic: A flexible read trimming tool for Illumina NGS data',
      url = 'http://www.usadellab.org/cms/index.php?page=trimmomatic'),

    'bwa' = newJournalCitation(
      authors = "Li H., Durbin R.",
      title = "Fast and accurate long-read alignment with Burrows-Wheeler transform",
      publication = "Bioinformatics",
      issue = "26",
      number = "5",
      pages = "589-595",
      year = "2010",
      url = "http://bioinformatics.oxfordjournals.org/content/26/5/589"),

    'picard' = newWebCitation(
      authors = 'Wysoker A., Fennell T., McCowan M., Tibbetts K.',
      title = 'Picard',
      url = 'http://broadinstitute.github.io/picard/'),

    'samtools' = newJournalCitation(
      authors = "Li H., Handsaker B., Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup",
      title = "The Sequence Alignment/Map format and SAMtools.",
      publication = "Bioinformatics",
      issue = "25",
      number = "16",
      pages = "2078-2079",
      year = "2009",
      url = "http://bioinformatics.oxfordjournals.org/content/25/16/2078"),

    'bcftools' = newJournalCitation(
      authors = "Li H., Handsaker B., Wysoker A., Fennell T., Ruan J., Homer N., Marth G., Abecasis G., Durbin R. and 1000 Genome Project Data Processing Subgroup",
      title = "The Sequence Alignment/Map format and SAMtools.",
      publication = "Bioinformatics",
      issue = "25",
      number = "16",
      pages = "2078-2079",
      year = "2009",
      url = "http://bioinformatics.oxfordjournals.org/content/25/16/2078"),

    'snpeff' = newJournalCitation(
      authors = " Cingolani P., Platts A., Wang Le L., Coon M., Nguyen T., Wang L., Land S.J., Lu X., Ruden D.M.",
      title = "A program for annotating and predicting the effects of single nucleotide polymorphisms, SnpEff: SNPs in the genome of Drosophila melanogaster strain w1118; iso-2; iso-3.",
      publication = "Fly(Austin)",
      issue = "6",
      number = "2",
      pages = "80-92",
      year = "2012",
      url = "https://www.landesbioscience.com/journals/fly/article/19695/"),

    'STARaligner' = newJournalCitation(
      authors = " Dobin A., Davis C.A., Schlesinger F., Drenkow J., Zaleski C., Jha S., Batut P., Chaisson M., Gingeras T.R",
      title = "STAR: ultrafast universal RNA-seq aligner",
      publication = "Bioinformatics",
      issue = "29",
      number = "1",
      pages = "15-21",
      year = "2013",
      url = "http://bioinformatics.oxfordjournals.org/content/early/2012/10/25/bioinformatics.bts635"),

    ## PacBio references - JT
    'HGAP' = newJournalCitation(
      authors = "Chen-Shan Chin, David H. Alexander,  Patrick Marks, Aaron A. Klammer, James Drake, Cheryl Heiner, Alicia Clum, Alex Copeland, John Huddleston, Evan E. Eichler, Stephen W. Turner and Jonas Korlach",
      title = "Nonhybrid, finished microbial genome assemblies from long-read SMRT sequencing data",
      publication = "Nature Methods",
      issue = "10",
      number = "0",
      pages = "563-569",
      year = "2013",
      url = "http://www.nature.com/nmeth/journal/v10/n6/full/nmeth.2474.html"
    ),

    'BLASR' = newJournalCitation(
      authors = "Mark J. Chaisson and Glenn Tesler",
      title = "Mapping single molecule sequencing reads using basic local alignment with successive refinement (BLASR): application and theory",
      publication = "BMC Bioinformatics",
      issue = "13",
      number = "238",
      pages = "238",
      year = "2012",
      url = "http://www.biomedcentral.com/1471-2105/13/238/abstract"
    ),

    'Celera' = newJournalCitation(
      authors = "Myers EW, Sutton GG, Delcher AL, Dew IM, Fasulo DP, Flanigan MJ, Kravitz SA, Mobarry CM, Reinert KH, Remington KA, Anson EL, Bolanos RA, Chou HH, Jordan CM, Halpern AL, Lonardi S, Beasley EM, Brandon RC, Chen L, Dunn PJ, Lai Z, Liang Y, Nusskern DR, Zhan M, Zhang Q, Zheng X, Rubin GM, Adams MD, Venter JC.",
      title = "A whole-genome assembly of Drosophila",
      publication = "Science",
      issue = "24",
      number = "287",
      pages = "2196-204",
      year = "2000",
      url = "http://www.ncbi.nlm.nih.gov/pubmed/10731133"
    ),

    'PacBio' = newWebCitation(
      authors = "Pacific BioSciences",
      title = "Publications",
      url = "http://www.pacificbiosciences.com/news_and_events/publications/"
    ),
	
	## RRNATagger references - JT
	'DNACLUST' = newJournalCitation(
      authors = "Ghodsi M, Liu B, Pop M.",
      title = "DNACLUST: accurate and efficient clustering of phylogenetic marker genes.",
      publication = "BMC Bioinformatics",
      issue = "", 
      number = "12",
      pages = "271",
      year = "2011",
      url = "http://www.biomedcentral.com/1471-2105/12/271"
    ),  

    'QIIME' = newJournalCitation(
      authors = "Caporaso JG, Kuczynski J, Stombaugh J, Bittinger K, Bushman FD, Costello EK, Fierer N, Peña AG, Goodrich JK, Gordon JI, Huttley GA, Kelley ST, Knights D, Koenig JE, Ley RE, Lozupone CA, McDonald D, Muegge BD, Pirrung M, Reeder J, Sevinsky JR, Turnbaugh PJ, Walters WA, Widmann J, Yatsunenko T, Zaneveld J, Knight R.",
      title = "QIIME allows analysis of high-throughput community sequencing data",
      publication = "Nature Methods",
      issue = "7",
      number = "5",
      pages = "335-6",
      year = "2010",
      url = "http://www.nature.com/nmeth/journal/v7/n5/full/nmeth.f.303.html"
    ),  

    'FLASH' = newJournalCitation(
      authors = "Magoc T and Salzberg SL.",
      title = "FLASH: fast length adjustment of short reads to improve genome assemblies. ",
      publication = "Bioinformatics",
      issue = "27",
      number = "21",
      pages = "2957-2963",
      year = "2011",
      url = "http://bioinformatics.oxfordjournals.org/content/27/21/2957.long"
    ),  

    'UCHIME' = newJournalCitation(
      authors = "Edgar RC, Haas BJ, Clemente JC, Quince C and Knight R.",
      title = "UCHIME improves sensitivity and speed of chimera detection.",
      publication = "Bioinformatics",
      issue = "27",
      number = "16",
      pages = "2194-2200",
      year = "2011",
      url = "http://bioinformatics.oxfordjournals.org/content/27/16/2194.long"
    ),  	
    
    'Greengenes1' = newJournalCitation(
      authors = "DeSantis TZ, Hugenholtz P, Larsen N, Rojas M, Brodie EL, Keller K, Huber T, Dalevi D, Hu P, Andersen GL.",
      title = "Greengenes, a chimera-checked 16S rRNA gene database and workbench compatible with ARB.",
      publication = "Appl. Environ. Microbiol.",
      issue = "72",
      number = "7",
      pages = "5069",
      year = "2006",
      url = "http://aem.asm.org/content/72/7/5069.long"
    ),  	
    
    'PyNAST' = newJournalCitation(
      authors = "Caporaso JG, Bittinger K, Bushman FD, DeSantis TZ, Andersen GL, Knight R.",
      title = "PyNAST: a flexible tool for aligning sequences to a template alignment.",
      publication = "Bioinformatics",
      issue = "26",
      number = "2",
      pages = "266",
      year = "267",
      url = "http://bioinformatics.oxfordjournals.org/content/26/2/266.long"
    ),  	
    
    'FastTree' = newJournalCitation(
      authors = "Price MN, Dehal PS, Arkin AP.",
      title = "FastTree: computing large minimum evolution trees with profiles instead of a distance matrix.",
      publication = "Mol. Biol. Evol.",
      issue = "26",
      number = "7",
      pages = "1641",
      year = "1650",
      url = "http://mbe.oxfordjournals.org/cgi/pmidlookup?view=long&pmid=19377059"
    ),  	

    ## CHIPseq references - JS
    'homer' = newJournalCitation(
      authors = "S. Heinz, C. Benner, N. Spann, E. Bertolino, Y.C. Lin, P. Laslo, J.X. Cheng, C. Murre, H. Singh, and C.K. Glass.",
      title = "Simple combinations of lineage-determining transcription factors prime cis-regulatory elements required for macrophage and b cell identities",
      publication = "Molecular cell",
      issue = "38",
      number = "4",
      pages = "576-589",
      year = "2010",
      url = "http://dx.doi.org/10.1016/j.molcel.2010.05.004"
    ),

    'MACS' = newJournalCitation(
      authors = "Y. Zhang, T. Liu, C.A. Meyer, J. Eeckhoute, D.S. Johnson, B.E. Bernstein, C. Nussbaum, R.M. Myers, M. Brown, W. Li, et al.",
      title = "Model-based analysis of chip-seq (macs)",
      publication = "Genome Biology",
      issue = "9",
      number = "9",
      pages = "R137",
      year = "2008",
      url = "http://liulab.dfci.harvard.edu/MACS/"
    ),

    'homerWeb' = newWebCitation(
      authors = "Benner C., Heinz S., Hutt K., Lin Y., Hsiao G., Alcalde F., Stender J., Sullivan A., Spann N., Garcia-Bassets I., Lam M., Rehlial M.",
      title = "HOMER website",
      url = "http://homer.salk.edu/homer/"
    ),

    'trinity' = newJournalCitation(
      authors = "Grabherr M.G., Haas B.J., Yassour M., Levin J.Z., Thompson D.A., Amit I., Adiconis X., Fan L., Raychowdhury R., Zeng Q., Chen Z., Mauceli E., Hacohen N., Gnirke A., Rhind N., di Palma F., Birren B.W., Nusbaum C., Lindblad-Toh K., Friedman N., Regev A.",
      title = "Full-length transcriptome assembly from RNA-seq data without a reference genome",
      publication = "Nature Biotechnology",
      issue = "29",
      number = "7",
      pages = "644-652",
      year = "2011",
      url = "http://www.nature.com/nbt/journal/vaop/ncurrent/abs/nbt.1883.html"
    ),

    'trinityProtocol' = newJournalCitation(
      authors = "Haas B.J., Papanicolaou A., Yassour M., Grabherr M., Blood P.D., Bowden J., Couger M.B., Eccles D., Li B., Lieber M., Macmanes M.D., Ott M., Orvis J., Pochet N., Strozzi F., Weeks N., Westerman R., William T., Dewey C.N., Henschel R., Leduc R.D., Friedman N., Regev A.",
      title = "De novo transcript sequence reconstruction from RNA-seq using the Trinity platform for reference generation and analysis",
      publication = "Nature Protocols",
      issue = "8",
      number = "8",
      pages = "1494-1512",
      year = "2013",
      url = "http://www.nature.com/nprot/journal/v8/n8/full/nprot.2013.084.html"
    ),

    'trinityWeb' = newWebCitation(
      authors = "Broad Institute and Hebrew University of Jerusalem",
      title = "RNA-Seq De novo Assembly Using Trinity",
      url = "http://trinityrnaseq.sourceforge.net/"
    ),

    'diginorm' = newWebCitation(
      authors = "C. Titus Brown, Adina Howe, Qingpeng Zhang, Alexis B. Pyrkosz, Timothy H. Brom",
      title = "A Reference-Free Algorithm for Computational Normalization of Shotgun Sequencing Data. arXiv:1203.4802 [q-bio.GN] (2012)",
      url = "http://arxiv.org/abs/1203.4802"
    ),

    'rsemWeb' = newWebCitation(
      authors = "Bo Li and Colin N. Dewey",
      title = "RSEM (RNA-Seq by Expectation-Maximization)",
      url = "http://deweylab.biostat.wisc.edu/rsem/"
    ),

    'rsem' = newJournalCitation(
      authors = "Bo Li and Colin N. Dewey",
      title = "RSEM: accurate transcript quantification from RNA-Seq data with or without a reference genome",
      publication = "BMC Bioinformatics",
      issue = "12",
      number = "323",
      pages = "",
      year = "2011",
      url = "http://www.biomedcentral.com/1471-2105/12/323"
    )
  )
}
# TODO: might eventually want to read in bibteX file and loop to convert to Nozzle citation
# Also, might mess up nozzle referencing system.
#newJournalCitation(
#  authors = "",
#  title = "",
#  publication = "",
#  issue = "",
#  number = "",
#  pages = "",
#  year = "",
#  url = "")
