#' Nozzle report for PacBio assembly reports.
#'
#' Created 2013-10-16
#' @author Julien Tremblay julien.tremblay_AT_mail.mcgill.ca 
#' @export
mugqicPipelineReportRRNATaggerDiversity <- function(report, project.path, ini.file.path, report.path) {
	
	# Overview
	sIntroduction = newSection("Introduction", exportId="Introduction", protection = PROTECTION.PUBLIC)

	sIntroduction.text1=newParagraph(
		"This document contains the current MUGQIC rRNA amplicon secondary analysis results. The information presented here reflects the current state of the analysis as of ", format(Sys.time(),format="%Y-%m-%d")
	)
	
  sIntroduction.text2 = newParagraph(
		"This secondary analysis of 16S/18S/ITS results (aka diversity pipeline) generated the following results/files: "
	)
	
	sIntroduction.text3  = newList(
		newParagraph(asStrong("Alpha diversity"), " which refers to the diversity inside each samples (i.e. how many different OTU are we observing in sample x?)"),
		newParagraph(asStrong("Beta diversity"), " which refersto the diversity accros samples (i.e. how similar/different are samples x and y?)"),
		newParagraph(asStrong("Phylogenic tree."), " Places each OTU in a taxonomic tree. Gives information on how distant are each OTUs phylogenetically."),
		newParagraph(asStrong("OTU heatmap."), " Heatmap representation of OTU distribution across samples."),
		newParagraph(asStrong("OTU tables."), " Matrices containing each OTU present per samples."),
		newParagraph(asStrong("RDP classification sheets."), " RDP classification lineages and scores for each OTUs."),
		newParagraph(asStrong("Tax summary."), " Contains each taxonomic lineage and read counts associated to each OTUs.")
	);

	sIntroduction = addTo(
		sIntroduction, 
		sIntroduction.text1, 
		sIntroduction.text2,
    sIntroduction.text3
	)

	print("Intro section ... Done")
		
	# RESULTS (MASTER)
	sResults       = newSection("Analysis and results", exportId="Results", protection = PROTECTION.PUBLIC)

	# RESULTS
	if (!file.exists(file.path(report.path,"Alpha_div"))) {
    file.symlink(file.path("../", "alpha_div"), file.path(report.path, "Alpha_div") )
	}
	if (!file.exists(file.path(report.path,"Beta_div"))) {
    file.symlink(file.path("../", "beta_div"), file.path(report.path, "Beta_div") )
	}
	if (!file.exists(file.path(report.path,"Fasttree"))) {
    file.symlink(file.path("../", "fasttree"), file.path(report.path, "Fasttree") )
	}
	if (!file.exists(file.path(report.path,"Heatmap"))) {
    file.symlink(file.path("../", "heatmap"), file.path(report.path, "Heatmap") )
	}
	if (!file.exists(file.path(report.path,"Obs"))) {
    file.symlink(file.path("../", "obs"), file.path(report.path, "Obs") )
	}
	if (!file.exists(file.path(report.path,"Otu_tables"))) {
    file.symlink(file.path("../", "otu_tables"), file.path(report.path, "Otu_tables") )
	}
	if (!file.exists(file.path(report.path,"Pynast"))) {
    file.symlink(file.path("../", "pynast"), file.path(report.path, "Pynast") )
	}
	if (!file.exists(file.path(report.path,"Rdp"))) {
    file.symlink(file.path("../", "rdp"), file.path(report.path, "Rdp") )
	}
	if (!file.exists(file.path(report.path,"Tax_summary"))) {
    file.symlink(file.path("../", "tax_summary"), file.path(report.path, "Tax_summary") )
	}

	# RESULTS-TAXONOMY
	ssTax          = newSection("Taxonomy")

	#if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.jpeg"))) {
	#	file.symlink(file.path("..", "../diversity/tax_summary/taxonomy_phylum_L2.jpeg"), file.path(report.path, "taxonomy_phylum_L2.jpeg") )
	#}
	#if (!file.exists(file.path(report.path,"taxonomy_phylum_L2.pdf"))) {
	#	file.symlink(file.path("..", "../diversity/tax_summary/taxonomy_phylum_L2.pdf"), file.path(report.path, "taxonomy_phylum_L2.pdf") )
	#}
	if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_absolute.jpeg"))) {
		file.symlink(file.path("..", "../diversity/tax_summary/taxonomy_phylum_L2_absolute.jpeg"), file.path(report.path, "taxonomy_phylum_L2_absolute.jpeg") )
	}
	if (!file.exists(file.path(report.path,"taxonomy_phylum_L2_absolute.pdf"))) {
		file.symlink(file.path("..", "../diversity/tax_summary/taxonomy_phylum_L2_absolute.pdf"), file.path(report.path, "taxonomy_phylum_L2_absolute.pdf") )
	}

	ssTax.text1    = newParagraph(
		"Figure 1 shows absolute taxonomic distribution of your samples"
	)
	fTaxAbs = newFigure( file="taxonomy_phylum_L2_absolute.jpeg", fileHighRes="taxonomy_phylum_L2_absolute.pdf", protection=PROTECTION.PUBLIC )
	#ssTax.text2    = newParagraph(
	#	"Figure 2 shows relative distribution of your samples"
	#)
	#fTaxAbs = newFigure( file="taxonomy_phylum_L2.jpeg", fileHighRes="taxonomy_phylum_L2.pdf", protection=PROTECTION.PUBLIC )
	
  ssTax          = addTo(ssTax, ssTax.text1)
	ssTax          = addTo(ssTax, fTaxAbs) 
	#ssTax          = addTo(ssTax, ssTax.text2)
	#ssTax          = addTo(ssTax, fTaxRel) 
  
  ## OTU heatmap
	ssHeat         = newSection("OTU heatmap")

	if (!file.exists(file.path(report.path,"OTU_heatmap.jpeg"))) {
		file.symlink(file.path("..", "../diversity/heatmap/OTU_heatmap.jpeg"), file.path(report.path, "OTU_heatmap.jpeg") )
	}
	if (!file.exists(file.path(report.path,"OTU_heatmap.pdf"))) {
		file.symlink(file.path("..", "../diversity/heatmap/OTU_heatmap.pdf"), file.path(report.path, "OTU_heatmap.pdf") )
	}
	ssHeat.text1 = newParagraph(
		"Figure 2 shows a heatmap of your OTU distribution. Samples were clustered (y-axis) using euclidean distances."
	)
	fHeat = newFigure( file="OTU_heatmap.jpeg", fileHighRes="OTU_heatmap.pdf", protection=PROTECTION.PUBLIC )
  ssHeat = addTo(ssHeat, ssHeat.text1)
  ssHeat = addTo(ssHeat, fHeat)

  ## UPGMA clustering
  ssUPGMA = newSection("UPGMA clustering")

	if (!file.exists(file.path(report.path,"UPGMA_weighted.jpeg"))) {
		file.symlink(file.path("..", "../diversity/beta_div/weighted_unifrac_otu_table_filtered_UPGMA.tree.jpeg"), file.path(report.path, "UPGMA_weighted.jpeg") )
	}
	ssUPGMA.text1 = newParagraph(
		"Figure 3 shows how your samples clusters one another using Unweighted Pair Group Method with Arithmetic Mean clustering."
	)
	fUPGMA = newFigure( file="UPGMA_weighted.jpeg", fileHighRes="UPGMA_weighted.pdf", protection=PROTECTION.PUBLIC )
  ssUPGMA = addTo(ssUPGMA, ssUPGMA.text1)
  ssUPGMA = addTo(ssUPGMA, fUPGMA)
  

	## RESULTS-ALPHADIV-BETADIV
	ssDiversity          = newSection("Diversity")
	if (!file.exists(file.path(report.path,"rarefactionPlots.jpeg"))) {
		file.symlink(file.path("..", "../diversity/alpha_div/plots/rarefactionPlots.jpeg"), file.path(report.path, "rarefactionPlots.jpeg") )
	}
	ssDiversity.text1    = newParagraph(
		"Figure 4 shows rarefaction curves for each of your samples (alpha diversity - Observed OTUs). These curves help to estimate if there was enough sequencing depth to saturate all OTUs present in your samples."
	)
	fRarefaction = newFigure( file="rarefactionPlots.jpeg", fileHighRes="rarefactionPlots.pdf", protection=PROTECTION.PUBLIC )
 
	# RESULTS-BETADIV
	#if (!file.exists(file.path(report.path,"UniFrac.jpeg"))) {
	#	file.symlink(file.path("..", "diversity/alpha_div/plots/UniFrac.jpeg"), file.path(report.path, "UniFrac.jpeg") )
	#}
	#ssDiversity.text2    = newParagraph(
	#	"Figure 4 shows UniFrac distances PCA plots (beta diversity)"
	#)
	#fRarefaction = newFigure( file="UniFrac.jpeg", fileHighRes="UniFrac.pdf", protection=PROTECTION.PUBLIC )

	## QIIME ALPHA DIV PLOTS (HTML)
	alphaDiv.file = "Alpha_div/plots/rarefaction_plots.html"
	alphaDiv.text1 = newParagraph(asStrong(asLink(url=alphaDiv.file,"Interactive html plots for alpha diversity is available here.")),protection = PROTECTION.PUBLIC)
	
  ## QIIME BETA DIV PLOTS 2D (HTML)
	betaDiv.file1 = "Beta_div/plots/weighted_unifrac_otu_table_filtered_coords_2D_PCoA_plots.html"
	betaDiv.text1 = newParagraph(asStrong(asLink(url=betaDiv.file1,"Interactive 2D html plots for beta diversity is available here.")),protection = PROTECTION.PUBLIC)

  ## QIIME BETA DIV PLOTS 3D (HTML)
	betaDiv.file2 = "Beta_div/3d_plots/weighted_unifrac_otu_table_filtered_coords_3D_PCoA_plots.html"
	betaDiv.text2 = newParagraph(asStrong(asLink(url=betaDiv.file2,"Interactive 3D html plots for beta diversity is available here.")),protection = PROTECTION.PUBLIC)
  
  ssDiversity = addTo(ssDiversity, ssDiversity.text1)
  ssDiversity = addTo(ssDiversity, fRarefaction)
  ssDiversity = addTo(ssDiversity, alphaDiv.text1)
  ssDiversity = addTo(ssDiversity, betaDiv.text1)
  ssDiversity = addTo(ssDiversity, betaDiv.text2)
	
  sResults       = addTo(sResults, ssTax)
  sResults       = addTo(sResults, ssHeat)
  sResults       = addTo(sResults, ssUPGMA)
  sResults       = addTo(sResults, ssDiversity)
	
  # METHODS
	sMethod        = newSection("Methods");
	sMethod.text1  = newList(
		newParagraph(asStrong("1-"), "This 16S/18S/ITS analysis pipeline takes for input a OTU table and a corresponding fasta file. These files are generated from the RRNATagger companion pipeline (or by other means...)"),
		newParagraph(asStrong("2-"), "Taxonomy assignment of OTUs is performed using the RDP classifier with a modified Greengenes training set built from a concatenation of the Greengenes database, Silva eukaryotes 18S r108, chloroplasts and mitochondria sequences rRNA SSU sequences."),
		newParagraph(asStrong("3-"), "Then, from these classified OTUs, diversity metrics are obtained by aligning OTU sequences on a Greengenes core reference alignment (DeSantis, 2006) using the PyNAST aligner (Caporaso, 2010). Alignments are filtered to keep only the hypervariable region part of the alignment."),
		newParagraph(asStrong("4-"), "A phylogenetic tree is then built from that alignment (from step 3) with FastTree (Price, 2010). Alpha (observed species) and beta (-weighted, unweighted UniFrac and Bray Curtis distances) diversity metrics, taxonomic classifications, UPGMA clustering and OTUs heatmap are then computed using the QIIME software suite (Kuczynski, 2011; Caporaso, 2010).")
	);

	sMethod          = addTo(sMethod, sMethod.text1)

	# References
	citeRef=getCitations()
	sReferences = newSection("References", exportId="References", protection = PROTECTION.PUBLIC)
	sReferences = addTo(sReferences, citeRef$QIIME, citeRef$Greengenes1, citeRef$PyNAST, citeRef$FastTree)

	report= addTo(
		report,
		sIntroduction,
		sMethod,
		sResults,
		sReferences
	)

	return(report)
}
