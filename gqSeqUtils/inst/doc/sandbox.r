
#Nozzle.R1::addTo                         Nozzle.R1::getGoogleAnalyticsId          Nozzle.R1::newResult
#Nozzle.R1::addToInput                    Nozzle.R1::getLogo                       Nozzle.R1::newSection
#Nozzle.R1::addToIntroduction             Nozzle.R1::getMaintainerAffiliation      Nozzle.R1::newSubSection
#Nozzle.R1::addToMeta                     Nozzle.R1::getMaintainerEmail            Nozzle.R1::newSubSubSection
#Nozzle.R1::addToMethods                  Nozzle.R1::getMaintainerName             Nozzle.R1::newTable
#Nozzle.R1::addToOverview                 Nozzle.R1::getRendererDate               Nozzle.R1::newWebCitation
#Nozzle.R1::addToReferences               Nozzle.R1::getRendererName               Nozzle.R1::PROTECTION.GROUP
#Nozzle.R1::addToResults                  Nozzle.R1::getReportId                   Nozzle.R1::PROTECTION.PRIVATE
#Nozzle.R1::addToSummary                  Nozzle.R1::getReportTitle                Nozzle.R1::PROTECTION.PUBLIC
#Nozzle.R1::asCode                        Nozzle.R1::getSignificantEntity          Nozzle.R1::PROTECTION.TCGA
#Nozzle.R1::asEmph                        Nozzle.R1::getSignificantResultsCount    Nozzle.R1::RDATA.REPORT
#Nozzle.R1::asFilename                    Nozzle.R1::getSoftwareName               Nozzle.R1::SECTION.CLASS.META
#Nozzle.R1::asLink                        Nozzle.R1::getSoftwareVersion            Nozzle.R1::SECTION.CLASS.RESULTS
#Nozzle.R1::asParameter                   Nozzle.R1::getSummary                    Nozzle.R1::setContactInformation
#Nozzle.R1::asReference                   Nozzle.R1::getTableFile                  Nozzle.R1::setCopyright
#Nozzle.R1::asStrong                      Nozzle.R1::HTML.FRAGMENT                 Nozzle.R1::setCustomPrintCss
#Nozzle.R1::asSummary                     Nozzle.R1::HTML.REPORT                   Nozzle.R1::setCustomScreenCss
#Nozzle.R1::asValue                       Nozzle.R1::IMAGE.TYPE.PDF                Nozzle.R1::setFigureFile
#Nozzle.R1::DEFAULT.REPORT.FILENAME       Nozzle.R1::IMAGE.TYPE.RASTER             Nozzle.R1::setFigureFileHighRes
#Nozzle.R1::DEFAULT.SIGNIFICANT.ENTITY    Nozzle.R1::IMAGE.TYPE.SVG                Nozzle.R1::setGoogleAnalyticsId
#Nozzle.R1::getContactInformationEmail    Nozzle.R1::isFigure                      Nozzle.R1::setLogo
#Nozzle.R1::getContactInformationLabel    Nozzle.R1::isTable                       Nozzle.R1::setMaintainerAffiliation
#Nozzle.R1::getContactInformationMessage  Nozzle.R1::LOGO.BOTTOM.CENTER            Nozzle.R1::setMaintainerEmail
#Nozzle.R1::getContactInformationSubject  Nozzle.R1::LOGO.BOTTOM.LEFT              Nozzle.R1::setMaintainerName
#Nozzle.R1::getCopyrightOwner             Nozzle.R1::LOGO.BOTTOM.RIGHT             Nozzle.R1::setNextReport
#Nozzle.R1::getCopyrightStatement         Nozzle.R1::LOGO.TOP.CENTER               Nozzle.R1::setParentReport
#Nozzle.R1::getCopyrightUrl               Nozzle.R1::LOGO.TOP.LEFT                 Nozzle.R1::setPreviousReport
#Nozzle.R1::getCopyrightYear              Nozzle.R1::LOGO.TOP.RIGHT                Nozzle.R1::setReportTitle
#Nozzle.R1::getCreatorDate                Nozzle.R1::newCitation                   Nozzle.R1::setSignificantEntity
#Nozzle.R1::getCreatorName                Nozzle.R1::newCustomReport               Nozzle.R1::setSoftwareName
#Nozzle.R1::getCustomPrintCss             Nozzle.R1::newFigure                     Nozzle.R1::setSoftwareVersion
#Nozzle.R1::getCustomScreenCss            Nozzle.R1::newJournalCitation            Nozzle.R1::setTableFile
#Nozzle.R1::getExportedElement            Nozzle.R1::newList                       Nozzle.R1::TABLE.SIGNIFICANT.DIGITS
#Nozzle.R1::getExportedElementIds         Nozzle.R1::newParagraph                  Nozzle.R1::writeReport
#Nozzle.R1::getFigureFile                 Nozzle.R1::newParameterList              
#Nozzle.R1::getFigureFileHighRes          Nozzle.R1::newReport 




# rsync -avPLr 20* flefebvre1@guillimin.clumeq.ca:~
# git clone https://lefebvrf@bitbucket.org/mugqic/rpackages.git 
# module load mugqic/R
# R CMD INSTALL --library=$R_LIBS rpackages/mugqicReporting
# R --vanilla
options(stringsAsFactors=FALSE)
library(ReportingTools)
library(Nozzle.R1)
library(stringr)
library(Biobase)
library(reshape)
library(gqUtils)
library(Biobase)


#initIllmSeqProject( nanuq.file= 'nanuq.csv' ,sequencer='hiseq' ,fastqs.links.dir = 'raw_reads_test' ,fastqs.legacy.linking=TRUE)

				
mugqicPipelineReportRNAseq <- function(report,project.path,ini.file.path,report.path)
{




	
	# project.path = getwd(); report.path = file.path(project.path,'2013_02_26_report'); ini.file.path=NULL
	
	##
	### Preparation (paths and params)

	# Where file are written
	fpath = file.path(report.path,'files')

	# Define the set of citations will be made by this report (later consider copying only certain files to reduce size)
	citations =  getCitations()[c('trimmomatic','tophat','cufflinks','edger','goseq','gatk')]
	
	# Should bams be copied or not (hard default FALSE)
	include.bams = getIniParams('includeBams','report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	if( is.na(include.bams) ){include.bams 	= FALSE ;print("includeBams not found in .ini file. Using hard default (FALSE)")}	

	# Should filtered Fastq be included?
	include.filtered.fastq = getIniParams('includeFilteredFastq','report', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
	if( is.na(include.filtered.fastq) ){include.filtered.fastq = FALSE ;print("includeFilteredFastq not found in .ini file. Using hard default (FALSE)")}

	##
	### Copy all the necessary files..
	print('Copying files...')

	# Copy sample annotation
	file.copy(file.path(project.path,'samples.csv'),fpath, overwrite=TRUE)

	# Block copying of bin/, metrics/, etc. 
	sapply(c('bin','metrics','fpkm','raw_counts','tracks','DGE','cuffdiff'),function(x)
	{
		file.copy(file.path(project.path,x), fpath, recursive=TRUE, overwrite=TRUE )
	})

	# Copy filtered fastqs if requested, but we still want to copy the .trim out files
	dir.create(file.path(fpath,'reads'),showWarnings=FALSE,recursive=TRUE)
	if(include.filtered.fastq){
		file.copy(file.path(project.path,'reads'), fpath, recursive=TRUE, overwrite=TRUE )
	}else{ # else, copy only the trim out files
		dir.create(file.path(fpath,'reads'),showWarnings=FALSE,recursive=TRUE)
		file.copy(list.files(file.path(project.path,'reads'),full.names=TRUE,recursive=FALSE,pattern='\\.trim\\.out')
			,file.path(fpath,'reads'), overwrite=TRUE,recursive=TRUE)
	}
	
	# Copy bams if requested
	dir.create(file.path(fpath,'alignment'),showWarnings=FALSE,recursive=TRUE)
	if(include.bams)
	{
		bams = list.files(file.path(project.path,'alignment'),pattern='\\.merged\\.mdup\\.ba(m|i)$',full.names=TRUE,recursive=TRUE)
		file.copy(bams,file.path(fpath,'alignment'), overwrite=TRUE,recursive=TRUE)
	}
	


	##
	### Report!

	# Overview Section
	file.copy(system.file('documents','RNAseq_pipeline.pdf',package='mugqicReporting'), fpath,overwrite=TRUE)
	file.copy(system.file('documents','RNAseq_pipeline.png',package='mugqicReporting'), fpath,overwrite=TRUE)
	file.copy(system.file('documents','RNAseq_pipeline.pptx',package='mugqicReporting'), fpath,overwrite=TRUE)
	report = addTo(report,addTo(newSection( "Overview" )
		#, newParagraph("bla ") 
		, newFigure(file.path('files','RNAseq_pipeline.png'),
		 'Pipeline workflow diagram.'
			,fileHighRes =file.path('files','RNAseq_pipeline.pdf'))
 		))	
		

	#' Creates a Nozzle section to report sample annotation.
	#' @export
	reportSampleAnnotation <- function(report, project.path, report.path)
	{
		s = getSamples(path=project.path,full=TRUE)
		write.csv(s,file=file.path(report.path,'files','samples.csv'),row.names=FALSE)
		
		report = addTo(report,addTo(newSection( "Sample List" )
		#, newParagraph("bla") 
		, newTable(s,'Samples included in the analysis and the corresponding \
				 annotations.',file=file.path('files','samples.csv'))
 		))
		return(report)
	}
	# Let's first get the sample list
	report = reportSampleAnnotation(report,project.path,report.path)
	
	# Raw Read files
	reportRaw <- function(report)
	{
		report = addTo( report,addTo(newSection( "Raw Sequencing Data" )

		, newParagraph(
		"Raw fastq files are not provided as a part of analysis reports and should be downloaded your "
		,asLink( getUrls()[['nanuq']],"Nanuq") , " project page, under the tab " ,asEmph("HiseqReadSets. ")
		,"Details of the library preparation, sequencing process and a thorough characterization thereof are available in Nanuq as well" )

		, newParagraph(
		"Users interested in storing large data sets and peforming further analysis themselves are encouraged to apply for a "
		,asLink( getUrls()[['ccapply']], "Compute Canada")," account. This gives researcher access to HPC ressources and\
			faciliates data transfer with our bioinformatics service." )	
 	

		))
		return(report)
	}
	report = reportRaw(report)	

	
	#' Report reference sequence/genome
	#'
	#'@export
	reportReference <- function(report,project.path,ini.file.path,report.path)
	{
		genome =  getIniParams(param = 'referenceFasta',ini.file.path = ini.file.path, project.path = project.path) 
		genome = gsub('\\.(fa|fasta)','', basename(genome) )
		chrsizes.fn = getIniParams(param = 'chromosomeSizeFile',ini.file.path = ini.file.path, project.path = project.path)
		file.copy(chrsizes.fn, file.path(report.path,'files'), overwrite=TRUE)
		chrsizes = read.delim(chrsizes.fn, header=FALSE)	
		colnames(chrsizes ) = c('Chromosome','Size (Mb)');chrsizes[,2] = chrsizes[,2]/1000000
		report = addTo( report,addTo(newSection( "Reference Sequence" )

		, newParagraph(
		"Processed reads were aligned to reference ", asStrong(genome),".")
		, newTable(chrsizes,'Reference Sequences',file=file.path('files',basename(chrsizes.fn)),significantDigits=Inf)	
 	
		))
		return(report)
	} # TODO: this section needs more meat
	# About the Reference sequence
	report = reportReference(report,project.path,ini.file.path,report.path)


	# Report on Trimming & Alignment & Coverage. Expect file to already be present in files/
	#' @export
	reportAlignmentsTable <- function(project.path,ini.file.path,report.path, include.bams, include.filtered.fastq )
	{

		df = getSamples(path = project.path, fn = "samples.csv", full = TRUE)[,'SampleID',drop=FALSE]

		# Deal with the read stats 
		rs = loadReadStats(path=file.path(project.path,'metrics')  ,samples = rownames(df) , merge.by.sample=TRUE)
		for(co in c( 'Raw',  'Filtered' ,'Filtered%','Aligned', 'Aligned%'))
		{
			rs[[co]] = sapply(rs[[co]],function(el){   asLink(file.path('files/metrics/readstats.AllSample.csv')    ,el)  })
		}# Append link to csv file		
		colnames(rs) = paste0('Nb. reads (',colnames(rs),')')# rename columns 	
		df = cbind(df,rs[rownames(df),]) # join
		
		# Trimmomatic details next to filtered %
		df[[ "Nb. reads (Filtered%)"]]  = paste0(df[[ "Nb. reads (Filtered%)"]]
			, ' (',  sapply(rownames(df),function(s) asLink(file.path('files','reads',paste0(s,'.trim.out')),'details')   ),')')

		# Include fastq
		if(include.filtered.fastq)
		{
			df[['Filt. fastq']] = asLink(file.path('files','reads'),'link')
		}else{df[['Filt. fastq']] ='on request'}
		df = df[,c(1:4,7,5,6)]# Reorder to put fastq next to read stats
	
		# Include bams
		if(include.bams)
		{
			df[['Bam file']] = asLink(file.path('files','alignment'),'link')
		}else{df[['Bam file']] ='on request'}



		

		#' This function will write xml session files for a series of samples and return igv's dynamic PHP URL
		#'
		#' @param genome IGV genome, see http://www.broadinstitute.org/igv/ControlIGV
		#' @param track.path path where the session file should point for track files
		#' @param base.path ...
		#'
		#' @export	
		prepareIGVSessionFile <- function(genome,track.path, base.path )
		{

			paths = data.frame('track'=track.path,'xml.path'=paste0(track.path, '.xml'))		
			paths$urls = paste0('http://www.broadinstitute.org/igv/projects/current/igv.php?sessionURL=',paths$xml)	
			paths$genome  = genome
			paths$xml = paste0(
				 '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n'
				,"<Global genome=\"",paths$genome,"\" version=\"3\">\n"
				,"<Resources>\n"
				,"<Resource name=\"",basename(paths$track),"\" path=\"",basename(paths$track),"\" relativePath=\"true\" />\n"
				,"</Resources>\n"
				,"</Global>\n"
			)
			mapply(function(path,content){writeLines(content,path)},file.path(base.path,paths$xml.path), paths$xml)
			
			return(paths$urls)
		} # TODO : add sample info? Argh I hate this function. Needs re-factoring
		# Coverage (tracks)  # https://groups.google.com/forum/#!msg/igv-help/kc4aQRGiFDs/KAAhMH_FJUMJ
		genome = getIniParams('igvGenome', ini.file.path=ini.file.path, project.path=project.path)
		track.path = file.path('files','tracks',rownames(df),paste0(rownames(df),'.bw'))
		igv.urls = prepareIGVSessionFile(genome, track.path, report.path)
		df[['Coverage Tracks']] = paste0(asLink(file.path('files','tracks'),'link'),' (',asLink(igv.urls,'IGV'),')')

		# Saturation Plots
		df[['Saturation Analysis']] = asEmph('coming soon')
		
		# FastQC
		df[['FastQC Report']] = asEmph('coming soon')
				
		# RNA-SeQC
		df[['RNA-SeQC Report']] = asLink(file.path('files','metrics','index.html'),'link')


		# Comment from the analyst
		df[['Comment']] =  getSamples(path = project.path, fn = "samples.csv", full = TRUE)[['Comment']]

		return(newTable(df,'Alignment Propreties'))
	} # TODO: Mathieu says will change if unstranded?

	aln.table = reportAlignmentsTable(project.path,ini.file.path,report.path, include.bams, include.filtered.fastq )

	report = addTo(report,addTo( newSection('Alignments'),
		newParagraph('Raw reads sequences were cleaned of adapter sequence traces and trimmed for minimum trailing quality using ',
				asEmph('Trimmomatic '),asReference(citations[['trimmomatic']])
				,'. In the table below, raw reads refers to reads output by the sequencer, while filtered reads are those \
				left after applying Trimmomatic. Alignment to the reference sequence was performed by the spliced-reads \
				aligner ',asEmph('TopHat '),
				asReference(citations[['tophat']]),'. The number of aligned reads indicated below corresponds to ',
				'the number of primary alignments. As a rule of thumb, more than 80% should be aligned to the reference sequence.')
		,newParagraph('We provide coverage track files in the ', asLink('http://genome.ucsc.edu/goldenPath/help/bigWig.html','bigWig'),
				 ' format.',' These tracks can be loaded in your favorite genome browser, for instance ',
				asLink('http://www.broadinstitute.org/igv/','IGV'),' or the '
				,asLink('http://genome.ucsc.edu/cgi-bin/hgGateway','UCSC Genome Browser'),'.')
		,newParagraph('A rich set of quality control metrics is available in the ',asLink('http://bioinformatics.oxfordjournals.org/\
				content/early/2012/04/24/bioinformatics.bts196','RNA-SeQC') ,' reports. Examples include the duplication rate or the rRNA content.')
		,aln.table
	)) # TODO: move web link to function



	
	# Expression at the Transcript Level
	reportCufflinks <- function(report,project.path,ini.file.path,report.path, citations )
	{

		# First, commit the gtf of known transcripts
		gtf = getIniParams(param ='referenceGtf',ini.file.path = ini.file.path, project.path = project.path) 
		file.copy(gtf,file.path(report.path,'files'),overwrite=TRUE)
		
		# Assume that exploratory.fpkm has been run beforehand!
		fpkms = exprs(loadFpkms(project.path=project.path,path=file.path(project.path,'fpkm','known')))
		write.csv(fpkms,file=file.path(report.path,'files','fpkm','known','fpkm.csv'))

		report = addTo(report,addTo(newSection( "Abundance Estimation for Known Transcripts" )
		
		, newParagraph("We employed the ",asEmph('cufflinks '),asReference(citations[['cufflinks']])
		," algorithm to assemble transcripts and estimate their abundances, expressed as "
		, asLink('http://cufflinks.cbcb.umd.edu/howitworks.html','FPKM') 
		," values, listed in the table below.")

		,newParagraph("The set of known isoforms used is from the file "
		,asLink(file.path('files',basename(gtf)),basename(gtf)) )

		,newParagraph("The full set of cufflinks results can be found in the "
			,asLink(file.path('files','fpkm'),'fpkm')," folder." )

	
		, newTable(colnames(fpkms),'FPKM values for each sample.',file=file.path('files','fpkm','known','fpkm.csv'))

		,addTo(newSubSection('Exploratory Analysis')

			,newFigure('FPKM Hierarchical Clustering',file=file.path('files','fpkm','known','exploratory','cordist_hclust.png'))

			,newFigure('FPKM PCA',file=file.path('files','fpkm','known','exploratory','pca.png'))
			,newFigure('FPKM MDS',file=file.path('files','fpkm','known','exploratory','mds.png'))
			,newFigure('Top sd heatmap',file=file.path('files','fpkm','known','exploratory','top_sd_heatmap.png'))
			,newFigure('PCA anova',file=file.path('files','fpkm','known','exploratory','pca_anova.png'))
				
	


	#	cordist_hclust.pdf  mds.pdf  pca_anova.pdf  pca.pdf  top_sd_heatmap.pdf
#-bash-4.1$ pwd
#/lb/project/mugqic/projects/testRNA_270/2013_02_27_report/files/fpkm/known/exploratory




			)				
		))
		return(report)
	}
	report = reportCufflinks(report,project.path,ini.file.path,report.path, citations )
	
	# ok.. here is what I will do... Finish the fpkm section with the matrix 
	# a a few nice figures... annotated please!! Subsection of exploratory figures.
	# - Dendrogram, top sd, my.pvca, PCA, MDS

	# Novel Transcript Discovery
	# 	
	
	# Gene Model Raw Counts	
	

	# Annotation
	# - link to gtf used, explanations about the whole process
	# - Transcript Count column (deconvolution)
	# - Gene count column
	#
	# Exploratory Analays (if time allows)
	# # clustering, top sd, etc. (maybe a quick and dirty?), sup, unsup, etc.. pige dans gqMicroarrays
	#
	# DTE Analysis one row per contract
	#
	# DGE analysis, one row per contrast
	#
	# New DGE Analaysis
	#
	# GOSEq analysis
	#
	# REferences (perhaps add references in main function?)







	##
	### References Section
	reference.section = newSection('References')
	for(cit in citations)
	{
		reference.section <- addTo( reference.section, cit )
	}
	report = addTo(report,reference.section)


	return(report)	
}
# TODO: when it comes out make certain section collapsable and others not


mugqicPipelineReport('RNAseq', ini.file.path='bin/rnaSeq.abacus.ini')





