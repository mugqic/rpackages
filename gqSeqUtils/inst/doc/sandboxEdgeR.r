library(gqSeqUtils)
options(stringsAsFactors=FALSE)
Sys.umask('0002')
theme_set(theme_bw())

# Example of Custom figure Boxplot of genes
project.path=getwd() # typically
ini.file.path='bin/rnaSeq.abacus.ini'
exploratoryRNAseq(project.path, ini.file.path)
 
# Define the counts and filter out lowly expressed genes
counts = eset = getHTSeqReadCountsEset(project.path,ini.file.path)
y= calcNormFactors( DGEList(counts=exprs(counts)))
exprs(eset) = cpm(y, log=TRUE)
eset.nsfiltered = eset[ apply(exprs(counts),MARGIN=1,function(ro){ sum(ro>=10)>=1 })   ,]
nrow(eset.nsfiltered)

# Most varaible genes
e = eset.nsfiltered
pheatmap.eset(e,  file='exploratory/1_EDA_topsd_heatmap.pdf'
              ,scale='row',cols.ann=c('Mutation',"Family","Gender","SeqBatch"),row.labels='SYMBOL',col.labels='SampleID')

# Clustering base on correlation distance
e = eset.nsfiltered
d = cor.dist(t(exprs(e)))
h = hclust(d,method='ward')
pdf("exploratory/1_EDA_hclust_cordist.pdf",height=7,width=5)
plot(h,label=paste( pData(e)$SeqBatch ,pData(e)$SampleID, pData(e)$Gender, pData(e)$Mutation       ),
      xlab='Samples',ylab='Cor. dist height', main='log2(CPM), all genes')
plclust_in_colour(h,lab=paste( pData(e)$SeqBatch ,pData(e)$SampleID, pData(e)$Gender, pData(e)$Mutation       ),
                 xlab='Samples',ylab='Cor. dist height', main='log2(CPM), all genes', lab.col=rainbow.colored(pData(e)$Mutation),
                  hang = 0.01)
dev.off()

# PCA analysis... try uout filtered or not?
e = eset.nsfiltered
pca = prcomp(exprs(e),center=TRUE,scale=TRUE)$rotation
pdf("exploratory/1_EDA_pca.pdf",height=5,width=5)
for(comb in pairwise.comparisons(paste0('PC',1:8),formula=FALSE) )
{
  print(comb)
  dat = pca[,comb]
  dat = cbind(dat,pData(e)[rownames(dat),])
  dat$Label = paste(dat$SampleID,dat$Gender,"\n",dat$SeqBatch)
  p = ggplot(data=dat,aes_string(x=comb[1],y=comb[2]))+
      geom_point(aes(color=Mutation,shape=Family),size=3,alpha=0.8)+
      geom_text(aes(label=Label),size=0.8)+
      theme(aspect.ratio=1)
      
  print(p)
}
dev.off()

# Control genes, pathways
e = eset
genes  = sort(unique(c(
                "CXCL9","CXCL10","IFIT1","IFITM1","IFITM3","RPS4Y1"
                ,fData(e)$SYMBOL[grepl("(^IFIT|^HLA)",fData(e)$SYMBOL)]
                )))
pdf(file="exploratory/1_EDA_selected_genes.pdf",height=6,width=6)
for(g in genes)
{
  print(g)
  dat = melt.ExpressionSet(e[fData(e)$SYMBOL %in% g,])
  p = ggplot(data=dat,aes(x=Mutation,y=Expression))+
    geom_jitter(aes(color=Mutation,shape=Family),position=position_jitter(width=0.3,height=0),alpha=0.8,size=5)+
    geom_text(aes(label=SampleID),size=1)+
    geom_boxplot(aes(color=Mutation),alpha=0,outlier.size=0)+
    ggtitle(g)+ylab("Expression (log2CPM)")
 #   geom_line(aes(group=Family))
  print(p)
}
dev.off()
#gènes: Cxcl9, Cxcl10, les gènes Ifit (Ifit1, Ifitm1, ifitm3), les gènes HLA
#GO Biological processes: immune response, defense response, inflammatory response

# MDS on the previous genes?
e  = eset
e = e[fData(e)$SYMBOL %in% genes[!grepl("^RPS",genes)],]
mds = cbind(as.data.frame( cmdscale(euc(e)) ),pData(e))
p = ggplot(data=mds,aes(x=V1,y=V2))+
  geom_point(aes(color=Mutation,shape=Family),size=5,alpha=0.8)+
  geom_text(aes(label=SampleID),size=1)+theme(aspect.ratio=1)+
  ggtitle("MDS on selected gene only\n(without RPS4Y1)")
pdf(file='exploratory/1_EDA_mds_selected_genes.pdf',width=4,height=4)
print(p)
dev.off()



# MDS all genes?
e  = eset.nsfiltered
mds = cbind(as.data.frame( cmdscale(euc(e)) ),pData(e))
p = ggplot(data=mds,aes(x=V1,y=V2))+
  geom_point(aes(color=Mutation,shape=Family),size=5,alpha=0.8)+
  geom_text(aes(label=SampleID),size=1)+theme(aspect.ratio=1)+
  ggtitle("MDS on all genes")
pdf(file='exploratory/1_EDA_mds.pdf',width=4,height=4)
print(p)
dev.off()


## edgeR
e = counts
e = e[,pData(e)$Family=='ISIK']
a = dgeaEdgerAnalysis(eset=e, variables=c("Mutation"), analysis.type = "ind.1wbl", factor1 = 'Mutation', 
          base.level.1 = 'WT') 
dgeaEdgerWriteAnalysisOutput(obj=a, output.dir = 'edger', fn = "analysis_output.csv", ret = FALSE) 
dgeaEdgerWriteTopTables(obj=a, output.dir = "edger/top_tables")
dgeaEdgerWriteTopHeatMaps(
  obj = a
  , output.dir 		= 'edger'
  , number 		= 50
  , fdr			= 1
  , p.value		= 0.01
  , lfc			= log2(1)
  , row.labels		= 'featureID'
  , remove.dup.row.labels = FALSE
  , col.labels		= 'SampleID'
  , scale='row' )


# Cluster Samples based on DEGs....
e = eset
top = a$TopTags[[1]]$table
top = top[top$PValue<=0.01,]
e = e[top$featureID,]
d = cor.dist(t(exprs(e)))
h = hclust(d,method='ward')
pdf("edger/2_DGEA_hclust_cordistP001.pdf",height=7,width=5)
plot(h,label=paste( pData(e)$SeqBatch ,pData(e)$SampleID, pData(e)$Gender, pData(e)$Mutation       ),
     xlab='Samples',ylab='Cor. dist height', main='log2(CPM), genes P<0.01')
dev.off()

e = eset
top = a$TopTags[[1]]$table
top = top[top$PValue<=0.01,]
e = e[top$featureID,]
mds = cbind(as.data.frame( cmdscale(euc(e)) ),pData(e))
p = ggplot(data=mds,aes(x=V1,y=V2))+
  geom_point(aes(color=Mutation,shape=Family),size=5,alpha=0.8)+
  geom_text(aes(label=SampleID),size=1)+theme(aspect.ratio=1)+
  ggtitle("MDS supervised")
pdf(file='edger/2_dgea_mds.pdf',width=4,height=4)
print(p)
dev.off()



e = getHTSeqReadCountsEset(project.path,ini.file.path)
Mutation=factor(pData(e)$Mutation,levels=c("WT","IRF8I15M"))
design = model.matrix(~Mutation)
rownames(design) = sampleNames(e)
y = DGEList(counts = exprs(e), genes = fData(e))
y = calcNormFactors(y)
y = estimateGLMCommonDisp(y, design)
y = estimateGLMTrendedDisp(y, design)
y = estimateGLMTagwiseDisp(y, design)
fit = glmFit(y, design)
lrt = glmLRT(fit,coef=2)
top = topTags(lrt)













 
 ## Plot whatever and write to file
 genes  = c("RSAD2","RPS4Y1")
fn="exploratory/example_custom.pdf"
e = getHTSeqReadCountsEset(project.path,ini.file.path)
y= calcNormFactors( DGEList(counts=exprs(e)))
exprs(e) = cpm(y, log=TRUE)
dat = melt.ExpressionSet(e[fData(e)$SYMBOL %in% genes,])
p = ggplot(data=dat,aes(x=Mutation,y=Expression))+
   geom_jitter(aes(color=Mutation,shape=Gender),position=position_jitter(width=0.3,height=0),alpha=0.8,size=5)+
   geom_text(aes(label=SampleID),size=0.5)+
   geom_boxplot(aes(color=Mutation),alpha=0,outlier.size=0)+
   facet_grid(featureID~Family)+ggtitle("Blah blah")
 pdf(file=fn,height=6,width=6);print(p);dev.off()

 ## Commit file to indx with a description
 commitToFileIndex(project.path, path=fn, type = "exploratory", description = "Some figure to highlight something about the data not part of the <bf>SOP<\bf>") 
 
 
 
 # some perl code to call the exploratory part?
 

 
 
 
 

 
 checkRNAseqSanity <- function(project.path=project.path,ini.file.path=ini.file.path)
 {
   s = getSamples() 
   f = getGenes()
   # ...
   #... etc.
   # etc.
   # especially, check for missing values or "" in sample anntoations levels///^
   # check sample description columns... are they in sample sheet
 }
 
 
 

























































































 









############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################################################################################
############################################################################################


# Create an eset
eset = loadReadCounts()
eset = new("ExpressionSet", exprs = as.matrix(  eset ) )
# Annotate it's samples
pData(eset) = getSamples()[sampleNames(eset),]

# Annotate it's features

#' This function reads extended annotations for genes (gene models not trasncript). This is typically used for DGEA. If not found, returns NULL.
#'
#'
#' @export
loadGeneAnnotation <- function( ini.file.path = NULL, project.path=getwd()  )
{
   fn = getIniParams('geneAnnotation','default', ini.file.path=ini.file.path, project.path=project.path, stopifnotfound=FALSE)
   if(is.na(fn))
   {
	warning('Could not find path to a gene annoation in ini file')
	return(NA)
   }	
   ann = read.delim(fn,colClasses='character',check.names=FALSE,comment.char='',quote='')
   rownames(ann) = ann[['featureID']]
   return(ann)		
}
# crap, meesed up weith my cluster, need to rsynch genomes
# two choices: designMatrix and contrastMatrix or parameter

fData(eset) = loadGeneAnnotation()[featureNames(eset),]

# Remember : paired analysis!!


Block = pData(eset)$Litter
Block = model.matrix(~-1+Block)
Effect = pData(eset)$Mutation
Effect = model.matrix(~-1+Effect)
Effect = Effect[,1,drop=FALSE];colnames(Effect) = 'MU'
design = cbind(Block,Effect)
rownames(design) = sampleNames(eset)
a = dgeaEdgerAnalysis(eset=eset, variables='Mutation', analysis.type='manual.manual',
design=design,coefs='MU'	)
dgeaEdgerWriteTopHeatMaps(
		  obj = a
		, output.dir 		= 'edgerPaired'
		, number 		= 50
		, fdr			= 0.25
		, p.value		= 0.01
		, lfc			= log2(1)
		, row.labels		= 'featureID'
		, remove.dup.row.labels = FALSE
		, col.labels		= 'SampleID'
		, scale='row' )
dgeaEdgerWriteAnalysisOutput(a, output.dir='edgerPaired' )
dgeaEdgerWriteTopTables(a, output.dir='edgerPaired')



a = dgeaEdgerAnalysis(eset=eset, variables='Mutation', analysis.type='ind.manual',coefs='MU-WT'	)
dgeaEdgerWriteTopHeatMaps(
		  obj = a
		, output.dir 		= 'edger'
		, number 		= 50
		, fdr			= 0.25
		, p.value		= 0.01
		, lfc			= log2(1)
		, row.labels		= 'featureID'
		, remove.dup.row.labels = FALSE
		, col.labels		= 'SampleID'
		, scale='row' )
dgeaEdgerWriteAnalysisOutput(a, output.dir='edger' )
dgeaEdgerWriteTopTables(a, output.dir='edger')













exploratoryAnalysis.expression(e=a$logCPM.eset, path='exploratory', col.labels='SampleID', row.labels='featureID', cols.ann='Mutation')

# TODO: perhaps baselined?

 dgeaEdgerAnalysis
function (eset, variables, analysis.type = "manual.manual", factor1 = NULL, 
    base.level.1 = NULL, factor2 = NULL, base.level.2 = NULL, 
    design = NULL, coefs = NULL, ngenes.adjust.method = "BH", 
    ngenes.p.values = c(0.05, 0.1, 0.25)) 






# module load mugqic/R && R --vanilla
#library(NBPSeq)
#data(arab)
#library(edgeR)
#library(gqUtils)
#library(Biobase)
#library(Nozzle.R1)
#library(ReportingTools)
#library(stringr)
#e = new('ExpressionSet',exprs=arab) ; pData(e)$Treat = c('m','m','h','h','c','c') ;pData(e)$Tissue = c('spleen','pbmc','spleen','pbmc','pbmc','spleen') ; pData(e)$Exp = 'A'; v=c('Treat','Tissue'); ty='ind.pw'
#e = new('ExpressionSet',exprs=arab) ; pData(e)$Treat = c('m','m','m','h','h','h') ;pData(e)$Tissue = c('spleen','spleen','spleen','spleen','spleen','spleen') ; pData(e)$Exp = 'A'; v=c('Treat','Tissue'); ty='ind.pw'
#source('gqSeqUtils/R/edgeR.r')
#fit = dgeaEdgerAnalysis(e, v, analysis.type='ind.pw')




The tables on the following pages contain the results of the differential gene expression analysis using DESeq and edgeR.
DESeq uses a model based on the negative binomial distribution [5]. edgeR uses empirical Bayes estimation and exact tests based
on the negative binomial distribution [6]. Raw read counts generated by HTSeq are used as input. The first column is the gene id,
the second column is the gene symbol, the third column is the log2 fold change of the level of expression of each gene, 
the fourth column is the nominal p-value of the test and the fifth column is the FDR adjusted p-value of the test.

The column CPM means counts-per-million-reads and represents the average CPM across all samples. However, this is NOT your typical "average"
, i.e. (x1+x2+..+xn)/n.  Count data is not normally distributed, so estimates of the expected values are not the arithmetic mean
, but something more complicated, based on the dispersion and total counts. The same goes for FC, the fold-change. Nevertheless
, you can still view FC and CPM as "averages". If this isn't complicated enough, edgeR reports log2(CPM) and log2(FC).
I have added the column FC by transforming the logFC back to the symmetrical fold-change.
The column "LR" is the likelihood-ratio statistic on which the p-value is based. This is the analog of the t-statistic.

Differential expression analysis of read counts is conducted using the functionalities of the R/Bioconductor package edgeR[1].
This methodology fits a generalized linear models (glm) to the read counts, assuming they are negative binomial distributed.

Likelihood ratio tests are then conducted to test for the statistical significcance of any effect of interest. the type of effect, can range from
simple pairwise differences, to more complicated ones such as double-subtraction(interaction term) or within-in subject (block) between treatment differences. 

Note that edgeR impliciteyl normalized AND tests are moderatd (borrowing information across genes/features)

 

A particular feature of edgeR functionality, both classic and glm, are empirical Bayes
methods that permit the estimation of gene


 # This function writes the abridged html and csv versions

dgeaEdgerWriteDetails <- function(output.dir=getwd(),fn='edger_details.html')
{
	# output.dir=getwd();fn='edger_details.html'
	report <- newCustomReport( "EdgeR details" );

	



	report <- addTo( report, newParagraph( 
		"EdgeR fits a linear model" ) );

	report <- addTo( report, addTo( newSection('Columns Description'),newParagraph( 
		"blah" ) ) );

	report <- addTo( report, addTo( newSection('Contrast Formulas'),newParagraph( 
		"double subtractions" ) ) );	

	report <- addTo( report, addTo( newSection('References'),newParagraph( 
		"blah" ) ) );

	writeReport( report, filename=file.path(output.dir,fn));




	# Ref
	#cit <- newCitation( authors=citation('edgeR')[4]$author, title=citation('edgeR')[4]$title, year=citation('edgeR')[4]$year, url="http://www.bioconductor.org/packages/2.11/bioc/html/edgeR.html" );
	# Intro
	#report <- addTo( report, addTo( newSection( "My Introduction" ), newParagraph( "Hello World! This is a paragraph of text!" ) ) );
	#report <- addTo( report, addTo( newSection('References'),simpleCitation ) );
	

	
	

		report2 <- addTo( report2, addTo( newSection( "My Methods" ), method2 ) );

		report2 <- addTo( report2, addTo( newSection( "My Results", class="results" ),
						addTo( newSubSection( "Nozzle Figure" ), figure1, figure2 ),
						addTo( newSubSection( "Nozzle Table" ), table1, table4, table3, table5 ), 
						addTo( newSubSection( "Nozzle Text" ), paragraph1, list, paragraph2, paragraphPublic,
									paragraphGroup, paragraphPrivate ),
						addTo( addTo( newSubSection( "Nozzle Results" ), newParagraph( "A result: ", asSummary( result4 ), ", yay!" ) ) ) ) ); 

		report2 <- addTo( report2, addTo( newSection( "My Bibliography" ), simpleCitation, webCitation, fullCitation ) );

		# set a copyright message without URL
		report2 <- setCopyright( report2, owner="Nils Gehlenborg", year=2013, statement="All rights reserved." ); 

		# set contact information for error reports (no "label" provided, "Contact" will be used as a default)
		report2 <- setContactInformation( report2, email="nils@hms.harvard.edu", subject="Problem with Nozzle Report", message="Hello World!\n\nThis is Nils!\n--End of Story--" );

		# set a custom CSS file (for display in the browser - a separate CSS file can be supplied to printing)
		# *** make sure that the CSS file is in the same directory as the report HTML file *** 
		report2 <- setCustomScreenCss( report2, "demo.css" );

		






	# Content
	

	TODO: That should be done with Nozzle


#	 [1] "Rank"         "featureID"    "logFC"        "logCPM"       "LR"          
# [6] "PValue"       "FDR"          "FC"           "counts.mock1" "counts.mock2"
#[11] "counts.mock3" "counts.hrcc1" "counts.hrcc2" "counts.hrcc3"
}


dgeaEdgerWriteRecommendedPlots()



#' This function returns a Nozzle section object
#'
#'
#' @export
nozzleSectionEdger <- function(fit, output.dir='edger_report' ,top.heatmap.n=50, censor.unreplicated=TRUE, SYMBOL.col='SYMBOL')
{

#	output.dir="edger_report" ;top.heatmap.n=50; censor.unreplicated=TRUE

	# Init dir
	dir.create(output.dir , showWarnings=FALSE, recursive=TRUE )
	

	

	# TODO: start off simple with the section before table of contrasts

	


	###
	####### Prepare files 
	
	# top 50 heatmaps
	# Analysis output file
	# all other files, see names(fit)
	# (keep handles? n file name?)

	###
	####### Prepare files

	

	#fit,report.dir=NULL ,top.heatmap.n=50, censor.unreplicated=TRUE


	# design, cont, analysis output,  replication, warning about no replication
	# heatmaps, etc.
		# replication numbers
	# censor?
	# significance column? 
	# filtered features?
	# insure FeatureID, SampleID, Symbol
	# make sure works witn a single contrast
	# test with dispersion

	# analysis outpuy
	# F-test analog
	# re-order with Symbols
	

	# heatmaps, analysis output, volcano, edgeR mds/BCV plots, etc, file of logCPM, 
# A function to interface with the pipeline


	# all already inclded in uA pipeline...!
	# not on P-valus and replicates: if you want the P-values to appear???? ha
	# report features tested, or at least their numbers
	# report samples tested and their phenotype...
	# report everytgin..
	# in other words, reporting everything!
	# edgeR standard plots : MDS and such...

	# Censor top...
	# analysis output table
	# what not...
	# if filtered... what happens??? NA P-values???
	# PAthway analysis?		
	# what about the css file???	

	# Perhaps use sparktable.. like an indicator for many genes or not.

}






