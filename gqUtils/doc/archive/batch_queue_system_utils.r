

#' This function is a simple wrapper to submit a system call to batch queue system
#'
#' All outputs are written in output.dir.
#'
#' @param calls the call, e.g. 'bowtie...'
#' @param job.names Name for the job, used in qsub/msub and naming files
#' @param batch.queue.system Is it 'sge', 'moab' or run 'local'
#' @param output.dir where to write stderr and queue system stout stderr
#' @param multicore.string See sge documentation
#' @param email Where to send notification (-m ae -M email)
#' @param wait Should the function hang on until all jobs finished on queue system
#' @param check.queue.every How often (seconds) should the qeue system be interrogated for job statues
#' @return Character vector of the results from system calls-> system(intern=TRUE)
#' @export
gq.queue.submit <-function(calls,job.names=names(calls),batch.queue.system=c('local','sge'),output.dir=file.path(getwd(),'job',''),multicore.string='3',email=NULL,wait=TRUE
			,check.queue.every=5)
{

	#calls=c('ls -alrth','ls');job.names=c('a','b');batch.queue.system='sge';output.dir=file.path(getwd(),'job','');multicore.string='3';email=NULL;wait=TRUE;check.queue.every=5
	if( anyDuplicated(job.names)        ) {stop('Job names must be unique')}
	if( length(calls)!=length(job.names) ) {stop('calls and job.names must have the same length')}
	if(! batch.queue.system %in% c('local','sge')) {stop('Invalid queue system')}

	require(Rsge)
	if(batch.queue.system=='local')
	{
		full.calls=calls
		results = sapply(full.calls,function(x){system(x,intern=TRUE)},simplify=FALSE) 
	}else{
		# Create output directory
		output.dir = file.path(output.dir,'') # just to make sure / is there.. think qsub expects this?
		dir.create(output.dir,showWarnings = FALSE, recursive = TRUE)
		output.filenames = paste(output.dir,job.names,'_output.txt',sep='') 
			
		# Add job output to calls
		c.calls = paste(calls,'>&',output.filenames)

		if(batch.queue.system=='sge')
		{
			# Build qsub calls
			q.calls =paste(
				'qsub -pe'
				,'multicore',multicore.string
				,'-cwd -N',job.names
				,'-o', output.dir,'-e', output.dir
				)
			if(!is.null(email)){q.calls = paste(q.calls,'-m ae -M',email)}
			full.calls = paste('echo ','\'',c.calls,'\'', ' | ', q.calls,sep='')
	
			# Submit
			print(full.calls)

			results =  sapply(full.calls,function(x){system(x,intern=TRUE)},simplify=FALSE) # this is the actual SUBMISSION
			print(results)
	
			# Wait until 
			Sys.sleep(5)
			while(wait)  # WAIT UNTIL all jobs finished
			{		
				Sys.sleep(check.queue.every)			
				ids      = as.character(sapply(results,sge.get.jobid))
				statuses = sapply(ids, sge.job.status,simplify=FALSE)
				print(cbind(names(statuses),statuses))
				
				if(any(statuses==1)){wait=TRUE}else{wait=FALSE}
			}
		}
	}
	return( results )
}

# TODO: send email to RSS or someting insead of email account
# - also, job.names could be left to names(calls). oh well
#gq.queue.submit(calls=c('ls -alrth','sleep 10'),job.names=c('test1','test2'),batch.queue.system='sge'
#	,output.dir=file.path(getwd(),'test',''),multicore.string='1',email='lefebvrf@gmail.com',wait=TRUE ,check.queue.every=2)








