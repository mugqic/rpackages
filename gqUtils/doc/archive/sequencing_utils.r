
#' Small function to retrive well formatted fastq file name
#' 
#' This function accepts a list named (named by read set ids) list of fastq files and returns well formatted names instead.
#' because this operation is done at trimiming and alignement, thought it was a good idea to have a funcito (naming scheme then easily modified)
#'
#' @title Rename fastq files
#' @param fastqs named list of fastq file names. Pairs separated by ',', e.g. x_R1.fastq.gz..
#' @author Lefebvre F
#' @export
#name.fastqs <-function(fastqs)   
#{
#
#	if(is.data.frame(fastqs)) # if data.frame supplied, will assume it is pdata with ReadSetIds as row names and column Filepath
#	{
#		fastqs        = strsplit(fastqs[['FilePath']],split=',')
#		names(raw.fastqs) = rownames(fastqs)
#	
#
#	x = sapply(names(fastqs),function(id)
#	{
#		input.fns = fastqs[[id]]
#		output.fns = sapply(1:length(input.fns),function(i){   file.path(getwd(),output.dir,paste(id,'_R',i,'','.fastq.gz',sep=''))   })
#		if(length(input.fns)>2){stop('Something wrong with the parsing of FilePath for fastq files...')}
#		output.fns
#	},simplify=FALSE)	
#	return(x)
#}


#' haha 
#'
#' @export
dummy <- function(x)
{
	# blas
{





#' This function trim a fastq file (or a pair) using trimmomatic.
#' @param input.fastqs A **list** of character vectors, each element of the list corresponding to the names of the inpput files. For single end, a character vector of length 1 will work
#' @param ouput.fastqs A list of character vectors, each element of the list corresponding to the names of the desired output files
#' @return System call string
#' @export
# gq.trim.fastqs.trimmomatic <-function(input.fastqs, output.fastqs
# 			,jar.path=system.file(file.path("exec",'trimmomatic'),package = "gqRNAseq")
# 			,java.path='java' ,threads=3,phred='-phred33'
# 			,steps.string = paste('ILLUMINACLIP:',system.file(file.path("extdata",'adapters.fa'),package = "gqRNAseq"),':2:30:15 TRAILING:30 MINLEN:32',sep='')
# 			)
# {
# 	mapply(function(input.fns,output.fns){
# 
#
#		calls = paste(java.path,'-classpath',jar.path)
#		if(length(input.fns)==1) # single end
#		{
#			calls = paste(calls,'org.usadellab.trimmomatic.TrimmomaticSE',phred,'-threads',threads
#				,input.fns[1],output.fns[1],steps.string
#			)
#		}
#		if(length(input.fns)==2) # paired ends
#		{
#			calls = paste(calls,'org.usadellab.trimmomatic.TrimmomaticPE',phred,'-threads',threads
#				,input.fns[1]
#				,input.fns[2]
#				,output.fns[1]
#				,paste(dirname(output.fns[1]),.Platform$file.sep,'single_',basename(output.fns[1]),sep='')
#				,output.fns[2]
#				,paste(dirname(output.fns[2]),.Platform$file.sep,'single_',basename(output.fns[2]),sep='')
#				,steps.string)
#		}
#		return(calls)
#
#	},input.fastqs, output.fastqs,SIMPLIFY=TRUE)
#}

# TODO make this function serializable?
#gq.trim.fastqs.trimmomatic(c('in1','in2'), c('out1','out2'))
