



#' my pvca
#'
#' @export
my.pvca <-function (e, covariates, n.components = 5)
{
    require(Biobase)
    pca = prcomp( t( exprs(e) ) )
    print("PCA done.")
    w = pca$x[, 1:min(ncol(e)-1,n.components)]


    anvs = sapply(colnames(w), function(pc) {
        sapply(covariates, function(cv) {
		#print(cv)
		  if( any(is.na(suppressWarnings(as.numeric( pData(e)[, cv,drop=TRUE]   )))) ){
            summary( lm(w[, pc,drop=TRUE] ~ factor(pData(e)[, cv,drop=TRUE])) )$r.squared
			}else{
				    summary( lm(w[, pc,drop=TRUE] ~ as.numeric( pData(e)[, cv,drop=TRUE] )   ) )$r.squared
			}



        }, simplify = FALSE)
    }, simplify = FALSE)


    anvs = melt(anvs)
    colnames(anvs) = c("Rsquared", "Covariate", "PC")
    anvs$Rsquared = 100 * anvs$Rsquared
    anvs$PC = factor(anvs$PC,order=TRUE,levels=  unique(anvs$PC)[order(as.numeric( gsub('PC','',unique(anvs$PC))   ))]   )

 #   p = qplot(data = anvs, x = Covariate, y = Rsquared, facets = ~PC,
 #       geom = "bar", fill = Covariate) + xlab("Covariate") +
  #      ylab("R^2 (%)")+ theme(axis.text.x = element_text(angle = 90, hjust = 1))

    p = ggplot(data=anvs,aes( x = Covariate, y = Rsquared) ) +
		facet_grid(~PC) + geom_bar(aes(fill=Covariate),stat='identity') + xlab("Covariate") +
        ylab("R^2 (%)")+ theme(axis.text.x = element_text(angle = 90, hjust = 1))

    p$pca = pca
    w = cbind(w, pData(e))
    p$w = w
    return(p)
}



#' ExpressionSet wrapper for my.pvca
#'
#' @export
pca.anova.eset <- function(e,fn='pca_anova.pdf',n.components = 5, covs.subset=varLabels(e))
{
	covs =	sapply(varLabels(e),function(co)is.character(pData(e)[[co]])) &
		sapply(varLabels(e),function(co) length(unique(pData(e)[[co]]))>1 )&
		sapply(varLabels(e),function(co) length(unique(pData(e)[[co]]))<length(pData(e)[[co]]) )
	covs = covs & ( varLabels(e) %in% covs.subset )


	if(sum(covs)>0)
	{
		pdf(fn,width= n.components*(0.5*sum(covs))   ,height=4 + 0.1*max(nchar(varLabels(e))) )
		p = my.pvca(e,varLabels(e)[covs], n.components)
		print(p)
		dev.off()
		return(p$data)
	}else{
    warning('No appropritate covariate to perform PVCA')
    pdf(fn,width= 7  ,height=4)
    plot(1,main="Plot impossible no appropriate covariates")
    dev.off()
	}
#& sapply(varLabels(e),function(co) !any(pData(e)[[co]]==''))

}




#' QC and expl functions need similar input and this operation needs to be repeated often This is just a general function to
#' turn an object into a matrix and rename it's columns.
#' @title Coerce uA data object into matrix
#' @param obj uA data, supported are classes ExpressionSet of EList.raw
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors
#' @author Lefebvre F
#' @return A matrix object with columns renamed and log2 transformed if needed
#' @export
uA.as.matrix <-function(obj,is.log2=NULL,alt.colnames=NULL)
{

	obj = as.matrix(obj) # This will work for ExpressionSet, EListRaw, etc.

	if(is.null(is.log2)) # if left unspecified, try to guess
	{
		is.log2 = guess.log2(obj)
	}

	if(!is.log2) # if not log2, transform
	{
		obj=log2(obj)
	}

	if(!is.null(alt.colnames)) # set alternative column names
	{
		if(is.vector(alt.colnames)){alt.colnames=data.frame(alt.colnames)} # be forgiving.. user might forget drop=FALSE
		colnames(obj) = make.unique.with.spaces( do.call(paste,alt.colnames))
		#colnames(obj) = make.unique( do.call(paste,alt.colnames))
	}

	return(obj)
}

#' Create a boxplot pdf from raw or normalized uA data object
#' @title Boxplots
#' @param x uA data some format that can be coered to matrix by uA.as.matrix
#' @param path to pdf, dir
#' @param fn pdf filename
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors
#' @param ylab y-label of the boxplot
#' @param xlab x-label of the boxplot
#' @param main title
#' @param desc string summary of the figure
#' @author Lefebvre F
#' @return A character vector with first element describing the plot and second the path to the file
#' @export
uA.boxplot <- function(x,path='.',fn='boxplot.pdf',is.log2=NULL,alt.colnames=NULL,ylab='Log2 Value',xlab='Arrays'
			,main=''
			,desc='Boxplot Visualization of log2 Values')
{
	x = uA.as.matrix(x,is.log2=is.log2,alt.colnames=alt.colnames)

	# Tune size of the pdf
	h=4 + 0.05*max(nchar(colnames(x)));
	w= 0.7+0.3*ncol(x);

	# ggplot2
	x = melt(x)
	p  = qplot(data=x,x=X2,y=value,geom='boxplot',outlier.size=0) +
		 xlab(xlab)+ ylab(ylab)+
		theme(axis.text.x = element_text(angle = -90, hjust = 0)) +
		ggtitle(main)
	pdf(file.path(path,fn),height=h,width=w)
	print(p)
	dev.off()

	ret = data.frame('Description'= desc
			,'path'=file.path(path,fn))
	return(ret)

}

#' Create a dendrogram from a microarray data object
#' @title Hierarchical clustering using correlation distance
#' @param x uA data some format that can be coered to matrix by uA.as.matrix
#' @param path to pdf, dir
#' @param fn pdf filename
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors
#' @param ylab y-label of dendrogram
#' @param main title
#' @param xlab x-label of the dendrogram
#' @param desc string summary of the figure
#' @param method Arg of the same name for hclust
#' @author Lefebvre F
#' @return A character vector with first element describing the plot and second the path to the file
#' @export
uA.cordist.hclust <- function(x,path='.',fn='cordist_hclust.pdf',is.log2=NULL,alt.colnames=NULL,ylab='Correlation Distance'
			,xlab='Arrays', main=''
			,desc='Hierachical clustering based on the correlation distance',method="average",...)
{
	x = uA.as.matrix(x,is.log2=is.log2,alt.colnames=alt.colnames)

	# Tune size of the pdf
	h=7;w= 2+0.3*ncol(x);

	# plot hclust

  if(ncol(x)>2){
    pdf(file.path(path,fn),height=h,width=w)
  	hcl  = hclust(d=cor.dist(t(x)),method=method)
	  plot(hcl,xlab=xlab,ylab=ylab,main=main,...)
    dev.off()
  }else{
    pdf(file.path(path,fn),height=4,width=7)
    plot(1,main='Only two samples, plot not possible')
    dev.off()
  }


	ret = data.frame('Description'= desc
			,'path'=file.path(path,fn))
	return(ret)


}

#' Create pairwise distance matrix plot, manhattan distance
#' @title Pairwise Manhattant distance heat map
#' @param x uA data some format that can be coered to matrix by uA.as.matrix
#' @param path to pdf, dir
#' @param fn pdf filename
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors
#' @param main title
#' @param lab axis label
#' @param desc string summary of the figure
#' @author Lefebvre FL, Goulet JP
#' @return A character vector with first element describing the plot and second the path to the file
#' @export
uA.pairwise.manhattan.hclust <- function(x,path='.',fn='pairwise_manhattan.pdf',is.log2=NULL,alt.colnames=NULL
			,main='',lab='Arrays'
			,desc='Pairwise manhattan distance heat map',...)
{
	x = uA.as.matrix(x,is.log2=is.log2,alt.colnames=alt.colnames)

	# Tune size of the pdf
	h= 3+0.3*ncol(x); w = h

	# lattice
	dd = dist2(x)
   	diag(dd) = 0
        dd.row = as.dendrogram(hclust(as.dist(dd)))
    	row.ord = order.dendrogram(dd.row)
    	legend = list(top = list(fun = dendrogramGrob, args = list(x = dd.row,
        	side = "top")))
    	tr = levelplot(dd[row.ord, row.ord], scales = list(x = list(rot = 90)),
        xlab = lab, ylab = lab, legend = legend)

	pdf(file.path(path,fn),height=h,width=w)
	plot(tr,...)
	dev.off()

	ret = data.frame('Description'= desc
			,'path'=file.path(path,fn))
	return(ret)


}




#' Create an on-the-fly mds
#' @title On the fly MDS
#' @param x uA data some format that can be coered to matrix by uA.as.matrix
#' @param path to pdf, dir
#' @param fn pdf filename
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param k Number of dimensions
#' @param pdata a one or two columns data frame. First col will be used as color, second as shape
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors. For the MDS or PCA, these are used as text labels on the scatterplot
#' @param main title
#' @param desc string summary of the figure
#' @author Lefebvre FL
#' @return A character vector with first element describing the plot and second the path to the file
#' @export
uA.mds <- function(x,path='.',fn='mds.pdf',is.log2=NULL,alt.colnames=NULL
			,k=2, pdata=NULL,main=''
			,desc='2D Multidimensional scaling plot (MDS)')
{

	x = uA.as.matrix(x,is.log2=is.log2,alt.colnames=alt.colnames)

	# Tune size of the pdf
	h= 4; w = 4.5

	# MDS
	if(ncol(x)>2){
	  pdf(file.path(path,fn))
	  mds = as.data.frame(cmdscale(euc(t(x)),k=k))
	  mds$.Label = rownames(mds)

	  p  = ggplot(data=mds,aes_string(x='V1',y='V2'))+geom_point(color='grey',alpha=0.8,size=3)+
	       theme(aspect.ratio =1 )

	  if(!is.null(pdata)){
	  	mds2 = cbind(mds,pdata)
	  	if(ncol(pdata)==1){
	  		p = p+geom_point(data=mds2,aes_string(col=colnames(pdata)[1]),size=4)
	  	} else if (ncol(pdata)==2){
	  		p = p+geom_point(data=mds2, aes_string(col=colnames(pdata)[1],shape=colnames(pdata)[2]),size=4)
	  	} else {
	  		stop('Too many columns supplied')
	  	}
  	  }
  	  if(require(ggrepel)){
  	  	p = p + geom_label_repel(aes(label=.Label),size=3) + ggtitle(main)
  	  } else {
	    p = p + geom_text(aes(label=.Label),size=3) + ggtitle(main)
	  }
	  print(p)
      dev.off()
	}else{
	  pdf(file.path(path,fn))
	  plot(1,main='Only two samples, plot not possible')
	  dev.off()
	}

	ret = data.frame('Description'= desc
			,'path'=file.path(path,fn))
	return(ret)
}










#' Create an on-the-fly PCA
#' @title On the fly PCA plot
#' @param x uA data some format that can be coered to matrix by uA.as.matrix
#' @param path to pdf, dir
#' @param fn pdf filename
#' @param is.log2 Are the intensities/expressions already logged2. If left NULL will try to guess
#' @param k Number of dimensions
#' @param pdata a one or two columns data frame. First col will be used as color, second as shape
#' @param alt.colnames A data.frame or char vector listing attributes to be pasted together as column descriptors. For the MDS or PCA, these are used as text labels on the scatterplot
#' @param main title
#' @param desc string summary of the figure
#' @author Lefebvre FL
#' @return A character vector with first element describing the plot and second the path to the file
#' @export
uA.pca <- function(x,path='.',fn='pca.pdf',is.log2=NULL,alt.colnames=NULL
			,k=2, pdata=NULL,main=''
			,desc='First two Principal Component (PCA)')
{

	x = uA.as.matrix(x,is.log2=is.log2,alt.colnames=alt.colnames)

	# Tune size of the pdf
	h= 4; w = 4.5

	# MDS
	mds = as.data.frame(prcomp(t(x))$x[,c(1:k)])
	mds$.Label = rownames(mds)

	p  = ggplot(data=mds,aes_string(x='PC1',y='PC2'))+geom_point(color='grey',alpha=0.8,size=3)+
		 theme(aspect.ratio = 1 )

 	if(!is.null(pdata)){
	  	mds2 = cbind(mds,pdata)
	  	if(ncol(pdata)==1){
	  		p = p+geom_point(data=mds2,aes_string(col=colnames(pdata)[1]),size=4)
	  	} else if (ncol(pdata)==2){
	  		p = p+geom_point(data=mds2, aes_string(col=colnames(pdata)[1],shape=colnames(pdata)[2]),size=4)
	  	} else {
	  		stop('Too many columns supplied')
	  	}
  	  }

  	  if(require(ggrepel)){
  	  	p = p + geom_label_repel(aes(label=.Label),size=3) + ggtitle(main)
  	  } else {
	    p = p + geom_text(aes(label=.Label),size=3) + ggtitle(main)
	  }

	pdf(file.path(path,fn))
	print(p)
	dev.off()

	ret = data.frame('Description'= desc
			,'path'=file.path(path,fn))
	return(ret)


}
# TODO: add higher order k>3 comparisons, 3D?, pairwise mds and PCA, etc.




