 .onAttach <- function(libname, pkgname)
 {
 	options(stringsAsFactors=FALSE)
 	


	# Add exec to path (aligners, etc)
#	path = Sys.getenv('PATH')
#	Sys.setenv('PATH'= paste(path,  system.file('exec',package='gqUtils')    ,sep=':')  ) 
#	exec.content = list.files(system.file('exec',package='gqUtils'))	

	packageStartupMessage('\n\nWelcome to the Genome Quebec R utilities package.\n\n')


#	packageStartupMessage('\n\nWelcome to the Genome Quebec R utilities package.\nThe content of the gqUtils/exec directory has been appended to $PATH:\n\n')
#	packageStartupMessage(paste(exec.content,collapse=', '))	
#	packageStartupMessage('\n\n')
 }





 #' This function reads an "extended" tsv format file. An optional header row starting with #'TYPE defines the type to coerce the column to (defaults to character), and allows masking a column by setting to NULL. Rows starting with "#" will also be ignored, as a way to eventually mask samples.
 #' 
 #'
 #' @export
 readTypedTSV <- function (file, apply.coltypes=TRUE, 
 		allowed.types =  c("NULL","logical", "integer", "numeric","complex","character", "raw","factor","Date","POSIXct","NA") ) 
 {
 	# file = "test.tsv"; allowed.types =  c("NA","NULL","logical", "integer", "numeric","complex","character", "raw"); apply.coltypes=TRUE
 	# Read
 	lines = readLines(file)
	
 	# colClasses (also acts as column mask)
 	colClasses =  lines[ grepl("^#'",lines) ]
 	colClasses =  gsub("^#'","",colClasses)
 	if(  length(colClasses) > 0  &  apply.coltypes )
 	{
 		colClasses = strsplit(colClasses,split='\t')[[1]]
 		illegal.types = unique(colClasses[  ! colClasses %in%  allowed.types ])
 		if(length(illegal.types)>0)
 		{
 			stop("Possible column types are ",paste(allowed.types,collapse=", "),". Got: ", paste(illegal.types,collapse=", ") )
 		}
 	}else{
 		colClasses = "character"
 	}

 	# Read the table data (# acts as row mask)
 	dat = read.table(
 		     text = lines[!grepl("^ *#",lines)]
 			, header = TRUE, sep='\t', quote="", na.strings = "NA", colClasses = colClasses
 			, check.names = FALSE, fill = TRUE, strip.white = TRUE
 			, blank.lines.skip = TRUE, comment.char = "", stringsAsFactors = FALSE )
     return(dat)
 }








#' This is a helper function that automatically creates contrasts of for a two-way anova like design. 
#' 
#' @title Create 2-way ANOVA contrast formulas
#' @param x An ExpressionSet. (eg. eset)
#' @param variables A character list of Factors from pData(x) used to fit the model. (eg. c("Protein","Adjuvant"))
#' @param f1 A character string, representing the first Factor from pData(x) used to fit the model.
#' @param f1.baseline A character string representing the baseline to substract from f1.
#' @param f2 A character string, representing the second Factor from pData(x) used to fit the model.
#' @param f2.baseline A character string representing the baseline to substract from f2.
#' @param sep factor separator, e.g. as in "Mu__Treated-Mu___Untreated"
#' @return A List of contrasts formulas vectors to input to dgLimmaContrastsFit.
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
dgANOVAContrasts <- function(x, variables, f1 = NULL, f1.baseline = NULL, f2 = NULL, 
    f2.baseline = NULL, sep = "_", unlist=FALSE) 
{
    if (any(!variables %in% varLabels(x))) {
        stop("Some element in variables is not present in the pData")
    }
    if (!f1 %in% variables) {
        stop("First factor not in variables")
    }
    if (!f1.baseline %in% pData(x)[[f1]]) {
        stop("Invalid baseline level for the first factor")
    }
    if (!f1.baseline %in% pData(x)[[f1]]) {
        stop("Invalid baseline level for the first factor")
    }
    if (!is.null(f2)) {
        if (!f2 %in% variables) {
            stop("Second factor not in variables")
        }
        if (!f2.baseline %in% pData(x)[[f2]]) {
            stop("Invalid baseline level for the second factor")
        }
    }
    l = list()
    cts = unique(pData(x)[, variables, drop = FALSE])
    rownames(cts) = NULL
    fixed = apply(cts[, setdiff(variables, f1), drop = FALSE], 
        MARGIN = 1, function(ro) paste(ro, collapse = sep))
    cts = by(cts, INDICES = fixed, function(df) {
        if (!f1.baseline %in% df[[f1]] | nrow(df) < 2) {
            return(NULL)
        }
        cls = apply(df[, variables, drop = FALSE], MARGIN = 1, 
            function(ro) paste(ro, collapse = sep))
        bl.cls = cls[df[[f1]] == f1.baseline]
        return(paste(setdiff(cls, bl.cls), bl.cls, sep = "-"))
    })
    cts = as.vector(unlist(cts))
    names(cts) = NULL
    if (is.null(f2)) {
        return(cts)
    }
    l[[f1]] = cts
    cts = unique(pData(x)[, variables, drop = FALSE])
    rownames(cts) = NULL
    fixed = apply(cts[, setdiff(variables, f2), drop = FALSE], 
        MARGIN = 1, function(ro) paste(ro, collapse = sep))
    cts = by(cts, INDICES = fixed, function(df) {
        if (!f2.baseline %in% df[[f2]] | nrow(df) < 2) {
            return(NULL)
        }
        cls = apply(df[, variables, drop = FALSE], MARGIN = 1, 
            function(ro) paste(ro, collapse = sep))
        bl.cls = cls[df[[f2]] == f2.baseline]
        return(paste(setdiff(cls, bl.cls), bl.cls, sep = "-"))
    })
    cts = as.vector(unlist(cts))
    names(cts) = NULL
    l[[f2]] = cts
    cts = unique(pData(x)[, variables, drop = FALSE])
    rownames(cts) = NULL
    fixed = apply(cts[, setdiff(variables, c(f1, f2)), drop = FALSE], 
        MARGIN = 1, function(ro) paste(ro, collapse = sep))
    cts = by(cts, INDICES = fixed, function(df) {
        if (length(setdiff(df[[f1]], f1.baseline)) == 0 | length(setdiff(df[[f2]], 
            f2.baseline)) == 0) {
            return(NULL)
        }
        cls = apply(df[, variables, drop = FALSE], MARGIN = 1, 
            function(ro) paste(ro, collapse = sep))
        sl = c()
        for (f1l in setdiff(df[[f1]], f1.baseline)) {
            for (f2l in setdiff(df[[f2]], f2.baseline)) {
                if (any(df[[f1]] == f1l & df[[f2]] == f2l) & 
                  any(df[[f1]] == f1.baseline & df[[f2]] == f2l) & 
                  any(df[[f1]] == f1l & df[[f2]] == f2.baseline) & 
                  any(df[[f1]] == f1.baseline & df[[f2]] == f2.baseline)) {
                  sl = c(sl, paste("(", cls[df[[f1]] == f1l & 
                    df[[f2]] == f2l], "-", cls[df[[f1]] == f1.baseline & 
                    df[[f2]] == f2l], ")-(", cls[df[[f1]] == 
                    f1l & df[[f2]] == f2.baseline], "-", cls[df[[f1]] == 
                    f1.baseline & df[[f2]] == f2.baseline], ")", 
                    sep = ""))
                }
            }
        }
        return(sl)
    })
    cts = as.vector(unlist(cts))
    names(cts) = NULL
    l[[paste(f1, f2, sep = "_int_")]] = cts

    if(unlist)
    {
	l = as.character( unlist(l) )
    }
    return(l)
}


#' Naive guess  of whether some values are on the log2 scale
#' @title Guess if log2 scale
#' @param x some object that can be meaningfully coerced to vector
#' @author Lefebvre F
#' @return TRUE or FALSE
#' @export
guess.log2 <- function(x)
{
	x = as.vector(x)
	return( max(x)< 50) # Could be fancier like Weibull test or what not
}




#' Function to make some columns of of data frame as ordererd factors...
#' useful for ggplot2.
#'
#' @title Order a data frame by factors...
#' @param df A data frame
#' @param orders A list with names refering to colnames() of df. Elements of
#' this list are ordered levels.
#' @return Data frame with rows ordered acording to orders.
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
ordered.factorize.data.frame <- function(df,orders)
{

	cols = names(orders)
	for(i in 1:length(cols))
	{
		df[[ cols[i] ]] = factor(df[[ cols[i] ]], ordered=TRUE, levels=orders[[i]])
	}
	return(df)
}

#' Utility function that modifies make.unique to return repeated spaces instead of .number
#'
#'
#' coming soon
#'
#' @param s character vector
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
make.unique.with.spaces <- function(s)
{
	require(stringr)
	s = paste(s,' ',sep='')
	x = make.unique(s)
	nb.spaces = as.numeric(str_extract(x, "[0-9]*$"))
	nb.spaces[is.na(nb.spaces)] = 0
	spaces    =  sapply(nb.spaces,function(nb)   paste(rep(' ',times=nb) ,collapse='')   )
	x = paste(s,spaces,sep='')
	return(x)
}

#' Helper function to enumerate all possible pairwise comparison contrasts
#'
#'
#' coming soon
#'
#' @param coefs coming soon
#' @param sep coming soon
#' @param tri coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
pairwise.comparisons <- function(coefs,sep='-',tri='lower',formula=TRUE)
{
	m = matrix(rep(coefs,times=length(coefs)),ncol=length(coefs))
	diag(m) = ''
	m = matrix(paste(m,t(m),sep=sep),ncol=nrow(m))		
	m = switch(tri,
              both = c(m[lower.tri(m)],m[upper.tri(m)]),
              lower = m[lower.tri(m)],
              upper = m[upper.tri(m)])
	
	if(!formula)
	{
		m = strsplit(m,split=sep)
	}

	return(m)


}







#' Function to melt an ExpressionSet object. Requires 'ArrayID' in varLabel and ProbeID in fvarLAbel
#'
#'
#' coming soon
#'
#' @param e coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#' 
melt.eset <- function(e)
{
	require(Biobase)
	dat = melt(exprs(e))
	colnames(dat) =  c("ProbeID","ArrayID","Expression")
	dat = merge(dat,fData(e),by="ProbeID",all.x=TRUE,all.y=FALSE)
	dat = merge(dat,pData(e),by="ArrayID",all.x=TRUE,all.y=FALSE)
	dat[['ProbeID']] = as.character(dat[['ProbeID']])
	dat[['ArrayID']] = as.character(dat[['ArrayID']])
	return(dat)
}




#' Function to melt an ExpressionSet object.
#'
#'
#' coming soon
#'
#' @param e coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
melt.ExpressionSet <- function(e)
{
	require(Biobase)

	fData(e)[['_FeatureID']] = featureNames(e)
	pData(e)[['_SampleID']]  = sampleNames(e)

	dat = melt(exprs(e))
	colnames(dat) =  c("_FeatureID","_SampleID","Expression")
	dat = merge(dat,fData(e),by="_FeatureID",all.x=TRUE,all.y=FALSE)
	dat = merge(dat,pData(e),by="_SampleID",all.x=TRUE,all.y=FALSE)
	dat[['_FeatureID']] = as.character(dat[['_FeatureID']])
	dat[['_SampleID']] = as.character(dat[['_SampleID']])
	return(dat)
}







#' Shortcut Wrapper for WriteXLS
#'
#' @title Shortcut Wrapper for WriteXLS
#' @param obj.string coming soon
#' @param fn coming soon
#' @param row.names coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
my.WriteXLS <- function(obj.string,fn,row.names=FALSE)
{
	require(WriteXLS)
	WriteXLS(x=obj.string, ExcelFileName = fn, row.names = row.names, 
    AdjWidth = TRUE, BoldHeaderRow = TRUE, FreezeRow = 1)
}








#' BH FDR correct p-values
#'
#'
#' coming soon
#'
#' @param pvals coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
my.multtest.BH <- function(pvals)
{
    require(multtest)
    adj = mt.rawp2adjp( pvals ,proc="BH")
    adj = adj$adjp[order(adj$index),]
    adj = adj[,"BH"]
   
    return(adj)
}






#' Return a vector of colors matching to the levels of a factor
#'
#'
#' coming soon
#'
#' @param f coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
rainbow.colored <- function(f)
{
    f = as.factor(f)
    mycols = rainbow(nlevels(f))
    mycols = sapply(f,function(l){mycols[which(l==levels(f))]})
    return(mycols)
}






#' cluster mat, a function from the pheatmap package
#'
#'
#' coming soon
#'
#' @param mat coming soon
#' @param distance coming soon
#' @param method coming soon
#' @note coming soon
#' @author Raivo Kolde
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
cluster_mat <- function(mat,distance,method)
{
    if (!(method %in% c("ward", "single", "complete", "average", 
        "mcquitty", "median", "centroid"))) {
        stop("clustering method has to one form the list: 'ward', 'single', 'complete', 'average', 'mcquitty', 'median' or 'centroid'.")
    }
    if (!(distance %in% c("correlation", "euclidean", "maximum", 
        "manhattan", "canberra", "binary", "minkowski"))) {
        stop("distance measure has to one form the list: 'correlation', 'euclidean', 'maximum', 'manhattan', 'canberra', 'binary', 'minkowski'")
    }
    if (distance == "correlation") {
        d = dist(1 - cor(t(mat)))
    }
    else {
        d = dist(mat, method = distance)
    }
    return(hclust(d, method = method))
}




#' scale mat, a function from the pheatmap package
#'
#'
#' coming soon
#'
#' @param mat coming soon
#' @param scale coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
scale_mat <-function(mat,scale)
{
    if (!(scale %in% c("none", "row", "column"))) {
        stop("scale argument shoud take values: 'none', 'row' or 'column'")
    }
    mat = switch(scale, none = mat, row = scale_rows(mat), column = t(scale_rows(t(mat))))
    return(mat)
}




#' scale rows, a function from the pheatmap package
#'
#'
#' coming soon
#'
#' @param x coming soon
#' @note coming soon
#' @author Raivo Kolde
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
scale_rows <-function(x)
{
    m = apply(x, 1, mean)
    s = apply(x, 1, sd)
    return((x - m)/s)
}






#' This function is a wrapper for pheatmap. It writes to pdf with height and width adjusted
#'
#' @title A pheatmap() wrapper
#' @param mat coming soon
#' @param annotation coming soon
#' @param fn coming soon
#' @param w.weight coming soon
#' @param h.weight coming soon
#' @param \dots coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
pheatmap.wrapper<- function(mat,annotation=NULL,fn,w.weight=1.1,h.weight=1,remove.zero.var=TRUE,...)
{
	require(pheatmap)
	#   error + dendrogram + heatmap       + gene labels                   + spacer + colorkey 
	w = 0   + 1      + ncol(mat)*1/6 + max(nchar(rownames(mat)))*1/8 + 0    + 1/2
	if(!is.null(annotation)){ 
		w = w + max( 1/16*max(nchar(unlist(annotation,recursive=TRUE)))+0/2, 1/16*max(nchar(colnames(annotation)))  ) 
	}
    
	#   error + dendrogram + heatmap       + gene labels                   + spacer
	h = 0     + 1          + nrow(mat)*1/6 + max(nchar(colnames(mat)))*1/8 + 0   
	if(!is.null(annotation)){ 
		h = h + 1/8*ncol(annotation) 
	}else{annotation = NA}

	if(remove.zero.var)
	{
		keep.row =  apply(mat,MARGIN=1,var) > 0
		#keep.col =  apply(mat,MARGIN=2,var) > 0
		if(any(!keep.row))
		{
			mat = mat[keep.row,]#keep.col]
			#annotation = annotation[ colnames(mat),]
			warning("zero variance rows or columns remove")
		}
	}# TODO: apply to columns also one day I suppose


	pdf(file=fn,height=h*h.weight,width=w*w.weight)
	if(nrow(mat)>1 & ncol(mat)>1){pheatmap(mat=mat,annotation=annotation,...)}else{plot.new();warning('Cannot plot matrix with one row or one col')}
	dev.off()
}









        

 

#' pheatmap eset() is a helper function for faster heatmap generation from an ExpressionSet
#'
#' @title Heatmap of an ExpressionSet
#' @param e ExpressionSet object
#' @param file pdf file name to write to
#' @param which.rows rows/cols that should be plotted. featureNames/sampleNames or integers. which.rows defaults to *top 75 standard deviation* and which.cols defaults to ALL samples
#' @param order.crit the function that should be used to order rows (decreasing). Ignored if which rows is not NULL
#' @param nrows the maximmum number of rows that should be plotted in case of which.rows is NULL. Ignored if which.rows not null
#' @param row.labels character vectors indicating which of fvarLabels/varLabels(e) should be used as row/col names
#' @param remove.dup.row.labels whether rows with same row.names should be collapsed by order of appearance in which.rows
#' @param which.cols rows/cols that should be plotted. featureNames/sampleNames or integers. which.rows defaults to *top 75 standard deviation* and which.cols defaults to ALL samples
#' @param col.labels character vectors indicating which of fvarLabels/varLabels(e) should be used as row/col names
#' @param cols.ann which varLabels should be incluled as annotation for pheatmap (color code at the top)
#' @param \dots further args to be passed to pheatmap
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon Checks Deal with the rows Deal with columns Now call
#' my.pheatmap
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
pheatmap.eset <- function(e,file = 'eset_heatmap_top_sd.pdf',which.rows = NULL
					,order.crit = stats::sd, nrows = 75, row.labels = NULL,remove.dup.row.labels = FALSE,
					which.cols = sampleNames(e),col.labels = NULL, cols.ann  = NULL, ... )
{
	require(Biobase)
	require(pheatmap)

	if(  (!is.null(which.rows)&length(which.rows)<2) |  ( length(which.cols)<2  ) | nrow(e)<2 | ncol(e)<2  )
	{
		warning('Cannot plot heatmap. Need at least 2 rows and two columns ')
		pdf(file=file,height=2,width=2);plot.new();dev.off();
		return(e)
	}

	## Checks	
	if(is.logical(which.rows)){which.rows = which(which.rows)}
	if(  is.numeric(which.rows) & any(! which.rows %in% c(1:nrow(e))    ) ){stop('Some rows specified are out of bounds')}
	if(is.character(which.rows) & any(! which.rows %in% featureNames(e) ) ){stop('Some rows not in featureNames')}
	if(! is.function(order.crit)                                          ){stop('order.crit has to be a function')}

	if(nrows > nrow(e) & is.null(which.rows)){warning('The specified number of rows exceeeds nrow(e)');nrows=nrow(e)}
	if(nrows > 2000 | length(which.rows)>2000 | length(which.cols)>2000){warning('No way. This is way too many rows or cols');pdf(file=file,height=2,width=2);plot.new();dev.off();return(e)}


	if(is.logical(which.cols)){which.cols = which(which.cols)}
	if(  is.numeric(which.cols) & any(! which.cols %in% c(1:ncol(e))    ) ){stop('Some cols specified are out of bounds')}
	if(is.character(which.cols) & any(! which.cols %in% sampleNames(e) ) ) {stop('Some cols not in sampleNames')}

	if(  !is.null(row.labels) & any( ! row.labels %in% fvarLabels(e)   ) ){stop('row.labels not all in fvarLabels(e)')}
	if(  !is.null(col.labels) & any( ! col.labels %in% varLabels(e)   ) ) {stop('col.labels not all in varLabels(e)')}
	if(  !is.null(cols.ann) & any( ! cols.ann %in% varLabels(e)   ) ) {stop('cols.ann not in varLabels(e)')}


	## Deal with the rows
	if(is.null(which.rows))
	{
		e = e[order(  apply(exprs(e),MARGIN=1,order.crit)   ,decreasing=TRUE),] # this sorts the rows of eset
	}else{
		e = e[which.rows,]
	}
	fData(e)[['featureNames']] = featureNames(e)
	if(is.null(row.labels))
	{	
		row.labels = 'featureNames'
	}
	if(remove.dup.row.labels)
	{
			e               = e[!duplicated( do.call(paste,fData(e)[,row.labels,drop=FALSE]) ) ,] 
			featureNames(e) =                do.call(paste,fData(e)[,row.labels,drop=FALSE])
	}else{
			featureNames(e) = make.unique(   do.call(paste,fData(e)[,row.labels,drop=FALSE]) )
	}
	if(is.null(which.rows)) 
	{
		e = e[1:nrows,] # limits the number or rows in case rows were ordered by non-specifc crit
	}


	## Deal with columns
	e = e[,which.cols]
	if(is.null(col.labels))
	{
		pData(e)[['ArrayID']] = sampleNames(e)
		col.labels = 'ArrayID'
	}
	sampleNames(e)  = make.unique( do.call(paste,pData(e)[,col.labels,drop=FALSE]) )
	if(!is.null(cols.ann))
	{
		ann = pData(e)[,cols.ann,drop=FALSE]
	}else{ann=NULL}

	if(nrow(e)<2 | ncol(e)<2)
	{
		warning('Cannot plot heatmap. Need at least 2 rows and two columns ')
		pdf(file=file,height=2,width=2);plot.new();dev.off();
		return(e)
	}
	
	## Now call my.pheatmap
	pheatmap.wrapper(exprs(e),annotation=ann,fn=file,...)	
	return(e)
}









#' Converts log2FC into FC (geometrical mean interpretation)
#'
#' @title Convert logFC into FC
#' @param x vector of log2(FC)
#' @note coming soon
#' @author Goulet JP
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#' 
toFold <- function(x)
{
	x[x>=0] =   2^(x[x>=0])
	x[x<0]  =  -2^(-x[x<0])
	return(x)
}





#' multtest BH
#'
#'
#' coming soon
#'
#' @param pvals coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
multtest.BH <- function(pvals)
{
    library(multtest)
    adj = mt.rawp2adjp( pvals ,proc="BH")
    adj = adj$adjp[order(adj$index),]
    adj = adj[,"BH"]
	
    return(adj)
}




#' Return a color string vector corresponding to x, with a different colour for each type of value provided in x.
#'
#'
#' coming soon
#'
#' @param x coming soon
#' @note coming soon
#' @author Lefebvre F
#' @seealso coming soon
#' @references coming soon
#' @keywords coming soon
#' @export
#' @examples
#'
#' # Example coming soon
#'
color.me <- function(x)
{
	x = factor(x)
	levels(x) = rainbow(nlevels(x)) # rainbow by default
	if(nlevels(x) == 2 )
	{
		levels(x) = c("blue","yellow")
	}
	if(nlevels(x) == 4 )
	{
		levels(x) = c("blue","cyan","yellow","magenta")
	}
	
	return(as.character(x))
}




#' hclust color function
#'
#' plclust_in_colour(fit, rowNames, unlist(sapply(rowNames, get.color)), main="hclust SWEDEN raw reads + asinh")
#' legend("topright", l.labels, col=l.colors, pch=15, cex=0.7)
#' @export
plclust_in_colour <- function( hclust, lab=hclust$labels, lab.col=rep(1,length(hclust$labels)), hang=0.1, ... )
{
     y <- rep(hclust$height,2)
     x <- as.numeric(hclust$merge)
     y <- y[which(x<0)]
     x <- x[which(x<0)]
     x <- abs(x)
     y <- y[order(x)]
     x <- x[order(x)]
     plot( hclust, labels=F, hang=hang, ... )
    text( x=x, y=y[hclust$order]-(max(hclust$height)*hang), labels=lab[hclust$order], cex=0.8, col=lab.col[hclust$order], srt=90, adj=c(1,0.5), xpd=NA, ... )
}





