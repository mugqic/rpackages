10 tags, 314 commits

HEAD        Tue Apr 26 16:21:18 2022 +0000        0 commits

1.0.7        Tue Apr 26 16:22:51 2022 +0000        199 commits

  Edouard Henrion <edouard.henrion@mcgill.ca>      13 commits

       f887df4 Add gqData to .gitignore
       6daa535 Add gqMicroarrays to .gitignore
       34bfd78 trying to make this repo lighter than it is
       a1beeb8 Version bump to 1.0.7-beta
       fca03ae Version bump to 1.0.6
       dfff0d0 Version bump to 1.0.6
       10f352d Revert "Version bump to 1.0.6"
       556a2fc Revert "Version bump to 2.2.3-beta"
       d566056 Revert "deleting gqSeqUtils_1.0.tar.gz"
       e4f8beb Revert "Version bump to 1.0.7-beta"
       3c71583 Version bump to 1.0.7-beta
       b8dafe0 Version bump to 2.2.3-beta
       3d40acc Version bump to 1.0.6

  Édouard Henrion <henrione@beluga2.int.ets1.calculquebec.ca>      1 commits

       33c4680 Updating VERION

  ehenrion <edouard.henrion@mcgill.ca>      1 commits

       07ad239 README.md edited online with Bitbucket

  Eloi Mercier <emercier@jonquille.genome.mcgill.ca>      4 commits

       8a66e96 added gene symbol to rnaseq_light heatmap
       045de06 added exploratoryAnalysisRNAseqLightKallisto to NAMESPACE
       05db104 added a new exploratory function for RNAseq_light
       b7dee32 added Clariom_S_Mouse plateform

  emercier <eloi.mercier@mcgill.ca>      12 commits

       8925629 in gqUtils/R/misc.r: fix pca function to correctly compute PC1 and PC2
       e5691be in exploratory.R: fix an issue in exploratoryAnalysisSmallRNA where the columns of the object were not properly selected
       403caf8 fix Porcine 1.0 st array
       544d97d added latest version 36 of porcine 1.0 array
       7dd9dba built gqData tar
       38efd0e in gqMicroarray/R/misc.r: improve labeling of mds and pca functions
       fe2a46e in gqMicroarray/R/misc.r: change mds and pca function to no longer duplicated pdata (no longer supported by ggplot 3.0)
       f88df70 added porcine 1.0 ST array
       b0715d1 in gqSeqUtils: fix variable names in exploratoryAnalysisSmallRNA
       ddea43b in gqSeqUtils: add exploratoryAnalysisSmallRNA to NAMESPACE
       46f7b18 Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       abc4117 in exploratory.r: add exploratoryAnalysisSmallRNA function for smallrna pipeline

  Francois Lefebvre <francois.lefebvre3@mail.mcgill.ca>      4 commits

       b81accc utils.r edited online with Bitbucket
       df7d72f README.md edited online with Bitbucket
       7bd2ad9 DESCRIPTION edited online with Bitbucket
       1b80e23 README.md edited online with Bitbucket

  Francois Lefebvre <francois.lefebvre@mcgill.ca>      1 commits

       4ba9d3b misc.r reverting back to prcomp default options for center and scale. .

  Francois Lefebvre <lefebvr3@ip03.m>      1 commits

       9a0b91e TrinotatePlus beta

  Francois Lefebvre <lefebvrf@gmail.com>      52 commits

       72b8060 no message
       bfbe998 Documentation update
       480782b Fixed integer overflow problem in DnaFastaStats and moved to it's own file
       9978d53 Minor correction to threshold reporting
       3709211 Doc update
       f6ec919 Fixed bug in sample names attribution
       dae5de3 Documentation
       8c657ec Adjustement for cuff norm run on split cab list (appends _0 to column names)
       7a80e0b Adjusted exploratory for python pipeline
       8ed8732 getBMsimple output improvement
       e9ff8d2 doc update
       d664253 Fucntion getBMsimple() to help genome installation (GO terms)
       f37c1e6 bug fix: calls to summary() would round off large numbers to 4 digits, reported length statistics would therefore be inaccurate for large numbers
       cf76e31 Account for possible absence of DEFAULT section
       be28e35 Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       21b9a55 getIniParams now returns default value if ini section empty or not found,
       99168c2 Bunch of changes requested by Joel to make compatible with Python pipeline
       2d07169 Changes in getIniParams() to accommodate [DEFAULT] ini spec
       16a2065 ini.file.path check for existence would return a warning. Fixed this
       914faaa some cleanup
       80ab39b prior.df argument to disable squeezing, e.g. with miRNA
       2b88ec5 no message
       3607e7b estimateDisp() not wiring well. Reverting
       ec6be9d Changed dgeaEdgerAnalysis to use the simplified estimateDisp() function, also allowing other args to estimateDisp(), e.g. prior.df to be able to disable shrinkage  totally, e.g. may be useful for miRNA count with a lot of multi-reads
       8fe005b Modified getIniParams to accommodate multiple ini files as input to be interpreted as a single stream.
       1b2ecb1 no message
       a6cfc65 roxygenised
       d72c74f First version of TrinotatePlus
       9bd0891 theme(), not coord_equal()
       fc632e3 Removed calls to ggplot2::opts()  , now deprecated.
       6c0644e log10_trans was apparently removed from ggplot2. Repalced with scale_x_log10()
       4ab25e7 Changed regular expression to handle new Trinity.fasta header notation
       bf372a6 Updated to support  HTseq-count newer special counter names (version>0.5.4)
       43e0926 Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       d3e04c3 Roxygenized
       c50eb97 splitDNAFasta returns filenames
       ad54e82 Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       27b0ba9 Removed tar.gz.builds
       0d4548e gqSeqUtils wasn't roxygenized
       282dd91 dgeaEdgerFit uses cpm(y,normalized.lib.size=TRUE,log=TRUE) instead of predFC(design=NULL).
       5fcc2a6 import methods
       53ebe68 running roxygen kept removing the import(methods) clause, which caused bugs with Rscript -e... added to .ini to make sure import clause is written
       e79beee removed useless require clauses
       46f56d8 removed DEPENDENCIES, old stuff
       107343b roxygenized
       cb1e3fc handling comments bettwe
       4233a02 getInitParams handles duplicate sections
       adfce14 Improved getIniParams() in preparation for use to generate master ini : comment.char new argument no longer hard-coded to #
       8adeaa7 Improved getIniParams() in preparation for use to generate master ini : comment.char new argument no longer hard-coded to #
       82dc82f Fixed minor bug in getIniParams() where a param section with no actual param defined would cause calls with as.data.frame=TRUE to crash
       0dc9724 Added a prototype of function dnaFastaStats which computes a bunch of stats on FASTA files.
       e9b5bcc roxygenized and build packages in prep for mugqic_pipeline release

  Joël Fillon <joel.fillon@mcgill.ca>      34 commits

       83b1b03 Fixed nozzle PacBio BLAST tsv read bug by adding quote="", stringsAsFactors=FALSE, comment.char=""
       b0148da BFXDEV-295 Update dnaFastaStats with Trinity 2.0
       32f8b49 BFXDEV-292 Minor updates in chipseq nozzle report
       d262a85 BFXDEV-292 More updates in nozzle report for chipseq pipeline
       b6ac7bf BFXDEV-292 Updated nozzle report for chipseq pipeline
       b5aabab Fixed readset trim table + genome name + 'lane' -> 'readset' in DNA nozzle report
       94b69e3 Fixed bug cuffArchive path + minor cufflinks/AllSamples mispelling
       70164c7 Updated RNA-Seq nozzle report with new genome INI parameters + removed transcript GO enrichment section
       cc34681 Renamed .ini param section 'report' into 'gq_seq_utils_report'
       42a2fae Updated RNAseq nozzle paths and config params
       b1e5427 Fixed 'default' in nozzleCHIPseq.r
       bb5a301 Fixed merge conflicts
       7348720 Fixed warning bug in nozzle config list check + updated pacbioAssembly nozzle report with Python pipeline
       0bc2bb3 Fixed merging conflicts in nozzleRNAseqDeNovo.r
       57c52f0 Replaced 'default' by 'DEFAULT' in ini param for RNASeq De Novo
       4f2e2ed Updated paths in RNAseq De Novo nozzle report
       a8dc522 Fixed blast config values removed from config file
       f121df1 Presentation modifications for RNA-Seq De Novo nozzle report
       a47ccd5 Updated trimming/normalization labels to be more explicit in RNA-Seq De Novo nozzle report
       e2a1eb8 Changed gzip file paths to zip file paths in RNA-Seq De Novo nozzle report
       57ebcac Fixed mismatch '}' in help
       9919fab Fixed normalization stats filepath
       ac5943d Fixed bug missing required scales library
       3dc1a3a Fixed normalization stats file name
       1c5ba94 More RNA-Seq De Novo nozzle report development
       2b7c771 More RNA-Seq nozzle report development
       85312f8 More RNA-Seq De Novo nozzle report + clean up code in info.r and utils.r
       18e5a50 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       0dd3e94 Added stats JPEG export
       9a217d4 Fixed component count regular expression
       ab2f94b Added ggplot Transcript Length Distribution histogram with N50 + space formatting
       054b8c6 More development on RNA-Seq De Novo Nozzle report
       5423d46 More development on RNA-Seq De Novo Nozzle report
       6f8068c First draft of RNA-Seq De Novo Assembly Pipeline

  Johanna Sandoval <johanna.sandoval@mail.mcgill.ca>      4 commits

       d002394 BFXDEV-396 change rownames in gene length from gene_id to 1 to allow for exploratory analysis using filtered transcripts
       e8465aa BFXDEV-396 generated roxygen doc for gqSeqUtils
       4fba65c BFXDEV-396 add exploratory analysis to rnaseq_denovo_assembly pipeline, added doc generated with roxygen
       0a86eed BFXDEV-396 add exploratory analysis to rnaseq_denovo_assembly pipeline

  johanna.sandoval@mail.mcgill.ca <johanna.sandoval@mail.mcgill.ca>      20 commits

       74885f6 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       2ebd1fc BFXDEV-96 detected a bug in genome ontology path, nozzle chipseq report
       1a3942a Merge branch 'master' of bitbucket.org:mugqic/rpackages
       2ad9ca4 BFXDEV-96 merged commits MBourgey and annlow nozzle report for partial chipseq pipeline analysis
       c8294dc BFXDEV-96 generate the standard report with partial chipseq pipeline results
       59bc07d BFXDEV-96 Nozzle RNAseq tested in 16 missing files scenarios
       b22e7c0 BFXDEV-96 generate DNASEQ nozzle report if species descriptor is not available
       ae9fa55  Bug BFXDEV-96 allow nozzle report for partial pipeline analysis - DNASEQ
       a073fc4 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       c8bbb2d BFXDEV-85 added number of reads after filtering steps and number of marked as duplicate reads to CHIPSEQ nozzle report
       198db3c BFXDEV-82 validate that general annotation stats are defined to generate this section's report
       9487ee3 BFXDEV-82 error message when annotation folder is absent (only broad peaks in design file)
       f1ad697 Merge branch 'master' of bitbucket.org:mugqic/Rpackages
       ec29b00 updated noozle INFO for tests of CHIPSEQ pipeline on Mammouth
       55fd57c Merge branch 'master' of bitbucket.org:mugqic/Rpackages
       47186ae Added minimal doc for chipseq pipeline noozle report
       df9f764 Completed the nozzle chipseq report
       1335c8d adding info for the CHIPseq pipeline report
       92091b1 adding the CHIPseq pipeline report, corrected bug
       6cbdef4 adding the CHIPseq pipeline report

  jtrembla <jtremblay514@gmail.com>      9 commits

       9c99986 modified some tables added the coverage of each contig in the blast table. BFXDEV-30
       6566e09 Fixed some parts of the methodology to include pacbio data types. BFXDEV-31
       5ff730b Added symlink to fastq directory for nc1 and nc2. BFXDEV-31
       b7fd375 Fix indice bug in barcode table for index.html file. BFXDEV-31
       dcc2ba2 Updated nozzle report to take into account new summary tables. BFXDEV-30
       25bced8 Added support for number of polishing rounds in nozzle report. BFXDEV-30
       3716b53 Corrected text for figure 3. BFXDEV-31
       4a6994b Added symlinks to trimmed\QC Fastqs. BFXDEV-31
       bccc169 corrected typo relative/absolute. BFXDEV-31

  Julien Tremblay <jtrembla@ip14.m>      2 commits

       dec6529 Undoing last commit. Wrong repo... :-(
       aaf426b updating nozzle for rrnadiversity

  Julien Tremblay <julien.tremblay@mail.mcgill.ca>      10 commits

       28d425a fixed libraryType selection. BFXDEV-31
       139b7b7 Added nozzle RRNATagger and RRNATaggerDiversity. BFXDEV-31
       a743e31 Added Nozzle report for RRNATagger.
       858d8be fixed path for mummer plots. BFXDEV-30
       9bd9a25 Updated nozzle pacbio. BFXDEV-30
       d5eca63 Fixes for the DNAseq nozzle report. Tested on PRJBFX-517 data.
       323ba7b Merge branch 'master' of bitbucket.org:mugqic/rpackages
       b2c1797 Added link to contig fasta file in nozzle report.
       e388b35 Added PacBio support for Nozzle report...
       56bb758 Added DNAseq in pipeline choices

  lefebvrf <francois.lefebvre3@mail.mcgill.ca>      1 commits

       7fd7a0e roxygenized gqSeqUtils

  lefebvrf <lefebvrf@gmail.com>      13 commits

       5dd0f1c Changes to documentation
       1aca47e minor change to cover trinity assembly projects with no diff expression
       120c55f Affy yeast genome 2.0 support added (and 3'IVT arrays in general).
       c3582b7 had wrong filter function return value
       005aae1 fix
       afee649 TrinotatePlus cdv instead of HTML
       2a05fbc Updated calculateGeneLengthsFromGtf to allow different features on which to reduce(), and allow writing to a file
       8ca76b5 Fixed initIllSeqProject following change in nanuq read set sheet  Multiplex Key column
       c3798a1 RNAseqdenovo report was not roxygenized and caused installation crash
       c2666a3 now imports rtracklayer
       a481e51 no message
       ffc03dc methods package
       a5b2de0 no message

  mathieu bourgey <mathieu.bourgey@mail.mcgill.ca>      17 commits

       a417693 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       9eecf8b gqSeqUtils - RNA nozzle corect wrong location for cufflink archive
       a1eac81 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       5e55ec3 gqSeqUtils - Update RNA nozzle reports
       fca98b4 gqSeqUtyils - revamp RNAseq report for pyhton pipeline + correct somme bugs in info.r
       d962e71 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       36698ee Merge branch 'master' of bitbucket.org:mugqic/rpackages
       f8f5993 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       a92eb53 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       ace325d start externalize the text section of pipeline reports
       95ac32d nozzleCHIPseq.r correcting formating and replacment errors BFXDEV-139
       2e67890 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       a67228e update nozzleDNA.r
       f22d609 nozzleDNA.R update
       3d15361 conflict in gqSeqUtils/R/info.r resolution
       e1fc278 nozzleDNA.R update
       0f1c956 nozzleDNA.R update

1.0.6        Wed Mar 20 16:03:36 2019 -0400        25 commits

  Edouard Henrion <edouard.henrion@mcgill.ca>      9 commits

       3a32f5d Removing gqData and gqMicroarrays as they are now part of the mugqic repository named microarrays
       864fb32 reinstalling all the deleted stuff
       6a51afe Revert "Version bump to 1.0.6"
       3f44031 Revert "Version bump to 2.2.3-beta"
       bd8bd77 Revert "Version bump to 1.0.7-beta"
       9765d34 Version bump to 1.0.7-beta
       414f0e7 delteting gqData and gqMicroarrays fomr the repo : too heavy...
       3828cd9 Version bump to 2.2.3-beta
       c6fc66e Version bump to 1.0.6

  ehenrion <edouard.henrion@mcgill.ca>      1 commits

       6826d41 README.md edited online with Bitbucket

  Eloi Mercier <emercier@jonquille.genome.mcgill.ca>      3 commits

       78b74ec added gene symbol to rnaseq_light heatmap
       a585f7f added exploratoryAnalysisRNAseqLightKallisto to NAMESPACE
       03d5ff0 added a new exploratory function for RNAseq_light

  emercier <eloi.mercier@mcgill.ca>      12 commits

       1ae45a1 in gqUtils/R/misc.r: fix pca function to correctly compute PC1 and PC2
       7beaefb in exploratory.R: fix an issue in exploratoryAnalysisSmallRNA where the columns of the object were not properly selected
       c285630 fix Porcine 1.0 st array
       033c712 added latest version 36 of porcine 1.0 array
       cf1df68 built gqData tar
       cef8df5 in gqMicroarray/R/misc.r: improve labeling of mds and pca functions
       ecac88a in gqMicroarray/R/misc.r: change mds and pca function to no longer duplicated pdata (no longer supported by ggplot 3.0)
       5dcb5a3 added porcine 1.0 ST array
       84cff6d in gqSeqUtils: fix variable names in exploratoryAnalysisSmallRNA
       a6e82cf in gqSeqUtils: add exploratoryAnalysisSmallRNA to NAMESPACE
       5011b4f Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       f409a49 in exploratory.r: add exploratoryAnalysisSmallRNA function for smallrna pipeline

1.0.5        Mon Jun 5 15:38:10 2017 -0400        6 commits

  Eloi Mercier <emercier@jonquille.genome.mcgill.ca>      2 commits

       5b32bc1 adding Clariom D Human
       ea3f2c9 added Clariom_S_Mouse plateform

  Francois Lefebvre <francois.lefebvre3@mail.mcgill.ca>      1 commits

       1a4ed81 utils.r edited online with Bitbucket

1.0.4        Wed Jan 6 14:06:00 2016 -0500        8 commits

  Francois Lefebvre <francois.lefebvre3@mail.mcgill.ca>      2 commits

       6dcfe91 README.md edited online with Bitbucket
       7d57ed9 DESCRIPTION edited online with Bitbucket

  Francois Lefebvre <lefebvrf@gmail.com>      1 commits

       52d77c2 no message

  Joël Fillon <joel.fillon@mcgill.ca>      1 commits

       928fa1d Fixed nozzle PacBio BLAST tsv read bug by adding quote="", stringsAsFactors=FALSE, comment.char=""

  Johanna Sandoval <johanna.sandoval@mail.mcgill.ca>      4 commits

       3f864a5 BFXDEV-396 change rownames in gene length from gene_id to 1 to allow for exploratory analysis using filtered transcripts
       3f840cf BFXDEV-396 generated roxygen doc for gqSeqUtils
       6398f79 BFXDEV-396 add exploratory analysis to rnaseq_denovo_assembly pipeline, added doc generated with roxygen
       2306540 BFXDEV-396 add exploratory analysis to rnaseq_denovo_assembly pipeline

1.0.3        Fri Feb 27 11:12:18 2015 -0500        1 commits

  Joël Fillon <joel.fillon@mcgill.ca>      1 commits

       01b06d0 BFXDEV-295 Update dnaFastaStats with Trinity 2.0

1.0.2        Fri Jan 30 15:06:02 2015 -0500        5 commits

  Francois Lefebvre <lefebvrf@gmail.com>      2 commits

       a27ca8e Documentation update
       97f6a84 Fixed integer overflow problem in DnaFastaStats and moved to it's own file

  Joël Fillon <joel.fillon@mcgill.ca>      3 commits

       7e4a8ae BFXDEV-292 Minor updates in chipseq nozzle report
       6f9706d BFXDEV-292 More updates in nozzle report for chipseq pipeline
       507d56b BFXDEV-292 Updated nozzle report for chipseq pipeline

1.0.1        Tue Dec 16 16:25:19 2014 -0500        2 commits

  Francois Lefebvre <lefebvrf@gmail.com>      1 commits

       718bb27 Minor correction to threshold reporting

  Joël Fillon <joel.fillon@mcgill.ca>      1 commits

       d39fe59 Fixed readset trim table + genome name + 'lane' -> 'readset' in DNA nozzle report

1.0.0        Thu Dec 11 17:06:03 2014 -0500        20 commits

  Francois Lefebvre <lefebvrf@gmail.com>      8 commits

       558d627 Doc update
       a76808f Fixed bug in sample names attribution
       2a9a7f1 Documentation
       08c339d Adjustement for cuff norm run on split cab list (appends _0 to column names)
       428349f Adjusted exploratory for python pipeline
       867facb getBMsimple output improvement
       e00c9e8 doc update
       6a4d50e Fucntion getBMsimple() to help genome installation (GO terms)

  Joël Fillon <joel.fillon@mcgill.ca>      2 commits

       0548eb0 Fixed bug cuffArchive path + minor cufflinks/AllSamples mispelling
       58973ea Updated RNA-Seq nozzle report with new genome INI parameters + removed transcript GO enrichment section

  mathieu bourgey <mathieu.bourgey@mail.mcgill.ca>      10 commits

       febb669 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       0686bca gqSeqUtils - RNA nozzle corect wrong location for cufflink archive
       5afdaec Merge branch 'master' of bitbucket.org:mugqic/rpackages
       8c9d1a7 gqSeqUtils - Update RNA nozzle reports
       285b3d8 gqSeqUtyils - revamp RNAseq report for pyhton pipeline + correct somme bugs in info.r
       1e85011 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       16f0aa1 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       ef9e079 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       2f70750 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       6f94174 start externalize the text section of pipeline reports

0.1        Tue Nov 4 09:50:48 2014 -0500        244 commits

  Francois Lefebvre <francois.lefebvre3@mail.mcgill.ca>      3 commits

       a7ee020 README.md edited online with Bitbucket
       dc7ff0d Merged in reportSetup (pull request #1)
       f88520a accessors.r edited online with Bitbucket

  Francois Lefebvre <lefebvr3@ip03.m>      1 commits

       6a1a406 TrinotatePlus beta

  Francois Lefebvre <lefebvrf@gmail.com>      95 commits

       73662f3 bug fix: calls to summary() would round off large numbers to 4 digits, reported length statistics would therefore be inaccurate for large numbers
       a8f9c20 Account for possible absence of DEFAULT section
       10b19dc Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       d95d252 getIniParams now returns default value if ini section empty or not found,
       f0b6269 Bunch of changes requested by Joel to make compatible with Python pipeline
       eaf6d4c Changes in getIniParams() to accommodate [DEFAULT] ini spec
       9b0a1b8 ini.file.path check for existence would return a warning. Fixed this
       e2665b2 some cleanup
       42bd221 prior.df argument to disable squeezing, e.g. with miRNA
       d95e62d no message
       154087d estimateDisp() not wiring well. Reverting
       5af5534 Changed dgeaEdgerAnalysis to use the simplified estimateDisp() function, also allowing other args to estimateDisp(), e.g. prior.df to be able to disable shrinkage  totally, e.g. may be useful for miRNA count with a lot of multi-reads
       bb99669 Modified getIniParams to accommodate multiple ini files as input to be interpreted as a single stream.
       f6be2fb no message
       cd21301 roxygenised
       782d9a1 First version of TrinotatePlus
       a563b46 theme(), not coord_equal()
       f1fb5fb Removed calls to ggplot2::opts()  , now deprecated.
       646bae0 log10_trans was apparently removed from ggplot2. Repalced with scale_x_log10()
       0496171 Changed regular expression to handle new Trinity.fasta header notation
       366567c Updated to support  HTseq-count newer special counter names (version>0.5.4)
       6e1a808 Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       6023049 Roxygenized
       0f511f8 splitDNAFasta returns filenames
       38f05fe Merge branch 'master' of https://bitbucket.org/mugqic/rpackages
       4101218 Removed tar.gz.builds
       6c9ac29 gqSeqUtils wasn't roxygenized
       80519af dgeaEdgerFit uses cpm(y,normalized.lib.size=TRUE,log=TRUE) instead of predFC(design=NULL).
       58fce0e import methods
       0e0f6b4 running roxygen kept removing the import(methods) clause, which caused bugs with Rscript -e... added to .ini to make sure import clause is written
       a0b2867 removed useless require clauses
       54aac21 removed DEPENDENCIES, old stuff
       a1b61a0 roxygenized
       8ab359a handling comments bettwe
       ea8bf67 getInitParams handles duplicate sections
       94d0f8f Improved getIniParams() in preparation for use to generate master ini : comment.char new argument no longer hard-coded to #
       dc8adfd Improved getIniParams() in preparation for use to generate master ini : comment.char new argument no longer hard-coded to #
       20669ca Fixed minor bug in getIniParams() where a param section with no actual param defined would cause calls with as.data.frame=TRUE to crash
       5c75e81 Added a prototype of function dnaFastaStats which computes a bunch of stats on FASTA files.
       983e958 roxygenized and build packages in prep for mugqic_pipeline release
       af5c986 no message
       ca698e1 doc update
       8af539a Changed png to pdf for scatterplot, transparency png not supported everywhere
       c90be79 Added scatterplot and excluded some plots for cases of just two samples
       25cd57a Fixed pvca in gqUItils to accomodate two-sample datasets
       d42215c Fixed uA.mds to handle two-sample datasets
       107533d Fixed uA.cordist.hclust() to handle  (stupid) cases with fewer than three samples.
       ef037a5 Added import() clauses to NAMESPACE. Apparently functions like IRanges::as.data.frame or GenomicRanges::split were not visible otherwise.
       b223615 Added GenomicRanges as explicit dependency
       37bc5cb require classed in utils function recently created a bug; split() was not calling the correct method.
       c785cac no message
       54c8570 no message
       3d38d1d no message
       a562231 improved extractGeneAnnotationFromGtf  to read gtf explicitly with format arg, + option to write to file
       9fb7cf6 Added parameter overwrite.sheets=TRUE to initIllProject() to be able to use this function in the context of the perl pipeline.
       c83146c Doc update
       ff8ac2b Human Gene ST 2.0 added to gqData
       7d0e2dd Doc update
       c31acdf Finished first iteration exploratory analysis  for RNA-seq
       70d5b02 Modified gqUtils uA functions slightly + added genefilter dependency
       1db377b Added C albicans custom array + fixed problem with printer slot not present for image plot()
       04c534e Added accessors for  trimoamticand samtools idxstats output
       e40d667 Added accessors for exp. variables, baseline levels, sample description columns, etc…
       793ef10 Bunch of changes
       c94d916 more changes
       26992ab no message
       e8ab2d7 no message
       d19727f Added Support for Affymetrix Gene St arrays
       12edd22 Added function loadReadCounts() that loads the counts at output by the Perl pipelines
       bc669fc roxygenized doc
       e86ad10 Small changes to PVCA functions
       6c045e5 Small mods to pca.anova.eset (subset of cove) and added MAxime's plclust_colored to gqUtils
       cfdd886 Removed Rsge dep.
       f8a647f Minor changes to agilent single channel pipeline:  background images now reported, also reading in AFE outlier flags and reporting their sum over all arrays (as an alternative to down-wieghting or discarding the spot altogether)
       99e056e updated doc
       c94349a Added Agilent rat support
       30e580a Finalized splitting
       fa2f3ca splitted module installtion scripts
       ff519e8 A bunch of updates in gqMicroarrays for Illumina Bead arrays
       6da4d7f ...
       ae241f2 Re-organized gqSeqUtils + added function to initialize seq project (sample setup + other stuff)
       e891a48 no message
       ae5d2c7 ...
       ae67657 A bunch of changes
       05ff360 re-arramged to a single package
       04d04d9 In the process of re-arranging code across packages
       0d97881 more progress
       bdbeb3d ...
       d8d427c first mugqic reporting commit
       8625a4e no message
       527068e added chmod to install script
       8701eb2 updated install.sh to use $MUGQIC_INSTALL_HOME
       0aa5c88 no message
       a908690 First major commit.
       348a8fa no message

  Joël Fillon <joel.fillon@mcgill.ca>      26 commits

       cfea28e Renamed .ini param section 'report' into 'gq_seq_utils_report'
       8881ef4 Updated RNAseq nozzle paths and config params
       43801da Fixed 'default' in nozzleCHIPseq.r
       3ad338b Fixed merge conflicts
       0ae994e Fixed warning bug in nozzle config list check + updated pacbioAssembly nozzle report with Python pipeline
       b91a8a3 Fixed merging conflicts in nozzleRNAseqDeNovo.r
       17fcbe3 Replaced 'default' by 'DEFAULT' in ini param for RNASeq De Novo
       721fac3 Updated paths in RNAseq De Novo nozzle report
       efa7fb4 Fixed blast config values removed from config file
       fcf3ce4 Presentation modifications for RNA-Seq De Novo nozzle report
       a2449b7 Updated trimming/normalization labels to be more explicit in RNA-Seq De Novo nozzle report
       d44c397 Changed gzip file paths to zip file paths in RNA-Seq De Novo nozzle report
       63e2c0a Fixed mismatch '}' in help
       670343e Fixed normalization stats filepath
       9a16dce Fixed bug missing required scales library
       ec08b5d Fixed normalization stats file name
       c72bbb4 More RNA-Seq De Novo nozzle report development
       928b42b More RNA-Seq nozzle report development
       d0f156c More RNA-Seq De Novo nozzle report + clean up code in info.r and utils.r
       e435d89 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       c6ea41c Added stats JPEG export
       febdce0 Fixed component count regular expression
       f959f77 Added ggplot Transcript Length Distribution histogram with N50 + space formatting
       17cbb0a More development on RNA-Seq De Novo Nozzle report
       5bbbfb3 More development on RNA-Seq De Novo Nozzle report
       2740b1a First draft of RNA-Seq De Novo Assembly Pipeline

  johanna.sandoval@mail.mcgill.ca <johanna.sandoval@mail.mcgill.ca>      20 commits

       2919dd8 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       b14a43b BFXDEV-96 detected a bug in genome ontology path, nozzle chipseq report
       c9a94f7 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       f589f90 BFXDEV-96 merged commits MBourgey and annlow nozzle report for partial chipseq pipeline analysis
       94561f1 BFXDEV-96 generate the standard report with partial chipseq pipeline results
       ef4452b BFXDEV-96 Nozzle RNAseq tested in 16 missing files scenarios
       be64616 BFXDEV-96 generate DNASEQ nozzle report if species descriptor is not available
       ae6b326  Bug BFXDEV-96 allow nozzle report for partial pipeline analysis - DNASEQ
       1db3a52 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       82b1116 BFXDEV-85 added number of reads after filtering steps and number of marked as duplicate reads to CHIPSEQ nozzle report
       9298598 BFXDEV-82 validate that general annotation stats are defined to generate this section's report
       0585110 BFXDEV-82 error message when annotation folder is absent (only broad peaks in design file)
       9b4d20c Merge branch 'master' of bitbucket.org:mugqic/Rpackages
       4d8956b updated noozle INFO for tests of CHIPSEQ pipeline on Mammouth
       49dbd3b Merge branch 'master' of bitbucket.org:mugqic/Rpackages
       7d898cf Added minimal doc for chipseq pipeline noozle report
       7a12b0a Completed the nozzle chipseq report
       1d24e21 adding info for the CHIPseq pipeline report
       240d1f8 adding the CHIPseq pipeline report, corrected bug
       93e1122 adding the CHIPseq pipeline report

  jtrembla <jtremblay514@gmail.com>      9 commits

       b3f65a6 modified some tables added the coverage of each contig in the blast table. BFXDEV-30
       6440115 Fixed some parts of the methodology to include pacbio data types. BFXDEV-31
       44c529b Added symlink to fastq directory for nc1 and nc2. BFXDEV-31
       ab0db85 Fix indice bug in barcode table for index.html file. BFXDEV-31
       0f97691 Updated nozzle report to take into account new summary tables. BFXDEV-30
       f68220f Added support for number of polishing rounds in nozzle report. BFXDEV-30
       ca18285 Corrected text for figure 3. BFXDEV-31
       576a4e8 Added symlinks to trimmed\QC Fastqs. BFXDEV-31
       0b99258 corrected typo relative/absolute. BFXDEV-31

  Julien Tremblay <jtrembla@ip14.m>      2 commits

       3de0378 Undoing last commit. Wrong repo... :-(
       7bf05c4 updating nozzle for rrnadiversity

  Julien Tremblay <julien.tremblay@mail.mcgill.ca>      10 commits

       c36282e fixed libraryType selection. BFXDEV-31
       fd9a13d Added nozzle RRNATagger and RRNATaggerDiversity. BFXDEV-31
       5eb88e5 Added Nozzle report for RRNATagger.
       88bd51d fixed path for mummer plots. BFXDEV-30
       4946e0f Updated nozzle pacbio. BFXDEV-30
       140ae6f Fixes for the DNAseq nozzle report. Tested on PRJBFX-517 data.
       747db2b Merge branch 'master' of bitbucket.org:mugqic/rpackages
       fac7c95 Added link to contig fasta file in nozzle report.
       5f38403 Added PacBio support for Nozzle report...
       222ceff Added DNAseq in pipeline choices

  lefebvrf <francois.lefebvre3@mail.mcgill.ca>      1 commits

       8539b7d roxygenized gqSeqUtils

  lefebvrf <lefebvrf@gmail.com>      38 commits

       6976ba1 Changes to documentation
       9352271 minor change to cover trinity assembly projects with no diff expression
       b3a5806 Affy yeast genome 2.0 support added (and 3'IVT arrays in general).
       8f44e09 had wrong filter function return value
       09c6b84 fix
       71c47a7 TrinotatePlus cdv instead of HTML
       76676e4 Updated calculateGeneLengthsFromGtf to allow different features on which to reduce(), and allow writing to a file
       96becb7 Fixed initIllSeqProject following change in nanuq read set sheet  Multiplex Key column
       ec0c6c2 RNAseqdenovo report was not roxygenized and caused installation crash
       b089394 now imports rtracklayer
       03adb78 no message
       c36647a methods package
       a7bb4a9 no message
       c9cdea6 Added function to read in /  access RSEM results to an ExpressionSet
       2289844 Report functions needed @export rocket
       36b2299 no message
       0560462 no message
       f32d0ec Corrected typo in samples.csv file name
       e80fde3 A bunch of additional changes.
       6f648a9 no message
       cbeb548 doc update
       eb6e9ec Added small function to compute gene lengths from gtf, and getGenes() now uses it if geneAnnotation does not contain it.
       5e6b177 doc update
       ee9c769 oxygen doc update, file index acessors and setters
       7a6f7f4 oxygen doc update
       5b7ea7c Added file index functions to keep track of plots generated during analysis for reporting.
       d2818b3 no message
       6dd29c7 Improved getCufflinksEset
       6c66745 Changed default accesor to csv
       36579f4 no message
       f86d0ab Changed maskedTable format to csv. More convenient for manual editing
       614311e ...
       cacadf3 Migrated some useful functions from gqMicroarrays to gqUtils, in preparation for standard expo. and QC of RNA-seq fpkm/cpm values in gqRNAseq/mugqicReporting.
       16828a7 added image, more progress
       76ebc64 ..
       5ced4af removed useless comments
       3dc7f49 Added R/bioc installation+packages+ linux env. modules creation trace script.
       bea0c04 Added list of favorite packages

  mathieu bourgey <mathieu.bourgey@mail.mcgill.ca>      18 commits

       ca85aaf nozzleCHIPseq.r correcting formating and replacment errors BFXDEV-139
       a60f3bc Merge branch 'master' of bitbucket.org:mugqic/rpackages
       d9d57ea update nozzleDNA.r
       c76c6e2 nozzleDNA.R update
       dc6a7ce conflict in gqSeqUtils/R/info.r resolution
       ff19156 nozzleDNA.R update
       55c1689 nozzleDNA.R update
       c70fb55 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       40301eb nozzleRNA.r add checkname =F to all file reading
       51faa39 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       278741a nozzleDNA update
       d6fe9cb nozzleDNA.R update
       be8c685 nozzleDNA and nozzleRNA  update
       69b189b nozzleDNA update
       171bdad nozzleDNA update
       d1f6ad7 nozzleRNA add ,comment.char =  to all read.table call + nozzleDNA update
       c6e8903 Merge branch 'master' into reportSetup
       061e935 nozzleDNA update

  mathieu Bourgey <mathieu.bourgey@mail.mcgill.ca>      5 commits

       0af58da finish version 1.0 of nozzleRNA
       b745f14 RNArepport update
       cacb5ed RNArepport update
       2378e87 building RNAseq report specific core
       7df0f2d test mugqicPipelineReportRNAseq()

  Mathieu Bourgey <mathieu.bourgey@mail.mcgill.ca>      16 commits

       6da3f25 nozzleDNA update
       b0d3b16 nozzleDNA update
       804a926 nozzleRNA add missing space in the text
       539f8cf nozzleRNA.r update adapt col names to checknames change
       a8a09e1 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       a03cef1 nozzleRNA.r update
       317d708 nozzleRNA update
       e54ad4b nozzleRNA update
       3597f51 nozzleRNA update
       56fec93 report RNA update
       5d7876c nozzleRNA add exploratory analysis
       f647e39 Merge branch 'master' of bitbucket.org:mugqic/rpackages
       5743104 nozzeDNA update
       28447cf nozzeDNA update
       00ff787 DNAnozzle update
       8c946e4 Starting DNAreport

